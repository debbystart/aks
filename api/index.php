<?php

require 'vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Setup Localtime to Indonesian
|--------------------------------------------------------------------------
|
*/
setlocale(LC_TIME, 'Indonesian');

/*
|--------------------------------------------------------------------------
| Define The Application
|--------------------------------------------------------------------------
|
*/
    $app = new \Slim\Slim(array(
        'cookies.encrypt' => true,
        'cookies.lifetime' => time() + (1*24*60*60), // 1 day
        'cookies.secret_key' => '_#_limys3cr3tms_#_',
        'cookies.cipher' => MCRYPT_RIJNDAEL_256,
        'cookies.cipher_mode' => MCRYPT_MODE_CBC,
        'debug' => true,
    ));

    // get all configuration
    $config = new Pimple();


/*
|--------------------------------------------------------------------------
| Define The Application Path
|--------------------------------------------------------------------------
|
| Here we just defined the path to the application directory. Most likely
| you will never need to change this value as the default setup should
| work perfectly fine for the vast majority of all our applications.
|
*/

$app->basePath = __DIR__;

$app->baseUrl = 'http://localhost/aks/api';
//$app->baseUrl = 'http://pufd.pangansari.co.id/api';

$app->baseUrlimg = 'http://localhost/aks/img';
//$app->baseUrlimg = 'http://pufd.pangansari.co.id/img';

$app->baseUr = 'http://localhost/aks/';
//$app->baseUr = 'http://pufd.pangansari.co.id/';


/*
|--------------------------------------------------------------------------
| Define The Eloquent ORM / Database connection
|--------------------------------------------------------------------------
|
*/
    $config['database'] = require dirname(__FILE__) . '/app/configs/database.php';

    // Make a new connection
    use Illuminate\Database\Capsule\Manager as Capsule;

    $capsule = new Capsule;

    $connection = $config['database']['connections'][$config['database']['default']];
    $capsule->addConnection($connection);

    // Set the event dispatcher used by Eloquent models... (optional)
    use Illuminate\Events\Dispatcher;
    use Illuminate\Container\Container;

    $capsule->setEventDispatcher(new Dispatcher(new Container));

    $capsule->setAsGlobal();
    $capsule->bootEloquent();
     
    // Connect using the Laravel Database component
    $conn = $capsule->connection();

    $app->db = $conn;


/*
|--------------------------------------------------------------------------
| Define The Hasing
|--------------------------------------------------------------------------
|
*/
    use Illuminate\Hashing\HashServiceProvider as Hash;

    $hash = new Hash($app);
    $hash->register();

/*
|--------------------------------------------------------------------------
| Define The Barcode
|--------------------------------------------------------------------------
|
*/
    use Dinesh\Barcode\BarcodeServiceProvider as Barcode;

    $barcode = new Barcode($app);
    $barcode->register();

    $app->DNS1D->setStorPath('public/barcodes/DNS1D/');
    $app->DNS2D->setStorPath('public/barcodes/DNS2D/');


/*
|--------------------------------------------------------------------------
| Define The Dompdf
|--------------------------------------------------------------------------
|
*/
    // disable DOMPDF's internal autoloader if you are using Composer
    define('DOMPDF_ENABLE_AUTOLOAD', false);

    // include DOMPDF's default configuration
    require_once dirname(__FILE__) . '/vendor/dompdf/dompdf/dompdf_config.inc.php';


/*
|--------------------------------------------------------------------------
| Define The Application Services
|--------------------------------------------------------------------------
|
*/
    // Register services
    // User logger service
    use Rlims\Services\UserLogger as UserLogger;
    $app->userlogger = new UserLogger($app);

    // Analysis Status logger service
    use Rlims\Services\AnalysisStatusLogger as AnalysisStatusLogger;
    $app->analysisstatuslogger = new AnalysisStatusLogger($app);

    // PDF Generator service
    use Rlims\Services\PDFGenerator as PDFGenerator;
    $app->PDFGenerator = new PDFGenerator($app);

    // Sample workflow logger service
    use Rlims\Services\SampleWorkflowLogger as SampleWorkflowLogger;
    $app->sampleworkflowlogger = new SampleWorkflowLogger($app);

    // Notifier service
    use Rlims\Services\Notifier as Notifier;
    $app->notifier = new Notifier($app);

    // Instrument usage logger service
    use Rlims\Services\InstrumentUsageLogger as InstrumentUsageLogger;
    $app->instrumentusagelogger = new InstrumentUsageLogger($app);

    // Mimetype service
    use Rlims\Services\MimeType as MimeType;
    $app->mimetype = new MimeType($app);
	
	// Mailer service
    use Rlims\Services\Mailer as Mailer;
    $app->Mailer = new Mailer($app);


//-----------------------------------------------------------------------------------------------------
// Check and set cookies

$authenticateForRole = function  ( $role = 'member') {

    return function () use ( $role ) {

        $app = \Slim\Slim::getInstance();

        $request = $app->request();

        $client_id = $app->getCookie('client_id', false);
        $client_token = $app->getCookie('client_token', false);

        $user_app = User_apps::find($client_id);

        if(!$user_app) {                        // client_id is not exist!
            $app->deleteCookie('client_id');
            $app->deleteCookie('client_token');
            $app->response()->status(400);
            $res = $app->response();
            $res['Content-Type'] = 'application/json';
            $res->body(json_encode(array(
                'error' => 'client_id_not_found'
            )));
            $app->stop();            
        }

        $app_secret_key = $user_app->secret_key;
        $isValidTokenKey = $app->hash->check($app_secret_key, $client_token);

        if(!$isValidTokenKey) {    // client_token is not match!
            $app->deleteCookie('client_id');
            $app->deleteCookie('client_token');
            $app->response()->status(400);
            $res = $app->response();
            $res['Content-Type'] = 'application/json';
            $res->body(json_encode(array(
                'error' => 'client_token_not_match'
            )));
            $app->stop();
        }

        $app_token_key_expired = intval($user_app->token_key_expire);

        if($app_token_key_expired < time()) {    // client_token is expired!
            $app->deleteCookie('client_id');
            $app->deleteCookie('client_token');
            $app->response()->status(400);
            $res = $app->response();
            $res['Content-Type'] = 'application/json';
            $res->body(json_encode(array(
                'error' => 'client_token_expired'
            )));
            $app->stop();
        }
    };
};

//-----------------------------------------------------------------------------------------------------
// Loading all routes

require dirname(__FILE__) . '/app/routes/auth.php';
require dirname(__FILE__) . '/app/routes/groups.php';
require dirname(__FILE__) . '/app/routes/roles.php';
require dirname(__FILE__) . '/app/routes/routes.php';
require dirname(__FILE__) . '/app/routes/users.php';
require dirname(__FILE__) . '/app/routes/companys.php';
require dirname(__FILE__) . '/app/routes/items.php';
require dirname(__FILE__) . '/app/routes/regions.php';
require dirname(__FILE__) . '/app/routes/customers.php';
require dirname(__FILE__) . '/app/routes/itemgroups.php';
require dirname(__FILE__) . '/app/routes/itemcategorys.php';
require dirname(__FILE__) . '/app/routes/itemsubcategorys.php';
require dirname(__FILE__) . '/app/routes/units.php';
require dirname(__FILE__) . '/app/routes/brands.php';
require dirname(__FILE__) . '/app/routes/suppliers.php';
require dirname(__FILE__) . '/app/routes/markets.php';
require dirname(__FILE__) . '/app/routes/shippings.php';
require dirname(__FILE__) . '/app/routes/purchaseorders.php';
require dirname(__FILE__) . '/app/routes/transactions.php';
require dirname(__FILE__) . '/app/routes/stocks.php';
require dirname(__FILE__) . '/app/routes/salesmans.php';
require dirname(__FILE__) . '/app/routes/salesorders.php';
require dirname(__FILE__) . '/app/routes/reasonstocks.php';
require dirname(__FILE__) . '/app/routes/owners.php';
require dirname(__FILE__) . '/app/routes/pricelists.php';
require dirname(__FILE__) . '/app/routes/salesorderalls.php';
require dirname(__FILE__) . '/app/routes/paymentlazadas.php';

$app->get('/', function() {
    echo '<h1>It works!</h1>';
});

//-----------------------------------------------------------------------------------------------------

$app->run();

?>