<?php

    $app->get('/companys', $authenticateForRole('member'), function () use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $requests = (array) json_decode($app->request()->getBody());

        $res = $app->response();

        $limit = $app->request->get('limit')?$app->request->get('limit'):0;
        $offset = $app->request->get('offset')?$app->request->get('offset'):0;
        $fields = $app->request->get('fields')?$app->request->get('fields'):null;
        $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

        $total = 0;
        $records = null;
		
	   $user_group_id = null;
	   $client_id = $app->getCookie('client_id', false);
	   $client_token = $app->getCookie('client_token', false);
	   $user_app = User_apps::find($client_id);

		if($user_app) {
			$user = Users::find($user_app->user_id);
			if($user)
				$user_id 				= intval($user->id);
				$user_group_id 		= intval($user->user_group_id);
				$user_company_id 	= intval($user->company_id);
        }

	   if($user_group_id == 1){
			$total = Companys::select();
			$source = Companys::select();   
	    }else{
			$total = Companys::select()->where('id','=',$user_company_id);
			$source = Companys::select()->where('id','=',$user_company_id);
	   }
       

        if($fields) {
            $total->select($fields);
            $source->select($fields);
        }
        

        if($app->request->get('where')) {

            $where = $app->request->get('where');

            $total->whereRaw($app->db->raw($where));
            $source->whereRaw($app->db->raw($where));
        }

        if($app->request->get('filter') && $app->request->get('filter_fields')) {

            $filter = $app->request->get('filter');
            $filter_fields = $app->request->get('filter_fields');

            $ft_fields = explode(',', $filter_fields);

            $where_like = '';

            for($i=0;$i<count($ft_fields);$i++) {

                if($i===0) {
                    $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }
                else
                {
                    $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }

                if($i===(count($ft_fields)-1)) {
                    $where_like .= ')';
                }

            }

            $total->whereRaw($app->db->raw($where_like));
            $source->whereRaw($app->db->raw($where_like));
        }

        $total = $total->count();

        if($limit>0)
            $source->take($limit)->skip($offset);

        if(!$orderby)
            $source = $source->orderByRaw('code')->get();
        else
            $source = $source->orderByRaw($orderby)->get();

        $out = '';

        if($source) {
            $out = json_encode(
                array('records' => $source->toArray(), 'total' => $total)
            );
        }

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->get('/companys/active', $authenticateForRole('member'), function () use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $requests = (array) json_decode($app->request()->getBody());

        $res = $app->response();

        $limit = $app->request->get('limit')?$app->request->get('limit'):0;
        $offset = $app->request->get('offset')?$app->request->get('offset'):0;
        $fields = $app->request->get('fields')?$app->request->get('fields'):null;
        $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

        $total = 0;
        $records = null;

        $total = Companys::select()->where('is_active', 1);
        $source = Companys::select()->where('is_active', 1);

        if($fields) {
            $total->select($fields);
            $source->select($fields);
        }
        

        if($app->request->get('where')) {

            $where = $app->request->get('where');

            $total->whereRaw($app->db->raw($where));
            $source->whereRaw($app->db->raw($where));
        }

        if($app->request->get('filter') && $app->request->get('filter_fields')) {

            $filter = $app->request->get('filter');
            $filter_fields = $app->request->get('filter_fields');

            $ft_fields = explode(',', $filter_fields);

            $where_like = '';

            for($i=0;$i<count($ft_fields);$i++) {

                if($i===0) {
                    $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }
                else
                {
                    $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }

                if($i===(count($ft_fields)-1)) {
                    $where_like .= ')';
                }

            }

            $total->whereRaw($app->db->raw($where_like));
            $source->whereRaw($app->db->raw($where_like));
        }

        $total = $total->count();

        if($limit>0)
            $source->take($limit)->skip($offset);

        if(!$orderby)
            $source = $source->orderByRaw('code')->get();
        else
            $source = $source->orderByRaw($orderby)->get();

        $out = '';

        if($source) {
            $out = json_encode(
                array('records' => $source->toArray(), 'total' => $total)
            );
        }

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });
	
	$app->get('/companys/img/file', $authenticateForRole('member'), function () use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $requests = (array) json_decode($app->request()->getBody());

        $res = $app->response();

        $limit = $app->request->get('limit')?$app->request->get('limit'):0;
        $offset = $app->request->get('offset')?$app->request->get('offset'):0;
        $fields = $app->request->get('fields')?$app->request->get('fields'):null;
        $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

		$user_group_id = null;
		$client_id = $app->getCookie('client_id', false);
		$client_token = $app->getCookie('client_token', false);
		$user_app = User_apps::find($client_id);

		if($user_app) {
			$user = Users::find($user_app->user_id);
			if($user)
				$user_group_id = intval($user->user_group_id);
		}
		
        $total = 0;
        $records = null;

        $total = Companys::select()->where('id','=',$user->company_id);
        $source = Companys::select()->where('id','=',$user->company_id);

        if($fields) {
            $total->select($fields);
            $source->select($fields);
        }
        

        if($app->request->get('where')) {

            $where = $app->request->get('where');

            $total->whereRaw($app->db->raw($where));
            $source->whereRaw($app->db->raw($where));
        }

        if($app->request->get('filter') && $app->request->get('filter_fields')) {

            $filter = $app->request->get('filter');
            $filter_fields = $app->request->get('filter_fields');

            $ft_fields = explode(',', $filter_fields);

            $where_like = '';

            for($i=0;$i<count($ft_fields);$i++) {

                if($i===0) {
                    $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }
                else
                {
                    $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }

                if($i===(count($ft_fields)-1)) {
                    $where_like .= ')';
                }

            }

            $total->whereRaw($app->db->raw($where_like));
            $source->whereRaw($app->db->raw($where_like));
        }

        $total = $total->count();

        if($limit>0)
            $source->take($limit)->skip($offset);

        if(!$orderby)
            $source = $source->orderByRaw('updated_at desc')->get();
        else
            $source = $source->orderByRaw($orderby)->get();

        $out = '';

        if($source) {
            $out = json_encode(
                array('records' => $source->toArray(), 'total' => $total)
            );
        }

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->get('/companys/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $res = $app->response();

        $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;

        if(!$expands){
            $company = Companys::find($id);
        }
        else{
            $expands_with = explode(',', $expands);
            $company = Companys::with($expands_with)->find($id);
        }

        if(!$company) {
            $res->status(400);
            $app->stop();                        
        }

        $out = $company->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->post('/companys', $authenticateForRole('member'), function () use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $company = new Companys;

        $company->code                      = $requests['code'];
		$company->name                      = $requests['name'];
        $company->is_active                 = $requests['is_active'];
		
		if(isset($requests['address']))
            $company->address = $requests['address'];
		
		if(isset($requests['phone']))
            $company->phone = $requests['phone'];
		
		if(isset($requests['email']))
            $company->email = $requests['email'];
				
        $company->save();
		
		if($company->type==true) {

                $file =  __DIR__. '/../../..' . $company->file_path;

                if(is_file($file)) {
                  unlink($file);
                }

               $company->file_path              = NULL;
               $company->file_type              = NULL;
               $company->save();

                if(isset($requests['attachments'])) {
                    $count = count($requests['attachments']);

                    for($i=0;$i<$count;$i++) {
                        
                        $attachment = (array)$requests['attachments'][$i];

                        // remove all files
                        $file =  __DIR__. '/../../..' . $attachment['file_path'];
                        if(is_file($file)) {
                          unlink($file);
                        }
                    }
                }
                
            }
            else {

                if(isset($requests['attachments'])) {
                    $count = count($requests['attachments']);

                    for($i=0;$i<$count;$i++) {
                        
                        $attachment = (array)$requests['attachments'][$i];

                        // remove old files
                        if($i<($count-1)) {
                            $file =  __DIR__. '/../../..' . $attachment['file_path'];
                            if(is_file($file)) {
                              unlink($file);
                            }
                        }

                        // save the last attachments
                        if($i===($count-1)) {
                            $company->file_path              = $attachment['file_path'];
                            $company->file_type              = $attachment['file_type'];
                            $company->save();
                        }
                    }
                }

            }

        if(!$company) {
            $res->status(400);
            $app->stop();                        
        }

        $out = $company->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);
        $res->status(201);
        $app->stop();

    });

    $app->put('/companys/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $company = Companys::find($id);
        if(!$company) {
            $res->status(400);
            $app->stop();
        }

		$company->code       				= $requests['code'];
		$company->name                      = $requests['name'];
        $company->is_active                 = $requests['is_active'];
		
		if(isset($requests['address']))
            $company->address = $requests['address'];
		
		if(isset($requests['phone']))
            $company->phone = $requests['phone'];
		
		if(isset($requests['email']))
            $company->email = $requests['email'];

        $company->save();
        if(!$company) {
            $res->status(400);
            $app->stop();                        
        }
		
		 if($company->type==true) {

                $file =  __DIR__. '/../../..' . $company->file_path;

                if(is_file($file)) {
                  unlink($file);
                }

               $company->file_path              = NULL;
               $company->file_type              = NULL;
               $company->save();

                if(isset($requests['attachments'])) {
                    $count = count($requests['attachments']);

                    for($i=0;$i<$count;$i++) {
                        
                        $attachment = (array)$requests['attachments'][$i];

                        // remove all files
                        $file =  __DIR__. '/../../..' . $attachment['file_path'];
                        if(is_file($file)) {
                          unlink($file);
                        }
                    }
                }
                
            }
            else {

                if(isset($requests['attachments'])) {
                    $count = count($requests['attachments']);

                    for($i=0;$i<$count;$i++) {
                        
                        $attachment = (array)$requests['attachments'][$i];

                        // remove old files
                        if($i<($count-1)) {
                            $file =  __DIR__. '/../../..' . $attachment['file_path'];
                            if(is_file($file)) {
                              unlink($file);
                            }
                        }

                        // save the last attachments
                        if($i===($count-1)) {
                            $company->file_path              = $attachment['file_path'];
                            $company->file_type              = $attachment['file_type'];
                            $company->save();
                        }
                    }
                }

            }
		
        $out = $company->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);
        $res->status(200);
        $app->stop();

    });

    $app->delete('/companys/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $company = Companys::find($id);

        if(!$company) {
            $res->status(400);
            $app->stop();                        
        }

        $company->is_active = false;
        $company->save();

        $company->delete();

        $res->status(200);
        $app->stop();

    });
	
	$app->post('/companys/files/upload', $authenticateForRole('member'), function () use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        if (!isset($_FILES['uploads'])) {
            echo "No files uploaded!!";
            return;
        }

        $type = 'company';

        $img = NULL;

        $files = $_FILES['uploads'];

        $cnt = count($files['name']);

        //var_dump($cnt);die();

        $extension =  NULL;

        if($cnt === 1) { // single file upload

            if ($files['error'] === 0) {

                // create unique name
                $name = uniqid('company-'.date('Ymd').'-');

                // Add missing file extension for known image types:
                if (strpos($name, '.') === false && preg_match('/^(image|audio)\/(gif|jpe?g|png|ogg|mp3|mp4|wav)/', $files['type'], $matches)) {
                    $name .= '.'.$matches[2];
                }
                else {
                    $res->body('invalid_file_types');
                    $res->status(400);
                    $app->stop();
                }

                if (function_exists('exif_imagetype')) {
                    switch(exif_imagetype($files['tmp_name'])){
                        case IMAGETYPE_JPEG:
                            $extensions = array('jpg', 'jpeg');
                            break;
                        case IMAGETYPE_PNG:
                            $extensions = array('png');
                            break;
                        case IMAGETYPE_GIF:
                            $extensions = array('gif');
                            break;
                    }
                    // Adjust incorrect image file extensions:
                    if (!empty($extensions)) {
                        $parts = explode('.', $name);
                        $extIndex = count($parts) - 1;
                        $ext = strtolower(@$parts[$extIndex]);
                        if (!in_array($ext, $extensions)) {
                            $parts[$extIndex] = $extensions[0];
                            $name = implode('.', $parts);
                        }
                    }
                }

                if (move_uploaded_file($files['tmp_name'], __DIR__. '/../../../media/uploads/' . $type . '/' . $name) === true) {
                    $img = array('file_path' => '/media/uploads/' . $type . '/' . $name, 'file_name' => $files['name'], 'file_type' => $files['type'], 'file_size' => $files['size']);
                }
            }
        }
 
        if (!$img) {
            $res->body('no_files_uploaded');
            $res->status(400);
            $app->stop();
        }

        // save to database
        $requests = (array) json_decode($app->request->post('myModel'));
        //var_dump($app->request->post('myModel'));die();

        $out = json_encode($img);
        $res->body($out);
        $res->status(201);
        $app->stop();

    });

     $app->post('/company/file', $authenticateForRole('member'), function () use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $company = Companys::where('file_path', $requests['file_path'])->first();

        if($company) {
            $company->file_path = NULL;
            $company->file_type = NULL;
            $company->save();
        }

        $file =  __DIR__. '/../../..' . $requests['file_path'];
        
        if(is_file($file)) {
          unlink($file);
        }

        $res->status(200);
        $app->stop();

    });