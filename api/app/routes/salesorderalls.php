<?php

$app->get('/salesorderalls', $authenticateForRole('member'), function () use ($app) {

    $app->response->headers->set('Content-Type', 'application/json');

    $requests = (array) json_decode($app->request()->getBody());

    $res = $app->response();

    $limit = $app->request->get('limit')?$app->request->get('limit'):0;
    $offset = $app->request->get('offset')?$app->request->get('offset'):0;
    $fields = $app->request->get('fields')?$app->request->get('fields'):null;
    $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

    $user_group_id = null;
    $client_id = $app->getCookie('client_id', false);
    $client_token = $app->getCookie('client_token', false);
    $user_app = User_apps::find($client_id);

    if($user_app) {
        $user = Users::find($user_app->user_id);
        if($user)
            $user_group_id = intval($user->user_group_id);
    }

    $total = 0;
    $records = null;

    $total = Salesorderalls::select($app->db->raw('data_salesorderall.date, data_salesorderall_detail.*'))
    ->join('data_salesorderall_detail','data_salesorderall_detail.salesorderall_id','=','data_salesorderall.id')
    ->where('data_salesorderall.company_id', $user->company_id);
    $source = Salesorderalls::select($app->db->raw('data_salesorderall.date, data_salesorderall_detail.*'))
    ->join('data_salesorderall_detail','data_salesorderall_detail.salesorderall_id','=','data_salesorderall.id')
    ->where('data_salesorderall.company_id', $user->company_id);

    if($fields) {
        $total->select($fields);
        $source->select($fields);
    }


    if($app->request->get('where')) {

        $where = $app->request->get('where');

        $total->whereRaw($app->db->raw($where));
        $source->whereRaw($app->db->raw($where));
    }

    if($app->request->get('filter') && $app->request->get('filter_fields')) {

        $filter = $app->request->get('filter');
        $filter_fields = $app->request->get('filter_fields');

        $ft_fields = explode(',', $filter_fields);

        $where_like = '';

        for($i=0;$i<count($ft_fields);$i++) {

            if($i===0) {
                $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
            }
            else
            {
                $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
            }

            if($i===(count($ft_fields)-1)) {
                $where_like .= ')';
            }

        }

        $total->whereRaw($app->db->raw($where_like));
        $source->whereRaw($app->db->raw($where_like));
    }

    $total = $total->count();

    if($limit>0)
        $source->take($limit)->skip($offset);

    if(!$orderby)
        $source = $source->orderByRaw('data_salesorderall_detail.store asc')->get();
    else
        $source = $source->orderByRaw($orderby)->get();

    // debug sql
    // $queries = $app->db->getQueryLog();
    // var_dump( $queries);die();
    

    $out = '';

    if($source) {
        $out = json_encode(
            array('records' => $source->toArray(), 'total' => $total)
        );
    }

    $res['Content-Type'] = 'application/json';
    $res->body($out);

});

$app->get('/Salesorderalls_detail/:delivery_id', $authenticateForRole('member'), function ($delivery_id) use ($app) {

    $app->response->headers->set('Content-Type', 'application/json');

    $requests = (array) json_decode($app->request()->getBody());

    $res = $app->response();

    $limit = $app->request->get('limit')?$app->request->get('limit'):0;
    $offset = $app->request->get('offset')?$app->request->get('offset'):0;
    $fields = $app->request->get('fields')?$app->request->get('fields'):null;
    $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

    $user_group_id = null;
    $client_id = $app->getCookie('client_id', false);
    $client_token = $app->getCookie('client_token', false);
    $user_app = User_apps::find($client_id);

    if($user_app) {
        $user = Users::find($user_app->user_id);
        if($user)
            $user_group_id = intval($user->user_group_id);
    }

    $total = 0;
    $records = null;

    if($user_group_id===1){
        $total = Salesorders_detail::select($app->db->raw('data_Salesorderall_detail.*,case when data_stock.qty is null then 0 else data_stock.qty end as stock,case when data_stock.qty is null then 0 else data_stock.qty end as mystock'))
        ->leftJoin('data_stock','data_stock.item_id','=','data_Salesorderall_detail.item_id')
        ->where('data_Salesorderall_detail.Salesorderall_id', $delivery_id)
        ->where('data_Salesorderall_detail.is_delivery', 0);
        $source = Salesorders_detail::select($app->db->raw('data_Salesorderall_detail.*,case when data_stock.qty is null then 0 else data_stock.qty end as stock,case when data_stock.qty is null then 0 else data_stock.qty end as mystock'))
        ->leftJoin('data_stock','data_stock.item_id','=','data_Salesorderall_detail.item_id')
        ->where('data_Salesorderall_detail.Salesorderall_id', $delivery_id)
        ->where('data_Salesorderall_detail.is_delivery', 0);
    }else{
        $total = Salesorders_detail::select($app->db->raw('data_Salesorderall_detail.*,case when data_stock.qty is null then 0 else data_stock.qty end as stock,case when data_stock.qty is null then 0 else data_stock.qty end as mystock'))
        ->leftJoin('data_stock','data_stock.item_id','=','data_Salesorderall_detail.item_id')
        ->where('Salesorderall_id', $delivery_id)
        ->where('data_Salesorderall_detail.is_delivery', 0)
        ->where('company_id', $user->company_id);
        $source = Salesorders_detail::select($app->db->raw('data_Salesorderall_detail.*,case when data_stock.qty is null then 0 else data_stock.qty end as stock,case when data_stock.qty is null then 0 else data_stock.qty end as mystock'))
        ->leftJoin('data_stock','data_stock.item_id','=','data_Salesorderall_detail.item_id')
        ->where('Salesorderall_id', $delivery_id)
        ->where('data_Salesorderall_detail.is_delivery', 0)
        ->where('company_id', $user->company_id);
    }

    if($fields) {
        $total->select($fields);
        $source->select($fields);
    }


    if($app->request->get('where')) {

        $where = $app->request->get('where');

        $total->whereRaw($app->db->raw($where));
        $source->whereRaw($app->db->raw($where));
    }

    if($app->request->get('filter') && $app->request->get('filter_fields')) {

        $filter = $app->request->get('filter');
        $filter_fields = $app->request->get('filter_fields');

        $ft_fields = explode(',', $filter_fields);

        $where_like = '';

        for($i=0;$i<count($ft_fields);$i++) {

            if($i===0) {
                $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
            }
            else
            {
                $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
            }

            if($i===(count($ft_fields)-1)) {
                $where_like .= ')';
            }

        }

        $total->whereRaw($app->db->raw($where_like));
        $source->whereRaw($app->db->raw($where_like));
    }

    $total = $total->count();

    if($limit>0)
        $source->take($limit)->skip($offset);

    if(!$orderby)
        $source = $source->orderByRaw('data_Salesorderall_detail.id asc')->get();
    else
        $source = $source->orderByRaw($orderby)->get();

    $out = '';

    if($source) {
        $out = json_encode(
            array('records' => $source->toArray(), 'total' => $total)
        );
    }

    $res['Content-Type'] = 'application/json';
    $res->body($out);

});

$app->get('/Salesorderalls/opendelivery', $authenticateForRole('member'), function () use ($app) {

    $app->response->headers->set('Content-Type', 'application/json');

    $requests = (array) json_decode($app->request()->getBody());

    $res = $app->response();

    $limit = $app->request->get('limit')?$app->request->get('limit'):0;
    $offset = $app->request->get('offset')?$app->request->get('offset'):0;
    $fields = $app->request->get('fields')?$app->request->get('fields'):null;
    $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

    $user_group_id = null;
    $client_id = $app->getCookie('client_id', false);
    $client_token = $app->getCookie('client_token', false);
    $user_app = User_apps::find($client_id);

    if($user_app) {
        $user = Users::find($user_app->user_id);
        if($user)
            $user_group_id = intval($user->user_group_id);
    }

    $total = 0;
    $records = null;

    if($user_group_id===1){
        $total = Salesorders::select()->whereRaw('is_save = 1 or is_save = 3');
        $source = Salesorders::select()->whereRaw('is_save = 1 or is_save = 3');
    }else{
        $total = Salesorders::select()->whereRaw('is_save = 1 or is_save = 3')->where('company_id', $user->company_id);
        $source = Salesorders::select()->whereRaw('is_save = 1 or is_save = 3')->where('company_id', $user->company_id);
    }

    if($fields) {
        $total->select($fields);
        $source->select($fields);
    }


    if($app->request->get('where')) {

        $where = $app->request->get('where');

        $total->whereRaw($app->db->raw($where));
        $source->whereRaw($app->db->raw($where));
    }

    if($app->request->get('filter') && $app->request->get('filter_fields')) {

        $filter = $app->request->get('filter');
        $filter_fields = $app->request->get('filter_fields');

        $ft_fields = explode(',', $filter_fields);

        $where_like = '';

        for($i=0;$i<count($ft_fields);$i++) {

            if($i===0) {
                $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
            }
            else
            {
                $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
            }

            if($i===(count($ft_fields)-1)) {
                $where_like .= ')';
            }

        }

        $total->whereRaw($app->db->raw($where_like));
        $source->whereRaw($app->db->raw($where_like));
    }

    $total = $total->count();

    if($limit>0)
        $source->take($limit)->skip($offset);

    if(!$orderby)
        $source = $source->orderByRaw('id desc')->get();
    else
        $source = $source->orderByRaw($orderby)->get();

    $out = '';

    if($source) {
        $out = json_encode(
            array('records' => $source->toArray(), 'total' => $total)
        );
    }

    $res['Content-Type'] = 'application/json';
    $res->body($out);

});

$app->get('/Salesorderalls/listdelivery', $authenticateForRole('member'), function () use ($app) {

    $app->response->headers->set('Content-Type', 'application/json');

    $requests = (array) json_decode($app->request()->getBody());

    $res = $app->response();

    $limit = $app->request->get('limit')?$app->request->get('limit'):0;
    $offset = $app->request->get('offset')?$app->request->get('offset'):0;
    $fields = $app->request->get('fields')?$app->request->get('fields'):null;
    $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

    $user_group_id = null;
    $client_id = $app->getCookie('client_id', false);
    $client_token = $app->getCookie('client_token', false);
    $user_app = User_apps::find($client_id);

    if($user_app) {
        $user = Users::find($user_app->user_id);
        if($user)
            $user_group_id = intval($user->user_group_id);
    }

    $total = 0;
    $records = null;

    if($user_group_id===1){
        $total = Transactions::select()->where('transaction_type', 2);
        $source = Transactions::select()->where('transaction_type', 2);
    }else{
        $total = Transactions::select()->where('transaction_type', 2)->where('company_id', $user->company_id);
        $source = Transactions::select()->where('transaction_type', 2)->where('company_id', $user->company_id);
    }

    if($fields) {
        $total->select($fields);
        $source->select($fields);
    }


    if($app->request->get('where')) {

        $where = $app->request->get('where');

        $total->whereRaw($app->db->raw($where));
        $source->whereRaw($app->db->raw($where));
    }

    if($app->request->get('filter') && $app->request->get('filter_fields')) {

        $filter = $app->request->get('filter');
        $filter_fields = $app->request->get('filter_fields');

        $ft_fields = explode(',', $filter_fields);

        $where_like = '';

        for($i=0;$i<count($ft_fields);$i++) {

            if($i===0) {
                $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
            }
            else
            {
                $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
            }

            if($i===(count($ft_fields)-1)) {
                $where_like .= ')';
            }

        }

        $total->whereRaw($app->db->raw($where_like));
        $source->whereRaw($app->db->raw($where_like));
    }

    $total = $total->count();

    if($limit>0)
        $source->take($limit)->skip($offset);

    if(!$orderby)
        $source = $source->orderByRaw('id desc')->get();
    else
        $source = $source->orderByRaw($orderby)->get();

    $out = '';

    if($source) {
        $out = json_encode(
            array('records' => $source->toArray(), 'total' => $total)
        );
    }

    $res['Content-Type'] = 'application/json';
    $res->body($out);

});

$app->get('/Salesorderalls/:id', $authenticateForRole('member'), function ($id) use ($app) {

    $app->response->headers->set('Content-Type', 'application/json');

    $res = $app->response();

    $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;

    if(!$expands){
        $Salesorderall = Salesorders::find($id);
    }
    else{
        $expands_with = explode(',', $expands);
        $Salesorderall = Salesorders::with($expands_with)->find($id);
    }

    if(!$Salesorderall) {
        $res->status(400);
        $app->stop();                        
    }

    $out = $Salesorderall->toJson();

    $res['Content-Type'] = 'application/json';
    $res->body($out);

});

$app->get('/Salesorderalls/listdelivery/:id', $authenticateForRole('member'), function ($id) use ($app) {

    $app->response->headers->set('Content-Type', 'application/json');

    $res = $app->response();

    $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;

    if(!$expands){
        $Salesorderall = Transactions::find($id);
    }
    else{
        $expands_with = explode(',', $expands);
        $Salesorderall = Transactions::with($expands_with)->find($id);
    }

    if(!$Salesorderall) {
        $res->status(400);
        $app->stop();                        
    }

    $out = $Salesorderall->toJson();

    $res['Content-Type'] = 'application/json';
    $res->body($out);

});

$app->post('/salesorderalls', $authenticateForRole('member'), function () use ($app) {

    $requests = (array) json_decode($app->request()->getBody());

    $app->response->headers->set('Content-Type', 'application/json');
    $res = $app->response();

    $client_id = $app->getCookie('client_id', false);
    $client_token = $app->getCookie('client_token', false);
    $user_app = User_apps::find($client_id);

    if($user_app) {
        $user = Users::find($user_app->user_id);
        if($user)
            $company_id = intval($user->company_id);
        $user_id    = intval($user->id);
    }

    try {

        $app->db->getPdo()->beginTransaction();

        $salesorderall = new Salesorderalls;

        $salesorderallall->id                     = Salesorderalls::getNextSalesorderallCode();
        $salesorderall->company_id                = $company_id;
        $salesorderall->date                      = $requests['date'];
        
        if(isset($requests['is_active']))
            $salesorderall->is_active              = $requests['is_active'];

        $so_id = $salesorderall->id;

        $salesorderall->save();

        if(!$salesorderall || !$detail) {
            $res->status(400);
            $app->stop();                        
        }

        $app->db->getPdo()->commit();

        $out = $salesorderall->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);
        $res->status(201);
        $app->stop();

    } catch (\PDOException $e) {

        $app->db->getPdo()->rollBack();

        $out = json_encode(array('error' => $e));
        $res['Content-Type'] = 'application/json';
        $res->body($out);
        $res->status(400);
        $app->stop();                        
    }

});

$app->put('/Salesorderalls/:id', $authenticateForRole('member'), function ($id) use ($app) {

    $requests = (array) json_decode($app->request()->getBody());

    $app->response->headers->set('Content-Type', 'application/json');
    $res = $app->response();

    $client_id = $app->getCookie('client_id', false);
    $client_token = $app->getCookie('client_token', false);
    $user_app = User_apps::find($client_id);

    if($user_app) {
        $user = Users::find($user_app->user_id);
        if($user)
            $company_id = intval($user->company_id);
        $user_id    = intval($user->id);
    }

    try {

        $app->db->getPdo()->beginTransaction();

        $Salesorderall = Salesorders::find($id);

        if(!$Salesorderall) {
            $res->status(400);
            $app->stop();
        }

        $Salesorderall->company_id                = $requests['company_id'];
        $Salesorderall->date                      = $requests['date'];
        $Salesorderall->customer_id               = $requests['customer_id'];
        $Salesorderall->mobile                    = $requests['mobile'];
        $Salesorderall->province                  = $requests['province'];
        $Salesorderall->city                      = $requests['city'];
        $Salesorderall->districts                 = $requests['districts'];
        $Salesorderall->village                   = $requests['village'];
        $Salesorderall->postal                    = $requests['postal'];
        $Salesorderall->address                   = $requests['address'];
        $Salesorderall->market_id                 = $requests['market_id'];
        $Salesorderall->salesman_id               = $requests['salesman_id'];
        $Salesorderall->selling                   = $requests['selling'];
        $Salesorderall->selling_date              = $requests['selling_date'];
        $Salesorderall->expired_selling_date      = $requests['expired_selling_date'];
        $Salesorderall->delivery_date             = $requests['delivery_date'];
        $Salesorderall->shipping_id               = $requests['shipping_id'];
        $Salesorderall->qty                       = $requests['qty'];
        $Salesorderall->total_item_weight         = $requests['total_item_weight'];
        $Salesorderall->bruto                     = $requests['bruto'];
        $Salesorderall->disc_rupiah               = $requests['disc_rupiah'];
        $Salesorderall->total                     = $requests['total'];
        $Salesorderall->totalhpp                  = $requests['totalhpp'];
        $Salesorderall->totalmarginrp             = $requests['totalmarginrp'];
        $Salesorderall->totaladminrp              = $requests['totaladminrp'];
        $Salesorderall->subtotal                  = $requests['subtotal'];
        $Salesorderall->user_id                   = $user_id;

        if(isset($requests['booking']))
            $Salesorderall->booking                = $requests['booking'];
        if(isset($requests['email']))
            $Salesorderall->email                  = $requests['email'];
        if(isset($requests['ongkir']))
            $Salesorderall->ongkir                 = $requests['ongkir'];
        if(isset($requests['shipping_note']))
            $Salesorderall->shipping_note          = $requests['shipping_note'];
        if(isset($requests['note']))
            $Salesorderall->note                   = $requests['note'];
        if(isset($requests['is_save']))
            $Salesorderall->is_save                = $requests['is_save'];
        if(isset($requests['is_active']))
            $Salesorderall->is_active              = $requests['is_active'];

        $so_id = $Salesorderall->id;

        $Salesorderall->save();

        if($requests['is_save'] == 2){
            $transaction = new Transactions;

            $transaction->transaction_type          = $requests['transaction_type'];
            $transaction->user_id                   = $user_id;
            $transaction->company_id                = $user->company_id;
            $transaction->code                      = Transactions::getNextTransSOcode();
            $transaction->d_c                       = 'C';

            if(isset($so_id))
                $transaction->so_id                 = $so_id;
            if(isset($requests['tot_price']))
                $transaction->total                 = 0 - $requests['tot_price'];
            if(isset($requests['selling_delivery']))
                $transaction->selling_delivery      = $requests['selling_delivery'];
            if(isset($requests['date_delivery']))
                $transaction->date                  = $requests['date_delivery'];
            if(isset($requests['ongkir_delivery']))
                $transaction->ongkir                = $requests['ongkir_delivery'];
            if(isset($requests['shipping_id_delivery']))
                $transaction->shipping_id           = $requests['shipping_id_delivery'];
            if(isset($requests['qty_trans']))
                $transaction->qty_trans             = $requests['qty_trans'];
            if(isset($requests['total_item_weight_delivery']))
                $transaction->total_item_weight     = $requests['total_item_weight_delivery'];
            if(isset($requests['note_delivery']))
                $transaction->note                  = $requests['note_delivery'];

            $transaction->save();

            $transaction_id = $transaction->id;
        }

        if($requests['Salesorderalls_detail']) {
            $quotaion_itemRequests = $requests['Salesorderalls_detail'];
            for ($i=0; $i < count($quotaion_itemRequests) ; $i++) {
                $quotaion_itemRequest = (array) $quotaion_itemRequests[$i];

                if(isset($quotaion_itemRequest['id'])){
                    $detail = Salesorders_detail::find($quotaion_itemRequest['id']);

                    if(!$detail) {
                        $res->status(400);
                        $app->stop();
                    }
                }else{
                    $detail = new Salesorders_detail;
                    $detail->id                   = Salesorders_detail::getNextSalesorderdetailCode();
                }

                $detail->Salesorderall_id            = $so_id;
                $detail->item_id                  = $quotaion_itemRequest['item_id'];
                $detail->item_name                = $quotaion_itemRequest['item_name'];
                $detail->item_unit                = $quotaion_itemRequest['item_unit'];
                $detail->qty                      = $quotaion_itemRequest['qty'];
                $detail->price                    = $quotaion_itemRequest['price'];
                $detail->bruto                    = $quotaion_itemRequest['bruto'];
                $detail->total_price              = $quotaion_itemRequest['total_price'];

                if(isset($quotaion_itemRequest['qty_trans']))
                    $detail->qty_trans          = $quotaion_itemRequest['qty_trans'];
                if(isset($quotaion_itemRequest['item_weight']))
                    $detail->item_weight           = $quotaion_itemRequest['item_weight'];
                if(isset($quotaion_itemRequest['total_item_weight']))
                    $detail->total_item_weight     = $quotaion_itemRequest['total_item_weight'];
                if(isset($quotaion_itemRequest['disc_percent']))
                    $detail->disc_percent          = $quotaion_itemRequest['disc_percent'];
                if(isset($quotaion_itemRequest['disc_rupiah']))
                    $detail->disc_rupiah           = $quotaion_itemRequest['disc_rupiah'];
                if(isset($quotaion_itemRequest['is_delivery']))
                    $detail->is_delivery           = $quotaion_itemRequest['is_delivery'];
                if(isset($quotaion_itemRequest['hpp']))
                    $detail->hpp                  = $quotaion_itemRequest['hpp'];
                if(isset($quotaion_itemRequest['marginrp']))
                    $detail->marginrp             = $quotaion_itemRequest['marginrp'];
                if(isset($quotaion_itemRequest['adminrp']))
                    $detail->adminrp              = $quotaion_itemRequest['adminrp'];

                $detail->save();

                $so_detail_id = $detail->id;

                if($requests['is_save'] == 2){

                    $item = Stocks::where('item_id', $quotaion_itemRequest['item_id'])->where('company_id', $requests['company_id'])->first();

                    if(!$item) {
                        $item = new Stocks;
                    }

                    $item->company_id               = $Salesorderall->company_id;
                    $item->item_id                  = $detail->item_id;
                    $item->item_name                = $detail->item_name;
                    $item->item_unit                = $detail->item_unit;
                    $item->qty                      = $item->qty - $detail->qty_trans;
                    if($item->qty > 0){
                        $item->total_price          = $item->total_price - ($item->hpp * $detail->qty_trans);
                        $item->hpp                  = $item->total_price / $item->qty;
                    }else{
                        $item->total_price          = 0;
                        $item->hpp                  = 0;
                    }   

                    $item->save();

                    $inventory = new Inventorys;

                    $inventory->transaction_id           = $transaction_id;
                    $inventory->item_id                  = $detail->item_id;
                    $inventory->item_name                = $detail->item_name;
                    $inventory->item_unit                = $detail->item_unit;
                    $inventory->qty                      = 0 - $detail->qty_trans;
                    $inventory->price                    = 0 - $detail->price;
                    $inventory->bruto                    = 0 - $detail->bruto;
                    $inventory->disc_percent             = 0 - $detail->disc_percent;
                    $inventory->disc_rupiah              = 0 - $detail->disc_rupiah;
                    $inventory->total_price              = $inventory->bruto - $inventory->disc_rupiah;
                    $inventory->stock_qty                = $item->qty;
                    $inventory->stock_total_price        = $item->total_price;
                    $inventory->stock_hpp                = $item->hpp;

                    if(isset($so_detail_id))
                        $inventory->so_detail_id          = $so_detail_id;
                    if(isset($quotaion_itemRequest['ongkir_delivery']))
                        $inventory->ongkir                = $quotaion_itemRequest['ongkir'];
                    if(isset($quotaion_itemRequest['item_weight']))
                        $inventory->item_weight           = $quotaion_itemRequest['item_weight'];
                    if(isset($quotaion_itemRequest['total_item_weight']))
                        $inventory->total_item_weight     = $quotaion_itemRequest['total_item_weight'];

                    $inventory->save();
                }
            }

            foreach($requests['detail_delete'] as $v){
                $detail_delete = Salesorders_detail::find($v);
                $detail_delete->delete();
            }

        }

        if($requests['is_save'] == 2){
            $delivery_list = Salesorders::find($so_id);

            if($delivery_list){

                $query =\Salesorders::select($app->db->raw('data_Salesorderall_detail.*'))
                ->join('data_Salesorderall_detail','data_Salesorderall.id','=','data_Salesorderall_detail.Salesorderall_id')
                ->where('data_Salesorderall.id','=', $so_id)
                ->get();
                $query_data = $query->toArray();
                $jmlh = count ($query_data);

                $query1 =\Salesorders::select($app->db->raw('data_Salesorderall_detail.*'))
                ->join('data_Salesorderall_detail','data_Salesorderall.id','=','data_Salesorderall_detail.Salesorderall_id')
                ->where('data_Salesorderall.id','=', $so_id)
                ->where('data_Salesorderall_detail.is_delivery','=',1)
                ->get();
                $query_data1 = $query1->toArray();
                $jmlh1 = count ($query_data1);

                if ($jmlh == $jmlh1){
                    $delivery_list->is_save = 2;
                    $delivery_list->save();
                }else{
                    $delivery_list->is_save = 3;
                    $delivery_list->save();
                }

            }

        }

        if(!$Salesorderall || !$detail) {
            $res->status(400);
            $app->stop();                        
        }

        $app->db->getPdo()->commit();

        $out = $Salesorderall->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);
        $res->status(201);
        $app->stop();

    } catch (\PDOException $e) {

        $app->db->getPdo()->rollBack();

        $out = json_encode(array('error' => $e));
        $res['Content-Type'] = 'application/json';
        $res->body($out);
        $res->status(400);
        $app->stop();                        
    }

});

$app->get('/Salesorderalls/print/so', $authenticateForRole('member'), function () use ($app) {

    if(!$app->request->get('id')) {
        echo 'Please specify the id!';
        exit(0);
    }

    $app->PDFGenerator->printSO();

});

$app->post('/salesorderalls/exams/excels/:date', $authenticateForRole('member'), function ($date)use ($app) {

    $requests = (array) json_decode($app->request()->getBody());

    $app->response->headers->set('Content-Type', 'application/json');
    $res = $app->response();

    $user_group_id = null;
    $client_id = $app->getCookie('client_id', false);
    $client_token = $app->getCookie('client_token', false);
    $user_app = User_apps::find($client_id);

    if($user_app) {
        $user = Users::find($user_app->user_id);
        if($user)
            $user_group_id = intval($user->user_group_id);
    }

    if (!isset($_FILES['uploads'])) {
        $out = json_encode(array('error' => "No files uploaded!!"));
        $res->body($out);
        $res->status(400);
        $app->stop();
    }

    $allowed_extensions =  array("pdf", "xlsx");
    $fileExcel = array();
    $files = $_FILES['uploads'];
    $cnt = count($files['name']);
    $extension =  NULL;

    if($cnt === 1) { // single file upload
        if ($files['error'] === 0) {
                // create unique name
            $name = uniqid('exams-excel-'.date('Ymd').'-');

            $extension = $app->mimetype->getExtensionFromMimeType($files['type']);
            if($extension)
                $name .= '.'.$extension;

            if(is_uploaded_file($files['tmp_name'])){
                if (copy($files['tmp_name'],__DIR__. '/../../../media/uploads/import_tools/salesorderall/' . $name) === true) {
                    $fileExcel[] = array('url' => '/media/uploads/import_tools/salesorderall/' . $name, 'name' => $files['name'], 'type' => $files['type'], 'size' => $files['size']);
                }
            }
        }
    }

    $imageCount = count($fileExcel);

    if ($imageCount == 0) {
        $out = json_encode(array('error' => "No files uploaded!!"));
        $res->body($out);
        $res->status(400);
        $app->stop();
    }

    $requests = (array) json_decode($app->request->post('myModel'));

    $filepath = __DIR__. '/../../../media/uploads/import_tools/salesorderall/' . $name;
    $app->notifier->importDataSalesorderall($filepath, $user->company_id, $user->id, $date);

    $out = json_encode(array('success' => "salesorderall!!"));

    $res->body($out);
    $res->status(201);
    $app->stop();

});