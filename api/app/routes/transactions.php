<?php

    $app->get('/transactions', $authenticateForRole('member'), function () use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $requests = (array) json_decode($app->request()->getBody());

        $res = $app->response();

        $limit = $app->request->get('limit')?$app->request->get('limit'):0;
        $offset = $app->request->get('offset')?$app->request->get('offset'):0;
        $fields = $app->request->get('fields')?$app->request->get('fields'):null;
        $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

        $user_group_id = null;
        $client_id = $app->getCookie('client_id', false);
        $client_token = $app->getCookie('client_token', false);
        $user_app = User_apps::find($client_id);

        if($user_app) {
            $user = Users::find($user_app->user_id);
            if($user)
              $user_group_id = intval($user->user_group_id);
        }

        $total = 0;
        $records = null;

        if($user_group_id===1){
            $total = Transactions::select();
            $source = Transactions::select();
        }else{
            $total = Transactions::select()->where('company_id', $user->company_id);
            $source = Transactions::select()->where('company_id', $user->company_id);
        }

        if($fields) {
            $total->select($fields);
            $source->select($fields);
        }
        

        if($app->request->get('where')) {

            $where = $app->request->get('where');

            $total->whereRaw($app->db->raw($where));
            $source->whereRaw($app->db->raw($where));
        }

        if($app->request->get('filter') && $app->request->get('filter_fields')) {

            $filter = $app->request->get('filter');
            $filter_fields = $app->request->get('filter_fields');

            $ft_fields = explode(',', $filter_fields);

            $where_like = '';

            for($i=0;$i<count($ft_fields);$i++) {

                if($i===0) {
                    $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }
                else
                {
                    $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }

                if($i===(count($ft_fields)-1)) {
                    $where_like .= ')';
                }

            }

            $total->whereRaw($app->db->raw($where_like));
            $source->whereRaw($app->db->raw($where_like));
        }

        $total = $total->count();

        if($limit>0)
            $source->take($limit)->skip($offset);

        if(!$orderby)
            $source = $source->orderByRaw('id desc')->get();
        else
            $source = $source->orderByRaw($orderby)->get();

        $out = '';

        if($source) {
            $out = json_encode(
                array('records' => $source->toArray(), 'total' => $total)
            );
        }

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });