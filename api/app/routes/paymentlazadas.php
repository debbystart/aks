<?php

$app->get('/paymentlazadas', $authenticateForRole('member'), function () use ($app) {

    $app->response->headers->set('Content-Type', 'application/json');

    $requests = (array) json_decode($app->request()->getBody());

    $res = $app->response();

    $limit = $app->request->get('limit')?$app->request->get('limit'):0;
    $offset = $app->request->get('offset')?$app->request->get('offset'):0;
    $fields = $app->request->get('fields')?$app->request->get('fields'):null;
    $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

    $user_group_id = null;
    $client_id = $app->getCookie('client_id', false);
    $client_token = $app->getCookie('client_token', false);
    $user_app = User_apps::find($client_id);

    if($user_app) {
        $user = Users::find($user_app->user_id);
        if($user)
            $user_group_id = intval($user->user_group_id);
    }

    $total = 0;
    $records = null;

    $total = Paymentlazadas::select($app->db->raw('data_paymentlazada.date, data_paymentlazada_detail.*'))
    ->join('data_paymentlazada_detail','data_paymentlazada_detail.paymentlazada_id','=','data_paymentlazada.id')
    ->where('data_paymentlazada.company_id', $user->company_id);
    $source = Paymentlazadas::select($app->db->raw('data_paymentlazada.date, data_paymentlazada_detail.*'))
    ->join('data_paymentlazada_detail','data_paymentlazada_detail.paymentlazada_id','=','data_paymentlazada.id')
    ->where('data_paymentlazada.company_id', $user->company_id);

    if($fields) {
        $total->select($fields);
        $source->select($fields);
    }


    if($app->request->get('where')) {

        $where = $app->request->get('where');

        $total->whereRaw($app->db->raw($where));
        $source->whereRaw($app->db->raw($where));
    }

    if($app->request->get('filter') && $app->request->get('filter_fields')) {

        $filter = $app->request->get('filter');
        $filter_fields = $app->request->get('filter_fields');

        $ft_fields = explode(',', $filter_fields);

        $where_like = '';

        for($i=0;$i<count($ft_fields);$i++) {

            if($i===0) {
                $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
            }
            else
            {
                $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
            }

            if($i===(count($ft_fields)-1)) {
                $where_like .= ')';
            }

        }

        $total->whereRaw($app->db->raw($where_like));
        $source->whereRaw($app->db->raw($where_like));
    }

    $total = $total->count();

    if($limit>0)
        $source->take($limit)->skip($offset);

    if(!$orderby)
        $source = $source->orderByRaw('data_paymentlazada_detail.store asc')->get();
    else
        $source = $source->orderByRaw($orderby)->get();

    // debug sql
    // $queries = $app->db->getQueryLog();
    // var_dump( $queries);die();
    

    $out = '';

    if($source) {
        $out = json_encode(
            array('records' => $source->toArray(), 'total' => $total)
        );
    }

    $res['Content-Type'] = 'application/json';
    $res->body($out);

});

$app->get('/Paymentlazadas_detail/:delivery_id', $authenticateForRole('member'), function ($delivery_id) use ($app) {

    $app->response->headers->set('Content-Type', 'application/json');

    $requests = (array) json_decode($app->request()->getBody());

    $res = $app->response();

    $limit = $app->request->get('limit')?$app->request->get('limit'):0;
    $offset = $app->request->get('offset')?$app->request->get('offset'):0;
    $fields = $app->request->get('fields')?$app->request->get('fields'):null;
    $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

    $user_group_id = null;
    $client_id = $app->getCookie('client_id', false);
    $client_token = $app->getCookie('client_token', false);
    $user_app = User_apps::find($client_id);

    if($user_app) {
        $user = Users::find($user_app->user_id);
        if($user)
            $user_group_id = intval($user->user_group_id);
    }

    $total = 0;
    $records = null;

    if($user_group_id===1){
        $total = Salesorders_detail::select($app->db->raw('data_Paymentlazada_detail.*,case when data_stock.qty is null then 0 else data_stock.qty end as stock,case when data_stock.qty is null then 0 else data_stock.qty end as mystock'))
        ->leftJoin('data_stock','data_stock.item_id','=','data_Paymentlazada_detail.item_id')
        ->where('data_Paymentlazada_detail.Paymentlazada_id', $delivery_id)
        ->where('data_Paymentlazada_detail.is_delivery', 0);
        $source = Salesorders_detail::select($app->db->raw('data_Paymentlazada_detail.*,case when data_stock.qty is null then 0 else data_stock.qty end as stock,case when data_stock.qty is null then 0 else data_stock.qty end as mystock'))
        ->leftJoin('data_stock','data_stock.item_id','=','data_Paymentlazada_detail.item_id')
        ->where('data_Paymentlazada_detail.Paymentlazada_id', $delivery_id)
        ->where('data_Paymentlazada_detail.is_delivery', 0);
    }else{
        $total = Salesorders_detail::select($app->db->raw('data_Paymentlazada_detail.*,case when data_stock.qty is null then 0 else data_stock.qty end as stock,case when data_stock.qty is null then 0 else data_stock.qty end as mystock'))
        ->leftJoin('data_stock','data_stock.item_id','=','data_Paymentlazada_detail.item_id')
        ->where('Paymentlazada_id', $delivery_id)
        ->where('data_Paymentlazada_detail.is_delivery', 0)
        ->where('company_id', $user->company_id);
        $source = Salesorders_detail::select($app->db->raw('data_Paymentlazada_detail.*,case when data_stock.qty is null then 0 else data_stock.qty end as stock,case when data_stock.qty is null then 0 else data_stock.qty end as mystock'))
        ->leftJoin('data_stock','data_stock.item_id','=','data_Paymentlazada_detail.item_id')
        ->where('Paymentlazada_id', $delivery_id)
        ->where('data_Paymentlazada_detail.is_delivery', 0)
        ->where('company_id', $user->company_id);
    }

    if($fields) {
        $total->select($fields);
        $source->select($fields);
    }


    if($app->request->get('where')) {

        $where = $app->request->get('where');

        $total->whereRaw($app->db->raw($where));
        $source->whereRaw($app->db->raw($where));
    }

    if($app->request->get('filter') && $app->request->get('filter_fields')) {

        $filter = $app->request->get('filter');
        $filter_fields = $app->request->get('filter_fields');

        $ft_fields = explode(',', $filter_fields);

        $where_like = '';

        for($i=0;$i<count($ft_fields);$i++) {

            if($i===0) {
                $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
            }
            else
            {
                $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
            }

            if($i===(count($ft_fields)-1)) {
                $where_like .= ')';
            }

        }

        $total->whereRaw($app->db->raw($where_like));
        $source->whereRaw($app->db->raw($where_like));
    }

    $total = $total->count();

    if($limit>0)
        $source->take($limit)->skip($offset);

    if(!$orderby)
        $source = $source->orderByRaw('data_Paymentlazada_detail.id asc')->get();
    else
        $source = $source->orderByRaw($orderby)->get();

    $out = '';

    if($source) {
        $out = json_encode(
            array('records' => $source->toArray(), 'total' => $total)
        );
    }

    $res['Content-Type'] = 'application/json';
    $res->body($out);

});

$app->get('/Paymentlazadas/opendelivery', $authenticateForRole('member'), function () use ($app) {

    $app->response->headers->set('Content-Type', 'application/json');

    $requests = (array) json_decode($app->request()->getBody());

    $res = $app->response();

    $limit = $app->request->get('limit')?$app->request->get('limit'):0;
    $offset = $app->request->get('offset')?$app->request->get('offset'):0;
    $fields = $app->request->get('fields')?$app->request->get('fields'):null;
    $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

    $user_group_id = null;
    $client_id = $app->getCookie('client_id', false);
    $client_token = $app->getCookie('client_token', false);
    $user_app = User_apps::find($client_id);

    if($user_app) {
        $user = Users::find($user_app->user_id);
        if($user)
            $user_group_id = intval($user->user_group_id);
    }

    $total = 0;
    $records = null;

    if($user_group_id===1){
        $total = Salesorders::select()->whereRaw('is_save = 1 or is_save = 3');
        $source = Salesorders::select()->whereRaw('is_save = 1 or is_save = 3');
    }else{
        $total = Salesorders::select()->whereRaw('is_save = 1 or is_save = 3')->where('company_id', $user->company_id);
        $source = Salesorders::select()->whereRaw('is_save = 1 or is_save = 3')->where('company_id', $user->company_id);
    }

    if($fields) {
        $total->select($fields);
        $source->select($fields);
    }


    if($app->request->get('where')) {

        $where = $app->request->get('where');

        $total->whereRaw($app->db->raw($where));
        $source->whereRaw($app->db->raw($where));
    }

    if($app->request->get('filter') && $app->request->get('filter_fields')) {

        $filter = $app->request->get('filter');
        $filter_fields = $app->request->get('filter_fields');

        $ft_fields = explode(',', $filter_fields);

        $where_like = '';

        for($i=0;$i<count($ft_fields);$i++) {

            if($i===0) {
                $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
            }
            else
            {
                $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
            }

            if($i===(count($ft_fields)-1)) {
                $where_like .= ')';
            }

        }

        $total->whereRaw($app->db->raw($where_like));
        $source->whereRaw($app->db->raw($where_like));
    }

    $total = $total->count();

    if($limit>0)
        $source->take($limit)->skip($offset);

    if(!$orderby)
        $source = $source->orderByRaw('id desc')->get();
    else
        $source = $source->orderByRaw($orderby)->get();

    $out = '';

    if($source) {
        $out = json_encode(
            array('records' => $source->toArray(), 'total' => $total)
        );
    }

    $res['Content-Type'] = 'application/json';
    $res->body($out);

});

$app->get('/Paymentlazadas/listdelivery', $authenticateForRole('member'), function () use ($app) {

    $app->response->headers->set('Content-Type', 'application/json');

    $requests = (array) json_decode($app->request()->getBody());

    $res = $app->response();

    $limit = $app->request->get('limit')?$app->request->get('limit'):0;
    $offset = $app->request->get('offset')?$app->request->get('offset'):0;
    $fields = $app->request->get('fields')?$app->request->get('fields'):null;
    $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

    $user_group_id = null;
    $client_id = $app->getCookie('client_id', false);
    $client_token = $app->getCookie('client_token', false);
    $user_app = User_apps::find($client_id);

    if($user_app) {
        $user = Users::find($user_app->user_id);
        if($user)
            $user_group_id = intval($user->user_group_id);
    }

    $total = 0;
    $records = null;

    if($user_group_id===1){
        $total = Transactions::select()->where('transaction_type', 2);
        $source = Transactions::select()->where('transaction_type', 2);
    }else{
        $total = Transactions::select()->where('transaction_type', 2)->where('company_id', $user->company_id);
        $source = Transactions::select()->where('transaction_type', 2)->where('company_id', $user->company_id);
    }

    if($fields) {
        $total->select($fields);
        $source->select($fields);
    }


    if($app->request->get('where')) {

        $where = $app->request->get('where');

        $total->whereRaw($app->db->raw($where));
        $source->whereRaw($app->db->raw($where));
    }

    if($app->request->get('filter') && $app->request->get('filter_fields')) {

        $filter = $app->request->get('filter');
        $filter_fields = $app->request->get('filter_fields');

        $ft_fields = explode(',', $filter_fields);

        $where_like = '';

        for($i=0;$i<count($ft_fields);$i++) {

            if($i===0) {
                $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
            }
            else
            {
                $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
            }

            if($i===(count($ft_fields)-1)) {
                $where_like .= ')';
            }

        }

        $total->whereRaw($app->db->raw($where_like));
        $source->whereRaw($app->db->raw($where_like));
    }

    $total = $total->count();

    if($limit>0)
        $source->take($limit)->skip($offset);

    if(!$orderby)
        $source = $source->orderByRaw('id desc')->get();
    else
        $source = $source->orderByRaw($orderby)->get();

    $out = '';

    if($source) {
        $out = json_encode(
            array('records' => $source->toArray(), 'total' => $total)
        );
    }

    $res['Content-Type'] = 'application/json';
    $res->body($out);

});

$app->get('/Paymentlazadas/:id', $authenticateForRole('member'), function ($id) use ($app) {

    $app->response->headers->set('Content-Type', 'application/json');

    $res = $app->response();

    $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;

    if(!$expands){
        $Paymentlazada = Salesorders::find($id);
    }
    else{
        $expands_with = explode(',', $expands);
        $Paymentlazada = Salesorders::with($expands_with)->find($id);
    }

    if(!$Paymentlazada) {
        $res->status(400);
        $app->stop();                        
    }

    $out = $Paymentlazada->toJson();

    $res['Content-Type'] = 'application/json';
    $res->body($out);

});

$app->get('/Paymentlazadas/listdelivery/:id', $authenticateForRole('member'), function ($id) use ($app) {

    $app->response->headers->set('Content-Type', 'application/json');

    $res = $app->response();

    $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;

    if(!$expands){
        $Paymentlazada = Transactions::find($id);
    }
    else{
        $expands_with = explode(',', $expands);
        $Paymentlazada = Transactions::with($expands_with)->find($id);
    }

    if(!$Paymentlazada) {
        $res->status(400);
        $app->stop();                        
    }

    $out = $Paymentlazada->toJson();

    $res['Content-Type'] = 'application/json';
    $res->body($out);

});

$app->post('/paymentlazadas', $authenticateForRole('member'), function () use ($app) {

    $requests = (array) json_decode($app->request()->getBody());

    $app->response->headers->set('Content-Type', 'application/json');
    $res = $app->response();

    $client_id = $app->getCookie('client_id', false);
    $client_token = $app->getCookie('client_token', false);
    $user_app = User_apps::find($client_id);

    if($user_app) {
        $user = Users::find($user_app->user_id);
        if($user)
            $company_id = intval($user->company_id);
        $user_id    = intval($user->id);
    }

    try {

        $app->db->getPdo()->beginTransaction();

        $paymentlazada = new Paymentlazadas;

        $paymentlazadaall->id                     = Paymentlazadas::getNextPaymentlazadaCode();
        $paymentlazada->company_id                = $company_id;
        $paymentlazada->date                      = $requests['date'];
        
        if(isset($requests['is_active']))
            $paymentlazada->is_active              = $requests['is_active'];

        $so_id = $paymentlazada->id;

        $paymentlazada->save();

        if(!$paymentlazada || !$detail) {
            $res->status(400);
            $app->stop();                        
        }

        $app->db->getPdo()->commit();

        $out = $paymentlazada->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);
        $res->status(201);
        $app->stop();

    } catch (\PDOException $e) {

        $app->db->getPdo()->rollBack();

        $out = json_encode(array('error' => $e));
        $res['Content-Type'] = 'application/json';
        $res->body($out);
        $res->status(400);
        $app->stop();                        
    }

});

$app->put('/Paymentlazadas/:id', $authenticateForRole('member'), function ($id) use ($app) {

    $requests = (array) json_decode($app->request()->getBody());

    $app->response->headers->set('Content-Type', 'application/json');
    $res = $app->response();

    $client_id = $app->getCookie('client_id', false);
    $client_token = $app->getCookie('client_token', false);
    $user_app = User_apps::find($client_id);

    if($user_app) {
        $user = Users::find($user_app->user_id);
        if($user)
            $company_id = intval($user->company_id);
        $user_id    = intval($user->id);
    }

    try {

        $app->db->getPdo()->beginTransaction();

        $Paymentlazada = Salesorders::find($id);

        if(!$Paymentlazada) {
            $res->status(400);
            $app->stop();
        }

        $Paymentlazada->company_id                = $requests['company_id'];
        $Paymentlazada->date                      = $requests['date'];
        $Paymentlazada->customer_id               = $requests['customer_id'];
        $Paymentlazada->mobile                    = $requests['mobile'];
        $Paymentlazada->province                  = $requests['province'];
        $Paymentlazada->city                      = $requests['city'];
        $Paymentlazada->districts                 = $requests['districts'];
        $Paymentlazada->village                   = $requests['village'];
        $Paymentlazada->postal                    = $requests['postal'];
        $Paymentlazada->address                   = $requests['address'];
        $Paymentlazada->market_id                 = $requests['market_id'];
        $Paymentlazada->salesman_id               = $requests['salesman_id'];
        $Paymentlazada->selling                   = $requests['selling'];
        $Paymentlazada->selling_date              = $requests['selling_date'];
        $Paymentlazada->expired_selling_date      = $requests['expired_selling_date'];
        $Paymentlazada->delivery_date             = $requests['delivery_date'];
        $Paymentlazada->shipping_id               = $requests['shipping_id'];
        $Paymentlazada->qty                       = $requests['qty'];
        $Paymentlazada->total_item_weight         = $requests['total_item_weight'];
        $Paymentlazada->bruto                     = $requests['bruto'];
        $Paymentlazada->disc_rupiah               = $requests['disc_rupiah'];
        $Paymentlazada->total                     = $requests['total'];
        $Paymentlazada->totalhpp                  = $requests['totalhpp'];
        $Paymentlazada->totalmarginrp             = $requests['totalmarginrp'];
        $Paymentlazada->totaladminrp              = $requests['totaladminrp'];
        $Paymentlazada->subtotal                  = $requests['subtotal'];
        $Paymentlazada->user_id                   = $user_id;

        if(isset($requests['booking']))
            $Paymentlazada->booking                = $requests['booking'];
        if(isset($requests['email']))
            $Paymentlazada->email                  = $requests['email'];
        if(isset($requests['ongkir']))
            $Paymentlazada->ongkir                 = $requests['ongkir'];
        if(isset($requests['shipping_note']))
            $Paymentlazada->shipping_note          = $requests['shipping_note'];
        if(isset($requests['note']))
            $Paymentlazada->note                   = $requests['note'];
        if(isset($requests['is_save']))
            $Paymentlazada->is_save                = $requests['is_save'];
        if(isset($requests['is_active']))
            $Paymentlazada->is_active              = $requests['is_active'];

        $so_id = $Paymentlazada->id;

        $Paymentlazada->save();

        if($requests['is_save'] == 2){
            $transaction = new Transactions;

            $transaction->transaction_type          = $requests['transaction_type'];
            $transaction->user_id                   = $user_id;
            $transaction->company_id                = $user->company_id;
            $transaction->code                      = Transactions::getNextTransSOcode();
            $transaction->d_c                       = 'C';

            if(isset($so_id))
                $transaction->so_id                 = $so_id;
            if(isset($requests['tot_price']))
                $transaction->total                 = 0 - $requests['tot_price'];
            if(isset($requests['selling_delivery']))
                $transaction->selling_delivery      = $requests['selling_delivery'];
            if(isset($requests['date_delivery']))
                $transaction->date                  = $requests['date_delivery'];
            if(isset($requests['ongkir_delivery']))
                $transaction->ongkir                = $requests['ongkir_delivery'];
            if(isset($requests['shipping_id_delivery']))
                $transaction->shipping_id           = $requests['shipping_id_delivery'];
            if(isset($requests['qty_trans']))
                $transaction->qty_trans             = $requests['qty_trans'];
            if(isset($requests['total_item_weight_delivery']))
                $transaction->total_item_weight     = $requests['total_item_weight_delivery'];
            if(isset($requests['note_delivery']))
                $transaction->note                  = $requests['note_delivery'];

            $transaction->save();

            $transaction_id = $transaction->id;
        }

        if($requests['Paymentlazadas_detail']) {
            $quotaion_itemRequests = $requests['Paymentlazadas_detail'];
            for ($i=0; $i < count($quotaion_itemRequests) ; $i++) {
                $quotaion_itemRequest = (array) $quotaion_itemRequests[$i];

                if(isset($quotaion_itemRequest['id'])){
                    $detail = Salesorders_detail::find($quotaion_itemRequest['id']);

                    if(!$detail) {
                        $res->status(400);
                        $app->stop();
                    }
                }else{
                    $detail = new Salesorders_detail;
                    $detail->id                   = Salesorders_detail::getNextSalesorderdetailCode();
                }

                $detail->Paymentlazada_id            = $so_id;
                $detail->item_id                  = $quotaion_itemRequest['item_id'];
                $detail->item_name                = $quotaion_itemRequest['item_name'];
                $detail->item_unit                = $quotaion_itemRequest['item_unit'];
                $detail->qty                      = $quotaion_itemRequest['qty'];
                $detail->price                    = $quotaion_itemRequest['price'];
                $detail->bruto                    = $quotaion_itemRequest['bruto'];
                $detail->total_price              = $quotaion_itemRequest['total_price'];

                if(isset($quotaion_itemRequest['qty_trans']))
                    $detail->qty_trans          = $quotaion_itemRequest['qty_trans'];
                if(isset($quotaion_itemRequest['item_weight']))
                    $detail->item_weight           = $quotaion_itemRequest['item_weight'];
                if(isset($quotaion_itemRequest['total_item_weight']))
                    $detail->total_item_weight     = $quotaion_itemRequest['total_item_weight'];
                if(isset($quotaion_itemRequest['disc_percent']))
                    $detail->disc_percent          = $quotaion_itemRequest['disc_percent'];
                if(isset($quotaion_itemRequest['disc_rupiah']))
                    $detail->disc_rupiah           = $quotaion_itemRequest['disc_rupiah'];
                if(isset($quotaion_itemRequest['is_delivery']))
                    $detail->is_delivery           = $quotaion_itemRequest['is_delivery'];
                if(isset($quotaion_itemRequest['hpp']))
                    $detail->hpp                  = $quotaion_itemRequest['hpp'];
                if(isset($quotaion_itemRequest['marginrp']))
                    $detail->marginrp             = $quotaion_itemRequest['marginrp'];
                if(isset($quotaion_itemRequest['adminrp']))
                    $detail->adminrp              = $quotaion_itemRequest['adminrp'];

                $detail->save();

                $so_detail_id = $detail->id;

                if($requests['is_save'] == 2){

                    $item = Stocks::where('item_id', $quotaion_itemRequest['item_id'])->where('company_id', $requests['company_id'])->first();

                    if(!$item) {
                        $item = new Stocks;
                    }

                    $item->company_id               = $Paymentlazada->company_id;
                    $item->item_id                  = $detail->item_id;
                    $item->item_name                = $detail->item_name;
                    $item->item_unit                = $detail->item_unit;
                    $item->qty                      = $item->qty - $detail->qty_trans;
                    if($item->qty > 0){
                        $item->total_price          = $item->total_price - ($item->hpp * $detail->qty_trans);
                        $item->hpp                  = $item->total_price / $item->qty;
                    }else{
                        $item->total_price          = 0;
                        $item->hpp                  = 0;
                    }   

                    $item->save();

                    $inventory = new Inventorys;

                    $inventory->transaction_id           = $transaction_id;
                    $inventory->item_id                  = $detail->item_id;
                    $inventory->item_name                = $detail->item_name;
                    $inventory->item_unit                = $detail->item_unit;
                    $inventory->qty                      = 0 - $detail->qty_trans;
                    $inventory->price                    = 0 - $detail->price;
                    $inventory->bruto                    = 0 - $detail->bruto;
                    $inventory->disc_percent             = 0 - $detail->disc_percent;
                    $inventory->disc_rupiah              = 0 - $detail->disc_rupiah;
                    $inventory->total_price              = $inventory->bruto - $inventory->disc_rupiah;
                    $inventory->stock_qty                = $item->qty;
                    $inventory->stock_total_price        = $item->total_price;
                    $inventory->stock_hpp                = $item->hpp;

                    if(isset($so_detail_id))
                        $inventory->so_detail_id          = $so_detail_id;
                    if(isset($quotaion_itemRequest['ongkir_delivery']))
                        $inventory->ongkir                = $quotaion_itemRequest['ongkir'];
                    if(isset($quotaion_itemRequest['item_weight']))
                        $inventory->item_weight           = $quotaion_itemRequest['item_weight'];
                    if(isset($quotaion_itemRequest['total_item_weight']))
                        $inventory->total_item_weight     = $quotaion_itemRequest['total_item_weight'];

                    $inventory->save();
                }
            }

            foreach($requests['detail_delete'] as $v){
                $detail_delete = Salesorders_detail::find($v);
                $detail_delete->delete();
            }

        }

        if($requests['is_save'] == 2){
            $delivery_list = Salesorders::find($so_id);

            if($delivery_list){

                $query =\Salesorders::select($app->db->raw('data_Paymentlazada_detail.*'))
                ->join('data_Paymentlazada_detail','data_Paymentlazada.id','=','data_Paymentlazada_detail.Paymentlazada_id')
                ->where('data_Paymentlazada.id','=', $so_id)
                ->get();
                $query_data = $query->toArray();
                $jmlh = count ($query_data);

                $query1 =\Salesorders::select($app->db->raw('data_Paymentlazada_detail.*'))
                ->join('data_Paymentlazada_detail','data_Paymentlazada.id','=','data_Paymentlazada_detail.Paymentlazada_id')
                ->where('data_Paymentlazada.id','=', $so_id)
                ->where('data_Paymentlazada_detail.is_delivery','=',1)
                ->get();
                $query_data1 = $query1->toArray();
                $jmlh1 = count ($query_data1);

                if ($jmlh == $jmlh1){
                    $delivery_list->is_save = 2;
                    $delivery_list->save();
                }else{
                    $delivery_list->is_save = 3;
                    $delivery_list->save();
                }

            }

        }

        if(!$Paymentlazada || !$detail) {
            $res->status(400);
            $app->stop();                        
        }

        $app->db->getPdo()->commit();

        $out = $Paymentlazada->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);
        $res->status(201);
        $app->stop();

    } catch (\PDOException $e) {

        $app->db->getPdo()->rollBack();

        $out = json_encode(array('error' => $e));
        $res['Content-Type'] = 'application/json';
        $res->body($out);
        $res->status(400);
        $app->stop();                        
    }

});

$app->get('/Paymentlazadas/print/so', $authenticateForRole('member'), function () use ($app) {

    if(!$app->request->get('id')) {
        echo 'Please specify the id!';
        exit(0);
    }

    $app->PDFGenerator->printSO();

});

$app->post('/paymentlazadas/exams/excels/:date', $authenticateForRole('member'), function ($date)use ($app) {

    $requests = (array) json_decode($app->request()->getBody());

    $app->response->headers->set('Content-Type', 'application/json');
    $res = $app->response();

    $user_group_id = null;
    $client_id = $app->getCookie('client_id', false);
    $client_token = $app->getCookie('client_token', false);
    $user_app = User_apps::find($client_id);

    if($user_app) {
        $user = Users::find($user_app->user_id);
        if($user)
            $user_group_id = intval($user->user_group_id);
    }

    if (!isset($_FILES['uploads'])) {
        $out = json_encode(array('error' => "No files uploaded!!"));
        $res->body($out);
        $res->status(400);
        $app->stop();
    }

    $allowed_extensions =  array("pdf", "xlsx");
    $fileExcel = array();
    $files = $_FILES['uploads'];
    $cnt = count($files['name']);
    $extension =  NULL;

    if($cnt === 1) { // single file upload
        if ($files['error'] === 0) {
                // create unique name
            $name = uniqid('exams-excel-'.date('Ymd').'-');

            $extension = $app->mimetype->getExtensionFromMimeType($files['type']);
            if($extension)
                $name .= '.'.$extension;

            if(is_uploaded_file($files['tmp_name'])){
                if (copy($files['tmp_name'],__DIR__. '/../../../media/uploads/import_tools/paymentlazada/' . $name) === true) {
                    $fileExcel[] = array('url' => '/media/uploads/import_tools/paymentlazada/' . $name, 'name' => $files['name'], 'type' => $files['type'], 'size' => $files['size']);
                }
            }
        }
    }

    $imageCount = count($fileExcel);

    if ($imageCount == 0) {
        $out = json_encode(array('error' => "No files uploaded!!"));
        $res->body($out);
        $res->status(400);
        $app->stop();
    }

    $requests = (array) json_decode($app->request->post('myModel'));

    $filepath = __DIR__. '/../../../media/uploads/import_tools/paymentlazada/' . $name;
    $app->notifier->importDataPaymentlazada($filepath, $user->company_id, $user->id, $date);

    $out = json_encode(array('success' => "paymentlazada!!"));

    $res->body($out);
    $res->status(201);
    $app->stop();

});