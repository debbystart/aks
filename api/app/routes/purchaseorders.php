<?php

    $app->get('/purchaseorders', $authenticateForRole('member'), function () use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $requests = (array) json_decode($app->request()->getBody());

        $res = $app->response();

        $limit = $app->request->get('limit')?$app->request->get('limit'):0;
        $offset = $app->request->get('offset')?$app->request->get('offset'):0;
        $fields = $app->request->get('fields')?$app->request->get('fields'):null;
        $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

        $user_group_id = null;
        $client_id = $app->getCookie('client_id', false);
        $client_token = $app->getCookie('client_token', false);
        $user_app = User_apps::find($client_id);

        if($user_app) {
            $user = Users::find($user_app->user_id);
            if($user)
              $user_group_id = intval($user->user_group_id);
        }

        $total = 0;
        $records = null;

        if($user_group_id===1){
            $total = Purchaseorders::select();
            $source = Purchaseorders::select();
        }else{
            $total = Purchaseorders::select()->where('company_id', $user->company_id);
            $source = Purchaseorders::select()->where('company_id', $user->company_id);
        }

        if($fields) {
            $total->select($fields);
            $source->select($fields);
        }
        

        if($app->request->get('where')) {

            $where = $app->request->get('where');

            $total->whereRaw($app->db->raw($where));
            $source->whereRaw($app->db->raw($where));
        }

        if($app->request->get('filter') && $app->request->get('filter_fields')) {

            $filter = $app->request->get('filter');
            $filter_fields = $app->request->get('filter_fields');

            $ft_fields = explode(',', $filter_fields);

            $where_like = '';

            for($i=0;$i<count($ft_fields);$i++) {

                if($i===0) {
                    $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }
                else
                {
                    $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }

                if($i===(count($ft_fields)-1)) {
                    $where_like .= ')';
                }

            }

            $total->whereRaw($app->db->raw($where_like));
            $source->whereRaw($app->db->raw($where_like));
        }

        $total = $total->count();

        if($limit>0)
            $source->take($limit)->skip($offset);

        if(!$orderby)
            $source = $source->orderByRaw('id desc')->get();
        else
            $source = $source->orderByRaw($orderby)->get();

        $out = '';

        if($source) {
            $out = json_encode(
                array('records' => $source->toArray(), 'total' => $total)
            );
        }

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->get('/purchaseorders_detail/:receive_id', $authenticateForRole('member'), function ($receive_id) use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $requests = (array) json_decode($app->request()->getBody());

        $res = $app->response();

        $limit = $app->request->get('limit')?$app->request->get('limit'):0;
        $offset = $app->request->get('offset')?$app->request->get('offset'):0;
        $fields = $app->request->get('fields')?$app->request->get('fields'):null;
        $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

        $user_group_id = null;
        $client_id = $app->getCookie('client_id', false);
        $client_token = $app->getCookie('client_token', false);
        $user_app = User_apps::find($client_id);

        if($user_app) {
            $user = Users::find($user_app->user_id);
            if($user)
              $user_group_id = intval($user->user_group_id);
        }

        $total = 0;
        $records = null;

        if($user_group_id===1){
            $total = Purchaseorders_detail::select($app->db->raw('data_purchaseorder_detail.*,case when data_stock.qty is null then 0 else data_stock.qty end as stock,case when data_stock.qty is null then 0 else data_stock.qty end as mystock'))
                    ->leftJoin('data_stock','data_stock.item_id','=','data_purchaseorder_detail.item_id')
                    ->where('data_purchaseorder_detail.purchaseorder_id', $receive_id);
            $source = Purchaseorders_detail::select($app->db->raw('data_purchaseorder_detail.*,case when data_stock.qty is null then 0 else data_stock.qty end as stock,case when data_stock.qty is null then 0 else data_stock.qty end as mystock'))
                    ->leftJoin('data_stock','data_stock.item_id','=','data_purchaseorder_detail.item_id')
                    ->where('data_purchaseorder_detail.purchaseorder_id', $receive_id);
        }else{
            $total = Purchaseorders_detail::select($app->db->raw('data_purchaseorder_detail.*,case when data_stock.qty is null then 0 else data_stock.qty end as stock,case when data_stock.qty is null then 0 else data_stock.qty end as mystock'))
                    ->leftJoin('data_stock','data_stock.item_id','=','data_purchaseorder_detail.item_id')
                   ->where('purchaseorder_id', $receive_id)
                   ->where('company_id', $user->company_id);
            $source = Purchaseorders_detail::select($app->db->raw('data_purchaseorder_detail.*,case when data_stock.qty is null then 0 else data_stock.qty end as stock,case when data_stock.qty is null then 0 else data_stock.qty end as mystock'))
                    ->leftJoin('data_stock','data_stock.item_id','=','data_purchaseorder_detail.item_id')
                   ->where('purchaseorder_id', $receive_id)
                   ->where('company_id', $user->company_id);
        }

        if($fields) {
            $total->select($fields);
            $source->select($fields);
        }
        

        if($app->request->get('where')) {

            $where = $app->request->get('where');

            $total->whereRaw($app->db->raw($where));
            $source->whereRaw($app->db->raw($where));
        }

        if($app->request->get('filter') && $app->request->get('filter_fields')) {

            $filter = $app->request->get('filter');
            $filter_fields = $app->request->get('filter_fields');

            $ft_fields = explode(',', $filter_fields);

            $where_like = '';

            for($i=0;$i<count($ft_fields);$i++) {

                if($i===0) {
                    $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }
                else
                {
                    $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }

                if($i===(count($ft_fields)-1)) {
                    $where_like .= ')';
                }

            }

            $total->whereRaw($app->db->raw($where_like));
            $source->whereRaw($app->db->raw($where_like));
        }

        $total = $total->count();

        if($limit>0)
            $source->take($limit)->skip($offset);

        if(!$orderby)
            $source = $source->orderByRaw('data_purchaseorder_detail.id asc')->get();
        else
            $source = $source->orderByRaw($orderby)->get();

        $out = '';

        if($source) {
            $out = json_encode(
                array('records' => $source->toArray(), 'total' => $total)
            );
        }

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->get('/purchaseorders/openreceive', $authenticateForRole('member'), function () use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $requests = (array) json_decode($app->request()->getBody());

        $res = $app->response();

        $limit = $app->request->get('limit')?$app->request->get('limit'):0;
        $offset = $app->request->get('offset')?$app->request->get('offset'):0;
        $fields = $app->request->get('fields')?$app->request->get('fields'):null;
        $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

        $user_group_id = null;
        $client_id = $app->getCookie('client_id', false);
        $client_token = $app->getCookie('client_token', false);
        $user_app = User_apps::find($client_id);

        if($user_app) {
            $user = Users::find($user_app->user_id);
            if($user)
              $user_group_id = intval($user->user_group_id);
        }

        $total = 0;
        $records = null;

        if($user_group_id===1){
            $total = Purchaseorders::select()->where('is_save', 1);
            $source = Purchaseorders::select()->where('is_save', 1);
        }else{
            $total = Purchaseorders::select()->where('is_save', 1)->where('company_id', $user->company_id);
            $source = Purchaseorders::select()->where('is_save', 1)->where('company_id', $user->company_id);
        }

        if($fields) {
            $total->select($fields);
            $source->select($fields);
        }
        

        if($app->request->get('where')) {

            $where = $app->request->get('where');

            $total->whereRaw($app->db->raw($where));
            $source->whereRaw($app->db->raw($where));
        }

        if($app->request->get('filter') && $app->request->get('filter_fields')) {

            $filter = $app->request->get('filter');
            $filter_fields = $app->request->get('filter_fields');

            $ft_fields = explode(',', $filter_fields);

            $where_like = '';

            for($i=0;$i<count($ft_fields);$i++) {

                if($i===0) {
                    $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }
                else
                {
                    $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }

                if($i===(count($ft_fields)-1)) {
                    $where_like .= ')';
                }

            }

            $total->whereRaw($app->db->raw($where_like));
            $source->whereRaw($app->db->raw($where_like));
        }

        $total = $total->count();

        if($limit>0)
            $source->take($limit)->skip($offset);

        if(!$orderby)
            $source = $source->orderByRaw('id desc')->get();
        else
            $source = $source->orderByRaw($orderby)->get();

        $out = '';

        if($source) {
            $out = json_encode(
                array('records' => $source->toArray(), 'total' => $total)
            );
        }

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->get('/purchaseorders/listreceive', $authenticateForRole('member'), function () use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $requests = (array) json_decode($app->request()->getBody());

        $res = $app->response();

        $limit = $app->request->get('limit')?$app->request->get('limit'):0;
        $offset = $app->request->get('offset')?$app->request->get('offset'):0;
        $fields = $app->request->get('fields')?$app->request->get('fields'):null;
        $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

        $user_group_id = null;
        $client_id = $app->getCookie('client_id', false);
        $client_token = $app->getCookie('client_token', false);
        $user_app = User_apps::find($client_id);

        if($user_app) {
            $user = Users::find($user_app->user_id);
            if($user)
              $user_group_id = intval($user->user_group_id);
        }

        $total = 0;
        $records = null;

        if($user_group_id===1){
            $total = Transactions::select()->where('transaction_type', 1);
            $source = Transactions::select()->where('transaction_type', 1);
        }else{
            $total = Transactions::select()->where('transaction_type', 1)->where('company_id', $user->company_id);
            $source = Transactions::select()->where('transaction_type', 1)->where('company_id', $user->company_id);
        }

        if($fields) {
            $total->select($fields);
            $source->select($fields);
        }
        

        if($app->request->get('where')) {

            $where = $app->request->get('where');

            $total->whereRaw($app->db->raw($where));
            $source->whereRaw($app->db->raw($where));
        }

        if($app->request->get('filter') && $app->request->get('filter_fields')) {

            $filter = $app->request->get('filter');
            $filter_fields = $app->request->get('filter_fields');

            $ft_fields = explode(',', $filter_fields);

            $where_like = '';

            for($i=0;$i<count($ft_fields);$i++) {

                if($i===0) {
                    $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }
                else
                {
                    $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }

                if($i===(count($ft_fields)-1)) {
                    $where_like .= ')';
                }

            }

            $total->whereRaw($app->db->raw($where_like));
            $source->whereRaw($app->db->raw($where_like));
        }

        $total = $total->count();

        if($limit>0)
            $source->take($limit)->skip($offset);

        if(!$orderby)
            $source = $source->orderByRaw('id desc')->get();
        else
            $source = $source->orderByRaw($orderby)->get();

        $out = '';

        if($source) {
            $out = json_encode(
                array('records' => $source->toArray(), 'total' => $total)
            );
        }

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->get('/purchaseorders/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $res = $app->response();

        $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;

        if(!$expands){
            $purchaseorder = Purchaseorders::find($id);
        }
        else{
            $expands_with = explode(',', $expands);
            $purchaseorder = Purchaseorders::with($expands_with)->find($id);
        }

        if(!$purchaseorder) {
            $res->status(400);
            $app->stop();                        
        }

        $out = $purchaseorder->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->post('/purchaseorders', $authenticateForRole('member'), function () use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $client_id = $app->getCookie('client_id', false);
        $client_token = $app->getCookie('client_token', false);
        $user_app = User_apps::find($client_id);

        if($user_app) {
            $user = Users::find($user_app->user_id);
            if($user)
              $company_id = intval($user->company_id);
              $user_id    = intval($user->id);
        }

        try {

            $app->db->getPdo()->beginTransaction();

            $purchaseorder = new Purchaseorders;

            $purchaseorder->id                        = Purchaseorders::getNextPurchaseorderCode();
            $purchaseorder->company_id                = $company_id;
            $purchaseorder->date                      = $requests['date'];
            $purchaseorder->supplier_id               = $requests['supplier_id'];
            $purchaseorder->shipping_id               = $requests['shipping_id'];
            $purchaseorder->to_name                   = $requests['to_name'];
            $purchaseorder->to_ship                   = $requests['to_ship'];
            $purchaseorder->ongkir                    = $requests['ongkir'];
            $purchaseorder->qty                       = $requests['qty'];
            $purchaseorder->total_item_weight         = $requests['total_item_weight'];
            $purchaseorder->bruto                     = $requests['bruto'];
            $purchaseorder->total                     = $requests['total'];
            $purchaseorder->user_id                   = $user_id;

            if(isset($requests['is_active']))
               $purchaseorder->is_active             = $requests['is_active'];
            if(isset($requests['note']))
               $purchaseorder->note                   = $requests['note'];
            if(isset($requests['is_save']))
               $purchaseorder->is_save                = $requests['is_save'];

            $po_id = $purchaseorder->id;

            $purchaseorder->save();

            if($requests['purchaseorders_detail']) {
                $quotaion_itemRequests = $requests['purchaseorders_detail'];
                for ($i=0; $i < count($quotaion_itemRequests) ; $i++) {
                    $quotaion_itemRequest = (array) $quotaion_itemRequests[$i];

                    $detail = new Purchaseorders_detail;

                    $detail->id                       = Purchaseorders_detail::getNextPurchaseorderdetailCode();
                    $detail->purchaseorder_id         = $po_id;
                    $detail->item_id                  = $quotaion_itemRequest['item_id'];
                    $detail->item_name                = $quotaion_itemRequest['item_name'];
                    $detail->item_unit                = $quotaion_itemRequest['item_unit'];
                    $detail->qty                      = $quotaion_itemRequest['qty'];
                    $detail->price                    = $quotaion_itemRequest['price'];
                    $detail->bruto                    = $quotaion_itemRequest['bruto'];
                    $detail->total_price_item         = $quotaion_itemRequest['total_price_item'];
                    $detail->total_price              = $quotaion_itemRequest['total_price'];

                    if(isset($quotaion_itemRequest['item_weight']))
                       $detail->item_weight           = $quotaion_itemRequest['item_weight'];
                    if(isset($quotaion_itemRequest['total_item_weight']))
                       $detail->total_item_weight     = $quotaion_itemRequest['total_item_weight'];
                    if(isset($quotaion_itemRequest['ongkir']))
                       $detail->ongkir                = $quotaion_itemRequest['ongkir'];
                    if(isset($quotaion_itemRequest['link']))
                       $detail->link                  = $quotaion_itemRequest['link'];

                    $detail->save();
                }
            }

            if(!$purchaseorder || !$detail) {
                $res->status(400);
                $app->stop();                        
            }

            $app->db->getPdo()->commit();

            $out = $purchaseorder->toJson();

            $res['Content-Type'] = 'application/json';
            $res->body($out);
            $res->status(201);
            $app->stop();

        } catch (\PDOException $e) {

            $app->db->getPdo()->rollBack();

            $out = json_encode(array('error' => $e));
            $res['Content-Type'] = 'application/json';
            $res->body($out);
            $res->status(400);
            $app->stop();                        
        }

    });

    $app->put('/purchaseorders/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $client_id = $app->getCookie('client_id', false);
        $client_token = $app->getCookie('client_token', false);
        $user_app = User_apps::find($client_id);

        if($user_app) {
            $user = Users::find($user_app->user_id);
            if($user)
              $company_id = intval($user->company_id);
              $user_id    = intval($user->id);
        }

        try {

            $app->db->getPdo()->beginTransaction();

            $purchaseorder = Purchaseorders::find($id);

            if(!$purchaseorder) {
                $res->status(400);
                $app->stop();
            }

            $purchaseorder->company_id                = $requests['company_id'];
            $purchaseorder->date                      = $requests['date'];
            $purchaseorder->supplier_id               = $requests['supplier_id'];
            $purchaseorder->shipping_id               = $requests['shipping_id'];
            $purchaseorder->to_name                   = $requests['to_name'];
            $purchaseorder->to_ship                   = $requests['to_ship'];
            $purchaseorder->ongkir                    = $requests['ongkir'];
            $purchaseorder->qty                       = $requests['qty'];
            $purchaseorder->total_item_weight         = $requests['total_item_weight'];
            $purchaseorder->bruto                     = $requests['bruto'];
            $purchaseorder->total                     = $requests['total'];
            $purchaseorder->user_id                   = $requests['user_id'];

            if(isset($requests['is_active']))
               $purchaseorder->is_active              = $requests['is_active'];
            if(isset($requests['note']))
               $purchaseorder->note                   = $requests['note'];
            if(isset($requests['note_receive']))
               $purchaseorder->note_receive           = $requests['note_receive'];
            if(isset($requests['is_save']))
               $purchaseorder->is_save                = $requests['is_save'];
            if(isset($requests['qty_receive']))
               $purchaseorder->qty_receive            = $requests['qty_receive'];
            if(isset($requests['to_name_receive']))
               $purchaseorder->to_name_receive        = $requests['to_name_receive'];
            if(isset($requests['date_receive']))
               $purchaseorder->date_receive           = $requests['date_receive'];

            $po_id = $purchaseorder->id;

            $purchaseorder->save();

            if($requests['is_save'] == 2){
                $transaction = new Transactions;

                if(isset($requests['ongkir']))
                    $transaction->ongkir                = $requests['ongkir'];
                if(isset($requests['shipping_id']))
                    $transaction->shipping_id           = $requests['shipping_id'];
                if(isset($requests['qty_receive']))
                    $transaction->qty_trans             = $requests['qty_receive'];
                if(isset($requests['total_item_weight']))
                    $transaction->total_item_weight     = $requests['total_item_weight'];
                if(isset($requests['note_delivery']))
                    $transaction->note                  = $requests['note_delivery'];

                $transaction->transaction_type          = $requests['transaction_type'];
                $transaction->user_id                   = $user_id;
                $transaction->company_id                = $user->company_id;
                $transaction->code                      = Transactions::getNextTransPOcode();
                $transaction->date                      = $purchaseorder->date_receive;
                $transaction->d_c                       = 'D';

                if(isset($po_id))
                    $transaction->po_id                 = $po_id;
                if(isset($purchaseorder->total))
                    $transaction->total                 = $purchaseorder->total;

                $transaction->save();

                $transaction_id = $transaction->id;
            }

            if($requests['purchaseorders_detail']) {
                $quotaion_itemRequests = $requests['purchaseorders_detail'];
                for ($i=0; $i < count($quotaion_itemRequests) ; $i++) {
                    $quotaion_itemRequest = (array) $quotaion_itemRequests[$i];

                    if(isset($quotaion_itemRequest['id'])){
                        $detail = Purchaseorders_detail::find($quotaion_itemRequest['id']);

                        if(!$detail) {
                            $res->status(400);
                            $app->stop();
                        }
                    }else{
                        $detail = new Purchaseorders_detail;
                        $detail->id                   = Purchaseorders_detail::getNextPurchaseorderdetailCode();
                    }

                    $detail->purchaseorder_id         = $po_id;
                    $detail->item_id                  = $quotaion_itemRequest['item_id'];
                    $detail->item_name                = $quotaion_itemRequest['item_name'];
                    $detail->item_unit                = $quotaion_itemRequest['item_unit'];
                    $detail->qty                      = $quotaion_itemRequest['qty'];
                    $detail->price                    = $quotaion_itemRequest['price'];
                    $detail->bruto                    = $quotaion_itemRequest['bruto'];
                    $detail->total_price_item         = $quotaion_itemRequest['total_price_item'];
                    $detail->total_price              = $quotaion_itemRequest['total_price'];

                    if(isset($quotaion_itemRequest['qty_receive']))
                       $detail->qty_receive           = $quotaion_itemRequest['qty_receive'];
                    if(isset($quotaion_itemRequest['item_weight']))
                       $detail->item_weight           = $quotaion_itemRequest['item_weight'];
                    if(isset($quotaion_itemRequest['total_item_weight']))
                       $detail->total_item_weight     = $quotaion_itemRequest['total_item_weight'];
                    if(isset($quotaion_itemRequest['ongkir']))
                       $detail->ongkir                = $quotaion_itemRequest['ongkir'];
                    if(isset($quotaion_itemRequest['link']))
                       $detail->link                  = $quotaion_itemRequest['link'];

                    $detail->save();

                    $po_detail_id = $detail->id;

                    if($requests['is_save'] == 2){

                        $item = Stocks::where('item_id', $quotaion_itemRequest['item_id'])->where('company_id', $requests['company_id'])->first();
                        
                        if(!$item) {
                            $item = new Stocks;
                        }

                        $item->company_id               = $purchaseorder->company_id;
                        $item->item_id                  = $detail->item_id;
                        $item->item_name                = $detail->item_name;
                        $item->item_unit                = $detail->item_unit;
                        $item->qty                      = $item->qty + $detail->qty_receive;
                        $item->total_price              = $item->total_price + $detail->total_price;
                        $item->hpp                      = $item->total_price / $item->qty;

                        $item->save();

                        $inventory = new Inventorys;
                        
                        $inventory->transaction_id           = $transaction_id;
                        $inventory->item_id                  = $detail->item_id;
                        $inventory->item_name                = $detail->item_name;
                        $inventory->item_unit                = $detail->item_unit;
                        $inventory->qty                      = $detail->qty_receive;
                        $inventory->price                    = $detail->price;
                        $inventory->bruto                    = $detail->bruto;
                        $inventory->total_price_item         = $detail->total_price_item;
                        $inventory->total_price              = $detail->total_price;
                        $inventory->stock_qty                = $item->qty;
                        $inventory->stock_total_price        = $item->total_price;
                        $inventory->stock_hpp                = $item->hpp;

                        if(isset($po_detail_id))
                           $inventory->po_detail_id          = $po_detail_id;
                        if(isset($quotaion_itemRequest['ongkir']))
                           $inventory->ongkir                = $quotaion_itemRequest['ongkir'];
                        if(isset($quotaion_itemRequest['item_weight']))
                           $inventory->item_weight           = $quotaion_itemRequest['item_weight'];
                        if(isset($quotaion_itemRequest['total_item_weight']))
                           $inventory->total_item_weight     = $quotaion_itemRequest['total_item_weight'];

                        $inventory->save();
                   }
                }

                foreach($requests['detail_delete'] as $v){
                    $detail_delete = Purchaseorders_detail::find($v);
                    $detail_delete->delete();
                }

            }

            if($requests['is_save'] == 2){
                if(!$purchaseorder || !$detail || !$transaction || !$item || !$inventory) {
                    $res->status(400);
                    $app->stop();                        
                }
            }else{
                if(!$purchaseorder || !$detail) {
                    $res->status(400);
                    $app->stop();                        
                }
            }



            $app->db->getPdo()->commit();

            $out = $purchaseorder->toJson();

            $res['Content-Type'] = 'application/json';
            $res->body($out);
            $res->status(201);
            $app->stop();

        } catch (\PDOException $e) {

            $app->db->getPdo()->rollBack();

            $out = json_encode(array('error' => $e));
            $res['Content-Type'] = 'application/json';
            $res->body($out);
            $res->status(400);
            $app->stop();                        
        }

    });

    $app->delete('/purchaseorders/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $purchaseorder = Purchaseorders::find($id);

        if(!$purchaseorder) {
            $res->status(400);
            $app->stop();                        
        }

        $purchaseorder->is_active = false;
        $purchaseorder->save();

        $purchaseorder->delete();

        $res->status(200);
        $app->stop();

    });

    $app->get('/purchaseorders/print/po', $authenticateForRole('member'), function () use ($app) {

        if(!$app->request->get('id')) {
            echo 'Please specify the id!';
            exit(0);
        }
        
        $app->PDFGenerator->printPO();

    });