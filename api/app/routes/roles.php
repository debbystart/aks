<?php

    $app->get('/roles', $authenticateForRole('member'), function () use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $requests = (array) json_decode($app->request()->getBody());

        $res = $app->response();

        $limit = $app->request->get('limit')?$app->request->get('limit'):0;
        $offset = $app->request->get('offset')?$app->request->get('offset'):0;
        $fields = $app->request->get('fields')?$app->request->get('fields'):null;
        $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

        $total = 0;
        $records = null;

        $total = Roles::select();
        $source = Roles::select();

        if($fields) {
            $total->select($fields);
            $source->select($fields);
        }
        

        if($app->request->get('where')) {

            $where = $app->request->get('where');

            $total->whereRaw($app->db->raw($where));
            $source->whereRaw($app->db->raw($where));
        }

        if($app->request->get('filter') && $app->request->get('filter_fields')) {

            $filter = $app->request->get('filter');
            $filter_fields = $app->request->get('filter_fields');

            $ft_fields = explode(',', $filter_fields);

            $where_like = '';

            for($i=0;$i<count($ft_fields);$i++) {

                if($i===0) {
                    $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }
                else
                {
                    $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }

                if($i===(count($ft_fields)-1)) {
                    $where_like .= ')';
                }

            }

            $total->whereRaw($app->db->raw($where_like));
            $source->whereRaw($app->db->raw($where_like));
        }

        $total = $total->count();

        if($limit>0)
            $source->take($limit)->skip($offset);

        if(!$orderby)
            $source = $source->orderByRaw('updated_at desc')->get();
        else
            $source = $source->orderByRaw($orderby)->get();

        $out = '';

        if($source) {
            $out = json_encode(
                array('records' => $source->toArray(), 'total' => $total)
            );
        }

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->get('/roles/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $res = $app->response();

        $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;

        if(!$expands){
            $role = Roles::find($id);
        }
        else{
            $expands_with = explode(',', $expands);
            $role = Roles::with($expands_with)->find($id);
        }

        if(!$role) {
            $res->status(400);
            $app->stop();                        
        }

        $out = $role->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->get('/roles/name/:name', $authenticateForRole('member'), function ($name) use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $res = $app->response();

        $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;

        if(!$expands){
            $roles = Roles::where('name', '=', $name)->first();
        }
        else{
            $expands_with = explode(',', $expands);
            $roles = Roles::with($expands_with)->where('name', '=', $name)->first();
        }

        if(!$roles) {
            $res->status(400);
            $app->stop();                        
        }

        $out = json_encode($roles->toArray());

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->post('/roles', $authenticateForRole('member'), function () use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $role = new Roles;

        // can not be null
        $role->name              = $requests['name'];
        $role->route_id          = $requests['route_id'];
        $role->is_active          = $requests['is_active'];

        // can be null
        if(isset($requests['can_create']))
            $role->can_create          = $requests['can_create'];

        if(isset($requests['can_read']))
            $role->can_read          = $requests['can_read'];

        if(isset($requests['can_edit']))
            $role->can_edit          = $requests['can_edit'];

        if(isset($requests['can_delete']))
            $role->can_delete          = $requests['can_delete'];
     
        $role->save();

        if(!$role) {
            $res->status(400);
            $app->stop();                        
        }

        $out = $role->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);
        $res->status(201);
        $app->stop();

    });

    $app->put('/roles/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $role = Roles::find($id);
        if(!$role) {
            $res->status(400);
            $app->stop();
        }

        // can not be null
        $role->name              = $requests['name'];
        $role->route_id          = $requests['route_id'];
        $role->is_active          = $requests['is_active'];

        // can be null
        if(isset($requests['can_create']))
            $role->can_create          = $requests['can_create'];

        if(isset($requests['can_read']))
            $role->can_read          = $requests['can_read'];

        if(isset($requests['can_edit']))
            $role->can_edit          = $requests['can_edit'];

        if(isset($requests['can_delete']))
            $role->can_delete          = $requests['can_delete'];

        $role->save();

        if(!$role) {
            $res->status(400);
            $app->stop();                        
        }

        $out = $role->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);
        $res->status(200);
        $app->stop();

    });

    $app->delete('/roles/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $role = Roles::find($id);

        if(!$role) {
            $res->status(400);
            $app->stop();                        
        }

        $role->is_active = false;
        $role->save();

        $role->delete();

        $res->status(200);
        $app->stop();

    });
