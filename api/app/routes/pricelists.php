<?php

$app->get('/pricelists', $authenticateForRole('member'), function () use ($app) {

	$app->response->headers->set('Content-Type', 'application/json');

	$requests = (array) json_decode($app->request()->getBody());

	$res = $app->response();

	$limit = $app->request->get('limit')?$app->request->get('limit'):0;
	$offset = $app->request->get('offset')?$app->request->get('offset'):0;
	$fields = $app->request->get('fields')?$app->request->get('fields'):null;
	$orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

	$user_group_id = null;
	$client_id = $app->getCookie('client_id', false);
	$client_token = $app->getCookie('client_token', false);
	$user_app = User_apps::find($client_id);

	if($user_app) {
		$user = Users::find($user_app->user_id);
		if($user){
			$user_Location_id		= intval($user->location_id);
			$user_Warehouse_id	= intval($user->warehouse_id);
			$user_id 				= intval($user->id);
			$user_group_id 		= intval($user->user_group_id);
			$user_company_id 	= intval($user->company_id);
		}
	}

	$total = 0;
	$records = null;

	$total = Pricelists::select()->where('company_id','=',$user_company_id);
	$source = Pricelists::select()->where('company_id','=',$user_company_id);

	if($fields) {
		$total->select($fields);
		$source->select($fields);
	}


	if($app->request->get('where')) {

		$where = $app->request->get('where');

		$total->whereRaw($app->db->raw($where));
		$source->whereRaw($app->db->raw($where));
	}

	if($app->request->get('filter') && $app->request->get('filter_fields')) {

		$filter = $app->request->get('filter');
		$filter_fields = $app->request->get('filter_fields');

		$ft_fields = explode(',', $filter_fields);

		$where_like = '';

		for($i=0;$i<count($ft_fields);$i++) {

			if($i===0) {
				$where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
			} else {
				$where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
			}

			if($i===(count($ft_fields)-1)) {
				$where_like .= ')';
			}

		}

		$total->whereRaw($app->db->raw($where_like));
		$source->whereRaw($app->db->raw($where_like));
	}

	$total = $total->count();

	if($limit>0){
		$source->take($limit)->skip($offset);
	}

	if(!$orderby){
		$source = $source->orderByRaw('id Desc')->get();
	}else{
		$source = $source->orderByRaw($orderby)->get();
	}

	$out = '';

	if($source) {
		$out = json_encode(
			array('records' => $source->toArray(), 'total' => $total)
		);
	}

	$res['Content-Type'] = 'application/json';
	$res->body($out);

});

$app->get('/pricelists/item', $authenticateForRole('member'), function () use ($app) {

	$app->response->headers->set('Content-Type', 'application/json');

	$requests = (array) json_decode($app->request()->getBody());

	$res = $app->response();

	$limit = $app->request->get('limit')?$app->request->get('limit'):0;
	$offset = $app->request->get('offset')?$app->request->get('offset'):0;
	$fields = $app->request->get('fields')?$app->request->get('fields'):null;
	$orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

	$user_group_id = null;
	$client_id = $app->getCookie('client_id', false);
	$client_token = $app->getCookie('client_token', false);
	$user_app = User_apps::find($client_id);

	if($user_app) {
		$user = Users::find($user_app->user_id);
		if($user)
			$user_group_id = intval($user->user_group_id);
	}

	$total = 0;
	$records = null;

	$total = Items::select($app->db->raw('data_stock.*, param_item.weight as item_weight'))
	->join('data_stock','data_stock.item_id','=','param_item.id')
	->where('data_stock.qty', '>', 0)
	->where('data_stock.company_id', $user->company_id);
	$source = Items::select($app->db->raw('data_stock.*, param_item.weight as item_weight'))
	->join('data_stock','data_stock.item_id','=','param_item.id')
	->where('data_stock.qty', '>', 0)
	->where('data_stock.company_id', $user->company_id);

	if($fields) {
		$total->select($fields);
		$source->select($fields);
	}

	if($app->request->get('where')) {

		$where = $app->request->get('where');

		$total->whereRaw($app->db->raw($where));
		$source->whereRaw($app->db->raw($where));
	}

	if($app->request->get('filter') && $app->request->get('filter_fields')) {

		$filter = $app->request->get('filter');
		$filter_fields = $app->request->get('filter_fields');

		$ft_fields = explode(',', $filter_fields);

		$where_like = '';

		for($i=0;$i<count($ft_fields);$i++) {

			if($i===0) {
				$where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
			}
			else
			{
				$where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
			}

			if($i===(count($ft_fields)-1)) {
				$where_like .= ')';
			}

		}

		$total->whereRaw($app->db->raw($where_like));
		$source->whereRaw($app->db->raw($where_like));
	}

	$total = $total->count();

	if($limit>0)
		$source->take($limit)->skip($offset);

	if(!$orderby)
		$source = $source->orderByRaw('param_item.name')->get();
	else
		$source = $source->orderByRaw($orderby)->get();

	$out = '';

	if($source) {
		$out = json_encode(
			array('records' => $source->toArray(), 'total' => $total)
		);
	}

	$res['Content-Type'] = 'application/json';
	$res->body($out);

});

$app->get('/pricelists/so/:market_id', $authenticateForRole('member'), function ($market_id) use ($app) {

	$app->response->headers->set('Content-Type', 'application/json');

	$requests = (array) json_decode($app->request()->getBody());

	$res = $app->response();

	$limit = $app->request->get('limit')?$app->request->get('limit'):0;
	$offset = $app->request->get('offset')?$app->request->get('offset'):0;
	$fields = $app->request->get('fields')?$app->request->get('fields'):null;
	$orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

	$user_group_id = null;
	$client_id = $app->getCookie('client_id', false);
	$client_token = $app->getCookie('client_token', false);
	$user_app = User_apps::find($client_id);

	if($user_app) {
		$user = Users::find($user_app->user_id);
		if($user){
			$user_group_id = intval($user->user_group_id);
			$user_company_id = intval($user->company_id);
		}
	}

	$total = 0;
	$records = null;

	$total = Pricelists::select($app->db->raw('param_pricelist_detail.*, case when data_stock.qty is null then 0 else data_stock.qty end as qty'))
	->join('param_pricelist_detail','param_pricelist_detail.pricelist_id','=','param_pricelist.id')
	->join('data_stock','data_stock.id','=','param_pricelist_detail.stock_id')
	->where('param_pricelist.market_id', $market_id)
	->where('param_pricelist.company_id', $user_company_id);
	$source = Pricelists::select($app->db->raw('param_pricelist_detail.*, case when data_stock.qty is null then 0 else data_stock.qty end as qty'))
	->join('param_pricelist_detail','param_pricelist_detail.pricelist_id','=','param_pricelist.id')
	->join('data_stock','data_stock.id','=','param_pricelist_detail.stock_id')
	->where('param_pricelist.market_id', $market_id)
	->where('param_pricelist.company_id', $user_company_id);

	if($fields) {
		$total->select($fields);
		$source->select($fields);
	}

	if($app->request->get('where')) {

		$where = $app->request->get('where');

		$total->whereRaw($app->db->raw($where));
		$source->whereRaw($app->db->raw($where));
	}

	if($app->request->get('filter') && $app->request->get('filter_fields')) {

		$filter = $app->request->get('filter');
		$filter_fields = $app->request->get('filter_fields');

		$ft_fields = explode(',', $filter_fields);

		$where_like = '';

		for($i=0;$i<count($ft_fields);$i++) {

			if($i===0) {
				$where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
			}
			else
			{
				$where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
			}

			if($i===(count($ft_fields)-1)) {
				$where_like .= ')';
			}

		}

		$total->whereRaw($app->db->raw($where_like));
		$source->whereRaw($app->db->raw($where_like));
	}

	$total = $total->count();

	if($limit>0)
		$source->take($limit)->skip($offset);

	if(!$orderby)
		$source = $source->orderByRaw('param_pricelist_detail.item_name')->get();
	else
		$source = $source->orderByRaw($orderby)->get();

	$out = '';

	if($source) {
		$out = json_encode(
			array('records' => $source->toArray(), 'total' => $total)
		);
	}

	$res['Content-Type'] = 'application/json';
	$res->body($out);

});

$app->get('/pricelists/:id', $authenticateForRole('member'), function ($id) use ($app) {

	$app->response->headers->set('Content-Type', 'application/json');

	$res = $app->response();

	$expands = $app->request->get('expands')?$app->request->get('expands'):NULL;

	if(!$expands){
		$customer = Pricelists::find($id);
	}else{
		$expands_with = explode(',', $expands);
		$customer = Pricelists::with($expands_with)->find($id);
	}

	if(!$customer) {
		$res->status(400);
		$app->stop();                        
	}

	$out = $customer->toJson();

	$res['Content-Type'] = 'application/json';
	$res->body($out);

});

$app->post('/pricelists', $authenticateForRole('member'), function () use ($app) {

	$requests = (array) json_decode($app->request()->getBody());

	$app->response->headers->set('Content-Type', 'application/json');
	$res = $app->response();

	$client_id = $app->getCookie('client_id', false);
	$client_token = $app->getCookie('client_token', false);
	$user_app = User_apps::find($client_id);

	if($user_app) {
		$user = Users::find($user_app->user_id);
		if($user)
			$company_id = intval($user->company_id);
		$user_id    = intval($user->id);
	}

	try {

		$app->db->getPdo()->beginTransaction();

		$pricelist = new Pricelists;

		$pricelist->id                        = Pricelists::getNextPricelistCode();
		$pricelist->company_id                = $company_id;
		$pricelist->name                      = $requests['name'];
		$pricelist->market_id                 = $requests['market_id'];
		$pricelist->start_date                = $requests['start_date'];
		$pricelist->end_date                  = $requests['end_date'];
		if(isset($requests['operational']))
			$pricelist->operational           = $requests['operational'];
		if(isset($requests['totalnominal']))
			$pricelist->totalnominal          = $requests['totalnominal'];
		$pricelist->user_id                   = $user_id;

		if(isset($requests['is_active']))
			$pricelist->is_active             = $requests['is_active'];
		if(isset($requests['note']))
			$pricelist->note                  = $requests['note'];

		$pl_id = $pricelist->id;

		$pricelist->save();

		if($requests['pricelists_detail']) {
			$quotaion_itemRequests = $requests['pricelists_detail'];
			for ($i=0; $i < count($quotaion_itemRequests) ; $i++) {
				$quotaion_itemRequest = (array) $quotaion_itemRequests[$i];

				$detail = new Pricelists_detail;

				$detail->id                       = Pricelists_detail::getNextPricelistdetailCode();
				$detail->pricelist_id         	  = $pl_id;
				$detail->stock_id                 = $quotaion_itemRequest['stock_id'];
				$detail->item_id                  = $quotaion_itemRequest['item_id'];
				$detail->item_name                = $quotaion_itemRequest['item_name'];
				$detail->item_unit                = $quotaion_itemRequest['item_unit'];
				$detail->item_weight              = $quotaion_itemRequest['item_weight'];
				$detail->hpp                      = $quotaion_itemRequest['hpp'];
				$detail->amounthpp                = $quotaion_itemRequest['amounthpp'];
				if(isset($quotaion_itemRequest['operational']))
					$detail->operational          = $quotaion_itemRequest['operational'];
				if(isset($quotaion_itemRequest['margin']))
					$detail->margin         	  = $quotaion_itemRequest['margin'];
				if(isset($quotaion_itemRequest['marginrp']))
					$detail->marginrp             = $quotaion_itemRequest['marginrp'];
				$detail->item_price               = $quotaion_itemRequest['item_price'];
				if(isset($quotaion_itemRequest['admin']))
					$detail->admin             	  = $quotaion_itemRequest['admin'];
				if(isset($quotaion_itemRequest['adminrp']))
					$detail->adminrp              = $quotaion_itemRequest['adminrp'];

				$detail->save();
			}
		}

		if($requests['operationals']) {
			$quotaion_itemRequests = $requests['operationals'];
			for ($i=0; $i < count($quotaion_itemRequests) ; $i++) {
				$quotaion_itemRequest = (array) $quotaion_itemRequests[$i];

				$operational = new Pricelists_operational;

				$operational->id                   = Pricelists_operational::getNextPricelistoperationalCode();
				$operational->pricelist_id         = $pl_id;
				$operational->name                 = $quotaion_itemRequest['name'];
				$operational->nominal              = $quotaion_itemRequest['nominal'];

				$operational->save();
			}
		}

		if(!$pricelist || !$detail) {
			$res->status(400);
			$app->stop();
		}

		$app->db->getPdo()->commit();

		$out = $pricelist->toJson();

		$res['Content-Type'] = 'application/json';
		$res->body($out);
		$res->status(201);
		$app->stop();

	} catch (\PDOException $e) {

		$app->db->getPdo()->rollBack();

		$out = json_encode(array('error' => $e));
		$res['Content-Type'] = 'application/json';
		$res->body($out);
		$res->status(400);
		$app->stop();
	}

});

$app->put('/pricelists/:id', $authenticateForRole('member'), function ($id) use ($app) {

	$requests = (array) json_decode($app->request()->getBody());

	$app->response->headers->set('Content-Type', 'application/json');
	$res = $app->response();

	$client_id = $app->getCookie('client_id', false);
	$client_token = $app->getCookie('client_token', false);
	$user_app = User_apps::find($client_id);

	if($user_app) {
		$user = Users::find($user_app->user_id);
		if($user)
			$company_id = intval($user->company_id);
		$user_id    = intval($user->id);
	}

	try {

		$app->db->getPdo()->beginTransaction();

		$pricelist = Pricelists::find($id);

		if(!$pricelist) {
			$res->status(400);
			$app->stop();
		}

		$pricelist->company_id                = $company_id;
		$pricelist->name                      = $requests['name'];
		$pricelist->market_id                 = $requests['market_id'];
		$pricelist->start_date                = $requests['start_date'];
		$pricelist->end_date                  = $requests['end_date'];
		if(isset($requests['operational']))
			$pricelist->operational           = $requests['operational'];
		if(isset($requests['totalnominal']))
			$pricelist->totalnominal          = $requests['totalnominal'];
		$pricelist->user_id                   = $user_id;

		if(isset($requests['is_active']))
			$pricelist->is_active             = $requests['is_active'];
		if(isset($requests['note']))
			$pricelist->note                  = $requests['note'];

		$pl_id = $pricelist->id;

		$pricelist->save();

		if($requests['pricelists_detail']) {
			$quotaion_itemRequests = $requests['pricelists_detail'];
			for ($i=0; $i < count($quotaion_itemRequests) ; $i++) {
				$quotaion_itemRequest = (array) $quotaion_itemRequests[$i];

				if(isset($quotaion_itemRequest['id'])){
					$detail = Pricelists_detail::find($quotaion_itemRequest['id']);

					if(!$detail) {
						$res->status(400);
						$app->stop();
					}
				}else{
					$detail = new Pricelists_detail;
					$detail->id                   = Pricelists_detail::getNextPricelistdetailCode();
				}

				$detail->pricelist_id         	  = $pl_id;
				$detail->stock_id                 = $quotaion_itemRequest['stock_id'];
				$detail->item_id                  = $quotaion_itemRequest['item_id'];
				$detail->item_name                = $quotaion_itemRequest['item_name'];
				$detail->item_unit                = $quotaion_itemRequest['item_unit'];
				$detail->item_weight              = $quotaion_itemRequest['item_weight'];
				$detail->hpp                      = $quotaion_itemRequest['hpp'];
				$detail->amounthpp                = $quotaion_itemRequest['amounthpp'];
				if(isset($quotaion_itemRequest['operational']))
					$detail->operational          = $quotaion_itemRequest['operational'];
				if(isset($quotaion_itemRequest['margin']))
					$detail->margin         	  = $quotaion_itemRequest['margin'];
				if(isset($quotaion_itemRequest['marginrp']))
					$detail->marginrp             = $quotaion_itemRequest['marginrp'];
				$detail->item_price               = $quotaion_itemRequest['item_price'];
				if(isset($quotaion_itemRequest['admin']))
					$detail->admin             	  = $quotaion_itemRequest['admin'];
				if(isset($quotaion_itemRequest['adminrp']))
					$detail->adminrp              = $quotaion_itemRequest['adminrp'];

				$detail->save();

			}

			foreach($requests['detail_delete'] as $v){
				$detail_delete = Pricelists_detail::find($v);
				$detail_delete->delete();
			}

		}

		if($requests['operationals']) {
			$quotaion_itemRequests = $requests['operationals'];
			for ($i=0; $i < count($quotaion_itemRequests) ; $i++) {
				$quotaion_itemRequest = (array) $quotaion_itemRequests[$i];

				if(isset($quotaion_itemRequest['id'])){
					$operational = Pricelists_operational::find($quotaion_itemRequest['id']);

					if(!$operational) {
						$res->status(400);
						$app->stop();
					}
				}else{
					$operational = new Pricelists_operational;
					$operational->id                   = Pricelists_operational::getNextPricelistoperationalCode();
				}

				$operational->pricelist_id         = $pl_id;
				$operational->name                 = $quotaion_itemRequest['name'];
				$operational->nominal              = $quotaion_itemRequest['nominal'];

				$operational->save();
			}

			foreach($requests['operational_delete'] as $v){
				$operational_delete = Pricelists_operational::find($v);
				$operational_delete->delete();
			}
		}

		if(!$pricelist || !$detail) {
			$res->status(400);
			$app->stop();                        
		}

		$app->db->getPdo()->commit();

		$out = $pricelist->toJson();

		$res['Content-Type'] = 'application/json';
		$res->body($out);
		$res->status(201);
		$app->stop();

	} catch (\PDOException $e) {

		$app->db->getPdo()->rollBack();

		$out = json_encode(array('error' => $e));
		$res['Content-Type'] = 'application/json';
		$res->body($out);
		$res->status(400);
		$app->stop();                        
	}

});