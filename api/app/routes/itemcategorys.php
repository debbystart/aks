<?php

    $app->get('/itemcategorys', $authenticateForRole('member'), function () use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $requests = (array) json_decode($app->request()->getBody());

        $res = $app->response();

        $limit = $app->request->get('limit')?$app->request->get('limit'):0;
        $offset = $app->request->get('offset')?$app->request->get('offset'):0;
        $fields = $app->request->get('fields')?$app->request->get('fields'):null;
        $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

        $total = 0;
        $records = null;

        $total = Itemcategorys::select();
        $source = Itemcategorys::select();

        if($fields) {
            $total->select($fields);
            $source->select($fields);
        }

        if($app->request->get('where')) {

            $where = $app->request->get('where');

            $total->whereRaw($app->db->raw($where));
            $source->whereRaw($app->db->raw($where));
        }

        if($app->request->get('filter') && $app->request->get('filter_fields')) {

            $filter = $app->request->get('filter');
            $filter_fields = $app->request->get('filter_fields');

            $ft_fields = explode(',', $filter_fields);

            $where_like = '';

            for($i=0;$i<count($ft_fields);$i++) {

                if($i===0) {
                    $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }
                else
                {
                    $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }

                if($i===(count($ft_fields)-1)) {
                    $where_like .= ')';
                }

            }

            $total->whereRaw($app->db->raw($where_like));
            $source->whereRaw($app->db->raw($where_like));
        }

        $total = $total->count();

        if($limit>0)
            $source->take($limit)->skip($offset);

        if(!$orderby)
            $source = $source->orderByRaw('name')->get();
        else
            $source = $source->orderByRaw($orderby)->get();

        $out = '';

        if($source) {
            $out = json_encode(
                array('records' => $source->toArray(), 'total' => $total)
            );
        }

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->get('/itemcategorys/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $res = $app->response();

        $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;

        if(!$expands){
            $itemcategory = Itemcategorys::find($id);
        }
        else{
            $expands_with = explode(',', $expands);
            $itemcategory = Itemcategorys::with($expands_with)->find($id);
        }

        if(!$itemcategory) {
            $res->status(400);
            $app->stop();                        
        }

        $out = $itemcategory->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->post('/itemcategorys', $authenticateForRole('member'), function () use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        try {

            $app->db->getPdo()->beginTransaction();

            $itemcategory = new Itemcategorys;

            $itemcategory->name                      = $requests['name'];
            $itemcategory->itemgroup_id              = $requests['itemgroup_id'];
            $itemcategory->id                        = $requests['itemgroup_id'].''.Itemcategorys::getNextItemcategoryCode($itemcategory->itemgroup_id);
            $itemcategory->is_active                 = $requests['is_active'];

            $itemcategory->save();

            if(!$itemcategory) {
                $res->status(400);
                $app->stop();                        
            }

            $app->db->getPdo()->commit();

            $out = $itemcategory->toJson();

            $res['Content-Type'] = 'application/json';
            $res->body($out);
            $res->status(200);
            $app->stop();

        } catch (\PDOException $e) {

            $app->db->getPdo()->rollBack();

            $out = json_encode(array('error' => $e));
            $res['Content-Type'] = 'application/json';
            $res->body($out);
            $res->status(400);
            $app->stop();                        
        }

    });

    $app->put('/itemcategorys/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $itemcategory = Itemcategorys::find($id);
        if(!$itemcategory) {
            $res->status(400);
            $app->stop();
        }

        $itemcategory->name                      = $requests['name'];
        $itemcategory->itemgroup_id              = $requests['itemgroup_id'];
        $itemcategory->is_active                 = $requests['is_active'];

        $itemcategory->save();

        if(!$itemcategory) {
            $res->status(400);
            $app->stop();                        
        }
        
        $out = $itemcategory->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);
        $res->status(200);
        $app->stop();

    });

    $app->delete('/itemcategorys/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $itemcategory = Itemcategorys::find($id);

        if(!$itemcategory) {
            $res->status(400);
            $app->stop();                        
        }

        $itemcategory->is_active = false;
        $itemcategory->save();

        $itemcategory->delete();

        $res->status(200);
        $app->stop();

    });