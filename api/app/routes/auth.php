<?php

    $app->get('/auth/isauthenticated', $authenticateForRole('member'), function () use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $res = $app->response();

        $client_id = $app->getCookie('client_id', false);
        $client_token = $app->getCookie('client_token', false);

        $user_app = User_apps::where('id', $client_id)
                ->where('token_key_expire', '>', time())
                ->first();

        if(!$user_app) {
            $res = $app->response();
            $app->response()->status(401);
            $app->stop();
        }

        $app_secret_key = $user_app->secret_key;
        $isValidTokenKey = $app->hash->check($app_secret_key, $client_token);

        if(!$isValidTokenKey) {
            $res = $app->response();
            $app->response()->status(401);
            $app->stop();
        }

        $user = $user_app->user()->first();

        if(!$user) {
            $res = $app->response();
            $app->response()->status(401);
            $app->stop();
        }

        $out = json_encode(array(
            'user' => $user->toArray()
        ));

        $res = $app->response();
        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });


    // LOGIN route
    $app->post('/auth/login', function () use ($app) {

        $request = (array) json_decode($app->request()->getBody());
        //var_dump($request);die();

        $email = $request['email'];
        $password = $request['password'];
        $app_name = $request['app_name'];
        $app_baseurl = $request['app_baseurl'];

        if(!$email && !$password && !$app_baseurl && !$app_name) {
            $app->response()->status(401);
            $app->stop();
        }

        // check user with email
        $user = Users::where('email', $email)
            ->where('active', 1)
            ->whereRaw('deleted_at IS NULL')
            ->first();

        // if not email then check USERNAME
        if(!$user) {
            $user = Users::where('username', $email)
                ->where('active', 1)
                ->whereRaw('deleted_at IS NULL')
                ->first();            
        }

        // if both email and USERNAME does not exist
        if(!$user) {

            $res = $app->response();
            $res['Content-Type'] = 'application/json';
            $res->body(json_encode(array(
                'flash' => 'Invalid Email or Password!'
            )));

            $app->stop();
        }

        // get the user password, by default password is hidden field
        $hashed_password = $user->getPassword();

        // check the password is it valid?
        $isValidPwd = $app->hash->check($password, $hashed_password);

        /*var_dump($hashed_password0);
        var_dump($hashed_password);
        die();*/

        if(!$isValidPwd) {
            $res = $app->response();
            $res['Content-Type'] = 'application/json';
            $res->body(json_encode(array(
                'flash' => 'Password not match!'
            )));

            $app->stop();
        }

        // check if the user has client ID
        $user_app = User_apps::where('user_id', $user->id)
            ->where('name', $app_name)
            ->where('baseurl', $app_baseurl);

        $user_app_count = intval($user_app->count());
 
        // if not exist then create new one
        if(intval($user_app_count) === 0) {

            $new_user_app = new User_apps();
            $new_user_app->user_id = $user->id;
            $new_user_app->secret_key = time() . $user->id . $user->getPassword();
            $new_user_app->name = $app_name;
            $new_user_app->baseurl = $app_baseurl;

            $app_token_key = $app->hash->make($new_user_app->secret_key);
            $new_user_app->token_key = $app_token_key;
            $new_user_app->token_key_expire = time() + (1*24*60*60); // 1 day;

            $new_user_app->save();

            $app->setCookie('client_id', $new_user_app->id);
            $app->setCookie('client_token', $app_token_key);

            // userlogger
            $app->userlogger->log($user->id,'login');

            $out = json_encode(array(
                'user' => $user->toArray()
            ));

            $res = $app->response();
            $res['Content-Type'] = 'application/json';
            $res->body($out);
            $app->stop();
            return;
        }

        // get the actual data
        $user_app = $user_app->first();

        $user_app->secret_key = time() . $user->id . $user->getPassword();
        $user_app->token_key = $app->hash->make($user_app->secret_key);
        $user_app->token_key_expire = time() + (1*24*60*60); // 1 day;

        $user_app->save();

        $app->setCookie('client_id', $user_app->id);
        $app->setCookie('client_token', $user_app->token_key);

        // userlogger
        $app->userlogger->log($user->id, 'login');

        $out = json_encode(array(
            'user' => $user->toArray()
        ));

        $res = $app->response();
        $res['Content-Type'] = 'application/json';
        $res->body($out);
         
    });

    // LOGOUT route
    $app->post('/auth/logout', $authenticateForRole('member'), function () use ($app) {

        $client_id = $app->getCookie('client_id', false);        
        $user_app = User_apps::find($client_id);
        $user_app->token_key_expire = time() - (1*24*60*60); // -1 day;
        $user_app->save();

        // userlogger
        $app->userlogger->log($user_app->user_id, 'logout');

        $app->deleteCookie('client_id');
        $app->deleteCookie('client_token');

        $out = json_encode(array(
            'flash' => 'Logout success...'
        ));

        $res = $app->response();
        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });
