<?php

    $app->get('/routes', $authenticateForRole('member'), function () use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $requests = (array) json_decode($app->request()->getBody());

        $res = $app->response();

        $limit = $app->request->get('limit')?$app->request->get('limit'):0;
        $offset = $app->request->get('offset')?$app->request->get('offset'):0;
        $fields = $app->request->get('fields')?$app->request->get('fields'):null;
        $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

        $total = 0;
        $records = null;

        $total = Routes::select();
        $source = Routes::select();

        if($fields) {
            $total->select($fields);
            $source->select($fields);
        }
        

        if($app->request->get('where')) {

            $where = $app->request->get('where');

            $total->whereRaw($app->db->raw($where));
            $source->whereRaw($app->db->raw($where));
        }

        if($app->request->get('filter') && $app->request->get('filter_fields')) {

            $filter = $app->request->get('filter');
            $filter_fields = $app->request->get('filter_fields');

            $ft_fields = explode(',', $filter_fields);

            $where_like = '';

            for($i=0;$i<count($ft_fields);$i++) {

                if($i===0) {
                    $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }
                else
                {
                    $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }

                if($i===(count($ft_fields)-1)) {
                    $where_like .= ')';
                }

            }

            $total->whereRaw($app->db->raw($where_like));
            $source->whereRaw($app->db->raw($where_like));
        }

        $total = $total->count();

        if($limit>0)
            $source->take($limit)->skip($offset);

        if(!$orderby)
            $source = $source->orderByRaw('updated_at desc')->get();
        else
            $source = $source->orderByRaw($orderby)->get();

        $out = '';

        if($source) {
            $out = json_encode(
                array('records' => $source->toArray(), 'total' => $total)
            );
        }

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->get('/routes/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $res = $app->response();

        $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;

        if(!$expands){
            $route = Routes::find($id);
        }
        else{
            $expands_with = explode(',', $expands);
            $route = Routes::with($expands_with)->find($id);
        }

        if(!$route) {
            $res->status(400);
            $app->stop();                        
        }

        $out = $route->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->get('/routes/name/:name', $authenticateForRole('member'), function ($name) use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $res = $app->response();

        $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;

        if(!$expands){
            $routes = Routes::where('name', '=', $name)->first();
        }
        else{
            $expands_with = explode(',', $expands);
            $routes = Routes::with($expands_with)->where('name', '=', $name)->first();
        }

        if(!$routes) {
            $res->status(400);
            $app->stop();                        
        }

        $out = json_encode($routes->toArray());

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->post('/routes', $authenticateForRole('member'), function () use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $route = new Routes;

        // can not be null
        $route->name        = $requests['name'];
        $route->label       = $requests['label'];

        // can be null

        $route->save();

        if(!$route) {
            $res->status(400);
            $app->stop();                        
        }

        $out = $route->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);
        $res->status(201);
        $app->stop();

    });

    $app->put('/routes/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $route = Routes::find($id);
        if(!$route) {
            $res->status(400);
            $app->stop();
        }

        // can not be null
        $route->name        = $requests['name'];
        $route->label       = $requests['label'];

        // can be null

        $route->save();

        if(!$route) {
            $res->status(400);
            $app->stop();                        
        }

        $out = $route->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);
        $res->status(200);
        $app->stop();

    });

    $app->delete('/routes/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $route = Routes::find($id);

        if(!$route) {
            $res->status(400);
            $app->stop();                        
        }

        $route->is_active = false;
        $route->save();

        $route->delete();

        $res->status(200);
        $app->stop();

    });