<?php

$app->get('/items', $authenticateForRole('member'), function () use ($app) {

    $app->response->headers->set('Content-Type', 'application/json');

    $requests = (array) json_decode($app->request()->getBody());

    $res = $app->response();

    $limit = $app->request->get('limit')?$app->request->get('limit'):0;
    $offset = $app->request->get('offset')?$app->request->get('offset'):0;
    $fields = $app->request->get('fields')?$app->request->get('fields'):null;
    $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

    $total = 0;
    $records = null;

    $total = Items::select();
    $source = Items::select();

    if($fields) {
        $total->select($fields);
        $source->select($fields);
    }

    if($app->request->get('where')) {

        $where = $app->request->get('where');

        $total->whereRaw($app->db->raw($where));
        $source->whereRaw($app->db->raw($where));
    }

    if($app->request->get('filter') && $app->request->get('filter_fields')) {

        $filter = $app->request->get('filter');
        $filter_fields = $app->request->get('filter_fields');

        $ft_fields = explode(',', $filter_fields);

        $where_like = '';

        for($i=0;$i<count($ft_fields);$i++) {

            if($i===0) {
                $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
            }
            else
            {
                $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
            }

            if($i===(count($ft_fields)-1)) {
                $where_like .= ')';
            }

        }

        $total->whereRaw($app->db->raw($where_like));
        $source->whereRaw($app->db->raw($where_like));
    }

    $total = $total->count();

    if($limit>0)
        $source->take($limit)->skip($offset);

    if(!$orderby)
        $source = $source->orderByRaw('name')->get();
    else
        $source = $source->orderByRaw($orderby)->get();

    $out = '';

    if($source) {
        $out = json_encode(
            array('records' => $source->toArray(), 'total' => $total)
        );
    }

    $res['Content-Type'] = 'application/json';
    $res->body($out);

});

$app->get('/items/po', $authenticateForRole('member'), function () use ($app) {

    $app->response->headers->set('Content-Type', 'application/json');

    $requests = (array) json_decode($app->request()->getBody());

    $res = $app->response();

    $limit = $app->request->get('limit')?$app->request->get('limit'):0;
    $offset = $app->request->get('offset')?$app->request->get('offset'):0;
    $fields = $app->request->get('fields')?$app->request->get('fields'):null;
    $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

    $total = 0;
    $records = null;

    $total = Items::select($app->db->raw('param_item.*, case when data_stock.qty is null then 0 else data_stock.qty end as qty'))
    ->leftjoin('data_stock','data_stock.item_id','=','param_item.id');
    $source = Items::select($app->db->raw('param_item.*, case when data_stock.qty is null then 0 else data_stock.qty end as qty'))
    ->leftjoin('data_stock','data_stock.item_id','=','param_item.id');

    if($fields) {
        $total->select($fields);
        $source->select($fields);
    }

    if($app->request->get('where')) {

        $where = $app->request->get('where');

        $total->whereRaw($app->db->raw($where));
        $source->whereRaw($app->db->raw($where));
    }

    if($app->request->get('filter') && $app->request->get('filter_fields')) {

        $filter = $app->request->get('filter');
        $filter_fields = $app->request->get('filter_fields');

        $ft_fields = explode(',', $filter_fields);

        $where_like = '';

        for($i=0;$i<count($ft_fields);$i++) {

            if($i===0) {
                $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
            }
            else
            {
                $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
            }

            if($i===(count($ft_fields)-1)) {
                $where_like .= ')';
            }

        }

        $total->whereRaw($app->db->raw($where_like));
        $source->whereRaw($app->db->raw($where_like));
    }

    $total = $total->count();

    if($limit>0)
        $source->take($limit)->skip($offset);

    if(!$orderby)
        $source = $source->orderByRaw('param_item.id')->get();
    else
        $source = $source->orderByRaw($orderby)->get();

    $out = '';

    if($source) {
        $out = json_encode(
            array('records' => $source->toArray(), 'total' => $total)
        );
    }

    $res['Content-Type'] = 'application/json';
    $res->body($out);

});

$app->get('/items/adjustment', $authenticateForRole('member'), function () use ($app) {

    $app->response->headers->set('Content-Type', 'application/json');

    $requests = (array) json_decode($app->request()->getBody());

    $res = $app->response();

    $limit = $app->request->get('limit')?$app->request->get('limit'):0;
    $offset = $app->request->get('offset')?$app->request->get('offset'):0;
    $fields = $app->request->get('fields')?$app->request->get('fields'):null;
    $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

    $total = 0;
    $records = null;

    $total = Items::select($app->db->raw('param_item.*, case when data_stock.qty is null then 0 else data_stock.qty end as qty, data_stock.hpp'))
    ->leftjoin('data_stock','data_stock.item_id','=','param_item.id');
    $source = Items::select($app->db->raw('param_item.*, case when data_stock.qty is null then 0 else data_stock.qty end as qty, data_stock.hpp'))
    ->leftjoin('data_stock','data_stock.item_id','=','param_item.id');

    if($fields) {
        $total->select($fields);
        $source->select($fields);
    }

    if($app->request->get('where')) {

        $where = $app->request->get('where');

        $total->whereRaw($app->db->raw($where));
        $source->whereRaw($app->db->raw($where));
    }

    if($app->request->get('filter') && $app->request->get('filter_fields')) {

        $filter = $app->request->get('filter');
        $filter_fields = $app->request->get('filter_fields');

        $ft_fields = explode(',', $filter_fields);

        $where_like = '';

        for($i=0;$i<count($ft_fields);$i++) {

            if($i===0) {
                $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
            }
            else
            {
                $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
            }

            if($i===(count($ft_fields)-1)) {
                $where_like .= ')';
            }

        }

        $total->whereRaw($app->db->raw($where_like));
        $source->whereRaw($app->db->raw($where_like));
    }

    $total = $total->count();

    if($limit>0)
        $source->take($limit)->skip($offset);

    if(!$orderby)
        $source = $source->orderByRaw('param_item.id')->get();
    else
        $source = $source->orderByRaw($orderby)->get();

    $out = '';

    if($source) {
        $out = json_encode(
            array('records' => $source->toArray(), 'total' => $total)
        );
    }

    $res['Content-Type'] = 'application/json';
    $res->body($out);

});

$app->get('/items/:id', $authenticateForRole('member'), function ($id) use ($app) {

    $app->response->headers->set('Content-Type', 'application/json');

    $res = $app->response();

    $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;

    if(!$expands){
        $item = Items::find($id);
    }
    else{
        $expands_with = explode(',', $expands);
        $item = Items::with($expands_with)->find($id);
    }

    if(!$item) {
        $res->status(400);
        $app->stop();                        
    }

    $out = $item->toJson();

    $res['Content-Type'] = 'application/json';
    $res->body($out);

});

$app->post('/items', $authenticateForRole('member'), function () use ($app) {

    $requests = (array) json_decode($app->request()->getBody());

    $app->response->headers->set('Content-Type', 'application/json');
    $res = $app->response();

    try {

        $app->db->getPdo()->beginTransaction();

        $item = new Items;

        $item->name                      = $requests['name'];
        $item->itemgroup_id              = $requests['itemgroup_id'];
        $item->itemcategory_id           = $requests['itemcategory_id'];
        $item->itemsubcategory_id        = $requests['itemsubcategory_id'];
        $item->id                        = $requests['itemsubcategory_id'].''.Items::getNextItemCode($item->itemsubcategory_id);
        if(isset($requests['barcode'])){
            $item->barcode               = $requests['barcode'];
        }else{
            $item->barcode               = $item->id;
        }
        $item->owner_id                  = $requests['owner_id'];
        $item->unit_id                   = $requests['unit_id'];
        $item->brand_id                  = $requests['brand_id'];
        $item->weight                    = $requests['weight'];
        $item->is_active                 = $requests['is_active'];

        if(isset($requests['long']))
            $item->long                      = $requests['long'];
        if(isset($requests['wide']))
            $item->wide                      = $requests['wide'];
        if(isset($requests['high']))
            $item->high                      = $requests['high'];
        if(isset($requests['item_sales_tax']))
            $item->item_sales_tax            = $requests['item_sales_tax'];
        if(isset($requests['note']))
            $item->note                      = $requests['note'];

        $item->save();

        if(!$item) {
            $res->status(400);
            $app->stop();                        
        }

        $app->db->getPdo()->commit();

        $out = $item->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);
        $res->status(200);
        $app->stop();

    } catch (\PDOException $e) {

        $app->db->getPdo()->rollBack();

        $out = json_encode(array('error' => $e));
        $res['Content-Type'] = 'application/json';
        $res->body($out);
        $res->status(400);
        $app->stop();                        
    }

});

$app->put('/items/:id', $authenticateForRole('member'), function ($id) use ($app) {

    $requests = (array) json_decode($app->request()->getBody());

    $app->response->headers->set('Content-Type', 'application/json');
    $res = $app->response();

    $item = Items::find($id);
    if(!$item) {
        $res->status(400);
        $app->stop();
    }

    $item->name                      = $requests['name'];
    $item->itemgroup_id              = $requests['itemgroup_id'];
    $item->itemcategory_id           = $requests['itemcategory_id'];
    $item->itemsubcategory_id        = $requests['itemsubcategory_id'];
    $item->unit_id                   = $requests['unit_id'];
    $item->brand_id                  = $requests['brand_id'];
    $item->owner_id                  = $requests['owner_id'];
    $item->weight                    = $requests['weight'];
    $item->is_active                 = $requests['is_active'];

    if(isset($requests['long']))
        $item->long                      = $requests['long'];
    if(isset($requests['wide']))
        $item->wide                      = $requests['wide'];
    if(isset($requests['high']))
        $item->high                      = $requests['high'];

    if(isset($requests['item_sales_tax']))
        $item->item_sales_tax            = $requests['item_sales_tax'];
    if(isset($requests['note']))
        $item->note                      = $requests['note'];

    $item->save();

    if(!$item) {
        $res->status(400);
        $app->stop();                        
    }
    
    $out = $item->toJson();

    $res['Content-Type'] = 'application/json';
    $res->body($out);
    $res->status(200);
    $app->stop();

});

$app->delete('/items/:id', $authenticateForRole('member'), function ($id) use ($app) {

    $requests = (array) json_decode($app->request()->getBody());

    $app->response->headers->set('Content-Type', 'application/json');
    $res = $app->response();

    $item = Items::find($id);

    if(!$item) {
        $res->status(400);
        $app->stop();                        
    }

    $item->is_active = false;
    $item->save();

    $item->delete();

    $res->status(200);
    $app->stop();

});