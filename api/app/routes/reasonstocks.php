<?php

    $app->get('/reasonstocks', $authenticateForRole('member'), function () use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $requests = (array) json_decode($app->request()->getBody());

        $res = $app->response();

        $limit = $app->request->get('limit')?$app->request->get('limit'):0;
        $offset = $app->request->get('offset')?$app->request->get('offset'):0;
        $fields = $app->request->get('fields')?$app->request->get('fields'):null;
        $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

        $total = 0;
        $records = null;

        $total = Reasonstocks::select();
        $source = Reasonstocks::select();

        if($fields) {
            $total->select($fields);
            $source->select($fields);
        }

        if($app->request->get('where')) {

            $where = $app->request->get('where');

            $total->whereRaw($app->db->raw($where));
            $source->whereRaw($app->db->raw($where));
        }

        if($app->request->get('filter') && $app->request->get('filter_fields')) {

            $filter = $app->request->get('filter');
            $filter_fields = $app->request->get('filter_fields');

            $ft_fields = explode(',', $filter_fields);

            $where_like = '';

            for($i=0;$i<count($ft_fields);$i++) {

                if($i===0) {
                    $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }
                else
                {
                    $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }

                if($i===(count($ft_fields)-1)) {
                    $where_like .= ')';
                }

            }

            $total->whereRaw($app->db->raw($where_like));
            $source->whereRaw($app->db->raw($where_like));
        }

        $total = $total->count();

        if($limit>0)
            $source->take($limit)->skip($offset);

        if(!$orderby)
            $source = $source->orderByRaw('id')->get();
        else
            $source = $source->orderByRaw($orderby)->get();

        $out = '';

        if($source) {
            $out = json_encode(
                array('records' => $source->toArray(), 'total' => $total)
            );
        }

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->get('/reasonstocks/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $res = $app->response();

        $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;

        if(!$expands){
            $reasonstock = Reasonstocks::find($id);
        }
        else{
            $expands_with = explode(',', $expands);
            $reasonstock = Reasonstocks::with($expands_with)->find($id);
        }

        if(!$reasonstock) {
            $res->status(400);
            $app->stop();                        
        }

        $out = $reasonstock->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->post('/reasonstocks', $authenticateForRole('member'), function () use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        try {

            $app->db->getPdo()->beginTransaction();

            $reasonstock = new Reasonstocks;

            $reasonstock->id                        = Reasonstocks::getNextReasonstockCode();
            $reasonstock->name                      = $requests['name'];
            $reasonstock->is_active                 = $requests['is_active'];

            $reasonstock->save();

            if(!$reasonstock) {
                $res->status(400);
                $app->stop();                        
            }

            $app->db->getPdo()->commit();

            $out = $reasonstock->toJson();

            $res['Content-Type'] = 'application/json';
            $res->body($out);
            $res->status(200);
            $app->stop();

        } catch (\PDOException $e) {

            $app->db->getPdo()->rollBack();

            $out = json_encode(array('error' => $e));
            $res['Content-Type'] = 'application/json';
            $res->body($out);
            $res->status(400);
            $app->stop();                        
        }

    });

    $app->put('/reasonstocks/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $reasonstock = Reasonstocks::find($id);
        if(!$reasonstock) {
            $res->status(400);
            $app->stop();
        }

        $reasonstock->name                      = $requests['name'];
        $reasonstock->is_active                 = $requests['is_active'];

        $reasonstock->save();

        if(!$reasonstock) {
            $res->status(400);
            $app->stop();                        
        }
        
        $out = $reasonstock->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);
        $res->status(200);
        $app->stop();

    });

    $app->delete('/reasonstocks/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $reasonstock = Reasonstocks::find($id);

        if(!$reasonstock) {
            $res->status(400);
            $app->stop();                        
        }

        $reasonstock->is_active = false;
        $reasonstock->save();

        $reasonstock->delete();

        $res->status(200);
        $app->stop();

    });

    $app->get('/reasonstocks/name/:name', $authenticateForRole('member'), function ($name) use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $res = $app->response();

        $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;

        if(!$expands){
            $reasonstocks = Reasonstocks::where('name', '=', $name)->first();
        }
        else{
            $expands_with = explode(',', $expands);
            $reasonstocks = Reasonstocks::with($expands_with)->where('name', '=', $name)->first();
        }

        if(!$reasonstocks) {
            $res->status(400);
            $app->stop();                        
        }

        $out = json_encode($reasonstocks->toArray());

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });