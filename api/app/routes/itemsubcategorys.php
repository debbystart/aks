<?php

    $app->get('/itemsubcategorys', $authenticateForRole('member'), function () use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $requests = (array) json_decode($app->request()->getBody());

        $res = $app->response();

        $limit = $app->request->get('limit')?$app->request->get('limit'):0;
        $offset = $app->request->get('offset')?$app->request->get('offset'):0;
        $fields = $app->request->get('fields')?$app->request->get('fields'):null;
        $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

        $total = 0;
        $records = null;

        $total = Itemsubcategorys::select();
        $source = Itemsubcategorys::select();

        if($fields) {
            $total->select($fields);
            $source->select($fields);
        }

        if($app->request->get('where')) {

            $where = $app->request->get('where');

            $total->whereRaw($app->db->raw($where));
            $source->whereRaw($app->db->raw($where));
        }

        if($app->request->get('filter') && $app->request->get('filter_fields')) {

            $filter = $app->request->get('filter');
            $filter_fields = $app->request->get('filter_fields');

            $ft_fields = explode(',', $filter_fields);

            $where_like = '';

            for($i=0;$i<count($ft_fields);$i++) {

                if($i===0) {
                    $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }
                else
                {
                    $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }

                if($i===(count($ft_fields)-1)) {
                    $where_like .= ')';
                }

            }

            $total->whereRaw($app->db->raw($where_like));
            $source->whereRaw($app->db->raw($where_like));
        }

        $total = $total->count();

        if($limit>0)
            $source->take($limit)->skip($offset);

        if(!$orderby)
            $source = $source->orderByRaw('name')->get();
        else
            $source = $source->orderByRaw($orderby)->get();

        $out = '';

        if($source) {
            $out = json_encode(
                array('records' => $source->toArray(), 'total' => $total)
            );
        }

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->get('/itemsubcategorys/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $res = $app->response();

        $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;

        if(!$expands){
            $itemsubcategory = Itemsubcategorys::find($id);
        }
        else{
            $expands_with = explode(',', $expands);
            $itemsubcategory = Itemsubcategorys::with($expands_with)->find($id);
        }

        if(!$itemsubcategory) {
            $res->status(400);
            $app->stop();                        
        }

        $out = $itemsubcategory->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->post('/itemsubcategorys', $authenticateForRole('member'), function () use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        try {

            $app->db->getPdo()->beginTransaction();

            $itemsubcategory = new Itemsubcategorys;
            
            $itemsubcategory->name                      = $requests['name'];
            $itemsubcategory->itemgroup_id              = $requests['itemgroup_id'];
            $itemsubcategory->itemcategory_id           = $requests['itemcategory_id'];
            $itemsubcategory->id                        = $requests['itemcategory_id'].''.Itemsubcategorys::getNextItemsubcategoryCode($itemsubcategory->itemcategory_id);
            $itemsubcategory->is_active                 = $requests['is_active'];

            $itemsubcategory->save();

            if(!$itemsubcategory) {
                $res->status(400);
                $app->stop();                        
            }

            $app->db->getPdo()->commit();

            $out = $itemsubcategory->toJson();

            $res['Content-Type'] = 'application/json';
            $res->body($out);
            $res->status(200);
            $app->stop();

        } catch (\PDOException $e) {

            $app->db->getPdo()->rollBack();

            $out = json_encode(array('error' => $e));
            $res['Content-Type'] = 'application/json';
            $res->body($out);
            $res->status(400);
            $app->stop();                        
        }

    });

    $app->put('/itemsubcategorys/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $itemsubcategory = Itemsubcategorys::find($id);
        if(!$itemsubcategory) {
            $res->status(400);
            $app->stop();
        }

        $itemsubcategory->name                      = $requests['name'];
        $itemsubcategory->itemgroup_id              = $requests['itemgroup_id'];
        $itemsubcategory->itemcategory_id           = $requests['itemcategory_id'];
        $itemsubcategory->is_active                 = $requests['is_active'];

        $itemsubcategory->save();

        if(!$itemsubcategory) {
            $res->status(400);
            $app->stop();                        
        }
        
        $out = $itemsubcategory->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);
        $res->status(200);
        $app->stop();

    });

    $app->delete('/itemsubcategorys/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $itemsubcategory = Itemsubcategorys::find($id);

        if(!$itemsubcategory) {
            $res->status(400);
            $app->stop();                        
        }

        $itemsubcategory->is_active = false;
        $itemsubcategory->save();

        $itemsubcategory->delete();

        $res->status(200);
        $app->stop();

    });