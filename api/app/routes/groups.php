<?php

    $app->get('/groups', $authenticateForRole('member'), function () use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $requests = (array) json_decode($app->request()->getBody());

        $res = $app->response();

        $limit = $app->request->get('limit')?$app->request->get('limit'):0;
        $offset = $app->request->get('offset')?$app->request->get('offset'):0;
        $fields = $app->request->get('fields')?$app->request->get('fields'):null;
        $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

        $total = 0;
        $records = null;

        $total = Groups::select();
        $source = Groups::select();

        if($fields) {
            $total->select($fields);
            $source->select($fields);
        }
        

        if($app->request->get('where')) {

            $where = $app->request->get('where');

            $total->whereRaw($app->db->raw($where));
            $source->whereRaw($app->db->raw($where));
        }

        if($app->request->get('filter') && $app->request->get('filter_fields')) {

            $filter = $app->request->get('filter');
            $filter_fields = $app->request->get('filter_fields');

            $ft_fields = explode(',', $filter_fields);

            $where_like = '';

            for($i=0;$i<count($ft_fields);$i++) {

                if($i===0) {
                    $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }
                else
                {
                    $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }

                if($i===(count($ft_fields)-1)) {
                    $where_like .= ')';
                }

            }

            $total->whereRaw($app->db->raw($where_like));
            $source->whereRaw($app->db->raw($where_like));
        }

        $total = $total->count();

        if($limit>0)
            $source->take($limit)->skip($offset);

        if(!$orderby)
            $source = $source->orderByRaw('updated_at desc')->get();
        else
            $source = $source->orderByRaw($orderby)->get();

        $out = '';

        if($source) {
            $out = json_encode(
                array('records' => $source->toArray(), 'total' => $total)
            );
        }

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->get('/groups/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $res = $app->response();

        $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;

        if(!$expands){
            $group = Groups::find($id);
        }
        else{
            $expands_with = explode(',', $expands);
            $group = Groups::with($expands_with)->find($id);
        }

        if(!$group) {
            $res->status(400);
            $app->stop();                        
        }

        $out = $group->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->post('/groups', $authenticateForRole('member'), function () use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $group = new Groups;

        // can not be null
        $group->name                  = $requests['name'];
        $group->description           = $requests['description'];
        $group->code                  = $requests['code'];
        $group->is_active             = $requests['is_active'];

        // can be null
     
        $group->save();

        // fill in roles
        if(isset($requests['roles'])) {

            $roles = $requests['roles'];

            if(count($roles)>0) {

                $roles_data = array();

                for ($i=0; $i < count($roles); $i++) { 

                    $role = $roles[$i];

                    array_push($roles_data, $role->id);
                }

                // save/sychronize in sample_role
                $group->roles()->sync($roles_data);

            }
        }

        if(!$group) {
            $res->status(400);
            $app->stop();                        
        }

        $out = $group->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);
        $res->status(201);
        $app->stop();

    });

    $app->put('/groups/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $group = Groups::find($id);
        if(!$group) {
            $res->status(400);
            $app->stop();
        }

        try {

            $app->db->getPdo()->beginTransaction();

            // can not be null
            $group->name              	  = $requests['name'];
            $group->description           = $requests['description'];
            $group->code                  = $requests['code'];
            $group->is_active             = $requests['is_active'];

            // can be null
          
            $group->save();


            // fill in roles
            if(isset($requests['roles'])) {

                $roles = $requests['roles'];

                if(count($roles)>0) {

                    $roles_data = array();

                    for ($i=0; $i < count($roles); $i++) { 

                        $role = $roles[$i];

                        array_push($roles_data, $role->id);
                    }

                    // save/sychronize in sample_role
                    $group->roles()->sync($roles_data);

                }

            }



            if(!$group) {
                $res->status(400);
                $app->stop();                        
            }

            $app->db->getPdo()->commit();

            $out = $group->toJson();

            $res['Content-Type'] = 'application/json';
            $res->body($out);
            $res->status(200);
            $app->stop();


        } catch (\PDOException $e) {
            // Woopsy
            $app->db->getPdo()->rollBack();

            $out = json_encode(array('error' => $e));

            $res->body($out);
            $res->status(400);
            $app->stop();                        
        }

    });

    $app->delete('/groups/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $group = Groups::find($id);

        if(!$group) {
            $res->status(400);
            $app->stop();                        
        }

        $group->is_active = false;
        $group->save();

        $group->delete();

        $res->status(200);
        $app->stop();

    });

    $app->get('/groups/name/:name', $authenticateForRole('member'), function ($name) use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $res = $app->response();

        $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;

        if(!$expands){
            $loans = Groups::where('name', '=', $name)->first();
        }
        else{
            $expands_with = explode(',', $expands);
            $loans = Groups::with($expands_with)->where('name', '=', $name)->first();
        }

        if(!$loans) {
            $res->status(400);
            $app->stop();                        
        }

        $out = json_encode($loans->toArray());

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });