<?php

    $app->get('/customers', $authenticateForRole('member'), function () use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $requests = (array) json_decode($app->request()->getBody());

        $res = $app->response();

        $limit = $app->request->get('limit')?$app->request->get('limit'):0;
        $offset = $app->request->get('offset')?$app->request->get('offset'):0;
        $fields = $app->request->get('fields')?$app->request->get('fields'):null;
        $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;

        $total = 0;
        $records = null;

        $user_group_id = null;
        $client_id = $app->getCookie('client_id', false);
        $client_token = $app->getCookie('client_token', false);
        $user_app = User_apps::find($client_id);
        
        if($user_app) {
            $user = Users::find($user_app->user_id);
            if($user){
                $user_group_id = intval($user->user_group_id);
                $user_company_id = intval($user->company_id);
            }
        }

        if($user_group_id == 1){
            $total = Customers::select();
            $source = Customers::select();
        }else{
            $total = Customers::select()
            ->where('company_id', $user_company_id);
            $source = Customers::select()
            ->where('company_id', $user_company_id);
        }

        if($fields) {
            $total->select($fields);
            $source->select($fields);
        }
        

        if($app->request->get('where')) {

            $where = $app->request->get('where');

            $total->whereRaw($app->db->raw($where));
            $source->whereRaw($app->db->raw($where));
        }

        if($app->request->get('filter') && $app->request->get('filter_fields')) {

            $filter = $app->request->get('filter');
            $filter_fields = $app->request->get('filter_fields');

            $ft_fields = explode(',', $filter_fields);

            $where_like = '';

            for($i=0;$i<count($ft_fields);$i++) {

                if($i===0) {
                    $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }
                else
                {
                    $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }

                if($i===(count($ft_fields)-1)) {
                    $where_like .= ')';
                }

            }

            $total->whereRaw($app->db->raw($where_like));
            $source->whereRaw($app->db->raw($where_like));
        }

        $total = $total->count();

        if($limit>0)
            $source->take($limit)->skip($offset);

        if(!$orderby)
            $source = $source->orderByRaw('created_at desc')->get();
        else
            $source = $source->orderByRaw($orderby)->get();

        $out = '';

        if($source) {
            $out = json_encode(
                array('records' => $source->toArray(), 'total' => $total)
            );
        }

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->get('/customers/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $res = $app->response();

        $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;

        if(!$expands){
            $customer = Customers::find($id);
        }
        else{
            $expands_with = explode(',', $expands);
            $customer = Customers::with($expands_with)->find($id);
        }

        if(!$customer) {
            $res->status(400);
            $app->stop();                        
        }

        $out = $customer->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->post('/customers', $authenticateForRole('member'), function () use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $user_group_id = null;
        $client_id = $app->getCookie('client_id', false);
        $client_token = $app->getCookie('client_token', false);
        $user_app = User_apps::find($client_id);
        
        if($user_app) {
            $user = Users::find($user_app->user_id);
            if($user){
                $user_group_id = intval($user->user_group_id);
                $user_company_id = intval($user->company_id);
            }
        }

        try {

            $app->db->getPdo()->beginTransaction();

            $customer = new Customers;

            $customer->id            = $requests['code'].''.Customers::getNextCustomerCode($requests['code']);
            $customer->company_id    = $user_company_id;
            $customer->name          = $requests['name'];
            $customer->mobile        = $requests['mobile'];
            $customer->province      = $requests['province'];
            $customer->city          = $requests['city'];
            $customer->address       = $requests['address'];
            $customer->market_id     = $requests['market_id'];
            $customer->is_active     = $requests['is_active'];

            if(isset($requests['districts']))
                $customer->districts     = $requests['districts'];
            if(isset($requests['village']))
                $customer->village       = $requests['village'];
            if(isset($requests['postal']))
                $customer->postal        = $requests['postal'];
            if(isset($requests['email']))
                $customer->email         = $requests['email'];
            if(isset($requests['note']))
                $customer->note          = $requests['note'];

            $customer->save();

            if(!$customer) {
                $res->status(400);
                $app->stop();                        
            }

            $app->db->getPdo()->commit();

            $out = $customer->toJson();

            $res['Content-Type'] = 'application/json';
            $res->body($out);
            $res->status(200);
            $app->stop();

        } catch (\PDOException $e) {

            $app->db->getPdo()->rollBack();

            $out = json_encode(array('error' => $e));
            $res['Content-Type'] = 'application/json';
            $res->body($out);
            $res->status(400);
            $app->stop();                        
        }

    });

    $app->put('/customers/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $customer = Customers::find($id);
        if(!$customer) {
            $res->status(400);
            $app->stop();
        }

        $customer->company_id    = $requests['company_id'];
        $customer->name          = $requests['name'];
        $customer->mobile        = $requests['mobile'];
        $customer->province      = $requests['province'];
        $customer->city          = $requests['city'];
        $customer->address       = $requests['address'];
        $customer->market_id     = $requests['market_id'];
        $customer->is_active     = $requests['is_active'];

        if(isset($requests['districts']))
        $customer->districts     = $requests['districts'];
        if(isset($requests['village']))
        $customer->village       = $requests['village'];
        if(isset($requests['postal']))
        $customer->postal        = $requests['postal'];
        if(isset($requests['email']))
        $customer->email         = $requests['email'];
        if(isset($requests['note']))
        $customer->note          = $requests['note'];

        $customer->save();

        if(!$customer) {
            $res->status(400);
            $app->stop();                        
        }
        
        $out = $customer->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);
        $res->status(200);
        $app->stop();

    });

    $app->delete('/customers/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $customer = Customers::find($id);

        if(!$customer) {
            $res->status(400);
            $app->stop();                        
        }

        $customer->is_active = false;
        $customer->save();

        $customer->delete();

        $res->status(200);
        $app->stop();

    });

    $app->get('/customers/mobile/:mobile', $authenticateForRole('member'), function ($mobile) use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $res = $app->response();

        $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;

        //$mobile = ."'"$mobile"'".;

        if(!$expands){
            $supplier = Customers::where('mobile', '=', $mobile)->first();
        }
        else{
            $expands_with = explode(',', $expands);
            $supplier = Customers::with($expands_with)->where('mobile', '=', $mobile)->first();
        }

        if(!$supplier) {
            $res->status(400);
            $app->stop();                        
        }

        $out = json_encode($supplier->toArray());

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });