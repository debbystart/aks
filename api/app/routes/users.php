<?php

    $app->get('/users', $authenticateForRole('member'), function () use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $requests = (array) json_decode($app->request()->getBody());

        $res = $app->response();

        $limit = $app->request->get('limit')?$app->request->get('limit'):0;
        $offset = $app->request->get('offset')?$app->request->get('offset'):0;
        $fields = $app->request->get('fields')?$app->request->get('fields'):'*';
        $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;
        
        //get user_id & user_group_id
        $user_group_id = null;
        $client_id = $app->getCookie('client_id', false);
        $client_token = $app->getCookie('client_token', false);
        $user_app = User_apps::find($client_id);

        if($user_app) {
            $user = Users::find($user_app->user_id);
            if($user)
                $user_group_id = intval($user->user_group_id);
        }

        $total = 0;
        $records = null;

        $total = Users::select($app->db->raw($fields));

        if(!$expands){
            $source = Users::select($app->db->raw($fields));
        }
        else{
            $expands_with = explode(',', $expands);
            $source = Users::with($expands_with)->select($app->db->raw($fields));
        }       

        if($app->request->get('where')) {

            $where = $app->request->get('where');

            $total->whereRaw($app->db->raw($where));
            $source->whereRaw($app->db->raw($where));
        }

        if($app->request->get('filter') && $app->request->get('filter_fields')) {

            $filter = $app->request->get('filter');
            $filter_fields = $app->request->get('filter_fields');

            $ft_fields = explode(',', $filter_fields);

            $where_like = '';

            for($i=0;$i<count($ft_fields);$i++) {

                if($i===0) {
                    $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }
                else
                {
                    $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }

                if($i===(count($ft_fields)-1)) {
                    $where_like .= ')';
                }

            }

            $total->whereRaw($app->db->raw($where_like));
            $source->whereRaw($app->db->raw($where_like));
        }

        $total = $total->count();

        if($limit>0)
            $source->take($limit)->skip($offset);

       if($user_group_id===1){
            $source = $source->orderByRaw('updated_at desc')->get();
        }else{
            $source = $source->orderByRaw('name asc')->get();
        }

        $out = '';
        if($source) {
            $out = json_encode(
                array('records' => $source->toArray(), 'total' => $total)
            );
        }

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->get('/users_ttd', $authenticateForRole('member'), function () use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $requests = (array) json_decode($app->request()->getBody());

        $res = $app->response();

        $limit = $app->request->get('limit')?$app->request->get('limit'):0;
        $offset = $app->request->get('offset')?$app->request->get('offset'):0;
        $fields = $app->request->get('fields')?$app->request->get('fields'):'*';
        $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;

        $total = 0;
        $records = null;

        $total = Users::select($app->db->raw($fields));

        if(!$expands){
            $source = Users::select($app->db->raw($fields));
        }
        else{
            $expands_with = explode(',', $expands);
            $source = Users::with($expands_with)->select($app->db->raw($fields));
        }       

        if($app->request->get('where')) {

            $where = $app->request->get('where');

            $total->whereRaw($app->db->raw($where));
            $source->whereRaw($app->db->raw($where));
        }

        if($app->request->get('filter') && $app->request->get('filter_fields')) {

            $filter = $app->request->get('filter');
            $filter_fields = $app->request->get('filter_fields');

            $ft_fields = explode(',', $filter_fields);

            $where_like = '';

            for($i=0;$i<count($ft_fields);$i++) {

                if($i===0) {
                    $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }
                else
                {
                    $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }

                if($i===(count($ft_fields)-1)) {
                    $where_like .= ')';
                }

            }

            $total->whereRaw($app->db->raw($where_like));
            $source->whereRaw($app->db->raw($where_like));
        }

        $total = $total->groupBy('name')->count();

        if($limit>0)
            $source->take($limit)->skip($offset);

        $source = $source->groupBy('name')->orderByRaw('updated_at desc')->get();

        $out = '';
        if($source) {
            $out = json_encode(
                array('records' => $source->toArray(), 'total' => $total)
            );
        }

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    
    $app->get('/users/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $res = $app->response();

        $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;

        if(!$expands){
            $users = Users::find($id);
        }
        else{
            $expands_with = explode(',', $expands);
            $users = Users::with($expands_with)->find($id);
        }

        if(!$users) {
            $res->status(400);
            $app->stop();                        
        }

        $out = $users->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->get('/users/email/:email', $authenticateForRole('member'), function ($email) use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $res = $app->response();

        $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;

        if(!$expands){
            $users = Users::where('email', '=', $email)->first();
        }
        else{
            $expands_with = explode(',', $expands);
            $users = Users::with($expands_with)->where('email', '=', $email)->first();
        }

        if(!$users) {
            $res->status(400);
            $app->stop();                        
        }

        $out = json_encode($users->toArray());

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->get('/users/nip/:nip', $authenticateForRole('member'), function ($nip) use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $res = $app->response();

        $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;

        if(!$users) {
            $res->status(400);
            $app->stop();                        
        }

        $out = json_encode($users->toArray());

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->get('/users/username/:username', $authenticateForRole('member'), function ($username) use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $res = $app->response();

        $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;

        if(!$expands){
            $users = Users::where('username', '=', $username)->first();
        }
        else{
            $expands_with = explode(',', $expands);
            $users = Users::with($expands_with)->where('username', '=', $username)->first();
        }

        if(!$users) {
            $res->status(400);
            $app->stop();                        
        }

        $out = json_encode($users->toArray());

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->post('/users', $authenticateForRole('member'), function () use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $user = new Users;

        $user->username         = $requests['username'];
        $user->name             = $requests['name'];
        $user->company_id       = $requests['company_id'];
        $user->email            = $requests['email'];
        $user->user_group_id    = $requests['user_group_id'];

        if(isset($requests['active']))
            $user->active             = $requests['active'];

        $user->save();

        if(!$user) {
            $res->status(400);
            $app->stop();                        
        }

        $out = $user->toJson();

        $res['Content-Type'] = 'application/json';
        $res->body($out);
        $res->status(201);
        $app->stop();

    });
    
    $app->put('/users/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $user = Users::find($id);
        if(!$user) {
            $res->status(400);
            $app->stop();
        }

        // validate who is the updater
        $client_id = $app->getCookie('client_id', false);
        $user_app = User_apps::find($client_id);
        $user_app_data = Users::find($user_app->user_id);

        if(!$user_app_data) {
            $res->status(400);
            $app->stop();
        }

        $user_app_data->toArray();

        $app->db->transaction(function () use ($user, $requests, $user_app_data, $app) {

        $user->username         = $requests['username'];
        $user->name             = $requests['name'];
        $user->company_id       = $requests['company_id'];
        $user->email            = $requests['email'];
        $user->user_group_id    = $requests['user_group_id'];

        if(isset($requests['active']))
            $user->active             = $requests['active'];

            
            if(isset($requests['old_pwd']) && isset($requests['new_pwd']) && isset($requests['retype_new_pwd'])){

                $old_pwd_db = $user->getPassword();

                // check the password is it valid?
                $isValidPwd = $app->hash->check($requests['old_pwd'], $old_pwd_db);

                if($isValidPwd && ($requests['new_pwd']===$requests['retype_new_pwd'])) {

                    $hashed_password        = $app->hash->make($requests['new_pwd']);
                    $user->password         = $hashed_password;                    

                    $user->save();
                    $user->push();
                }
                else {

                    $res = $app->response();
                    $res['Content-Type'] = 'application/json';
                    $res->body(json_encode(array(
                        'error' => 'crud.oldPwd.error'
                    )));
                    $app->response()->status(400);
                    $app->stop();

                }

            }
            else {
                if (isset($requests['new_pwd']) && isset($requests['retype_new_pwd'])) {
                    $hashed_password        = $app->hash->make($requests['new_pwd']);
                    $user->password         = $hashed_password;

                    $user->save();
                    $user->push();
                }
                else {
                    $user->save();
                    $user->push();
                }
            }

        });

        $res->status(200);
        $app->stop();

    });

    $app->delete('/users/:id', $authenticateForRole('member'), function ($id) use ($app) {

        $requests = (array) json_decode($app->request()->getBody());

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $users = Users::find($id);

        if(!$users) {
            $res->status(400);
            $app->stop();                        
        }

        $users->active = 0;
        $users->save();

        $users->delete();

        $out = '';
        $res->body($out);

    });

    $app->get('/users/analysts', $authenticateForRole('member'), function () use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $requests = (array) json_decode($app->request()->getBody());

        $res = $app->response();

        $limit = $app->request->get('limit')?$app->request->get('limit'):0;
        $offset = $app->request->get('offset')?$app->request->get('offset'):0;
        $fields = $app->request->get('fields')?$app->request->get('fields'):null;

        $total = 0;
        $records = null;

        $total = Users::select();
        $source = Users::select();

        if($fields) {
            $total->select($fields);
            $source->select($fields);
        }
        

        if($app->request->get('where')) {

            $where = $app->request->get('where');

            $total->whereRaw($app->db->raw($where));
            $source->whereRaw($app->db->raw($where));
        }

        if($app->request->get('filter') && $app->request->get('filter_fields')) {

            $filter = $app->request->get('filter');
            $filter_fields = $app->request->get('filter_fields');

            $ft_fields = explode(',', $filter_fields);

            $where_like = '';

            for($i=0;$i<count($ft_fields);$i++) {

                if($i===0) {
                    $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }
                else
                {
                    $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }

                if($i===(count($ft_fields)-1)) {
                    $where_like .= ')';
                }

            }

            $total->whereRaw($app->db->raw($where_like));
            $source->whereRaw($app->db->raw($where_like));
        }

        $total = $total->count();

        if($limit>0)
            $source->take($limit)->skip($offset);

        $source = $source->orderByRaw('updated_at desc')->get();

        $out = '';

        if($source) {
            $out = json_encode(
                array('records' => $source->toArray(), 'total' => $total)
            );
        }

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });

    $app->post('/user_projects/:id/user_project/unlink/:user_id', $authenticateForRole('member'), function ($id, $user_id) use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');
        $res = $app->response();

        $requests = (array) json_decode($app->request()->getBody());

        $user_project = User_projects::find($id);

        if(!$user_project) {
            $res->status(400);
            $app->stop();                        
        }

        //if($quotation_item->filepath!==null && file_exists(__DIR__. './../../..' .$internal_purchase_upload->filepath))
          //  unlink(__DIR__. './../../..' .$internal_purchase_upload->filepath);

        $user_project->delete();

        $res->status(201);
        $app->stop();

    });
    
    
    $app->get('/user/transactions', $authenticateForRole('member'), function () use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $requests = (array) json_decode($app->request()->getBody());

        $res = $app->response();

        $limit = $app->request->get('limit')?$app->request->get('limit'):0;
        $offset = $app->request->get('offset')?$app->request->get('offset'):0;
        $fields = $app->request->get('fields')?$app->request->get('fields'):'*';
      $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;
        $expands = $app->request->get('expands')?$app->request->get('expands'):NULL;
    
        
        //get user_id & user_group_id
        $user_group_id = null;
        $client_id = $app->getCookie('client_id', false);
        $client_token = $app->getCookie('client_token', false);
        $user_app = User_apps::find($client_id);

        if($user_app) {
            $user = Users::find($user_app->user_id);
            if($user)
                $user_group_id = intval($user->user_group_id);
                $user_Warehouse_id = intval($user->warehouse_id);
                $user_Location_id = intval($user->location_id); 
        }
        
        $total = 0;
        $records = null;

  
        $total = Users::select()
                 ->where('location_id','=',$user_Location_id);
        $source = Users::select()
                 ->where('location_id','=',$user_Location_id);
     
        if($fields) {
            $total->select($fields);
            $source->select($fields);
        }

        if($app->request->get('where')) {

            $where = $app->request->get('where');

            $total->whereRaw($app->db->raw($where));
            $source->whereRaw($app->db->raw($where));
        }

        if($app->request->get('filter') && $app->request->get('filter_fields')) {

            $filter = $app->request->get('filter');
            $filter_fields = $app->request->get('filter_fields');

            $ft_fields = explode(',', $filter_fields);

            $where_like = '';

            for($i=0;$i<count($ft_fields);$i++) {

                if($i===0) {
                    $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }
                else
                {
                    $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }

                if($i===(count($ft_fields)-1)) {
                    $where_like .= ')';
                }

            }

            $total->whereRaw($app->db->raw($where_like));
            $source->whereRaw($app->db->raw($where_like));
        }

        $total = $total->get()->count();

        if($limit>0)
            $source->take($limit)->skip($offset);

        if(!$orderby)
            $source = $source->orderByRaw('updated_at desc')->get();
        else
            $source = $source->orderByRaw($orderby)->get();

        $out = '';

        if($source) {
            $out = json_encode(
                array('records' => $source->toArray(), 'total' => $total)
            );
        }

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });
    
    $app->get('/users_po', $authenticateForRole('member'), function () use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $requests = (array) json_decode($app->request()->getBody());

        $res = $app->response();

        $limit = $app->request->get('limit')?$app->request->get('limit'):0;
        $offset = $app->request->get('offset')?$app->request->get('offset'):0;
        $fields = $app->request->get('fields')?$app->request->get('fields'):null;
        $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;
        
    //get user_id & user_group_id
        $user_group_id = null;
        $purchase_order = null;
        $client_id = $app->getCookie('client_id', false);
        $client_token = $app->getCookie('client_token', false);
        $user_app = User_apps::find($client_id);

        if($user_app) {
            $user = Users::find($user_app->user_id);
            if($user)
                $user_group_id = intval($user->user_group_id);
                $user_id = intval($user->id);
                $user_company_id = intval($user->company_id);
        }

        $total = 0;
        $records = null;

        $total = Users::select($app->db->raw('*'))
             ->where('user_group_id','>',10)
             ->where('user_group_id','<',14)
             ->where('company_id','=',$user_company_id)
             ->where('active','=',1)
             ->groupBy('name');
        $source =  Users::select($app->db->raw('*'))
             ->where('user_group_id','>',10)
             ->where('user_group_id','<',14)
             ->where('company_id','=',$user_company_id)
             ->where('active','=',1)
             ->groupBy('name');

        if($fields) {
            $total->select($fields);
            $source->select($fields);
        }
        

        if($app->request->get('where')) {

            $where = $app->request->get('where');

            $total->whereRaw($app->db->raw($where));
            $source->whereRaw($app->db->raw($where));
        }

        if($app->request->get('filter') && $app->request->get('filter_fields')) {

            $filter = $app->request->get('filter');
            $filter_fields = $app->request->get('filter_fields');

            $ft_fields = explode(',', $filter_fields);

            $where_like = '';

            for($i=0;$i<count($ft_fields);$i++) {

                if($i===0) {
                    $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }
                else
                {
                    $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }

                if($i===(count($ft_fields)-1)) {
                    $where_like .= ')';
                }

            }

            $total->whereRaw($app->db->raw($where_like));
            $source->whereRaw($app->db->raw($where_like));
        }

        $total = $total->count();

        if($limit>0)
            $source->take($limit)->skip($offset);

        if(!$orderby)
            $source = $source->orderByRaw('name asc')->get();
        else
            $source = $source->orderByRaw($orderby)->get();

        $out = '';

        if($source) {
            $out = json_encode(
                array('records' => $source->toArray(), 'total' => $total)
            );
        }

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });
    
    $app->get('/users_ap', $authenticateForRole('member'), function () use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $requests = (array) json_decode($app->request()->getBody());

        $res = $app->response();

        $limit = $app->request->get('limit')?$app->request->get('limit'):0;
        $offset = $app->request->get('offset')?$app->request->get('offset'):0;
        $fields = $app->request->get('fields')?$app->request->get('fields'):null;
        $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;
        
    //get user_id & user_group_id
        $user_group_id = null;
        $purchase_order = null;
        $client_id = $app->getCookie('client_id', false);
        $client_token = $app->getCookie('client_token', false);
        $user_app = User_apps::find($client_id);

        if($user_app) {
            $user = Users::find($user_app->user_id);
            if($user)
                $user_group_id = intval($user->user_group_id);
                $user_id = intval($user->id);
                $user_company_id = intval($user->company_id);
        }

        $total = 0;
        $records = null;

        $total = Users::select($app->db->raw('*'))
             ->where('user_group_id','=',17)
             ->where('company_id','=',$user_company_id)
             ->where('active','=',1);
        $source =  Users::select($app->db->raw('*'))
             ->where('user_group_id','=',17)
             ->where('company_id','=',$user_company_id)
             ->where('active','=',1);

        if($fields) {
            $total->select($fields);
            $source->select($fields);
        }
        

        if($app->request->get('where')) {

            $where = $app->request->get('where');

            $total->whereRaw($app->db->raw($where));
            $source->whereRaw($app->db->raw($where));
        }

        if($app->request->get('filter') && $app->request->get('filter_fields')) {

            $filter = $app->request->get('filter');
            $filter_fields = $app->request->get('filter_fields');

            $ft_fields = explode(',', $filter_fields);

            $where_like = '';

            for($i=0;$i<count($ft_fields);$i++) {

                if($i===0) {
                    $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }
                else
                {
                    $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }

                if($i===(count($ft_fields)-1)) {
                    $where_like .= ')';
                }

            }

            $total->whereRaw($app->db->raw($where_like));
            $source->whereRaw($app->db->raw($where_like));
        }

        $total = $total->count();

        if($limit>0)
            $source->take($limit)->skip($offset);

        if(!$orderby)
            $source = $source->orderByRaw('name asc')->get();
        else
            $source = $source->orderByRaw($orderby)->get();

        $out = '';

        if($source) {
            $out = json_encode(
                array('records' => $source->toArray(), 'total' => $total)
            );
        }

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });
    
    $app->get('/users_ar', $authenticateForRole('member'), function () use ($app) {

        $app->response->headers->set('Content-Type', 'application/json');

        $requests = (array) json_decode($app->request()->getBody());

        $res = $app->response();

        $limit = $app->request->get('limit')?$app->request->get('limit'):0;
        $offset = $app->request->get('offset')?$app->request->get('offset'):0;
        $fields = $app->request->get('fields')?$app->request->get('fields'):null;
        $orderby = $app->request->get('orderby')?$app->request->get('orderby'):null;
        
    //get user_id & user_group_id
        $user_group_id = null;
        $purchase_order = null;
        $client_id = $app->getCookie('client_id', false);
        $client_token = $app->getCookie('client_token', false);
        $user_app = User_apps::find($client_id);

        if($user_app) {
            $user = Users::find($user_app->user_id);
            if($user)
                $user_group_id = intval($user->user_group_id);
                $user_id = intval($user->id);
                $user_company_id = intval($user->company_id);
        }

        $total = 0;
        $records = null;

        $total = Users::select($app->db->raw('*'))
             ->where('user_group_id','=',19)
             ->where('company_id','=',$user_company_id)
             ->where('active','=',1);
        $source =  Users::select($app->db->raw('*'))
             ->where('user_group_id','=',19)
             ->where('company_id','=',$user_company_id)
             ->where('active','=',1);

        if($fields) {
            $total->select($fields);
            $source->select($fields);
        }
        

        if($app->request->get('where')) {

            $where = $app->request->get('where');

            $total->whereRaw($app->db->raw($where));
            $source->whereRaw($app->db->raw($where));
        }

        if($app->request->get('filter') && $app->request->get('filter_fields')) {

            $filter = $app->request->get('filter');
            $filter_fields = $app->request->get('filter_fields');

            $ft_fields = explode(',', $filter_fields);

            $where_like = '';

            for($i=0;$i<count($ft_fields);$i++) {

                if($i===0) {
                    $where_like .= '('. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }
                else
                {
                    $where_like .= ' OR '. preg_replace('/\s+/', '', $ft_fields[$i]) . ' LIKE \'%'. $filter. '%\' ';
                }

                if($i===(count($ft_fields)-1)) {
                    $where_like .= ')';
                }

            }

            $total->whereRaw($app->db->raw($where_like));
            $source->whereRaw($app->db->raw($where_like));
        }

        $total = $total->count();

        if($limit>0)
            $source->take($limit)->skip($offset);

        if(!$orderby)
            $source = $source->orderByRaw('name asc')->get();
        else
            $source = $source->orderByRaw($orderby)->get();

        // debug sql
        //$queries = $app->db->getQueryLog();
        //var_dump( $queries);die();
        //$last_query = end($queries);
        //var_dump( $last_query);die();

        $out = '';

        if($source) {
            $out = json_encode(
                array('records' => $source->toArray(), 'total' => $total)
            );
        }

        $res['Content-Type'] = 'application/json';
        $res->body($out);

    });