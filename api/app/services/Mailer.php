<?php namespace RLIMS\Services;

DEFINE('WEBSITE_URL', 'http://localhost/codhris');

class Mailer {

	/**
	 * The application instance.
	 */
	protected $app;

	/**
	 * The failures
	 */
	protected $failures;

	/**
	 *
	 */
	public function __construct($app)
	{
		$this->app = $app;
	}

	public function getFailures() {
		return $this->failures;
	}

	/**
	 * send
	 *
	 * @return void
	 */
	public function send($subject, $from = array(), $to = array(), $message, $replacements = array()) {

		$this->failures = NULL;

		$mailUsername = 'rces@rialachas.com';
		$mailPassword = 'Rialachas123';

		// Create the Transport
		$transport = \Swift_SmtpTransport::newInstance('smtp.rialachas.com', 465, 'ssl')
		->setUsername($mailUsername)
		->setPassword($mailPassword);

		// Create the Mailer using your created Transport
		$mailer = \Swift_Mailer::newInstance($transport);

		$decorator = new \Swift_Plugins_DecoratorPlugin($replacements);

		$mailer->registerPlugin($decorator);

		// Create the swift_message
		$swift_message = \Swift_Message::newInstance()

		  // Give the swift_message a subject
		->setSubject($subject)

		  // Set the From address with an associative array
		->setFrom($from)

		  // Set the To addresses with an associative array
		->setTo($to)

		  // Give it a body
		->setBody($message, 'text/html', 'iso-8859-2')

		;


		// Send the swift_message
		if (!$mailer->send($swift_message, $failures)) {

			$this->failures = $failures;

			return FALSE;
		}

		return TRUE;

		/*
		Failures:
		Array (
		  0 => receiver@bad-domain.org,
		  1 => other-receiver@bad-domain.org
		)
		*/
	}

	public function sendEmailInpatient($userEmail, $userName, $transfer) {

		$from 			= array('adminhr@pangansari.co.id' => 'Dana Rawat Inap');
		$sendTo 		= array($userEmail => $userName);

		$replacements 	= array();
		$replacements[$userEmail] = array(
			'{username}'=>$userName,
			'{transfer}'=>$transfer
		);
		
		$message_tpl = 'Dear,';
		$message_tpl .= '<br>Bpk/Ibu. {username}';
		$message_tpl .= '<br><br>Sehubungan dengan pencairan dana atas kwitansi yang Anda berikan, telah diproses oleh Admin PT. Pangansari Utama (PSU),</b> Kami berharap Bapak/Ibu untuk lebih menjaga kesehatan diri sendiri & keluarga.';
		$message_tpl .= '<br><br>Rincian:';
		
		$message_tpl .= '<br>Jumlah yang ditransfer : Rp. {transfer}';
		
		$message_tpl .= '<br><br>Best regards,';
		$message_tpl .= '<br><br>Head Office:'; 
		$message_tpl .= '<br>Jl. Poncol Raya No. 24 Ciracas, Jakarta Timur 13740 - Indonesia,';
		$message_tpl .= '<br>Telephone: (021) 871 7870 (general number),';
		$message_tpl .= '<br>Telephone: (021) 870 2004 (direct marketing),';
		$message_tpl .= '<br>Fax: (021) 871 7869 (general number)';

		return $this->send('Pencarian Dana Rawat Inap PT. PANGANSARI UTAMA', $from, $sendTo, $message_tpl, $replacements);
	}
	
	public function sendEmailOutpatient($userEmail, $userName, $payment, $plafon, $saveplafon) {

		$from 			= array('adminhr@pangansari.co.id' => 'Dana Rawat Jalan');
		$sendTo 		= array($userEmail => $userName);

		$replacements 	= array();
		$replacements[$userEmail] = array(
			'{username}'=>$userName,
			'{payment}'=>$payment,
			'{plafon}'=>$plafon,
			'{saveplafon}'=>$saveplafon
		);
		
		$message_tpl = 'Dear,';
		$message_tpl .= '<br>Bpk/Ibu. {username}';
		$message_tpl .= '<br><br>Sehubungan dengan pencairan dana atas kwitansi yang Anda berikan, telah diproses oleh Admin PT. Pangansari Utama (PSU),</b> Kami berharap Bapak/Ibu untuk lebih menjaga kesehatan diri sendiri & keluarga.';
		$message_tpl .= '<br><br>Rincian:';
		
		$message_tpl .= '<br>Plafon      : Rp. {saveplafon}';
		$message_tpl .= '<br>Pembayaran  : Rp. {payment}';
		$message_tpl .= '<br>Sisa Plafon : Rp. {plafon}';
		
		$message_tpl .= '<br><br>Best regards,';
		$message_tpl .= '<br><br>Head Office:'; 
		$message_tpl .= '<br>Jl. Poncol Raya No. 24 Ciracas, Jakarta Timur 13740 - Indonesia,';
		$message_tpl .= '<br>Telephone: (021) 871 7870 (general number),';
		$message_tpl .= '<br>Telephone: (021) 870 2004 (direct marketing),';
		$message_tpl .= '<br>Fax: (021) 871 7869 (general number)';

		return $this->send('Pencarian Dana Rawat Jalan PT. PANGANSARI UTAMA', $from, $sendTo, $message_tpl, $replacements);
	}

	public function sendEmailMcu($userEmail, $userName, $pdf_location) {

		$from 			= array('adminhr@pangansari.co.id' => 'Surat Keterangan MCU');
		$sendTo 		= array($userEmail => $userName);

		$replacements 	= array();
		$replacements[$userEmail] = array(
			'{username}'=>$userName
		);
		
		$message_tpl = 'Dear,';
		$message_tpl .= '<br>Bpk/Ibu. {username}';
		$message_tpl .= '<br><br>Sehubungan dengan MCU, telah diproses oleh Admin PT. Pangansari Utama (PSU),</b> Kami lampirkan file pendukung';
		
		$message_tpl .= '<br><br>Best regards,';
		$message_tpl .= '<br><br>Head Office:'; 
		$message_tpl .= '<br>Jl. Poncol Raya No. 24 Ciracas, Jakarta Timur 13740 - Indonesia,';
		$message_tpl .= '<br>Telephone: (021) 871 7870 (general number),';
		$message_tpl .= '<br>Telephone: (021) 870 2004 (direct marketing),';
		$message_tpl .= '<br>Fax: (021) 871 7869 (general number)';

		$this->failures = NULL;

		$mailUsername = 'rces@rialachas.com';
		$mailPassword = 'Rialachas123';

		// Create the Transport
		$transport = \Swift_SmtpTransport::newInstance('smtp.rialachas.com', 465, 'ssl')
		->setUsername($mailUsername)
		->setPassword($mailPassword);

		// Create the Mailer using your created Transport
		$mailer = \Swift_Mailer::newInstance($transport);

		$decorator = new \Swift_Plugins_DecoratorPlugin($replacements);

		$mailer->registerPlugin($decorator);

		// Create the swift_message
		$swift_message = \Swift_Message::newInstance()

		  // Give the swift_message a subject
		->setSubject('Surat Keterangan MCU')

		  // Set the From address with an associative array
		->setFrom($from)

		  // Set the To addresses with an associative array
		->setTo($sendTo)

		  // Give it a body
		->setBody($message_tpl, 'text/html', 'iso-8859-2')

		->attach(\Swift_Attachment::fromPath($pdf_location))

		;


		// Send the swift_message
		if (!$mailer->send($swift_message, $failures)) {

			$this->failures = $failures;

			return FALSE;
		}

		return TRUE;

	}

	public function sendEmailMcuResult($userEmail, $userName, $data, $data_result) {

		if($data_result[0]->status == 1){
			$status = "Sehat";
			$message = "";
		} else {
			$status = "Tidak Sehat";
			$message = "<br><br>Karna hasil menyatakan tidak sehat, maka harap lakukan MCU ulang";
		}

		$from 			= array('adminhr@pangansari.co.id' => 'Hasil Tes MCU');
		$sendTo 		= array($userEmail => $userName);

		$replacements 	= array();
		$replacements[$userEmail] = array(
			'{username}'=>$userName,
			'{name}'=>$data->name,
			'{date_birthday}'=>date("d M Y", strtotime($data->date_birthday)),
			'{location}'=>$data->location,
			'{hospital}'=>$data->hospital,
			'{date_result}'=>date("d M Y", strtotime($data_result[0]->date_result)),
			'{result}'=>$data_result[0]->result,
			'{note}'=>$data_result[0]->note,
			'{status}'=>$status,
			'{message}'=>$message
		);
		
		$message_tpl = 'Dear,';
		$message_tpl .= '<br>Bpk/Ibu. {username}';
		$message_tpl .= '<br><br>Sehubungan dengan permintaan anda untuk melakukan MCU, telah terbit hasilnya, sbb:';
		$message_tpl .= '<br><br>Rincian:';
		
		$message_tpl .= '<br>Nama 					: {name}';
		$message_tpl .= '<br>Tanggal Lahir  		: {date_birthday}';
		$message_tpl .= '<br>Lokasi  				: {location}';
		$message_tpl .= '<br>Rumah Sakit/Klinik 	: {hospital}';
		$message_tpl .= '<br>Tanggal Hasil 			: {date_result}';
		$message_tpl .= '<br>Status 				: {status}';
		$message_tpl .= '<br>Hasil 					: {result}';
		$message_tpl .= '<br>Catatan 				: {note}';

		$message_tpl .= '{message}';
		
		$message_tpl .= '<br><br>Best regards,';
		$message_tpl .= '<br><br>Head Office:'; 
		$message_tpl .= '<br>Jl. Poncol Raya No. 24 Ciracas, Jakarta Timur 13740 - Indonesia,';
		$message_tpl .= '<br>Telephone: (021) 871 7870 (general number),';
		$message_tpl .= '<br>Telephone: (021) 870 2004 (direct marketing),';
		$message_tpl .= '<br>Fax: (021) 871 7869 (general number)';

		return $this->send('Hasil Tes MCU', $from, $sendTo, $message_tpl, $replacements);
	}

}