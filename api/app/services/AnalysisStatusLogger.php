<?php namespace Rlims\Services;

class AnalysisStatusLogger {

	/**
	 * The application instance.
	 */
	protected $app;

	/**
	 *
	 */
	public function __construct($app)
	{
		$this->app = $app;
	}

	/**
	 * Log
	 *
	 * @return void
	 */
	public function log($sample_id, $analysis_id, $user_id, $analysis_status_id) {

        // validate user
        $user = \Users::find($user_id);

        if($user) {
        	
	        $log = new \Analysis_status_logs();

	        $status_date 				= date('Y-m-d');

	        $log->status_date        	= $status_date;
	        $log->sample_id        		= $sample_id;
	        $log->analysis_id       	= $analysis_id;
	        $log->user_id       		= $user->id;
	        $log->analysis_status_id    = $analysis_status_id;

	        $log->save();

			/*var_dump('AnalysisStatusLogger::log'); 
			die();*/

        }
	}
}