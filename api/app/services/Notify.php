<?php namespace Rlims\Services;

class Notifier {

	/**
	 * The application instance.
	 */
	protected $app;

	/**
	 *
	 */
	public function __construct($app)
	{
		$this->app = $app;
	}

	/**
	 * Process		: Sampel dikirim ke Bidang
	 * Created by 	: Staff TU
	 * Notify for 	: Ka Bidang, GROUP_ID=5
	 * Message 		: Ada sampel baru: SAMPLE_ID yang harus diuji oleh analis
	 */

	public function importDataSalesorderall($filepath, $company_id, $user_id, $date) {

		$sheets = array(
			"sheet1"
		);

		$objReader = \PHPExcel_IOFactory::createReaderForFile($filepath);

		if(!$objReader->canRead($filepath)) {
			$res = $this->app->response();
			$res['Content-Type'] = 'application/json';
			$res->body(json_encode(array(
				'error' => 'File excel tidak valid !!!'
			)));
			$this->app->response()->status(401);
			$this->app->stop();
		}

		$objReader->setReadDataOnly(true);
		$objReader->setLoadSheetsOnly($sheets);
		$objExcel = $objReader->load($filepath);

		try {
			$this->app->db->getPdo()->beginTransaction();

			$wsCertifications = $objExcel->getSheetByName('sheet1');

			if(!$wsCertifications) {
				$this->app->db->getPdo()->rollBack();

				$res = $this->app->response();
				$res['Content-Type'] = 'application/json';
				$res->body(json_encode(array(
					'error' => 'Tidak ditemukan nama sheet \'sheet1\' !!!'
				)));
				$this->app->response()->status(400);
				$this->app->stop();
			}

			$wsCertifications1 = $wsCertifications->toArray(null,false,false,false,false);
			for ($j=1; $j < count($wsCertifications1); $j++) {
				$data1 = $wsCertifications1[$j];

				$getsalesorderall_detail = \Salesorderalls_detail::select()
				->where('order_no', $data1[0])
				->where('package_no', $data1[1])
				->get();
				$getsalesorderall_detail = $getsalesorderall_detail->toArray();

				if(count($getsalesorderall_detail) > 0){
					$this->app->db->getPdo()->rollBack();

					$res = $this->app->response();
					$res['Content-Type'] = 'application/json';
					$res->body(json_encode(array(
						'error' => 'Nomor order ('.$data1[0].') sudah pernah di input mohon cek data yang akan diupload !!!'
					)));
					$this->app->response()->status(400);
					$this->app->stop();
				}
			}

			$soall_id = '';
			if($wsCertifications) {

				$wsCertifications = $wsCertifications->toArray(null,false,false,false,false);

				$salesorderall = new \Salesorderalls;

				$salesorderall->id               = \Salesorderalls::getNextSalesorderallCode();
				$salesorderall->company_id       = $company_id;
				$salesorderall->user_id          = $user_id;
				$salesorderall->date             = $date;
				$salesorderall->is_active        = 0;

				$soall_id = $salesorderall->id;

				$pricetotal = 0;
				
				for ($i=1; $i < count($wsCertifications); $i++) {
					$data = $wsCertifications[$i];

					$salesorderall_detail = new \Salesorderalls_detail;
					$salesorderall_detail->id			= \Salesorderalls_detail::getNextSalesorderalldetailCode();
					$salesorderall_detail->salesorderall_id				= $soall_id;
					$salesorderall_detail->order_no						= $data[0];
					$salesorderall_detail->package_no 					= $data[1];
					if(isset($data[2]))
						$salesorderall_detail->marketplace 				= $data[2];
					if(isset($data[3]))
						$salesorderall_detail->store 					= $data[3];
					if(isset($data[4]))
						$salesorderall_detail->order_status				= $data[4];
					if(isset($data[5]))
						$salesorderall_detail->marketplace_status		= $data[5];
					if(isset($data[6]))
						$salesorderall_detail->order_created_time		= $data[6];
					if(isset($data[7]))
						$salesorderall_detail->order_paid_time			= $data[7];
					if(isset($data[8]))
						$salesorderall_detail->order_shipped_time		= $data[8];
					if(isset($data[9]))
						$salesorderall_detail->payment_method			= $data[9];
					if(isset($data[10]))
						$salesorderall_detail->remark_from_buyer		= $data[10];
					if(isset($data[11]))
						$salesorderall_detail->note_for_cs				= $data[11];
					if(isset($data[12]))
						$salesorderall_detail->note_for_pick			= $data[12];
					if(isset($data[13]))
						$salesorderall_detail->product_name				= $data[13];
					if(isset($data[14]))
						$salesorderall_detail->sku 						= $data[14];
					if(isset($data[15]))
						$salesorderall_detail->variation_name			= $data[15];
					if(isset($data[16]))
						$salesorderall_detail->price 					= $data[16];
					if(isset($data[17]))
						$salesorderall_detail->quantity					= $data[17];
					if(isset($data[18]))
						$salesorderall_detail->product_subtotal			= $data[18];
					if(isset($data[19]))
						$salesorderall_detail->order_total				= $data[19];
					if(isset($data[20]))
						$salesorderall_detail->voucher					= $data[20];
					if(isset($data[21]))
						$salesorderall_detail->management_fee			= $data[21];
					if(isset($data[22]))
						$salesorderall_detail->transaction_fee			= $data[22];
					if(isset($data[23]))
						$salesorderall_detail->currency					= $data[23];
					if(isset($data[24]))
						$salesorderall_detail->merchant_sku_name		= $data[24];
					if(isset($data[25]))
						$salesorderall_detail->merchant_sku_name		= $data[25];
					if(isset($data[26]))
						$salesorderall_detail->shipping_warehouse		= $data[26];
					if(isset($data[27]))
						$salesorderall_detail->shelf 					= $data[27];
					if(isset($data[28]))
						$salesorderall_detail->buyer_designed_logistics	= $data[28];
					if(isset($data[29]))
						$salesorderall_detail->shipping_option			= $data[29];
					if(isset($data[30]))
						$salesorderall_detail->shipment_method			= $data[30];
					if(isset($data[31]))
						$salesorderall_detail->tracking_number			= $data[31];
					if(isset($data[32]))
						$salesorderall_detail->shipping_fee				= $data[32];
					if(isset($data[33]))
						$salesorderall_detail->username_buyer			= $data[33];
					if(isset($data[34]))
						$salesorderall_detail->receiver_name			= $data[34];
					if(isset($data[35]))
						$salesorderall_detail->phone_number				= $data[35];
					if(isset($data[36]))
						$salesorderall_detail->delivery_address			= $data[36];
					if(isset($data[37]))
						$salesorderall_detail->town						= $data[37];
					if(isset($data[38]))
						$salesorderall_detail->district_area			= $data[38];
					if(isset($data[39]))
						$salesorderall_detail->city 					= $data[39];
					if(isset($data[40]))
						$salesorderall_detail->province_state			= $data[40];
					if(isset($data[41]))
						$salesorderall_detail->country 					= $data[41];
					if(isset($data[42]))
						$salesorderall_detail->post_code				= $data[42];

					$salesorderall_detail->is_active      				= 0;

					$pricetotal = $pricetotal + (intval($data[16]) * intval($data[17]));

					$salesorderall_detail->save();
				}

				$salesorderall->pricetotal                				= $pricetotal;
				
				$salesorderall->save();

			}

			$this->app->db->getPdo()->commit();

		} catch (\PDOException $e) {

			$this->app->db->getPdo()->rollBack();

			$out = json_encode(array('error' => $e));

			$res = $this->app->response();
			$res['Content-Type'] = 'application/json';
			$res->body($out);
			$res->status(400);
			$this->app->stop();                        
		}

	}

	public function importDataPaymentlazada($filepath, $company_id, $user_id, $date) {

		$sheets = array(
			"sheet1"
		);

		$objReader = \PHPExcel_IOFactory::createReaderForFile($filepath);

		if(!$objReader->canRead($filepath)) {
			$res = $this->app->response();
			$res['Content-Type'] = 'application/json';
			$res->body(json_encode(array(
				'error' => 'File excel tidak valid !!!'
			)));
			$this->app->response()->status(401);
			$this->app->stop();
		}

		$objReader->setReadDataOnly(true);
		$objReader->setLoadSheetsOnly($sheets);
		$objExcel = $objReader->load($filepath);

		try {
			$this->app->db->getPdo()->beginTransaction();

			$wsCertifications = $objExcel->getSheetByName('sheet1');

			if(!$wsCertifications) {
				$this->app->db->getPdo()->rollBack();

				$res = $this->app->response();
				$res['Content-Type'] = 'application/json';
				$res->body(json_encode(array(
					'error' => 'Tidak ditemukan nama sheet \'sheet1\' !!!'
				)));
				$this->app->response()->status(400);
				$this->app->stop();
			}

			$wsCertifications1 = $wsCertifications->toArray(null,false,false,false,false);
			for ($j=1; $j < count($wsCertifications1); $j++) {
				$data1 = $wsCertifications1[$j];

				$getpaymentlazada_detail = \Paymentlazadas_detail::select()
				->where('order_no', $data1[13])
				->get();
				$getpaymentlazada_detail = $getpaymentlazada_detail->toArray();

				if(count($getpaymentlazada_detail) > 0){
					$this->app->db->getPdo()->rollBack();

					$res = $this->app->response();
					$res['Content-Type'] = 'application/json';
					$res->body(json_encode(array(
						'error' => 'Nomor order ('.$data1[13].') sudah pernah di input mohon cek data yang akan diupload !!!'
					)));
					$this->app->response()->status(400);
					$this->app->stop();
				}
			}

			$paylazid = '';
			if($wsCertifications) {

				$wsCertifications = $wsCertifications->toArray(null,false,false,false,false);

				$paymentlazada = new \Paymentlazadas;

				$paymentlazada->id               = \Paymentlazadas::getNextPaymentlazadaCode();
				$paymentlazada->company_id       = $company_id;
				$paymentlazada->user_id          = $user_id;
				$paymentlazada->date             = $date;
				$paymentlazada->is_active        = 0;

				$paylazid = $paymentlazada->id;
				
				for ($i=1; $i < count($wsCertifications); $i++) {
					$data = $wsCertifications[$i];

					$paymentlazada_detail = new \Paymentlazadas_detail;
					$paymentlazada_detail->id			= \Paymentlazadas_detail::getNextPaymentlazadadetailCode();
					$paymentlazada_detail->paymentlazada_id				= $paylazid;
					
					$paymentlazada_detail->transaction_date				= $data[0];
					if(isset($data[1]))
					$paymentlazada_detail->transaction_type				= $data[1];
					if(isset($data[2]))
					$paymentlazada_detail->fee_name 					= $data[2];
					if(isset($data[3]))
					$paymentlazada_detail->transaction_number 			= $data[3];
					if(isset($data[4]))
					$paymentlazada_detail->product_name 				= $data[4];
					if(isset($data[5]))
					$paymentlazada_detail->seller_sku 					= $data[5];
					if(isset($data[6]))
					$paymentlazada_detail->lazada_sku 					= $data[6];
					if(isset($data[7]))
					$paymentlazada_detail->amount 						= $data[7];
					if(isset($data[8]))
					$paymentlazada_detail->vat_in_amount 				= $data[8];
					if(isset($data[9]))
					$paymentlazada_detail->wht_amount 					= $data[9];
					if(isset($data[10]))
					$paymentlazada_detail->wht_included_in_amount 		= $data[10];
					if(isset($data[11]))
					$paymentlazada_detail->statement 					= $data[11];
					if(isset($data[12]))
					$paymentlazada_detail->paid_status 					= $data[12];
					if(isset($data[13]))
					$paymentlazada_detail->order_no 					= $data[13];
					if(isset($data[14]))
					$paymentlazada_detail->order_item_no 				= $data[14];
					if(isset($data[15]))
					$paymentlazada_detail->order_item_status 			= $data[15];
					if(isset($data[16]))
					$paymentlazada_detail->shipping_provider 			= $data[16];
					if(isset($data[17]))
					$paymentlazada_detail->shipping_speed 				= $data[17];
					if(isset($data[18]))
					$paymentlazada_detail->shipment_type 				= $data[18];
					if(isset($data[19]))
					$paymentlazada_detail->reference 					= $data[19];
					if(isset($data[20]))
					$paymentlazada_detail->comment 						= $data[20];
					if(isset($data[21]))
					$paymentlazada_detail->paymentrefid 				= $data[21];

					$paymentlazada_detail->is_active      				= 0;


					$paymentlazada_detail->save();
				}

				
				$paymentlazada->save();

			}

			$this->app->db->getPdo()->commit();

		} catch (\PDOException $e) {

			$this->app->db->getPdo()->rollBack();

			$out = json_encode(array('error' => $e));

			$res = $this->app->response();
			$res['Content-Type'] = 'application/json';
			$res->body($out);
			$res->status(400);
			$this->app->stop();                        
		}

	}
	
}