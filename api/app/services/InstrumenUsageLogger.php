<?php namespace Rlims\Services;

class InstrumentUsageLogger {

	/**
	 * The application instance.
	 */
	protected $app;

	/**
	 *
	 */
	public function __construct($app)
	{
		$this->app = $app;
	}

	/**
	 * Log
	 *
	 * @return void
	 */
	public function log($sample_id, $instrument_id) {
        	
        $log = new \Instrument_usage_logs();

        $date_used 				= date('Y-m-d H:i:s');

        $log->date_used        	= $date_used;
        $log->sample_id        	= $sample_id;
        $log->instrument_id     = $instrument_id;

        $log->save();

		/*var_dump('InstrumentUsageLogger::log'); 
		die();*/

	}
}