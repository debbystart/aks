<?php namespace Rlims\Services;

class MimeType {

	/**
	 * The application instance.
	 */
	protected $app;

	/**
	 * The application instance.
	 */
	protected $mimePath;

	/**
	 *
	 */
	public function __construct($app)
	{
		$this->app = $app;
		$this->mimePath = dirname(__FILE__) . './../../../media';
	}

	protected function getLocalMimeTypes() {

		$mimePath = $this->mimePath;
		$regex = "/([\w\+\-\.\/]+)\t+([\w\s]+)/i"; 
		$lines = file("$mimePath/mime.types", FILE_IGNORE_NEW_LINES); 
		foreach($lines as $line) { 
			if (substr($line, 0, 1) == '#') continue; // skip comments 
			if (!preg_match($regex, $line, $matches)) continue; // skip mime types w/o any extensions 
			$mime = $matches[1]; 
			$extensions = explode(" ", $matches[2]); 
			foreach($extensions as $ext) $mimeArray[trim($ext)] = $mime; 
		} 
		return ($mimeArray); 
	}

	/**
	 * get
	 *
	 * @return void
	 */
	public function getMimeFromFile($filename) {

		$fileext = substr(strrchr($filename, '.'), 1); 
		if (empty($fileext)) return (false); 

		$mimePath = $this->mimePath;
		//echo "$mimePath/mime.types";

		$regex = "/^([\w\+\-\.\/]+)\s+(\w+\s)*($fileext\s)/i"; 

		$lines = file("$mimePath/mime.types"); 

		foreach($lines as $line) { 
			if (substr($line, 0, 1) == '#') continue; // skip comments 
				$line = rtrim($line) . " "; 
			if (!preg_match($regex, $line, $matches)) continue; // no match to the extension 
				return ($matches[1]); 
		}

		return (false); // no match at all 
	}

	/**
	 * get extension
	 *
	 * @return void
	 */
	public function getExtensionFromMimeType($mimetype) {
		
		$localMimeTypes = $this->getLocalMimeTypes();
		foreach($localMimeTypes as $key => $value) {
			if($value===$mimetype)
				return $key;
		}

		return (false); // no match at all         	
	}
}