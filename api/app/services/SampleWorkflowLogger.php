<?php namespace Rlims\Services;

class SampleWorkflowLogger {

	/**
	 * The application instance.
	 */
	protected $app;

	/**
	 *
	 */
	public function __construct($app)
	{
		$this->app = $app;
	}

	/**
	 * Log
	 *
	 * @return void
	 */
	public function log($sample_id, $sample_workflow_status_id, $sample_date_received, $user_id) {

        // validate user
        $user = \Users::find($user_id);

        if($user) {
        	
	        $log = new \Sample_workflow_logs();

	        $status_date 				= date('Y-m-d');

            $log->sample_id                 = $sample_id;
            $log->workflow_status_id        = $sample_workflow_status_id;
            $log->status_date               = $sample_date_received;
            $log->is_active                 = TRUE;
            $log->user_id               	= $user_id;
            
            $log->save();

			/*var_dump('SampleWorkflowLogger::log'); 
			die();*/

        }
	}
}