<?php namespace Rlims\Services;

class PDFGenerator {

	/**
	 * The application instance.
	 */
	protected $app;

	/**
	 *
	 */
	public function __construct($app)
	{
		$this->app = $app;
	}

	/**
	 * Make
     * PDFGenerator Test
     *   $html = '<img src="http://localhost/rialachas-lims/api/' . $app->DNS1D->getBarcodePNGPath("4445645656", "C128",3,33) . '" alt="barcode"   />';
     *   $app->PDFGenerator->make($html);
     *   exit(0);
     *
	 * @return void
	 */
	public function make($html) {



		$dompdf = new \DOMPDF();
		$dompdf->load_html(stripslashes($html));
		$dompdf->render();

		$dompdf->stream("dompdf_out.pdf", array("Attachment" => false));

		exit(0);

	}

	public function save($html, $id, $approved_by, $received_by, $finance_by) {



		$dompdf = new \DOMPDF();
		$dompdf->load_html(stripslashes($html));
		$dompdf->render();

		$pdf = $dompdf->output();
		$name = "aks" . $id . $approved_by . $received_by . $finance_by . ".pdf";
		// print_r(getcwd());exit;
		// print_r($_SERVER['PHP_SELF']);exit;
		$file_location = $_SERVER['DOCUMENT_ROOT']."/aks/media/".$name;
		file_put_contents($file_location,$pdf);

		return($file_location);
		// exit(0);

	}

  public function bulan($bln){
    $b = date('m', strtotime($bln));
    switch($b){
      case '01':
      $bulan = "Januari";
      break;
      case '02':
      $bulan = "Februari";
      break;
      case '03':
      $bulan = "Maret";
      break;
      case '04':
      $bulan = "April";
      break;
      case '05':
      $bulan = "Mei";
      break;
      case '06':
      $bulan = "Juni";
      break;
      case '07':
      $bulan = "Juli";
      break;
      case '08':
      $bulan = "Agustus";
      break;
      case '09':
      $bulan = "September";
      break;
      case '10':
      $bulan = "Oktober";
      break;
      case '11':
      $bulan = "November";
      break;
      case '12':
      $bulan = "Desember";
      break;
      default:
      $bulan = "";
      break;
    }

    return $bulan;
  }

  public function printPO() {

    $id    = $this->app->request->get('id');
    $purchaseorder = \Purchaseorders::find($id);

    $today = date("d M Y");

    $html   =  '<html>
    <title>Purchaseorder</title>
    <head>
    <style>
    @page { margin: 230px 30px 0px 50px; }
            #header { position: fixed; left: 6px; top: -200px; right: 16px; text-align: center; }
            #content  { margin: 10px 16px 180px 5px; }
            #footer { position: fixed; left: 10px; bottom: 200px; right: 0px;}
    body {
      background-color: transparent;
      color: black;
      font-family: "verdana", "sans-serif";
      margin: 0px;
      padding-top: -120px;
      padding-bottom: 20px;
      font-size: 9px;
    }
    </style>


    </head>
    <body>
    <script type="text/php">
    if ( isset($pdf) ) {
      $font = Font_Metrics::get_font("Arial");
      $pdf->page_text(780, 560, "{PAGE_NUM} of {PAGE_COUNT}", $font, 8, array(0,0,0));
    }
    </script>
    <div id="header">';

    $header   = require $this->app->basePath . '/app/templates/header_purchaseorder.php';

    $html .= $header;

    $html .= '</div><br/>

    <div id="content">';

    $html .= '<table class="table table-bordered" width="100%">
    <thead style="border-style:solid;border-width:1px 0px 1px 0px;font-size: 11px;">
    <tr>
    <th style="border-style:solid;border-width:0px 1px 0px 1px;"><b>No</b></th>
    <th style="border-style:solid;border-width:0px 1px 0px 0px;"><b>ID Item</b></th>
    <th style="border-style:solid;border-width:0px 1px 0px 0px;"><b>Nama Item</b></th>
    <th style="border-style:solid;border-width:0px 1px 0px 0px;"><b>Merek</b></th>
    <th style="border-style:solid;border-width:0px 1px 0px 0px;"><b>Unit</b></th>
    <th style="border-style:solid;border-width:0px 1px 0px 0px;"><b>Harga</b></th>
    <th style="border-style:solid;border-width:0px 1px 0px 0px;"><b>Qty</b></th>
    <th style="border-style:solid;border-width:0px 1px 0px 0px;"><b>Ongkir/Disc</b></th>
    <th style="border-style:solid;border-width:0px 1px 0px 0px;"><b>Bruto</b></th>
    <th style="border-style:solid;border-width:0px 1px 0px 0px;"><b>Total</b></th>
    </tr>
    </thead>
    <tbody style="font-size: 10px;">';

    $q = \Purchaseorders::select($this->app->db->raw('data_purchaseorder_detail.*, param_item.brand_id'))
    ->join('data_purchaseorder_detail','data_purchaseorder.id','=','data_purchaseorder_detail.purchaseorder_id')
    ->join('param_item','param_item.id','=','data_purchaseorder_detail.item_id')
    ->where('data_purchaseorder.id', $id)
    ->orderby('data_purchaseorder_detail.item_name')
    ->get();

    $datas = $q->toArray();

    for ($i=0; $i < count($datas) ; $i++){
      $d = $datas[$i];
      $no = $i+1;

      $brand = \Brands::find($d['brand_id']);

      $html .='
      <tr>
      <td width="2%" valign="top" style="text-align: center;border-style:solid;border-width:0px 1px 1px 1px">'.$no.'</td>
      <td width="5%" valign="top" style="text-align: center;border-style:solid;border-width:0px 1px 1px 0px">'.$d['item_id'].'</td>
      <td width="20%" valign="top" style="text-align: left;border-style:solid;border-width:0px 1px 1px 0px">'.$d['item_name'].'</td>
      <td width="5%" valign="top" style="text-align: center;border-style:solid;border-width:0px 1px 1px 0px">'.$brand->name.'</td>
      <td width="3%" valign="top" style="text-align: center;border-style:solid;border-width:0px 1px 1px 0px">'.$d['item_unit'].'</td>
      <td width="4%" valign="top" style="text-align: right;border-style:solid;border-width:0px 1px 1px 0px">'.number_format($d['price']).'</td>
      <td width="3%" valign="top" style="text-align: center;border-style:solid;border-width:0px 1px 1px 0px">'.number_format($d['qty']).'</td>
      <td width="5%" valign="top" style="text-align: right;border-style:solid;border-width:0px 1px 1px 0px">'.number_format($d['ongkir']).'</td>
      <td width="5%" valign="top" style="text-align: right;border-style:solid;border-width:0px 1px 1px 0px">'.number_format($d['bruto']).'</td>
      <td width="5%" valign="top" style="text-align: right;border-style:solid;border-width:0px 1px 1px 0px">'.number_format($d['total_price']).'</td>
      </tr>
      ';
    }

    $html .='
    <tr bgcolor="#46c37b">
    <td valign="top" style="text-align: center;border-style:solid;border-width:0px 1px 1px 1px" colspan="6">TOTAL</td>
    <td valign="top" style="text-align: center;border-style:solid;border-width:0px 1px 1px 0px">'.number_format($purchaseorder->qty).'</td>
    <td valign="top" style="text-align: right;border-style:solid;border-width:0px 1px 1px 0px">Rp. '.number_format($purchaseorder->ongkir).'</td>
    <td valign="top" style="text-align: right;border-style:solid;border-width:0px 1px 1px 0px">Rp. '.number_format($purchaseorder->bruto).'</td>
    <td valign="top" style="text-align: right;border-style:solid;border-width:0px 1px 1px 0px">Rp. '.number_format($purchaseorder->total).'</td>
    </tr>
    ';

    $html .= '</tbody><br><br>
    </table>';

    $html .= '<div id="footer" style="position:fixed;bottom:210px;">
    <table width="100%" style="font-size: 11px">
    <tbody>
    <tr style="text-align: center;">
    <td>Indramayu, '.$today.'<br>Disetujui Oleh,</td>
    </tr>
    <tr style="text-align: center;">
    <td>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <span>
    _____________________
    <span>
    </td>
    </tr>
    </tbody>
    </table>
    </div>';

    $html  .= '</div>';
    $end   = require $this->app->basePath . '/app/templates/end.php';
    $html  .= $end;

        //echo  $html;
    $dompdf = new \DOMPDF();
    $dompdf->load_html(stripslashes($html));
    $dompdf->set_paper('A4', 'landscape');
    $dompdf->render();

    $dompdf->stream("purchaseorder.pdf", array("Attachment" => false));

    exit(0);

  }

  public function printSO() {

    $id    = $this->app->request->get('id');
    $salesorder = \Salesorders::find($id);
    $company = \Companys::find($salesorder->company_id);

    $today = date("d M Y");

    $html   =  '<html>
    <title>Salesorder</title>
    <head>
    <style>
    @page { margin: 230px 30px 0px 50px; }
            #header { position: fixed; left: 6px; top: -200px; right: 16px; text-align: center; }
            #content  { margin: 10px 16px 180px 5px; }
            #footer { position: fixed; left: 10px; bottom: 200px; right: 0px;}
    body {
      background-color: transparent;
      color: black;
      font-family: "verdana", "sans-serif";
      margin: 0px;
      padding-top: -150px;
      padding-bottom: 20px;
      font-size: 9px;
    }
    </style>


    </head>
    <body>
    <script type="text/php">
    if ( isset($pdf) ) {
      $font = Font_Metrics::get_font("Arial");
      $pdf->page_text(780, 560, "{PAGE_NUM} of {PAGE_COUNT}", $font, 8, array(0,0,0));
    }
    </script>
    <div id="header">';

    $html .= '</div><br/>

    <div id="content">';

    $html .= '<table style="width: 460px; min-height: 300px; border: solid 1px #424242">
    <tbody>
    <tr>
    <td style="vertical-align: top;">
    <div class="header_wrapper">
    <table width="100%" cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
    <td class="logo" colspan="2" rowspan="2">
    <img src="'.$this->app->baseUr.'img/label.png" alt="">
    </td>
    <td class="label_wrapper" colspan="2">
    <div class="box left" style="width:100%;padding: 0;font-size: 11px;line-height: 0.3cm;height: 0.8cm;font-weight: bold;border-bottom: 0;margin-bottom: 2px;border-top: 0;"><br>
    <div">
    <span style="text-align: left;font-size: 15px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Label Pengiriman</span>
    <span style="display:block;font-weight: normal;color: #808080;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sales Order: '.$salesorder->id.'</span>
    </div>
    </div>
    </td>
    <hr style="border:1px #000000 dotted;">
    </tr>
    </tbody>
    </table>
    </div>

    <table style="width: 100%;">
    <tbody>
    <tr>
    <td style="padding-bottom: 0px; text-align: center;">
    <div style="margin: 0; text-align: center;">
    <img id="barcode-printout-1" width="90%" height="60" src="'.$this->app->baseUr.'img/barcode.png">
    </div>
    <span style="font-size: 12px; color: #808080;">Kode Booking</span>
    <span style="font-size: 12px; font-weight: 600; color: #333333;">: '.$salesorder->booking.'</span>
    </td>
    </tr>
    </tbody>
    </table>


    <table style="width: 100%;">
    <tbody>
    <tr>
    <td style="width: 25%;">
    <img src="https://cdn.tokopedia.net/img/kurir/logo_jne.png" alt="" width="80%;">
    </td>
    <td style="width: 25%;">
    <div style="font-size: 12px; font-weight: bold; color: #333333;">JNE</div>
    <div style="font-size: 12px; color: #333333;">YES</div>
    </td>
    <td style="width: 60%;" colspan="2">
    <div style="font-size: 12px; color: #808080;">Nomor Penjualan</div>
    <div style="font-size: 12px; color: #333333;">'.$salesorder->selling.'</div>
    </td>
    </tr>
    </tbody>
    </table>

    <table style="width: 100%;">
    <tbody>
    <tr>
    <td style="width: 41%;">
    <div style="padding-bottom: 5px; padding-left: 20px; font-size: 12px; font-weight: normal; color: #666666;">Tanggal Penjualan</div>
    <div style="padding-left: 20px; font-size: 12px; color: #333333;"><b>'.date('d', strtotime($salesorder->selling_date)).' '.$this->bulan($salesorder->selling_date).' '.date('Y', strtotime($salesorder->selling_date)).'</b></div>
    </td>
    <td style="width: 30%;">
    <div style="padding-bottom: 5px; font-size: 12px; color: #333333;">Ongkir</div>
    <div style="font-size: 12px; font-weight: bold; color: #333333;">Rp. '.number_format($salesorder->ongkir).'</div>
    </td>
    <td style="width: 20%; padding-right: 10px;">
    <div style="padding-bottom: 5px; font-size: 12px; color: #333333;">Berat</div>
    <div style="font-size: 12px; font-weight: bold; color: #333333;">'.number_format($salesorder->total_item_weight).' gr</div>
    </td>
    </tr>
    </tbody>
    </table>

    <table style="width: 100%;padding: 0 8px;">
    <tbody>
    <tr>
    <td style="text-align: center;">
    <div class="textbox_wrapper">
    <img src="https://cdn.tokopedia.net/img/kurir/icon-money-alt.png" alt="">Paket ini tidak menggunakan asuransi pihak logistik.
    </div>
    </td>
    </tr>
    </tbody>
    </table>

    <div class="additional_info_wrapper">
    <table style="width: 100%;">
    <tbody>
    <tr>
    <th>Kepada</th>
    <td>
    <div class="text-head"><b>'.$salesorder->customer.'</b></div>
    <div class="text-content">
    '.$salesorder->address.', '.strtoupper($salesorder->village).'<br>
    '.$salesorder->districts.', '.$salesorder->city.' - '.$salesorder->province.' '.$salesorder->postal.' <br>
    '.$salesorder->mobile.'
    </div>
    </td>
    </tr>
    <tr>
    <th>Dari</th>
    <td>
    <div class="text-head"><b>'.$company->name.'</b></div>
    <div class="text-content">
    '.$company->address.'
    '.$company->phone.' / '.$company->email.' / www.aksesoren.com
    </div>
    </td>
    </tr>
    </tbody>
    </table>
    </div>


    <table style="width: 100%;">
    <tbody>
    <tr>
    <td colspan="2" style="position: relative; padding: 5px 0;">
    <div style="border-top: dashed 1px #BDBDBD;"></div>
    <div style="position: absolute; top: -5px; right: 0;"><img src="https://cdn.tokopedia.net/img/kurir/icon-cut.png" width="14px;" alt=""></div>
    </td>
    </tr>


    <tr>
    <td style="width: 17%; padding-left: 10px; vertical-align: top;">
    <div style="font-size: 11px; color: #333333;">'.$salesorder->qty.' buah</div>
    </td>
    <td style="padding-right: 10px;">
    <div style="padding-bottom: 10px;">';

    $q = \Salesorders::select($this->app->db->raw('data_salesorder_detail.*'))
    ->join('data_salesorder_detail','data_salesorder.id','=','data_salesorder_detail.salesorder_id')
    ->where('data_salesorder.id', $id)
    ->get();

    $datas = $q->toArray();

    for ($i=0; $i < count($datas) ; $i++){
      $d = $datas[$i];
      $no = $i+1;

      $html  .= '<div style="font-size: 9px; color: #333333;">'.$no.'. '.$d['item_name'].' / '.$d['qty'].' buah</div>';

    }

    if($salesorder->shipping_note != null){
      $shipping_note = $salesorder->shipping_note;
    }else{
      $shipping_note = '-';
    }

    $html  .= '<div style="font-size: 10px; color: #333333"><br>
    Keterangan: '.$shipping_note.'
    </div>
    </div>
    </td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    </tbody>
    </table>';

    $html  .= '</div>';
    $end   = require $this->app->basePath . '/app/templates/end.php';
    $html  .= $end;


        //echo  $html;
    $dompdf = new \DOMPDF();
    $dompdf->load_html(stripslashes($html));
    $dompdf->set_paper('A4', 'portrait');
    $dompdf->render();

    $dompdf->stream("purchaseorder.pdf", array("Attachment" => false));

    exit(0);

  }

}

