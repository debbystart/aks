<?php namespace Rlims\Services;

class PHPExcelReader {

	/**
	 * The application instance.
	 */
	protected $app;

	/**
	 * The storagePath
	 */
	protected $storagePath;

	/**
	 *
	 */
	public function __construct($app) {
		$this->app = $app;
		$this->storagePath = dirname(__FILE__) . '/../../../media/uploads/import_tools';
	}

	public function getStoragePath() {
		return $this->storagePath;
	}

	public function setStoragePath($path) {
		if(is_dir($path)) {
			$this->storagePath = $path;
			return true;
		}
		return false;
	}

	/**
	 * Import tools
	 */
	public function importDataFromExcel($filepath, $insertMode='OVERWRITE') {

		$_insertMode = 'OVERWRITE';

		if($insertMode!==$_insertMode)
			$_insertMode = $insertMode;

		$sheets = array(
			"question",
			"answer",
			"passage",
			"domain",
			"subdomain",
			"certification",
			"exam_set"
		);


		$objReader = \PHPExcel_IOFactory::createReaderForFile($filepath);

		if(!$objReader->canRead($filepath)) {
            $res = $this->app->response();
            $res['Content-Type'] = 'application/json';
            $res->body(json_encode(array(
                'error' => 'File is not a valid Excel file'
            )));
            $this->app->response()->status(401);
            $this->app->stop();
		}

		$objReader->setReadDataOnly(true);
		$objReader->setLoadSheetsOnly($sheets);
		$objExcel = $objReader->load($filepath);

		//$worksheetsInfo = $objReader->listWorksheetInfo($filepath);
		//var_dump($worksheetsInfo);

		try {

            $this->app->db->getPdo()->beginTransaction();
            // database queries here

            // 1. certification worksheet
            $wsCertifications = $objExcel->getSheetByName('certification');

			if(!$wsCertifications) {

	            $this->app->db->getPdo()->rollBack();

	            $res = $this->app->response();
	            $res['Content-Type'] = 'application/json';
	            $res->body(json_encode(array(
	                'error' => 'Can\'t find certification sheet'
	            )));
	            $this->app->response()->status(400);
	            $this->app->stop();
			}

			if($wsCertifications) {

				$wsCertifications = $wsCertifications->toArray(null,false,false,false,false);

	            // insert the certifications
	            for ($i=1; $i < count($wsCertifications); $i++) { 

	            	$data = $wsCertifications[$i];

	            	// check for duplicate data
	            	$certification = \Certifications::where('code', $data[2])->first();

	            	if($certification) {
	            		
	            		if($_insertMode==='OVERWRITE') {
			            	$certification->name 				= $data[1];
			            	$certification->code 				= $data[2];
			            	$certification->description 		= $data[3];
			            	$certification->number_of_questions = $data[4];
			            	$certification->duration 			= $data[5];
			            	$certification->passing_grade 		= $data[6];
			            	$certification->save();
	            		}
	            	}
	            	else {
		            	$certification = new \Certifications;
		            	$certification->name 				= $data[1];
		            	$certification->code 				= $data[2];
		            	$certification->description 		= $data[3];
		            	$certification->number_of_questions = $data[4];
		            	$certification->duration 			= $data[5];
		            	$certification->passing_grade 		= $data[6];
		            	$certification->save();
	            	}
	            	
	            }
			}

            // 2. domain worksheet
            $wsDomains = $objExcel->getSheetByName('domain');

			if($wsDomains) {

				$wsDomains = $wsDomains->toArray(null,false,false,false,false);
				//var_dump($wsDomains);

	            // insert the domains
	            for ($i=1; $i < count($wsDomains); $i++) { 

	            	$data = $wsDomains[$i];
	            	//var_dump($data); $this->app->db->getPdo()->rollBack(); die();

	            	$_certification = \Certifications::where('code',$data[1])->first();
	
	            	if($_certification) {

		            	// check for duplicate data
		            	$domain = \Domains::where('code', $data[3])->first();

		            	if($domain) {
		            		
		            		if($_insertMode==='OVERWRITE') {
				            	$domain->certificationid 	= $_certification->id;
				            	$domain->name 				= $data[2];
				            	$domain->code 				= $data[3];
				            	$domain->description 		= $data[4];
				            	$domain->proportion 		= $data[5];
				            	$domain->save();
		            		}
		            	}
		            	else {
			            	$domain = new \Domains;
			            	$domain->certificationid 	= $_certification->id;
			            	$domain->name 				= $data[2];
			            	$domain->code 				= $data[3];
			            	$domain->description 		= $data[4];
			            	$domain->proportion 		= $data[5];
			            	$domain->save();

							// create exam_set
							//$exam_set = new \Exam_sets;
							//$exam_set->certificationid 		= $_certification->id;
							//$exam_set->domainid 			= $domain->id;
							//$exam_set->name 				= "Exam set " . $domain->name;
							//$exam_set->code 				= "Code";
			            	//$exam_set->save();
		            	}

	            	}
	            	
	            }
			}

            // 3. subdomain worksheet
            $wsSubdomains = $objExcel->getSheetByName('subdomain');

			if($wsSubdomains) {

				$wsSubdomains = $wsSubdomains->toArray(null,false,false,false,false);
	            
	            // insert the subdomains
	            for ($i=1; $i < count($wsSubdomains); $i++) { 

	            	$data = $wsSubdomains[$i];

	            	$_domain = \Domains::where('code',$data[1])->first();
	
	            	if($_domain) {

		            	// check for duplicate data
		            	$subdomain = \Subdomains::where('code', $data[3])->first();

		            	if($subdomain) {
		            		
		            		if($_insertMode==='OVERWRITE') {
				            	$subdomain->domainid 			= $_domain->id;
				            	$subdomain->name 				= $data[2];
				            	$subdomain->code 				= $data[3];
				            	$subdomain->description 		= $data[4];
				            	$subdomain->save();
		            		}
		            	}
		            	else {
			            	$subdomain = new \Subdomains;
			            	$subdomain->domainid 			= $_domain->id;
			            	$subdomain->name 				= $data[2];
			            	$subdomain->code 				= $data[3];
			            	$subdomain->description 		= $data[4];
			            	$subdomain->save();
		            	}
	            	}
	            	
	            }
			}

			// 4. exam_set worksheet
            $wsExam_sets = $objExcel->getSheetByName('exam_set');

			if($wsExam_sets) {

				$wsExam_sets = $wsExam_sets->toArray(null,false,false,false,false);
				
	            
	            // insert the exam_sets
	            for ($i=1; $i < count($wsExam_sets); $i++) { 

	            	$data = $wsExam_sets[$i];
					//var_dump($data); $this->app->db->getPdo()->rollBack(); die();

	            	$_certification_ = \Certifications::where('code',$data[1])->first();
	
	            	if($_certification_) {

		            	// check for duplicate data
		            	$exam_set = \Exam_sets::where('code', $data[3])->first();

		            	if($exam_set) {
		            		
		            		if($_insertMode==='OVERWRITE') {
								$exam_set->certificationid		= $_certification_->id;
				            	$exam_set->name 				= $data[2];
				            	$exam_set->code 				= $data[3];
				            	$exam_set->description 			= $data[4];

				            	$_domain_questions_ = \Questions::select($this->app->db->raw('exam_setid, domainid'))
				            		->groupBy($this->app->db->raw('exam_setid, domainid'))
				            		->having('exam_setid', '=', $exam_set->id)
				            		->get();

				            	$domain_questions = array();
				            	foreach ($_domain_questions_ as $_domain_question_) {
				            		array_push($domain_questions, $_domain_question_->domainid);
				            	}

				            	if(count($domain_questions)>0) {
				            		$exam_set->domains()->sync($domain_questions);
				            	}

				            	$exam_set->save();
		            		}
		            	}
		            	else {
			            	$exam_set = new \Exam_sets;
							$exam_set->certificationid		= $_certification_->id;
			            	$exam_set->name 				= $data[2];
			            	$exam_set->code 				= $data[3];
			            	$exam_set->description 			= $data[4];
			            	$exam_set->save();

			            	$_domain_questions_ = \Questions::select($this->app->db->raw('exam_setid, domainid'))
			            		->groupBy($this->app->db->raw('exam_setid, domainid'))
			            		->having('exam_setid', '=', $exam_set->id)
			            		->get();

			            	$domain_questions = array();
			            	foreach ($_domain_questions_ as $_domain_question_) {
			            		array_push($domain_questions, $_domain_question_->domainid);
			            	}

			            	if(count($domain_questions)>0) {
			            		$exam_set->domains()->sync($domain_questions);
			            	}

			            	$exam_set->save();

		            	}
	            	}
	            	
	            }
			}

            // 5. question worksheet
            $wsQuestions = $objExcel->getSheetByName('question');

			if($wsQuestions) {

				$wsQuestions = $wsQuestions->toArray(null,false,false,false,false);
	            
	            // insert the subdomains
	            for ($i=1; $i < count($wsQuestions); $i++) { 

	            	$data = $wsQuestions[$i];

	            	$_domain = \Domains::where('code',$data[1])->first();
	            	$_subdomain = \Subdomains::where('code',$data[2])->first();
					$_exam_set = \Exam_sets::where('code',$data[8])->first();
	            	
	            	//if($_domain) {
		            	//$_exam_set = \Exam_sets::where('domainid',$_domain->id)->first();
	            	//}
	
	            	if($_domain && $_subdomain && $_exam_set) {

		            	// check for duplicate data
		            	$question = \Questions::where('code', $data[4])->first();

		            	if($question) {
		            		
		            		if($_insertMode==='OVERWRITE') {
				            	$question->domainid 			= $_domain->id;
				            	$question->subdomainid 			= $_subdomain->id;
				            	$question->certificationid 		= $_domain->certificationid;
				            	$question->name 				= $data[3];
				            	$question->code 				= $data[4];
				            	$question->explanation 			= $data[5];
								//$question->exam_setid 			= $data[8];
								//$question->exam_setid 			= $_exam_set->id;
	
				            	if(isset($data[6]))
					            	$question->passage 				= $data[6];

				            	if(isset($data[7]))
					            	$question->difficulty_level		= $data[7];		
								
								if(isset($data[8]))
				            		$question->exam_setid 			= $_exam_set->id;
							
				            	$question->save();
		            		}
		            	}
		            	else {
			            	$question = new \Questions;
			            	$question->domainid 			= $_domain->id;
			            	$question->subdomainid 			= $_subdomain->id;
			            	$question->certificationid 		= $_domain->certificationid;
			            	$question->name 				= $data[3];
			            	$question->code 				= $data[4];
			            	$question->explanation 			= $data[5];
							//$question->exam_setid 			= $data[8];
							//$question->exam_setid 			= $_exam_set->id;

			            	if(isset($data[6]))
				            	$question->passage 				= $data[6];

			            	if(isset($data[7]))
				            	$question->difficulty_level		= $data[7];
							
							if(isset($data[8]))
				            	$question->exam_setid 			= $_exam_set->id;

			            	$question->save();
		            	}

	            	}
	            	
	            }
			}

            // 7. answer worksheet
            $wsAnswers = $objExcel->getSheetByName('answer');

			if($wsAnswers) {

				$wsAnswers = $wsAnswers->toArray(null,false,false,false,false);
	            
	            // insert the answers
	            for ($i=1; $i < count($wsAnswers); $i++) { 

	            	$data = $wsAnswers[$i];

	            	$_question = \Questions::where('code',$data[1])->first();
	
	            	if($_question) {

		            	// check for duplicate data
		            	$answer = \Answers::where('code', $data[2])->first();

		            	if($answer) {
		            		
		            		if($_insertMode==='OVERWRITE') {
				            	$answer->questionid 		= $_question->id;
				            	$answer->code 				= $data[2];
				            	$answer->answer_text 		= $data[3];
				            	$answer->correct 			= $data[4];
				            	$answer->save();
		            		}
		            	}
		            	else {
			            	$answer = new \Answers;
			            	$answer->questionid 		= $_question->id;
			            	$answer->code 				= $data[2];
			            	$answer->answer_text 		= $data[3];
			            	$answer->correct 			= $data[4];
			            	$answer->save();
		            	}

	            	}
	            	
	            }
			}
			
			// 8. exam_set worksheet again
            $wsExam_sets = $objExcel->getSheetByName('exam_set');

			if($wsExam_sets) {

				$wsExam_sets = $wsExam_sets->toArray(null,false,false,false,false);
				
	            
	            // insert the exam_sets
	            for ($i=1; $i < count($wsExam_sets); $i++) { 

	            	$data = $wsExam_sets[$i];
					//var_dump($data); $this->app->db->getPdo()->rollBack(); die();

	            	$_certification_ = \Certifications::where('code',$data[1])->first();
	
	            	if($_certification_) {

		            	// check for duplicate data
		            	$exam_set = \Exam_sets::where('code', $data[3])->first();

		            	if($exam_set) {
		            		
		            		if($_insertMode==='OVERWRITE') {
								$exam_set->certificationid		= $_certification_->id;
				            	$exam_set->name 				= $data[2];
				            	$exam_set->code 				= $data[3];
				            	$exam_set->description 			= $data[4];

				            	$_domain_questions_ = \Questions::select($this->app->db->raw('exam_setid, domainid'))
				            		->groupBy($this->app->db->raw('exam_setid, domainid'))
				            		->having('exam_setid', '=', $exam_set->id)
				            		->get();

				            	$domain_questions = array();
				            	foreach ($_domain_questions_ as $_domain_question_) {
				            		array_push($domain_questions, $_domain_question_->domainid);
				            	}

				            	if(count($domain_questions)>0) {
				            		$exam_set->domains()->sync($domain_questions);
				            	}

				            	$exam_set->save();
		            		}
		            	}
		            	else {
			            	$exam_set = new \Exam_sets;
							$exam_set->certificationid		= $_certification_->id;
			            	$exam_set->name 				= $data[2];
			            	$exam_set->code 				= $data[3];
			            	$exam_set->description 			= $data[4];
			            	$exam_set->save();

			            	$_domain_questions_ = \Questions::select($this->app->db->raw('exam_setid, domainid'))
			            		->groupBy($this->app->db->raw('exam_setid, domainid'))
			            		->having('exam_setid', '=', $exam_set->id)
			            		->get();

			            	$domain_questions = array();
			            	foreach ($_domain_questions_ as $_domain_question_) {
			            		array_push($domain_questions, $_domain_question_->domainid);
			            	}

			            	if(count($domain_questions)>0) {
			            		$exam_set->domains()->sync($domain_questions);
			            	}

			            	$exam_set->save();

		            	}
	            	}
	            	
	            }
			}

            $this->app->db->getPdo()->commit();

        } catch (\PDOException $e) {
            // Woopsy
            $this->app->db->getPdo()->rollBack();

            $out = json_encode(array('error' => $e));

            $res = $this->app->response();
            $res['Content-Type'] = 'application/json';
            $res->body($out);
            $res->status(400);
            $this->app->stop();                        
        }

	}
}

/*
The following code will only read row 1 and rows 20 to 30 of any sheet in the Excel file:

class MyReadFilter implements PHPExcel_Reader_IReadFilter {

    public function readCell($column, $row, $worksheetName = '') {
        // Read title row and rows 20 - 30
        if ($row == 1 || ($row >= 20 && $row <= 30)) {
            return true;
        }
        return false;
    }

}

$objReader = new PHPExcel_Reader_Excel2003XML();
$objReader->setReadFilter( new MyReadFilter() );
$objPHPExcel = $objReader->load("06largescale.xml");
*/
