<?php

class Salesorders_detail extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'data_salesorder_detail';
	protected $primaryKey = 'id';

	public static function getNextSalesorderdetailCode() {

		$app = \Slim\Slim::getInstance();


		$last_count = 1;

      // get last count
		$salesorder = Salesorders_detail::select('id')
		->orderBy('id', 'desc')
		->first();

		if($salesorder) {
			$data = $salesorder->id;
			$last_count = intval($data) + 1;
		}

		$COUNTER = sprintf(intval($last_count));

		return $COUNTER;
	}
}