<?php

class Routes extends Illuminate\Database\Eloquent\Model
{
    protected $table = 'use_route';
    protected $primaryKey = 'id';

    //protected $softDelete = true;

  //------------------------------------------------------------------------------------
  // relationships

    public function roles() {
      return $this->hasMany('Roles', 'route_id', 'id');
    }


  //------------------------------------------------------------------------------------
  // custom fields

}