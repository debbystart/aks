<?php

class Paymentlazadas_detail extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'data_paymentlazada_detail';
	protected $primaryKey = 'id';

	public static function getNextPaymentlazadadetailCode() {

		$app = \Slim\Slim::getInstance();


		$last_count = 1;

      // get last count
		$paymentlazada_detail = Paymentlazadas_detail::select('id')
		->orderBy('id', 'desc')
		->first();

		if($paymentlazada_detail) {
			$data = $paymentlazada_detail->id;
			$last_count = intval($data) + 1;
		}

		$COUNTER = sprintf(intval($last_count));

		return $COUNTER;
	}
}