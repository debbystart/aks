<?php

class User_apps extends Illuminate\Database\Eloquent\Model
{
    protected $table = 'use_user_application';
	protected $primaryKey = 'id';

	// protected $softDelete = true;

	//------------------------------------------------------------------------------------
	// custom fields

	//------------------------------------------------------------------------------------
	// relationships

	public function user() {
		return $this->belongsTo('Users');
	}

	public function users() {
		return $this->hasMany('Users', 'id', 'user_id');
	}

	//------------------------------------------------------------------------------------
	// custom query

}