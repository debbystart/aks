<?php

class Roles extends Illuminate\Database\Eloquent\Model
{
    protected $table = 'use_role';
    protected $primaryKey = 'id';

    // protected $softDelete = true;

    protected $appends = array('route_name', 'route_label');

  //------------------------------------------------------------------------------------
  // relationships

    // Many to Many with role table will need role_role table as pivot table
    public function groups(){
        return $this->belongsToMany('Groups', 'use_group_role', 'role_id', 'group_id');
    }

    public function route() {
      return $this->belongsTo('Routes');
    }

  //------------------------------------------------------------------------------------
  // custom fields

    public function getRouteNameAttribute() {
         $route = $this->route()->first();
      return ($route?$route->name:null);
    }

    public function getRouteLabelAttribute() {
         $route = $this->route()->first();
      return ($route?$route->label:null);
    }

}