<?php

class Salesorderalls extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'data_salesorderall';
	protected $primaryKey = 'id';

	public static function getNextSalesorderallCode() {

		$app = \Slim\Slim::getInstance();


		$last_count = 1;

		$CODE = 'SA';

		$TAHUN_BULAN = date('ym');

      // get last count
		$salesorderall = Salesorderalls::select('id')
		->where('id','like', $CODE.$TAHUN_BULAN.'%')
		->orderBy('id', 'desc')
		->first();

		if($salesorderall) {
			$data = explode($CODE.$TAHUN_BULAN, $salesorderall->id);
			$last_count = intval($data[1]) + 1;
		}

		$curr_count = '';
		$curr_count = sprintf('%04d', $curr_count + intval($last_count));
		$COUNTER = $curr_count;

		return $CODE.$TAHUN_BULAN.$COUNTER;
	}

}