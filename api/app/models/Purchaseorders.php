<?php

class Purchaseorders extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'data_purchaseorder';
	protected $primaryKey = 'id';

	protected $appends = array('supplier', 'shipping', 'market', 'province', 'city', 'districts', 'village', 'from_ship', 'postal');

	public function supplier() {
		return $this->belongsTo('Suppliers');
	}

	public function getSupplierAttribute() {
		$supplier = $this->supplier()->first();
		return ($supplier?$supplier->name:null);
	}

	public function getMarketAttribute() {
		$supplier = $this->supplier()->first();
		return ($supplier?$supplier->market_id:null);
	}

	public function getProvinceAttribute() {
		$supplier = $this->supplier()->first();
		return ($supplier?$supplier->province:null);
	}

	public function getCityAttribute() {
		$supplier = $this->supplier()->first();
		return ($supplier?$supplier->city:null);
	}

	public function getDistrictsAttribute() {
		$supplier = $this->supplier()->first();
		return ($supplier?$supplier->districts:null);
	}

	public function getVillageAttribute() {
		$supplier = $this->supplier()->first();
		return ($supplier?$supplier->village:null);
	}

	public function getPostalAttribute() {
		$supplier = $this->supplier()->first();
		return ($supplier?$supplier->postal:null);
	}

	public function getFromShipAttribute() {
		$supplier = $this->supplier()->first();
		return ($supplier?$supplier->address:null);
	}

	public function shipping() {
		return $this->belongsTo('Shippings');
	}

	public function getShippingAttribute() {
		$shipping = $this->shipping()->first();
		return ($shipping?$shipping->name:null);
	}

	public function purchaseorders_detail(){
		return $this->hasMany('Purchaseorders_detail', 'purchaseorder_id', 'id');
	}

	public static function getNextPurchaseorderCode() {

		$app = \Slim\Slim::getInstance();


		$last_count = 1;

		$CODE = 'PO';

		$TAHUN_BULAN = date('ym');

      // get last count
		$purchaseorder = Purchaseorders::select('id')
		->where('id','like', $CODE.$TAHUN_BULAN.'%')
		->orderBy('id', 'desc')
		->first();

		if($purchaseorder) {
			$data = explode($CODE.$TAHUN_BULAN, $purchaseorder->id);
			$last_count = intval($data[1]) + 1;
		}

		$curr_count = '';
		$curr_count = sprintf('%04d', $curr_count + intval($last_count));
		$COUNTER = $curr_count;

		return $CODE.$TAHUN_BULAN.$COUNTER;
	}

}