<?php

class Customers extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'param_customer';
	protected $primaryKey = 'id';

	protected $appends = array('market');

	public function market() {
		return $this->belongsTo('Markets');
	}

	public function getMarketAttribute() {
		$market = $this->market()->first();
		return ($market?$market->name:null);
	}

	public static function getNextCustomerCode($id) {

		$app = \Slim\Slim::getInstance();

		$idx = "'".$id."'";

		$last_count = 1;

		$customer = Customers::select($app->db->raw('right(id, 4) as id'))
		->whereRaw('left(id, 3) = '.$idx)
		->orderBy('id', 'desc')
		->first();

		if($customer) {
			$data = $customer->id;
			$last_count = intval($data) + 1;
		}

		$curr_count = '';
		$curr_count = sprintf('%04d', $curr_count + intval($last_count));
		$COUNTER = $curr_count;

		return $COUNTER;
	}
}