<?php

class Purchaseorders_detail extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'data_purchaseorder_detail';
	protected $primaryKey = 'id';

	public static function getNextPurchaseorderdetailCode() {

		$app = \Slim\Slim::getInstance();


		$last_count = 1;

      // get last count
		$purchaseorder = Purchaseorders_detail::select('id')
		->orderBy('id', 'desc')
		->first();

		if($purchaseorder) {
			$data = $purchaseorder->id;
			$last_count = intval($data) + 1;
		}

		$COUNTER = sprintf(intval($last_count));

		return $COUNTER;
	}
}