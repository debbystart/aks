<?php

class Pricelists_operational extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'param_pricelist_operational';
	protected $primaryKey = 'id';

	public static function getNextPricelistoperationalCode() {

		$app = \Slim\Slim::getInstance();

		$last_count = 1;

		$pricelist_operational = Pricelists_operational::select('id')
		->orderBy('id', 'desc')
		->first();

		if($pricelist_operational) {
			$data = $pricelist_operational->id;
			$last_count = intval($data) + 1;
		}

		$COUNTER = sprintf(intval($last_count));

		return $COUNTER;
	}
}