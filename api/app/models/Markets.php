<?php

class Markets extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'param_market';
	protected $primaryKey = 'id';

	public static function getNextMarketCode() {

		$app = \Slim\Slim::getInstance();

		$last_count = 1;

      // get last count
		$market = Markets::select('id')
		->orderBy('id', 'desc')
		->first();

		if($market) {
			$data = $market->id;
			$last_count = intval($data) + 1;
		}

		$curr_count = '';
		$curr_count = sprintf('%02d', $curr_count + intval($last_count));
		$COUNTER = $curr_count;

		return $COUNTER;
	}
}