<?php

class Itemgroups extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'param_itemgroup';
	protected $primaryKey = 'id';

	public static function getNextItemgroupCode() {

		$app = \Slim\Slim::getInstance();

		$last_count = 1;

		$itemgroup = Itemgroups::select('id')
		->orderBy('id', 'desc')
		->first();

		if($itemgroup) {
			$data = $itemgroup->id;
			$last_count = intval($data) + 1;
		}

		$curr_count = '';
		$curr_count = sprintf('%02d', $curr_count + intval($last_count));
		$COUNTER = $curr_count;

		return $COUNTER;
	}
}