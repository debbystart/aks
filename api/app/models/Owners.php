<?php

class Owners extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'param_owner';
	protected $primaryKey = 'id';

	public static function getNextOwnerCode() {

		$app = \Slim\Slim::getInstance();

		$last_count = 1;

		$owner = Owners::select('id')
		->orderBy('id', 'desc')
		->first();

		if($owner) {
			$data = $owner->id;
			$last_count = intval($data) + 1;
		}

		$curr_count = '';
		$curr_count = sprintf('%02d', $curr_count + intval($last_count));
		$COUNTER = $curr_count;

		return $COUNTER;
	}
}