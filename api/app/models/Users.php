<?php

class Users extends Illuminate\Database\Eloquent\Model
{
  protected $table = 'use_user';
  protected $primaryKey = 'id';

  protected $hidden = array('password');
  protected $appends = array('group', 'admin', 'can_read', 'can_create', 'can_update', 'can_delete', 'has_roles','routers');

    // protected $softDelete = true;

  protected static $unguarded = true;

  public function getPassword() {
   return $this->password;
 }

  //------------------------------------------------------------------------------------
  // relationships	
 
 public function user_group(){
  return $this->belongsTo('Groups');
}

public function user_apps() {
 return $this->hasMany('User_apps', 'user_id', 'id');
}

  //------------------------------------------------------------------------------------
  // custom fields

public function getUserGroupAttribute() {
 $user_group = $this->user_group()->first();
 return ($user_group?$user_group->toArray():null);
}

public function getUserAppsAttribute() {
 $user_apps = $this->user_apps()->get();
 return ($user_apps?$user_apps->toArray():array());
}

public function getGroupAttribute() {
 $user_group = $this->user_group()->first();
 return $user_group->name;
}

public function getAdminAttribute() {
 $user_group = $this->user_group()->first();
       return 1; //(($user_group->can_read==1 && $user_group->can_create==1 && $user_group->can_update==1 && $user_group->can_delete==1)?true:false);
     }

     public function getCanReadAttribute() {
       $user_group = $this->user_group()->first();
       return 1; //$user_group->can_read;
     }

     public function getCanCreateAttribute() {
       $user_group = $this->user_group()->first();
       return 1; //$user_group->can_create;
     }

     public function getCanUpdateAttribute() {
       $user_group = $this->user_group()->first();
       return 1; //$user_group->can_update;
     }

     public function getCanDeleteAttribute() {
       $user_group = $this->user_group()->first();
       return 1; //$user_group->can_delete;
     }

     public function getHasRolesAttribute() {
       $user_group = $this->user_group()->with('roles')->first();
       $roles = array();

        // get all available roles
       foreach($user_group->roles as $role) {
        $roles[] = $role->name;
      }

      return $roles;
    }
    

    public function getRoutersAttribute() {
     $user_group = $this->user_group()->with('roles')->first();
     $roles = array();

        // get all available roles
     foreach($user_group->roles as $role) {
      if($role->route_name) {
        $roles[] = array(
          'route' => $role->route_name,
          'can_create' => !(!$role->can_create),
          'can_read' => !(!$role->can_read),
          'can_update' => !(!$role->can_edit),
          'can_delete' => !(!$role->can_delete),
          'can_approved' => !(!$role->can_approved),
        );            
      }
    }
    return $roles;
  }

  //------------------------------------------------------------------------------------
  // custom query

  public static function users_with_group() {

   $app = \Slim\Slim::getInstance();

   $select = array(
    't_user.*', 
    't_user_group.id as user_group_id', 
    't_user_group.name', 
    't_user_group.can_create', 
    't_user_group.can_read', 
    't_user_group.can_update', 
    't_user_group.can_delete' 
  );

   return Users::select($select)
   ->join('t_user_group', 't_user.user_group_id', '=', 't_user_group.id')
   ->whereRaw('t_user.deleted_at is null')
   ->whereRaw('t_user_group.deleted_at is null');
 }

}