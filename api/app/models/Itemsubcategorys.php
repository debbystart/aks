<?php

class Itemsubcategorys extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'param_itemsubcategory';
	protected $primaryKey = 'id';

	protected $appends = array('group_name', 'category_name');

	public function itemgroup() {
		return $this->belongsTo('Itemgroups');
	}

	public function itemcategory() {
		return $this->belongsTo('Itemcategorys');
	}

	public function getGroupNameAttribute() {
		$itemgroup = $this->itemgroup()->first();
		return ($itemgroup?$itemgroup->name:null);
	}

	public function getCategoryNameAttribute() {
		$itemcategory = $this->itemcategory()->first();
		return ($itemcategory?$itemcategory->name:null);
	}

	public static function getNextItemsubcategoryCode($id) {

		$app = \Slim\Slim::getInstance();

		$last_count = 1;

		$itemsubcategory = Itemsubcategorys::select($app->db->raw('right(id, 2) as id'))
		->whereRaw('left(id, 4) = '.$id)
		->orderBy('id', 'desc')
		->first();

		if($itemsubcategory) {
			$data = $itemsubcategory->id;
			$last_count = intval($data) + 1;
		}

		$curr_count = '';
		$curr_count = sprintf('%02d', $curr_count + intval($last_count));
		$COUNTER = $curr_count;

		return $COUNTER;
	}
}