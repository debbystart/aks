<?php

class Suppliers extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'param_supplier';
	protected $primaryKey = 'id';

	protected $appends = array('market');

	public function market() {
		return $this->belongsTo('Markets');
	}

	public function getMarketAttribute() {
		$market = $this->market()->first();
		return ($market?$market->name:null);
	}

	public static function getNextSupplierCode() {

		$app = \Slim\Slim::getInstance();

		$last_count = 1;

		$supplier = Suppliers::select('id')
		->orderBy('id', 'desc')
		->first();

		if($supplier) {
			$data = $supplier->id;
			$last_count = intval($data) + 1;
		}

		$curr_count = '';
		$curr_count = sprintf('%03d', $curr_count + intval($last_count));
		$COUNTER = $curr_count;

		return $COUNTER;
	}
}