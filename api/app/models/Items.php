<?php

class Items extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'param_item';
	protected $primaryKey = 'id';

	protected $appends = array('group_name', 'category_name', 'subcategory_name', 'unit', 'brand');

	public function itemgroup() {
		return $this->belongsTo('Itemgroups');
	}

	public function itemcategory() {
		return $this->belongsTo('Itemcategorys');
	}

	public function itemsubcategory() {
		return $this->belongsTo('Itemsubcategorys');
	}

	public function unit() {
		return $this->belongsTo('Units');
	}

	public function brand() {
		return $this->belongsTo('Brands');
	}

	public function getGroupNameAttribute() {
		$itemgroup = $this->itemgroup()->first();
		return ($itemgroup?$itemgroup->name:null);
	}

	public function getCategoryNameAttribute() {
		$itemcategory = $this->itemcategory()->first();
		return ($itemcategory?$itemcategory->name:null);
	}

	public function getSubcategoryNameAttribute() {
		$itemsubcategory = $this->itemsubcategory()->first();
		return ($itemsubcategory?$itemsubcategory->name:null);
	}

	public function getUnitAttribute() {
		$unit = $this->unit()->first();
		return ($unit?$unit->name:null);
	}

	public function getBrandAttribute() {
		$brand = $this->brand()->first();
		return ($brand?$brand->name:null);
	}

	public static function getNextItemCode($id) {

		$app = \Slim\Slim::getInstance();

		$last_count = 1;

      // get last count
		$item = Items::select($app->db->raw('right(id, 4) as id'))
		->whereRaw('left(id, 6) = '.$id)
		->orderBy('id', 'desc')
		->first();

		if($item) {
			$data = $item->id;
			$last_count = intval($data) + 1;
		}

		$curr_count = '';
		$curr_count = sprintf('%04d', $curr_count + intval($last_count));
		$COUNTER = $curr_count;

		return $COUNTER;
	}
}