<?php

class Salesorderalls_detail extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'data_salesorderall_detail';
	protected $primaryKey = 'id';

	public static function getNextSalesorderalldetailCode() {

		$app = \Slim\Slim::getInstance();


		$last_count = 1;

      // get last count
		$salesorder_detail = Salesorderalls_detail::select('id')
		->orderBy('id', 'desc')
		->first();

		if($salesorder_detail) {
			$data = $salesorder_detail->id;
			$last_count = intval($data) + 1;
		}

		$COUNTER = sprintf(intval($last_count));

		return $COUNTER;
	}
}