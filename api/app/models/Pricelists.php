<?php

class Pricelists extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'param_pricelist';
	protected $primaryKey = 'id';

	protected $appends = array('market');

	public function market() {
		return $this->belongsTo('Markets');
	}

	public function getMarketAttribute() {
		$market = $this->market()->first();
		return ($market?$market->name:null);
	}

	public function pricelists_detail(){
		return $this->hasMany('Pricelists_detail', 'pricelist_id', 'id');
	}

	public function operationals(){
		return $this->hasMany('Pricelists_operational', 'pricelist_id', 'id');
	}

	public static function getNextPricelistCode() {

		$app = \Slim\Slim::getInstance();


		$last_count = 1;

		$CODE = 'PL';

		$TAHUN_BULAN = date('ym');

      // get last count
		$pricelist = Pricelists::select('id')
		->where('id','like', $CODE.$TAHUN_BULAN.'%')
		->orderBy('id', 'desc')
		->first();

		if($pricelist) {
			$data = explode($CODE.$TAHUN_BULAN, $pricelist->id);
			$last_count = intval($data[1]) + 1;
		}

		$curr_count = '';
		$curr_count = sprintf('%04d', $curr_count + intval($last_count));
		$COUNTER = $curr_count;

		return $CODE.$TAHUN_BULAN.$COUNTER;
	}

}