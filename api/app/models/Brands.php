<?php

class Brands extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'param_brand';
	protected $primaryKey = 'id';

	public static function getNextBrandCode() {

		$app = \Slim\Slim::getInstance();

		$last_count = 1;

		$brand = Brands::select('id')
		->orderBy('id', 'desc')
		->first();

		if($brand) {
			$data = $brand->id;
			$last_count = intval($data) + 1;
		}

		$curr_count = '';
		$curr_count = sprintf('%03d', $curr_count + intval($last_count));
		$COUNTER = $curr_count;

		return $COUNTER;
	}
}