<?php

class Salesorders extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'data_salesorder';
	protected $primaryKey = 'id';

	protected $appends = array('customer', 'shipping', 'market', 'province', 'city', 'districts', 'village', 'salesman');

	public function customer() {
		return $this->belongsTo('Customers');
	}

	public function getCustomerAttribute() {
		$customer = $this->customer()->first();
		return ($customer?$customer->name:null);
	}

	public function getMarketAttribute() {
		$customer = $this->customer()->first();
		return ($customer?$customer->market_id:null);
	}

	public function getProvinceAttribute() {
		$customer = $this->customer()->first();
		return ($customer?$customer->province:null);
	}

	public function getCityAttribute() {
		$customer = $this->customer()->first();
		return ($customer?$customer->city:null);
	}

	public function getDistrictsAttribute() {
		$customer = $this->customer()->first();
		return ($customer?$customer->districts:null);
	}

	public function getVillageAttribute() {
		$customer = $this->customer()->first();
		return ($customer?$customer->village:null);
	}

	public function shipping() {
		return $this->belongsTo('Shippings');
	}

	public function getShippingAttribute() {
		$shipping = $this->shipping()->first();
		return ($shipping?$shipping->name:null);
	}

	public function salesman() {
		return $this->belongsTo('Salesmans');
	}

	public function getSalesmanAttribute() {
		$salesman = $this->salesman()->first();
		return ($salesman?$salesman->name:null);
	}

	public function salesorders_detail(){
		return $this->hasMany('Salesorders_detail', 'salesorder_id', 'id');
	}

	public static function getNextSalesorderCode() {

		$app = \Slim\Slim::getInstance();


		$last_count = 1;

		$CODE = 'SO';

		$TAHUN_BULAN = date('ym');

      // get last count
		$salesorder = Salesorders::select('id')
		->where('id','like', $CODE.$TAHUN_BULAN.'%')
		->orderBy('id', 'desc')
		->first();

		if($salesorder) {
			$data = explode($CODE.$TAHUN_BULAN, $salesorder->id);
			$last_count = intval($data[1]) + 1;
		}

		$curr_count = '';
		$curr_count = sprintf('%04d', $curr_count + intval($last_count));
		$COUNTER = $curr_count;

		return $CODE.$TAHUN_BULAN.$COUNTER;
	}

}