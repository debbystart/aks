<?php

class Itemcategorys extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'param_itemcategory';
	protected $primaryKey = 'id';

	protected $appends = array('group_name');

	public function itemgroup() {
		return $this->belongsTo('Itemgroups');
	}

	public function getGroupNameAttribute() {
		$itemgroup = $this->itemgroup()->first();
		return ($itemgroup?$itemgroup->name:null);
	}

	public static function getNextItemcategoryCode($id) {

		$app = \Slim\Slim::getInstance();

		$last_count = 1;

		$itemcategory = Itemcategorys::select($app->db->raw('right(id, 2) as id'))
		->whereRaw('left(id, 2) = '.$id)
		->orderBy('id', 'desc')
		->first();

		if($itemcategory) {
			$data = $itemcategory->id;
			$last_count = intval($data) + 1;
		}

		$curr_count = '';
		$curr_count = sprintf('%02d', $curr_count + intval($last_count));
		$COUNTER = $curr_count;

		return $COUNTER;
	}
}