<?php

class Transactions extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'data_transaction';
	protected $primaryKey = 'id';

	protected $appends = array('receive_save', 'supplier', 'shipping', 'to_name', 'delivery_save', 'shipping_delivery', 'customer', 'city', 'ongkir_receive', 'province', 'districts', 'village', 'postal', 'mobile', 'market_id', 'shipping_note', 'address');

	public function purchaseorder() {
		return $this->belongsTo('Purchaseorders', 'po_id', 'id');
	}

	public function getReceiveSaveAttribute() {
		$purchaseorder = $this->purchaseorder()->first();
		return ($purchaseorder?$purchaseorder->is_save:null);
	}

	public function getSupplierAttribute() {
		$purchaseorder = $this->purchaseorder()->first();
		return ($purchaseorder?$purchaseorder->supplier:null);
	}

	public function getShippingAttribute() {
		$purchaseorder = $this->purchaseorder()->first();
		return ($purchaseorder?$purchaseorder->shipping:null);
	}

	public function getToNameAttribute() {
		$purchaseorder = $this->purchaseorder()->first();
		return ($purchaseorder?$purchaseorder->to_name:null);
	}

	public function getOngkirReceiveAttribute() {
		$purchaseorder = $this->purchaseorder()->first();
		return ($purchaseorder?$purchaseorder->ongkir:null);
	}

	public function salesorder() {
		return $this->belongsTo('Salesorders', 'so_id', 'id');
	}

	public function getDeliverySaveAttribute() {
		$salesorder = $this->salesorder()->first();
		return ($salesorder?$salesorder->is_save:null);
	}

	public function getCustomerAttribute() {
		$salesorder = $this->salesorder()->first();
		return ($salesorder?$salesorder->customer_id:null);
	}

	public function getCityAttribute() {
		$salesorder = $this->salesorder()->first();
		return ($salesorder?$salesorder->city:null);
	}

	public function getProvinceAttribute() {
		$salesorder = $this->salesorder()->first();
		return ($salesorder?$salesorder->province:null);
	}

	public function getDistrictsAttribute() {
		$salesorder = $this->salesorder()->first();
		return ($salesorder?$salesorder->districts:null);
	}

	public function getVillageAttribute() {
		$salesorder = $this->salesorder()->first();
		return ($salesorder?$salesorder->village:null);
	}

	public function getPostalAttribute() {
		$salesorder = $this->salesorder()->first();
		return ($salesorder?$salesorder->postal:null);
	}

	public function getMobileAttribute() {
		$salesorder = $this->salesorder()->first();
		return ($salesorder?$salesorder->mobile:null);
	}

	public function getMarketIdAttribute() {
		$salesorder = $this->salesorder()->first();
		return ($salesorder?$salesorder->market_id:null);
	}

	public function getShippingNoteAttribute() {
		$salesorder = $this->salesorder()->first();
		return ($salesorder?$salesorder->shipping_note:null);
	}

	public function getAddressAttribute() {
		$salesorder = $this->salesorder()->first();
		return ($salesorder?$salesorder->address:null);
	}

	public function shipping_delivery() {
		return $this->belongsTo('Shippings', 'shipping_id', 'id');
	}

	public function getShippingDeliveryAttribute() {
		$shipping_delivery = $this->shipping_delivery()->first();
		return ($shipping_delivery?$shipping_delivery->name:null);
	}

	public function inventorys_detail(){
		return $this->hasMany('Inventorys', 'transaction_id', 'id');
	}

	public static function getNextTransPOcode() {

		$app = \Slim\Slim::getInstance();


		$last_count = 1;

		$CODE = 'RV';

		$TAHUN_BULAN = date('ym');

      // get last count
		$purchaseorder = Transactions::select('code')
		->where('code','like', $CODE.$TAHUN_BULAN.'%')
		->orderBy('code', 'desc')
		->first();

		if($purchaseorder) {
			$data = explode($CODE.$TAHUN_BULAN, $purchaseorder->code);
			$last_count = intval($data[1]) + 1;
		}

		$curr_count = '';
		$curr_count = sprintf('%04d', $curr_count + intval($last_count));
		$COUNTER = $curr_count;

		return $CODE.$TAHUN_BULAN.$COUNTER;
	}

	public static function getNextTransSOcode() {

		$app = \Slim\Slim::getInstance();


		$last_count = 1;

		$CODE = 'DV';

		$TAHUN_BULAN = date('ym');

      // get last count
		$purchaseorder = Transactions::select('code')
		->where('code','like', $CODE.$TAHUN_BULAN.'%')
		->orderBy('code', 'desc')
		->first();

		if($purchaseorder) {
			$data = explode($CODE.$TAHUN_BULAN, $purchaseorder->code);
			$last_count = intval($data[1]) + 1;
		}

		$curr_count = '';
		$curr_count = sprintf('%04d', $curr_count + intval($last_count));
		$COUNTER = $curr_count;

		return $CODE.$TAHUN_BULAN.$COUNTER;
	}
}