<?php

class Shippings extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'param_shipping';
	protected $primaryKey = 'id';

	public static function getNextShippingCode() {

		$app = \Slim\Slim::getInstance();

		$last_count = 1;

      // get last count
		$shipping = Shippings::select('id')
		->orderBy('id', 'desc')
		->first();

		if($shipping) {
			$data = $shipping->id;
			$last_count = intval($data) + 1;
		}

		$curr_count = '';
		$curr_count = sprintf('%02d', $curr_count + intval($last_count));
		$COUNTER = $curr_count;

		return $COUNTER;
	}
}