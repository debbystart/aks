<?php

class Groups extends Illuminate\Database\Eloquent\Model
{
    protected $table = 'use_group';
    protected $primaryKey = 'id';

    // protected $softDelete = true;

    protected $appends = array('user_count', 'role_count');

  //------------------------------------------------------------------------------------
  // relationships

    public function users() {
      return $this->hasMany('Users', 'user_group_id', 'id');
    }

    // Many to Many with group table will need group_role table as pivot table
    public function roles(){
        return $this->belongsToMany('Roles', 'use_group_role', 'group_id', 'role_id');
    }

  //------------------------------------------------------------------------------------
  // custom fields

    public function getUserCountAttribute() {
       $users = $this->users()->count();
       return ($users?intval($users):0);
    }

    public function getRoleCountAttribute() {
       $roles = $this->roles()->count();
       return ($roles?intval($roles):0);
    }

}