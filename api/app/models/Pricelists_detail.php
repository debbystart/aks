<?php

class Pricelists_detail extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'param_pricelist_detail';
	protected $primaryKey = 'id';

	protected $appends = array('stock', 'hppstock');

	public function stock() {
		return $this->belongsTo('Stocks');
	}

	public function getStockAttribute() {
		$stock = $this->stock()->first();
		return ($stock?$stock->qty:null);
	}

	public function getHppstockAttribute() {
		$stock = $this->stock()->first();
		return ($stock?$stock->hpp:null);
	}

	public static function getNextPricelistdetailCode() {

		$app = \Slim\Slim::getInstance();

		$last_count = 1;

		$pricelist_detail = Pricelists_detail::select('id')
		->orderBy('id', 'desc')
		->first();

		if($pricelist_detail) {
			$data = $pricelist_detail->id;
			$last_count = intval($data) + 1;
		}

		$COUNTER = sprintf(intval($last_count));

		return $COUNTER;
	}
}