<?php

class Reasonstocks extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'param_reasonstock';
	protected $primaryKey = 'id';

	public static function getNextReasonstockCode() {

		$app = \Slim\Slim::getInstance();

		$last_count = 1;

		$reasonstock = Reasonstocks::select('id')
		->orderBy('id', 'desc')
		->first();

		if($reasonstock) {
			$data = $reasonstock->id;
			$last_count = intval($data) + 1;
		}

		$curr_count = '';
		$curr_count = sprintf('%02d', $curr_count + intval($last_count));
		$COUNTER = $curr_count;

		return $COUNTER;
	}
}