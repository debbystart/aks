module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('bower.json'),
    cssmin: {
      css: {
        src: 'styles/app.css',
        dest: 'styles/app.min.css'
      }
    },
  });

  grunt.loadNpmTasks('grunt-css');

  // Default task.
  grunt.registerTask('dev', 'cssmin');

};