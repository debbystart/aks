angular.module('directives.customValidations.uniqueRoutename', ['restangular'])

.directive('uniqueRoutename', function($http, Restangular) {
  var toId;
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, elem, attr, ctrl) { 

      scope.$watch(attr.ngModel, function(value) {

        if(value) {

          Restangular.one('routes/name', value).get().then(function(user) {

            // console.log(user);
            // console.log(scope.$eval(attr.uniqueRoutename));

            if(user.id===scope.$eval(attr.uniqueRoutename))
              ctrl.$setValidity('uniqueRoutename', true);
            else
              ctrl.$setValidity('uniqueRoutename', false);            

          }, function(response) {
            ctrl.$setValidity('uniqueRoutename', true);
          });

        }

      });

    }
  }
});