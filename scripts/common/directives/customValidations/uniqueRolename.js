angular.module('directives.customValidations.uniqueRolename', ['restangular'])

.directive('uniqueRolename', function($http, Restangular) {
  var toId;
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, elem, attr, ctrl) { 

      scope.$watch(attr.ngModel, function(value) {

        if(value) {

          Restangular.one('roles/name', value).get().then(function(user) {

            // console.log(user);
            // console.log(scope.$eval(attr.uniqueRolename));

            if(user.id===scope.$eval(attr.uniqueRolename))
              ctrl.$setValidity('uniqueRolename', true);
            else
              ctrl.$setValidity('uniqueRolename', false);            

          }, function(response) {
            ctrl.$setValidity('uniqueRolename', true);
          });

        }

      });

    }
  }
});