angular.module('directives.customValidations.uniqueSuppliername', ['restangular'])

.directive('uniqueSuppliername', function($http, Restangular) {
  var toId;
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, elem, attr, ctrl) { 

      scope.$watch(attr.ngModel, function(value) {

        if(value) {

          Restangular.one('suppliers/name', value).get().then(function(user) {

            if(user.id===scope.$eval(attr.uniqueSuppliername))
              ctrl.$setValidity('uniqueSuppliername', true);
            else
              ctrl.$setValidity('uniqueSuppliername', false);            

          }, function(response) {
            ctrl.$setValidity('uniqueSuppliername', true);
          });

        }

      });

    }
  }
});