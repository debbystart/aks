angular.module('directives.customValidations.uniqueItemname', ['restangular'])

.directive('uniqueItemname', function($http, Restangular) {
  var toId;
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, elem, attr, ctrl) { 

      scope.$watch(attr.ngModel, function(value) {

        if(value) {

          Restangular.one('items/name', value).get().then(function(user) {

            if(user.id===scope.$eval(attr.uniqueItemname))
              ctrl.$setValidity('uniqueItemname', true);
            else
              ctrl.$setValidity('uniqueItemname', false);            

          }, function(response) {
            ctrl.$setValidity('uniqueItemname', true);
          });

        }

      });

    }
  }
});