angular.module('directives.customValidations.uniqueBrandname', ['restangular'])

.directive('uniqueBrandname', function($http, Restangular) {
  var toId;
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, elem, attr, ctrl) { 

      scope.$watch(attr.ngModel, function(value) {

        if(value) {

          Restangular.one('brands/name', value).get().then(function(user) {

            // console.log(user);
            // console.log(scope.$eval(attr.uniqueBrandname));

            if(user.id===scope.$eval(attr.uniqueBrandname))
              ctrl.$setValidity('uniqueBrandname', true);
            else
              ctrl.$setValidity('uniqueBrandname', false);            

          }, function(response) {
            ctrl.$setValidity('uniqueBrandname', true);
          });

        }

      });

    }
  }
});