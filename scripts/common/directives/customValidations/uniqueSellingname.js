angular.module('directives.customValidations.uniqueSellingname', ['restangular'])

.directive('uniqueSellingname', function($http, Restangular) {
  var toId;
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, elem, attr, ctrl) { 

      scope.$watch(attr.ngModel, function(value) {

        if(value) {

          Restangular.one('salesorders/name', value).get().then(function(user) {

            if(user.id===scope.$eval(attr.uniqueSellingname))
              ctrl.$setValidity('uniqueSellingname', true);
            else
              ctrl.$setValidity('uniqueSellingname', false);            

          }, function(response) {
            ctrl.$setValidity('uniqueSellingname', true);
          });

        }

      });

    }
  }
});