angular.module('directives.customValidations.uniqueCustomernumber', ['restangular'])

.directive('uniqueCustomernumber', function($http, Restangular) {
  var toId;
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, elem, attr, ctrl) { 

      scope.$watch(attr.ngModel, function(value) {

        if(value) {

          Restangular.one('customers/mobile', value).get().then(function(user) {

            if(user.id===scope.$eval(attr.uniqueCustomernumber))
              ctrl.$setValidity('uniqueCustomernumber', true);
            else
              ctrl.$setValidity('uniqueCustomernumber', false);            

          }, function(response) {
            ctrl.$setValidity('uniqueCustomernumber', true);
          });

        }

      });

    }
  }
});