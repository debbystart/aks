angular.module('directives.customValidations', [
	'directives.customValidations.integer', 
	'directives.customValidations.float',
	'directives.customValidations.uniqueEmail',
	'directives.customValidations.checkPwd',
	'directives.customValidations.uniqueNip',
	'directives.customValidations.uniqueUsername',
	'directives.customValidations.uniqueRoutename',
	'directives.customValidations.uniqueRolename',
	'directives.customValidations.uniqueItemname',
	'directives.customValidations.uniqueItemgroupname',
	'directives.customValidations.uniqueUnitname',
	'directives.customValidations.uniqueBrandname',
	'directives.customValidations.uniqueSellingname',
	'directives.customValidations.uniqueSuppliername',
	'directives.customValidations.uniqueCustomernumber',

]);