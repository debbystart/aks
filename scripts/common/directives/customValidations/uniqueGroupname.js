angular.module('directives.customValidations.uniqueGroupname', ['restangular'])

.directive('uniqueGroupname', function($http, Restangular) {
  var toId;
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, elem, attr, ctrl) { 

      scope.$watch(attr.ngModel, function(value) {

        if(value) {

          Restangular.one('groups/name', value).get().then(function(user) {

            // console.log(user);
            // console.log(scope.$eval(attr.uniqueGroupname));

            if(user.id===scope.$eval(attr.uniqueGroupname))
              ctrl.$setValidity('uniqueGroupname', true);
            else
              ctrl.$setValidity('uniqueGroupname', false);            

          }, function(response) {
            ctrl.$setValidity('uniqueGroupname', true);
          });

        }

      });

    }
  }
});