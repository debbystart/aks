angular.module('directives.customValidations.uniqueNip', ['restangular'])

.directive('uniqueNip', function($http, Restangular) {
  var toId;
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, elem, attr, ctrl) { 

      scope.$watch(attr.ngModel, function(value) {

        if(value) {

          Restangular.one('users/nip', value).get().then(function(user) {

            // console.log(user);
            // console.log(scope.$eval(attr.uniqueNip));

            if(user.id===scope.$eval(attr.uniqueNip))
              ctrl.$setValidity('uniqueNip', true);
            else
              ctrl.$setValidity('uniqueNip', false);            

          }, function(response) {
            ctrl.$setValidity('uniqueNip', true);
          });

        }

      });

    }
  }
});