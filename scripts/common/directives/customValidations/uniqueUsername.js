angular.module('directives.customValidations.uniqueUsername', ['restangular'])

.directive('uniqueUsername', function($http, Restangular) {
  var toId;
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, elem, attr, ctrl) { 

      scope.$watch(attr.ngModel, function(value) {

        if(value) {

          Restangular.one('users/username', value).get().then(function(user) {

            // console.log(user);
            // console.log(scope.$eval(attr.uniqueUsername));

            if(user.id===scope.$eval(attr.uniqueUsername))
              ctrl.$setValidity('uniqueUsername', true);
            else
              ctrl.$setValidity('uniqueUsername', false);            

          }, function(response) {
            ctrl.$setValidity('uniqueUsername', true);
          });

        }

      });

    }
  }
});