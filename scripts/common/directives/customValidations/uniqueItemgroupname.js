angular.module('directives.customValidations.uniqueItemgroupname', ['restangular'])

.directive('uniqueItemgroupname', function($http, Restangular) {
  var toId;
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, elem, attr, ctrl) { 

      scope.$watch(attr.ngModel, function(value) {

        if(value) {

          Restangular.one('itemgroups/name', value).get().then(function(user) {

            // console.log(user);
            // console.log(scope.$eval(attr.uniqueItemgroupname));

            if(user.id===scope.$eval(attr.uniqueItemgroupname))
              ctrl.$setValidity('uniqueItemgroupname', true);
            else
              ctrl.$setValidity('uniqueItemgroupname', false);            

          }, function(response) {
            ctrl.$setValidity('uniqueItemgroupname', true);
          });

        }

      });

    }
  }
});