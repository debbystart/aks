angular.module('directives.customValidations.uniqueEmail', ['restangular'])

.directive('uniqueEmail', function($http, Restangular) {
  var toId;
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, elem, attr, ctrl) { 

      scope.$watch(attr.ngModel, function(value) {

        if(value) {

          Restangular.one('users/email', value).get().then(function(user) {

            // console.log(user);
            // console.log(scope.$eval(attr.uniqueEmail));

            if(user.id===scope.$eval(attr.uniqueEmail))
              ctrl.$setValidity('uniqueEmail', true);
            else
              ctrl.$setValidity('uniqueEmail', false);            

          }, function(response) {
            ctrl.$setValidity('uniqueEmail', true);
          });

        }

      });

    }
  }
});