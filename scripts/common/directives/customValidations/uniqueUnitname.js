angular.module('directives.customValidations.uniqueUnitname', ['restangular'])

.directive('uniqueUnitname', function($http, Restangular) {
  var toId;
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, elem, attr, ctrl) { 

      scope.$watch(attr.ngModel, function(value) {

        if(value) {

          Restangular.one('units/name', value).get().then(function(user) {

            // console.log(user);
            // console.log(scope.$eval(attr.uniqueUnitname));

            if(user.id===scope.$eval(attr.uniqueUnitname))
              ctrl.$setValidity('uniqueUnitname', true);
            else
              ctrl.$setValidity('uniqueUnitname', false);            

          }, function(response) {
            ctrl.$setValidity('uniqueUnitname', true);
          });

        }

      });

    }
  }
});