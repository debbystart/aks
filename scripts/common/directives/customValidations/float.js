angular.module('directives.customValidations.float', [])

.directive('smartFloat', function() {
  /*return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      var FLOAT_REGEXP = /^\d+\.?\d+$/;
      ctrl.$parsers.unshift(function(viewValue) {
        
        if(viewValue!==undefined)
          viewValue = parseFloat(viewValue.replace(',', '.'));

        if (FLOAT_REGEXP.test(viewValue)) {
           ctrl.$setValidity('smartFloat', true);
        } else {
          ctrl.$setValidity('smartFloat', false);
        }
        return viewValue;
      });
    }
  };*/
  var FLOAT_REGEXP = /^\-?\d+((\.|\,)\d+)?$/;
  return {
      require: 'ngModel',
      link: function (scope, elm, attrs, ctrl) {
          ctrl.$parsers.unshift(function (viewValue) {
              if (FLOAT_REGEXP.test(viewValue)) {
                  ctrl.$setValidity('float', true);
                  return parseFloat(viewValue.replace(',', '.'));
              } else {
                  ctrl.$setValidity('float', false);
                  return undefined;
              }
          });

          /*ctrl.$formatters.push(function (value) {
              if (value) {
                  value = value.toFixed(2).replace('.', ',');
              }

              return value;
          })*/
      }
  };
});