angular.module('directives.crud.buttons', [])

.directive('crudButtons', function ($compile) {
  return {
    restrict:'E',
    replace:true,
    template:
    '<div>'+
      '<button type="button" class="btn btn-danger remove" ng-click="remove()" ng-show="(canRemove() && canDelete())">Delete</button> '+
      '<button ng-hide="!(canCreate() || canUpdate())" type="button" class="btn btn-link revert" ng-click="revert()" ng-disabled="!canRevert()">Revert</button> '+
      '<button ng-hide="!(canCreate() || canUpdate())" type="button" ng-disabled="!canSave()" class="btn btn-success" ng-click="save()">Save</button> '+
      '<button ng-hide="hideBack()" type="button" class="btn btn-link" ng-click="back()">Back</button> '+
    '</div>',

    compile: function(tElement, tAttrs){

      return {
        post: function(scope, element, attrs, controller, transclude) {

          if (attrs.route) {

            var rewrittenEl = '<div><button type="button" class="btn btn-danger remove" ng-click="remove()" ng-show="(canRemove(\'' + attrs.route + '\') && canDelete(\'' + attrs.route + '\'))">Delete</button> '+
            '<button ng-hide="!(canCreate(\'' + attrs.route + '\') || canUpdate(\'' + attrs.route + '\'))" type="button" class="btn btn-link revert" ng-click="revert()" ng-disabled="!canRevert()">Revert</button> '+
            '<button ng-hide="!(canCreate(\'' + attrs.route + '\') || canUpdate(\'' + attrs.route + '\'))" type="button" ng-disabled="!canSave()" class="btn btn-success" ng-click="save()">Save</button> '+
            '<button ng-hide="hideBack()" type="button" class="btn btn-link" ng-click="back()">Back</button> '+
          '</div>';

            var e = angular.element(rewrittenEl);
            $compile(e.contents())(scope);
            element.replaceWith(e);

          }
        }
      };
    }

  };
});