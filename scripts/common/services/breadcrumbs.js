angular.module('services.breadcrumbs', []);
angular.module('services.breadcrumbs').factory('breadcrumbs', ['$rootScope', '$location', function($rootScope, $location){

  var breadcrumbs = [];
  var breadcrumbsService = {};

  //we want to update breadcrumbs only when a route is actually changed
  //as $location.path() will get updated imediatelly (even if route change fails!)
  $rootScope.$on('$routeChangeSuccess', function(event, current){

    var pathElements = $location.path().split('/'), result = [], i;
    var breadcrumbPath = function (index) {
      return '#/' + (pathElements.slice(0, index + 1)).join('/');
    };

    pathElements.shift();
    for (i=0; i<pathElements.length; i++) {
      result.push({name: pathElements[i], path: breadcrumbPath(i)});
    }

    breadcrumbs = result;
  });

  breadcrumbsService.getAll = function() {
    return breadcrumbs;
  };

  breadcrumbsService.getFirst = function() {
    if(breadcrumbs.length>1)
      return breadcrumbs[1] || {};
    else
      return breadcrumbs[0] || {};
  };

  breadcrumbsService.isMatchedRoute = function(route) {
    var routes = route.split('/');
    var matchCount = 0;
    for (var i = 0; i < routes.length; i++) {
      if(i<breadcrumbs.length) {
        if(breadcrumbs[i].name === routes[i]) {
          matchCount++;
        }        
      }
    }
    if(matchCount===routes.length){
      return true;
    }
    return false;
  };

  return breadcrumbsService;
}]);