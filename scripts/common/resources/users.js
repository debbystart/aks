angular.module('resources.users', ['restangular']);
angular.module('resources.users')

	.factory('Users', ['$rootScope','Restangular', '$q', function ($rootScope, Restangular, $q) {

		var resources = {
			getList: function () {
				return Restangular.all('users').getList();
			},
			find: function(id) {
				return Restangular.one('users', id).get();
			},
			$save: function(data) {
		        $rootScope.$broadcast('users:added');
		        return Restangular.all('users').post(data);
			},
			$remove: function(data) {
				$rootScope.$broadcast('users:removed');
				return data.remove();
			},
			$update: function(data) {
				$rootScope.$broadcast('users:updated');
				return data.put();
			},
			$saveOrUpdate: function (data) {
				if(data.id) {
					return this.$update(data);
				}
				return this.$save(data);
			}
		}

		return resources;

	}]);
