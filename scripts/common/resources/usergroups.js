angular.module('resources.usergroups', ['restangular']);
angular.module('resources.usergroups')

	.factory('Usergroups', ['$rootScope','Restangular', '$q', function ($rootScope, Restangular, $q) {

		var resources = {
			getList: function () {
				return Restangular.all('usergroups').getList();
			},
			find: function(id) {
				return Restangular.one('usergroups', id).get();
			},
			$save: function(data) {
		        $rootScope.$broadcast('usergroups:added');
		        return Restangular.all('usergroups').post(data);
			},
			$remove: function(data) {
				$rootScope.$broadcast('usergroups:removed');
				return data.remove();
			},
			$update: function(data) {
				$rootScope.$broadcast('usergroups:updated');
				return data.put();
			},
			$saveOrUpdate: function (data) {
				if(data.id) {
					return this.$update(data);
				}
				return this.$save(data);
			}
		}

		return resources;

	}]);
