angular.module('resources.customers', ['restangular']);
angular.module('resources.customers')

	.factory('customers', ['$rootScope','Restangular', '$q', function ($rootScope, Restangular, $q) {

		var resources = {
			getList: function () {
				return Restangular.all('customers').getList();
			},
			find: function(id) {
				return Restangular.one('customers', id).get();
			},
			$save: function(data) {
		        $rootScope.$broadcast('customers:added');
		        return Restangular.all('customers').post(data);
			},
			$remove: function(data) {
				$rootScope.$broadcast('customers:removed');
				return data.remove();
			},
			$update: function(data) {
				$rootScope.$broadcast('customers:updated');
				return data.put();
			},
			$saveOrUpdate: function (data) {
				if(data.id) {
					return this.$update(data);
				}
				return this.$save(data);
			}
		}

		return resources;

	}]);
