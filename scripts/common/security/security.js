// Based loosely around work by Witold Szczerba - https://github.com/witoldsz/angular-http-auth
angular.module('security.service', [
  'security.retryQueue',    // Keeps track of failed requests that need to be retried once the user logs in
  'security.login',         // Contains the login form template and controller
  'ui.bootstrap.dialog'     // Used to display the login form as a modal dialog.
  ])

.factory('security', ['$http', '$q', '$location', 'securityRetryQueue', '$dialog', 'APP_CONFIG', function($http, $q, $location, queue, $dialog, APP_CONFIG) {

  // Redirect to the given url (defaults to '/')
  function redirect(url) {
    url = url || '/';
    $location.path(url);
  }

  // Login form dialog stuff
  var loginDialog = null;
  function openLoginDialog() {
    if ( loginDialog ) {
      //throw new Error('Trying to open a dialog that is already open!');
    }
    else{
      loginDialog = $dialog.dialog();
      //loginDialog.open('scripts/common/security/login/form.tpl.html', 'LoginFormController').then(onLoginDialogClose);      
    }
  }
  function closeLoginDialog(success) {
    if (loginDialog) {
      loginDialog.close(success);
    }
  }
  function onLoginDialogClose(success) {
    loginDialog = null;
    if ( success ) {
      queue.retryAll();
    } else {
      queue.cancelAll();
      redirect();
    }
  }

  // Register a handler for when an item is added to the retry queue
  queue.onItemAddedCallbacks.push(function(retryItem) {
    if ( queue.hasMore() && $location.path() !== '/home') {
      service.showLogin();
    }
  });

  // The public API of the service
  var service = {

    // Get the first reason for needing a login
    getLoginReason: function() {
      return queue.retryReason();
    },

    // Show the modal login dialog
    showLogin: function() {
      openLoginDialog();
    },

    // Attempt to authenticate a user by the given email and password
    login: function(email, password) {
      var request = $http.post('api/auth/login', {email: email, password: password, app_name: APP_CONFIG.appName, app_baseurl: APP_CONFIG.appBaseUrl});
      return request.then(function(response) {
        service.currentUser = response.data.user;
        if ( service.isAuthenticated() ) {
          closeLoginDialog(true);
        }
    //location.reload();
  });
    },

    // Give up trying to login and clear the retry queue
    cancelLogin: function() {
      closeLoginDialog(false);
      redirect();
    },

    // Logout the current user and redirect
    logout: function(redirectTo) {
      $http.post('api/auth/logout').then(function() {
        service.currentUser = null;
        window.location.replace("/aks/login.html#/home");
        // redirect(redirectTo);
    //location.reload();
  });
    },

    // Ask the backend to see if a user is already authenticated - this may be from a previous session.
    requestCurrentUser: function() {
      if ( service.isAuthenticated() ) {
        return $q.when(service.currentUser);
      } else {
        return $http.get('api/auth/isauthenticated').then(function(response) {
          service.currentUser = response.data.user;
          return service.currentUser;
        },function errorCallback(){
          if(window.location.href != "/aks/login.html#/home"){
            window.location.replace("/aks/login.html#/home");
            // console.log(window.location.href);
          }
          // console.log(window.location.href);
        });
      }
    },

    // Information about the current user
    currentUser: null,

    // Is the current user authenticated?
    isAuthenticated: function(){
      return !!service.currentUser;
    },

    isAuthorizedRoles: function(roles){
      if(!service.currentUser)
        return false;

      for (var i = 0; i < roles.length; i++) {
        if($.inArray(roles[i], service.currentUser.has_roles)>-1)
          return true;
      };
      return false;
    },
    
    // Is the current user an adminstrator?
    isAdmin: function() {
      return !!(service.currentUser && service.currentUser.admin);
    },

    /*canRead: function() {
      return !!(service.currentUser && service.currentUser.can_read===1);
    },
    canUpdate: function() {
      return !!(service.currentUser && service.currentUser.can_update===1);
    },
    canCreate: function() {
      return !!(service.currentUser && service.currentUser.can_create===1);
    },
    canDelete: function() {
      return !!(service.currentUser && service.currentUser.can_delete===1);
    },*/
    
    canCreate: function(route) {

      if(!route)
        return !!(service.currentUser);

      if(!service.currentUser)
        return false;

      var routers = service.currentUser.routers;
      for (var i = 0; i < routers.length; i++) {

        var router = routers[i];
        if(router.route===route && router.can_create)
          return true;

      };
      
      return false;
    },
    canRead: function(route) {

      if(!route)
        return !!(service.currentUser);

      if(!service.currentUser)
        return false;

      var routers = service.currentUser.routers;
      for (var i = 0; i < routers.length; i++) {

        var router = routers[i];
        if(router.route===route && router.can_read)
          return true;

      };
      
      return false;
    },
    canUpdate: function(route) {

      if(!route)
        return !!(service.currentUser && service.currentUser.can_update===1);

      if(!service.currentUser)
        return false;

      var routers = service.currentUser.routers;
      for (var i = 0; i < routers.length; i++) {

        var router = routers[i];
        if(router.route===route && router.can_update)
          return true;

      };
      
      return false;
    },
    canDelete: function(route) {

      if(!route)
        return !!(service.currentUser);

      if(!service.currentUser)
        return false;

      var routers = service.currentUser.routers;
      for (var i = 0; i < routers.length; i++) {

        var router = routers[i];
        if(router.route===route && router.can_delete)
          return true;

      };
      
      return false;
    },
    canApproved: function(route) {

      if(!service.currentUser)
        return false;

      var routers = service.currentUser.routers;
      for (var i = 0; i < routers.length; i++) {

        var router = routers[i];
        if(router.route===route && router.can_approved)
          return true;

      };
      
      return false;
    },

    isAuthorizedRoute: function(route, action){

      if(!route)
        return false;

      if(!service.currentUser)
        return false;

      var routers = service.currentUser.routers;
      for (var i = 0; i < routers.length; i++) {

        var router = routers[i];

        switch(action) {
          case 'can_create': 
          if(router.route===route && router.can_create)
            return true;
          break;
          case 'can_read': 
          if(router.route===route && router.can_read)
            return true;
          break;
          case 'can_update': 
          if(router.route===route && router.can_update)
            return true;
          break;
          case 'can_delete': 
          if(router.route===route && router.can_delete)
            return true;
          break;
          case 'can_approve': 
          if(router.route===route && router.can_approve)
            return true;
          break;
        }

      };
      
      return false;
    },

  };

  return service;
}]);