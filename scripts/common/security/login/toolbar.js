angular.module('security.login.toolbar', [])

// The loginToolbar directive is a reusable widget that can show login or logout buttons
// and information the current authenticated user
.directive('loginToolbar', ['$window','$compile', 'security' ,'$location', '$timeout', 'Restangular', '$sce', function($window, $compile, security, $location, $timeout, Restangular, $sce) {
  var directive = {
    templateUrl: 'scripts/common/security/login/toolbar.tpl.html',
    restrict: 'E',
    replace: true,
    scope: true,
    link: function($scope, $element, $attrs, $controller) {
      $scope.isAuthenticated = security.isAuthenticated;
      $scope.isAuthorizedRoles = security.isAuthorizedRoles;
      $scope.profile = function () { $location.path('/home/profile'); }
      $scope.login = security.showLogin;
      $scope.logout = security.logout;
      $scope.$watch(function() {
        return security.currentUser;
      }, function(currentUser) {
        $scope.currentUser = currentUser;
      });
	  
	  
	  //calculator
	   $scope.calculator = function() {
	   window.open("calculator.modal.tpl.html", "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, top=500, left=500, width=255, height=300");
	   }
	  
	  

      // notifications
	  $scope.rejected_non_cash = 0;
	  $scope.open = 0;
	  $scope.need_approval_non_cash = 0;
	  $scope.reject_price_list = 0;
	  $scope.need_approval_price_list = 0;
	  $scope.rejected_cash = 0;
	  $scope.need_approval_cash = 0;
	  $scope.sumnotif = 0;
      $scope.notifyUnreadCount = 99;
      $scope.notifications = [];
	  
	  $scope.noncash = function(){
		  $location.path('home/purchase_request/NonCash');
	  }
	  
	  $scope.cash = function(){
		  
	  	  $location.path('home/purchase_request/Cash');
	  } 
	  
	  $scope.opened = function(){
		  
	  	  $location.path('home/price_list');
	  }

      $scope.notificationRead = function(index) {

        if($scope.notifications.length===0)
          return;

        var notification = $scope.notifications[index];
        
        if(!notification.is_read){

          var item = $scope.notifications.one(notification.id);
          item.is_read = true;

          item.put().then(function() {
            $scope.notifyUnreadCount--;
            notification.is_read = true;
            $scope.notifications[index] = notification;
 
            $location.path($scope.notifications[index].link);
          });          
        }
        else {
          $location.path($scope.notifications[index].link);          
        }

      };
      
      $scope.deliberatelyTrustDangerousSnippet = function(message) {
        return $sce.trustAsHtml(message);
      };

      $scope.showAcceptButton = function(index) {

        if($scope.notifications.length===0)
          return false;

        var notification = $scope.notifications[index];
        
        if(notification.is_read)
         return false;

        var msg = notification.message;

        if(msg.indexOf('yang harus diuji oleh analis')>-1)
          return true;

        return false;

      };
      
     /*  (function tick(){ */

          var user_id = null;

          if($scope.currentUser) {
            user_id = $scope.currentUser.id;
			
			if($scope.currentUser.user_group_id === 1 || $scope.currentUser.user_group_id === 2 || $scope.currentUser.user_group_id === 3 || $scope.currentUser.user_group_id === 4){
				Restangular.one("purchase_requests/non_cash/rejected").getList().then(function(rejected_non_cash) {
				  
				  $scope.rejected_non_cash = rejected_non_cash.totals;
										
				})
				
			    Restangular.one("purchase_requests/cash/rejected").getList().then(function(rejected_cash) {
              
				$scope.rejected_cash = rejected_cash.totals;
			
				})
			}
			
			if($scope.currentUser.user_group_id === 1 || $scope.currentUser.user_group_id === 5 || $scope.currentUser.user_group_id === 3 || $scope.currentUser.user_group_id === 4){			
				Restangular.one("purchase_requests/non_cash/need_approval").getList().then(function(need_approval_non_cash) {
						  
					$scope.need_approval_non_cash = need_approval_non_cash.totals;
					  
				})
				
				Restangular.one("purchase_requests/cash/need_approval").getList().then(function(need_approval_cash) {
					  
					  $scope.need_approval_cash = need_approval_cash.totals;
					 
				
				})
				
			}
			if($scope.currentUser.user_group_id === 7){			
				Restangular.one("purchase_requests/non_cash/procurement/open").getList().then(function(open) {
						  
					$scope.open = open.totals;
				})
				
				Restangular.one("/price_lists/reject").getList().then(function(reject_price_list) {
						  
					$scope.reject_price_list = reject_price_list.totals;
				})
								
			}
			if($scope.currentUser.user_group_id === 5){			
								
				Restangular.one("/price_lists/need_approval").getList().then(function(need_approval_price_list) {
						  
					$scope.need_approval_price_list = need_approval_price_list.totals;
				})
								
			}
					
			
			$scope.sumnotif = Number($scope.rejected_non_cash)+ Number($scope.need_approval_non_cash) + Number($scope.rejected_cash)+ Number($scope.need_approval_cash)+ Number($scope.open)+ Number($scope.reject_price_list)+ Number($scope.need_approval_price_list);
			console.log($scope.sumnotif)

			/* $timeout(tick, 5000);   */
         } else {
           /*  $timeout(tick, 5000); */
          }
     
     /*  })(); */
    }
  };
  return directive;
}]);