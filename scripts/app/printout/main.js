angular.module('printoutapp', [

  'ui.bootstrap',
  'ui.bootstrap.tpls',
  'ui.bootstrap.tooltip',

  'ngRoute',
  'restangular',

  'services.breadcrumbs',
  'services.i18nNotifications',
  'services.httpRequestTracker',
  'security',

  'directives.barcodeGenerator',

]);

angular.module('printoutapp').constant('APP_CONFIG', {
  appName: 'Rialachas LIMS',
  appBaseUrl: 'http://localhost/rialachas-lims-v1'
});

angular.module('printoutapp').config(function(RestangularProvider) {
    RestangularProvider
      .setBaseUrl('api');
 
    RestangularProvider.setRequestInterceptor(
      function(elem, operation, what) {
    
        if (operation === 'put') {
          elem.id = undefined;
          return elem;
        }
        return elem;
    });      

});

//TODO: move those messages to a separate module
angular.module('printoutapp').constant('I18N.MESSAGES', {
  'errors.route.changeError':'Route change error',
  'login.reason.notAuthorized':"Anda tidak memiliki izin akses yang diperlukan. Apakah Anda ingin login sebagai orang lain?",
  'login.reason.notAuthenticated':"Anda harus login untuk mengakses bagian dari aplikasi.",
  'login.error.invalidCredentials': "Login gagal. Silakan periksa kredensial Anda dan coba lagi.",
  'login.error.serverError': "Ada masalah dengan otentikasi: {{exception}}."
});

angular.module('printoutapp').config(['$routeProvider', '$locationProvider', 'RestangularProvider', function ($routeProvider, $locationProvider, RestangularProvider) {
  /*$locationProvider.html5Mode(true);*/

  $routeProvider
    .when('/generate_barcodes', {
      controller: 'GenerateBarcodesCtrl',
      templateUrl: 'scripts/app/printout/generate_barcodes.tpl.html',
      resolve: {
        item: function(Restangular, $route){
          return $route.current.params;
        }
      }
    })
    .otherwise({redirectTo:'/home'});

}]);

angular.module('printoutapp').run(['security', function(security) {
  // Get the current user when the application starts
  // (in case they are still logged in from a previous session)
  security.requestCurrentUser();
}]);

//-------------------------------------------------------------------------------------------------------------

angular.module('printoutapp').controller('AppCtrl', ['$scope', 'i18nNotifications', 'localizedMessages', 'security', function($scope, i18nNotifications, security) {

  $scope.isAuthenticated = security.isAuthenticated;
  $scope.notifications = i18nNotifications;

  $scope.removeNotification = function (notification) {
    i18nNotifications.remove(notification);
  };

  $scope.$on('$routeChangeError', function(event, current, previous, rejection){
    i18nNotifications.pushForCurrentRoute('errors.route.changeError', 'error', {}, {rejection: rejection});
  });

}]);

//-------------------------------------------------------------------------------------------------------------

angular.module('printoutapp').controller('GenerateBarcodesCtrl', [ '$timeout', '$scope', '$window', '$location', '$route', 'security', 'APP_CONFIG', 'Restangular', 'item', 'i18nNotifications', function($timeout, $scope, $window, $location, $route, security, APP_CONFIG, Restangular, item, i18nNotifications) {

  var original = item.id;

  $scope.items = original;

  console.log(original);

  $timeout(function() {
    //$window.print();    
  });

}])

//-------------------------------------------------------------------------------------------------------------

;