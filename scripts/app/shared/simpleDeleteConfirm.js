angular.module('shared.dialog.delete-factory', ['ui.bootstrap.dialog','shared.dialog.delete', 'restangular','services.i18nNotifications']);
angular.module('shared.dialog.delete-factory')
  .factory('simpleDeleteConfirm', ['$http', '$q', '$location', '$dialog', 'Restangular', 'i18nNotifications', function($http, $q, $location, $dialog, Restangular, i18nNotifications){

    // dialog
    var Dialog = null;

    function openDialog(record, nextRoute) {
      if ( Dialog ) {
        throw new Error('Trying to open a dialog that is already open!');
      }

      simpleDeleteConfirm.record = Restangular.copy(record);
      simpleDeleteConfirm.nextRoute = nextRoute;
      
      Dialog = $dialog.dialog();
      Dialog.open('scripts/app/shared/simpledeleteconfirmation.tpl.html', 'SimpleDeleteConfirmCtrl').then(onDialogClose);
    }

    function closeDialog(success) {
      if (Dialog) {

        if(success) {
          simpleDeleteConfirm.record.remove().then(function() {
            i18nNotifications.pushForNextRoute('crud.remove.success', 'success', {id:null});
            $location.path(simpleDeleteConfirm.nextRoute);
            simpleDeleteConfirm.record = null;
            simpleDeleteConfirm.nextRoute = null;
          });          
        }

        Dialog.close(success);
        Dialog = null;

      }
    }

    function onDialogClose(success) {
      Dialog = null;
      if ( success ) {
        console.log('simpleDeleteConfirm():true');        
      } else {
        console.log('simpleDeleteConfirm():false');
      }
    }

    var simpleDeleteConfirm = {
        record: null,
        nextRoute: null,
        // Show the modal login dialog
        openDialog: function(record, nextRoute) {
          openDialog(record, nextRoute);
        },
        closeDialog: function(success) {
            closeDialog(success);
        }
    };

    return simpleDeleteConfirm;
  }]);


angular.module('shared.dialog.delete', ['services.localizedMessages', 'shared.dialog.delete-factory']);
angular.module('shared.dialog.delete')
  .controller('SimpleDeleteConfirmCtrl', ['$scope', 'localizedMessages', 'simpleDeleteConfirm', function($scope, localizedMessages, simpleDeleteConfirm) {

    $scope.ok = function() {
       simpleDeleteConfirm.closeDialog(true);
    }

    $scope.cancel = function() {
       simpleDeleteConfirm.closeDialog(false);
    };
    
  }]);

angular.module('shared.dialog', ['shared.dialog.delete', 'shared.dialog.delete-factory']);
