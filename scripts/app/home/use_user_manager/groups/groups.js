angular.module('user_manager.groups', [

	'ui.bootstrap.datepicker',
	'ui.bootstrap.typeahead',
	'ui.bootstrap.modal',

	'restangular',

	'services.i18nNotifications'
])

.config(['$routeProvider', 'RestangularProvider', function ($routeProvider, RestangularProvider) {

  $routeProvider
    .when('/home/group', {
      controller: 'GroupListCtrl',
      templateUrl: 'scripts/app/home/use_user_manager/groups/list.tpl.html'
    })
    .when('/home/group/edit/:id', {
      controller: 'GroupEditCtrl',
      templateUrl: 'scripts/app/home/use_user_manager/groups/detail.tpl.html',
      resolve: {
        item: function(Restangular, $route){
          return Restangular.one('groups', $route.current.params.id).get({expands:'roles'});
        }
      }
    })
    .when('/home/group/create', {
      controller: 'GroupCreateCtrl',
      templateUrl: 'scripts/app/home/use_user_manager/groups/detail.tpl.html'
    })
    .otherwise({redirectTo:'/home'});

    RestangularProvider.setRestangularFields({
      id: 'id'
    });

    RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
      if (operation === 'put') {
        elem._id = undefined;
            return elem;
      }
      return elem;
    });
}])

.controller('GroupListCtrl', ['$http', '$route', '$scope', '$location', 'Restangular', 'security', 'i18nNotifications', '$dialog', function ($http, $route, $scope, $location, Restangular, security, i18nNotifications, $dialog) {

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;
  $scope.isAuthorizedRoute = security.isAuthorizedRoute;

  $scope.create = function() {
    $location.path('home/group/create');
  }

  $scope.canSelect = function() {
    return ($scope.gridOptions.selectedItems.length===0?false:true);
  }

  $scope.select = function () {
    $modalInstance.close($scope.gridOptions.selectedItems[0]);
  };

  $scope.back = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.detail = function(){

    var selectedItem = $scope.gridOptions.selectedItems[0];

    $location.path('home/group/edit/'+selectedItem.id);

  }
  $scope.listUser = function(){

    var selectedItem = $scope.gridOptions.selectedItems[0];

    $location.path('home/user').search({where:'user_group_id='+selectedItem.id});

  }

  //--------------------------------------------------------------------------------------------------------------------------------------
  /* ng-Grid */  

  $scope.filterOptions = {
        filterText: '',
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [20, 50, 100],
        pageSize: 50,
        currentPage: 1
    };  
    $scope.setPagingData = function(data, page, pageSize){  
        var pagedData = data.records;
        $scope.myData = pagedData;
        $scope.totalServerItems = data.total;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        setTimeout(function () {
            var custom_get = {};

            if($route.current.params)
              custom_get = $route.current.params;

            custom_get.offset = (page - 1) * pageSize; 
            custom_get.limit = pageSize;

            if (searchText) {
                var ft = searchText.toUpperCase();
                custom_get.filter = ft;
                custom_get.filter_fields = 'name';

                Restangular.all("groups").getList(custom_get).then(function(groups) {
                  $scope.setPagingData(groups,page,pageSize);
                });
            } 
            else {
                Restangular.all("groups").getList(custom_get).then(function(groups) {
                  $scope.setPagingData(groups,page,pageSize);
                });
            }
        }, 100);
    };
  
    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, null);
  
    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
          $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
          $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
 
    $scope.gridOptions = {
      data: 'myData',
      enablePaging: true,
      showFooter: true,
      multiSelect: false,
      selectedItems: [],
      totalServerItems:'totalServerItems',
      pagingOptions: $scope.pagingOptions,
      filterOptions: $scope.filterOptions,
      enableColumnResize: true,
      columnDefs: [{ field: 'name', displayName: 'Name', width: '350'},
                   { field: 'description', displayName: 'Description', width: '300'},
                   { field: 'code', displayName: 'Code', width: '250'},
                   { field: 'role_count', displayName: 'Total Role', width: '250', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ row.getProperty(col.field) }}</span> role{{ (row.getProperty(col.field)>1?\'s\' : \'\') }}</div>' },
                   { field: 'user_count', displayName: 'Total User', width: '250', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ row.getProperty(col.field) }}</span> user{{ (row.getProperty(col.field)>1?\'s\' : \'\') }}</div>' },
                   { field: 'is_active', displayName: 'Active', width: '200', cellClass: 'text-center', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ (row.getProperty(col.field)==1?\'Ya\' : \'Tidak\') }}</span></div>' }]
    };

  //--------------------------------------------------------------------------------------------------------------------------------------

}])

.controller('GroupEditCtrl', ['$window', '$timeout', '$scope', '$location', 'Restangular', 'item', 'i18nNotifications', '$dialog', 'security', 'simpleDeleteConfirm', function ($window, $timeout, $scope, $location, Restangular, item, i18nNotifications, $dialog, security, simpleDeleteConfirm) {

  //debugger;

  for (var i = 0; i < item.roles.length; i++) {
    delete item.roles[i].pivot;
  };


  var original = item;
  $scope.group = Restangular.copy(original);

  Restangular.all("roles").getList({orderby: 'name asc'}).then(function(roles) {
    $scope.roles = roles;
  });

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;
  $scope.isAuthorizedRoute = security.isAuthorizedRoute;

  $scope.back = function() {
    $window.history.back();
  }

  $scope.canSave = function() {
    return $scope.groupForm.$valid && 
      !angular.equals($scope.group, original);
  }
  $scope.save = function() {
    $scope.group.put().then(function() {
      i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : $scope.group.no_nppr});
      $location.path('/home/group');
    });
  };

  $scope.canRemove = function() {
    return $scope.canDelete;
  }
  $scope.remove = function() {
    simpleDeleteConfirm.openDialog(original, '/home/group');
  };

  $scope.canRevert = function() {
    return !angular.equals($scope.group, original);
  }
  $scope.revert = function() {
    $scope.group =  Restangular.copy(original);
    $scope.groupForm.$setPristine();
  }

}])

.controller('GroupCreateCtrl', ['$window', '$scope', '$location', 'Restangular', 'security', 'i18nNotifications', function ($window, $scope, $location, Restangular, security, i18nNotifications) {
  //debugger;
  var original = {
    is_active: 1
  };

  $scope.group =  Restangular.copy(original);

  Restangular.all("roles").getList({orderby:'name asc'}).then(function(roles) {
    $scope.roles = roles;
  });

  // group permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;
  $scope.isAuthorizedRoute = security.isAuthorizedRoute;

  $scope.isRoleChecked = function(role_id) {
    debugger;

    for (var i = 0; i < $scope.group.roles.length; i++) {
      if($scope.group.roles[i].id === role_id)
        return true;
    };

    return false;
  }

  $scope.canRemove = function() {
    return false;
  }
  $scope.canSave = function() {
  	return $scope.groupForm.$valid && 
      !angular.equals($scope.group, original);
  };

  $scope.back = function() {
    $window.history.back();
  }

  $scope.save = function() {
    Restangular.all('groups').post($scope.group).then(function(group) {
      i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : null});
      $location.path('/home/group');
    });
  }

  $scope.canRevert = function() {
    return $scope.groupForm.$valid && 
      !angular.equals($scope.group, original);
  }
  $scope.revert = function() {
    $scope.group =  Restangular.copy(original);
    $scope.groupForm.$setPristine();
  }

}]);

