angular.module('user_manager', [
	'user_manager.users',
	'user_manager.groups',
	'user_manager.roles'
]);