angular.module('user_manager.users', [

	'ui.bootstrap.datepicker',
	'ui.bootstrap.typeahead',
	'ui.bootstrap.modal',

	'restangular',

	'services.i18nNotifications'
	])

.config(['$routeProvider', 'RestangularProvider', function ($routeProvider, RestangularProvider) {

	$routeProvider
	.when('/home/user', {
		controller: 'UserListCtrl',
		templateUrl: 'scripts/app/home/use_user_manager/users/list.tpl.html'
	})
	.when('/home/user/edit/:id', {
		controller: 'UserEditCtrl',
		templateUrl: 'scripts/app/home/use_user_manager/users/detail.tpl.html',
		resolve: {
			item: function(Restangular, $route){
				return Restangular.one('users', $route.current.params.id).get();
			}
		}
	})
	.when('/home/user/changepwd/:id', {
		controller: 'UserChangePwdCtrl',
		templateUrl: 'scripts/app/home/use_user_manager/users/changepwd.tpl.html',
		resolve: {
			item: function(Restangular, $route){
				return Restangular.one('users', $route.current.params.id).get();
			}
		}
	})
	.when('/home/user/create', {
		controller: 'UserCreateCtrl',
		templateUrl: 'scripts/app/home/use_user_manager/users/detail.tpl.html'
	})
	.otherwise({redirectTo:'/home'});

	RestangularProvider.setRestangularFields({
		id: 'id'
	});

	RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
		if (operation === 'put') {
			elem._id = undefined;
			return elem;
		}
		return elem;
	});
}])

.controller('UserListCtrl', ['$http', '$scope', '$location', 'Restangular', 'security', 'i18nNotifications', '$dialog', '$route','$window', function ($http, $scope, $location, Restangular, security, i18nNotifications, $dialog, $route, $window) {

	$scope.curr_user = {};

	$scope.$watch(function() {
		return security.currentUser;
	}, function(currentUser) {
		if(currentUser!==null){
			$scope.curr_user = currentUser;
		}
	});

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;
  $scope.isAuthorizedRoute = security.isAuthorizedRoute;

  $scope.create = function() {
  	$location.path('home/user/create');
  }

  $scope.canSelect = function() {
  	return ($scope.gridOptions.selectedItems.length===0?false:true);
  }

  $scope.select = function () {
  	$modalInstance.close($scope.gridOptions.selectedItems[0]);
  };

  $scope.back = function () {
  	$modalInstance.dismiss('cancel');
  };

  $scope.detail = function(){
  	var selectedItem = $scope.gridOptions.selectedItems[0];
  	$location.path('home/user/edit/'+selectedItem.id);
  }

  $scope.canChangePwd = function() {
  	return ($scope.gridOptions.selectedItems.length===0?false:true) && $scope.canCreate && $scope.canUpdate;
  }

  $scope.changePwd = function () {
  	var selectedItem = $scope.gridOptions.selectedItems[0];

    if($scope.curr_user) {
    	if($scope.curr_user.id === selectedItem.id)
    		$location.path('home/profile');
    	else
    		$location.path('home/user/changepwd/'+selectedItem.id);
    }
  };

  //
  $scope.change_filter = function(){
   $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
 }

 $scope.filterOptions = {
   filterText: null,
   useExternalFilter: true
 };
 $scope.totalServerItems = 0;
 $scope.pagingOptions = {
   pageSizes: [20, 500, 100],
   pageSize: 20,
   currentPage: 1
 };  
 $scope.setPagingData = function(data, page, pageSize){  
   var pagedData = data.records;
   $scope.myData = pagedData;
   $scope.totalServerItems = data.total;
   if (!$scope.$$phase) {
    $scope.$apply();}
  };

  $scope.getPagedDataAsync = function (pageSize, page, searchText) {
   setTimeout(function () {
    var custom_get = {};
    console.log($scope.filterOptions.filterText)

    if($route.current.params)
     custom_get = $route.current.params;

   custom_get.offset = (page - 1) * pageSize; 
   custom_get.limit = pageSize;

   if (searchText='$scope.filterOptions.filterText') {
    custom_get.filter = $scope.filterOptions.filterText;
    custom_get.filter_fields = 'user.name,email';

    Restangular.all("users").getList(custom_get).then(function(users) {
     $scope.setPagingData(users,page,pageSize);
   });
  } 
  else {
   Restangular.all("users").getList(custom_get).then(function(users) {
    $scope.setPagingData(users,page,pageSize);
  });
 }},100);
 };

 $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

 $scope.$watch('pagingOptions', function (newVal, oldVal) {
   if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
  }}, true);

 $scope.gridOptions = {
   data: 'myData',
   enablePaging: true,
   showFooter: true,
   multiSelect: false,
   selectedItems: [],
   totalServerItems:'totalServerItems',
   pagingOptions: $scope.pagingOptions,
   filterOptions: $scope.filterOptions,
   enableColumnResize: true,
   columnDefs: [{ field: 'name', displayName: 'Name', width: '450'},
   { field: 'username', displayName: 'Username', width: '350'},
   { field: 'email', displayName: 'Email', width: '300'},
   { field: 'group', displayName: 'Group', width: '300'},
   { field: 'active', displayName: 'Active', width: '200', cellClass: 'text-center', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ (row.getProperty(col.field)==1?\'Ya\' : \'Tidak\') }}</span></div>' }]
 };

}])

.controller('UserEditCtrl', ['$window', '$timeout', '$modal', '$scope', '$location', 'Restangular', 'item', 'i18nNotifications', '$dialog', 'security', 'simpleDeleteConfirm', function ($window, $timeout, $modal, $scope, $location, Restangular, item, i18nNotifications, $dialog, security, simpleDeleteConfirm) {

	//debugger;

	var original = item;
	$scope.user = Restangular.copy(original);

	Restangular.all("groups").getList().then(function(usergroups) {
		$scope.usergroups = usergroups.records;
	});
	
	Restangular.all("companys").getList().then(function(companys) {
		$scope.companys = companys.records;
	});

	// user permissions
	$scope.canCreate    = security.canCreate;
	$scope.canUpdate    = security.canUpdate;
	$scope.canRead      = security.canRead;
	$scope.canDelete    = security.canDelete;

	$scope.back = function() {
		$window.history.back();
	}

	$scope.canSave = function() {
		return $scope.userForm.$valid && 
		!angular.equals($scope.user, original);
	}
	$scope.save = function() {
		$scope.user.put().then(function() {
			i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : null});
			$location.path('/home/user');
		});
	};

	$scope.canRemove = function() {
		return $scope.canDelete && original.id!=1;
	}
	$scope.remove = function() {
		simpleDeleteConfirm.openDialog(original, '/home/user');
	};

	$scope.canRevert = function() {
		return $scope.userForm.$valid && 
		!angular.equals($scope.user, original);
	}
	$scope.revert = function() {
		$scope.user =  Restangular.copy(original);
		$scope.userForm.$setPristine();
	}

}])

.controller('UserChangePwdCtrl', ['$window', '$timeout', '$scope', '$location', 'Restangular', 'item', 'i18nNotifications', '$dialog', 'security', 'simpleDeleteConfirm', function ($window, $timeout, $scope, $location, Restangular, item, i18nNotifications, $dialog, security, simpleDeleteConfirm) {

  //debugger;

  var original = item;
  $scope.user = Restangular.copy(original);

  //----------------------------------------------------------------------------------------------

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.back = function() {
  	$window.history.back();
  }

  $scope.canSave = function() {
  	return $scope.changePwdUserForm.$valid && 
  	!angular.equals($scope.user, original);
  }
  $scope.save = function() {
  	$scope.user.put().then(function(response) {
  		if(response)
  			i18nNotifications.pushForCurrentRoute('crud.changePwd.error', 'error', {error : response.error});
  		else {
  			i18nNotifications.pushForNextRoute('crud.changePwd.success', 'success', {id : $scope.user.id});
  			$location.path('/home/user');
  		}

  	});
  };

  $scope.canRemove = function() {
  	return false;
  }
  $scope.remove = function() {
  };

  $scope.canRevert = function() {
  	return !angular.equals($scope.user, original);
  }
  $scope.revert = function() {
  	$scope.user =  Restangular.copy(original);
  	$scope.changePwdUserForm.$setPristine();
  }

}])

.controller('UserCreateCtrl', ['$window', '$scope', '$location', 'Restangular', '$modal', 'security', 'i18nNotifications', function ($window, $scope, $location, Restangular, $modal, security, i18nNotifications) {
  //debugger;
  var original = {};
  original.user_group_id = 1;
  original.user_projects = [];
  
  $scope.user =  Restangular.copy(original);

  Restangular.all("groups").getList().then(function(usergroups) {
  	$scope.usergroups = usergroups.records;
  });

  Restangular.all("companys").getList().then(function(companys) {
  	$scope.companys = companys.records;
  });

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.canRemove = function() {
  	return false;
  }
  $scope.canSave = function() {
  	return $scope.userForm.$valid && 
  	!angular.equals($scope.user, original);
  };

  $scope.back = function() {
  	$window.history.back();
  }

  $scope.save = function() {

  	$scope.user.new_pwd = $scope.user.nip;
  	$scope.user.retype_new_pwd = $scope.user.nip;

  	Restangular.all('users').post($scope.user).then(function(user) {
  		i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : null});
  		$location.path('/home/user');
  	});
  }

  $scope.canRevert = function() {
  	return $scope.userForm.$valid && 
  	!angular.equals($scope.user, original);
  }
  $scope.revert = function() {
  	$scope.user =  Restangular.copy(original);
  	$scope.userForm.$setPristine();
  }

}]);

