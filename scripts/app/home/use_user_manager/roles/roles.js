angular.module('user_manager.roles', [

	'ui.bootstrap.datepicker',
	'ui.bootstrap.typeahead',
	'ui.bootstrap.modal',

	'restangular',

	'services.i18nNotifications'
])

.config(['$routeProvider', 'RestangularProvider', function ($routeProvider, RestangularProvider) {

  $routeProvider
    .when('/home/role', {
      controller: 'RoleListCtrl',
      templateUrl: 'scripts/app/home/use_user_manager/roles/list.tpl.html'
    })
    .when('/home/role/edit/:id', {
      controller: 'RoleEditCtrl',
      templateUrl: 'scripts/app/home/use_user_manager/roles/detail.tpl.html',
      resolve: {
        item: function(Restangular, $route){
          return Restangular.one('roles', $route.current.params.id).get({expands:'route'});
        }
      }
    })
    .when('/home/role/create', {
      controller: 'RoleCreateCtrl',
      templateUrl: 'scripts/app/home/use_user_manager/roles/detail.tpl.html'
    })
    .otherwise({redirectTo:'/home'});

    RestangularProvider.setRestangularFields({
      id: 'id'
    });

    RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
      if (operation === 'put') {
        elem._id = undefined;
            return elem;
      }
      return elem;
    });
}])

.controller('RoleListCtrl', ['$http', '$route', '$scope', '$location', 'Restangular', 'security', 'i18nNotifications', '$dialog', function ($http, $route, $scope, $location, Restangular, security, i18nNotifications, $dialog) {

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;
  $scope.isAuthorizedRoute = security.isAuthorizedRoute;

  $scope.create = function() {
    $location.path('home/role/create');
  }

  $scope.canSelect = function() {
    return ($scope.gridOptions.selectedItems.length===0?false:true);
  }

  $scope.select = function () {
    $modalInstance.close($scope.gridOptions.selectedItems[0]);
  };

  $scope.back = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.detail = function(){

    var selectedItem = $scope.gridOptions.selectedItems[0];

    $location.path('home/role/edit/'+selectedItem.id);

  }
  $scope.listGroup = function(){
    $location.path('home/group'); //.search({where:'role_id='+selectedItem.id});

  }

  //--------------------------------------------------------------------------------------------------------------------------------------
  /* ng-Grid */  

  $scope.filterOptions = {
        filterText: '',
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [50, 100, 150],
        pageSize: 50,
        currentPage: 1
    };  
    $scope.setPagingData = function(data, page, pageSize){  
        var pagedData = data.records;
        $scope.myData = pagedData;
        $scope.totalServerItems = data.total;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        setTimeout(function () {
            var custom_get = {};

            if($route.current.params)
              custom_get = $route.current.params;

            custom_get.offset = (page - 1) * pageSize; 
            custom_get.limit = pageSize;

            if (searchText) {
                var ft = searchText.toUpperCase();
                custom_get.filter = ft;
                custom_get.filter_fields = 'name';

                Restangular.all("roles").getList(custom_get).then(function(roles) {
                  $scope.setPagingData(roles,page,pageSize);
                });
            } 
            else {
                Restangular.all("roles").getList(custom_get).then(function(roles) {
                  $scope.setPagingData(roles,page,pageSize);
                });
            }
        }, 100);
    };
  
    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, null);
  
    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
          $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
          $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
 
    $scope.gridOptions = {
      data: 'myData',
      enablePaging: true,
      showFooter: true,
      multiSelect: false,
      selectedItems: [],
      totalServerItems:'totalServerItems',
      pagingOptions: $scope.pagingOptions,
      filterOptions: $scope.filterOptions,
      enableColumnResize: true,
      columnDefs: [
          { field: 'name', displayName: 'Role Name', width: '300'},
          { field: 'route_name', displayName: 'Route Name', width: '300'},
          { field: 'can_create', displayName: 'Can Create', width: '200', cellClass: 'text-center', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ (row.getProperty(col.field)==1?\'Ya\' : \'Tidak\') }}</span></div>' },
          { field: 'can_read', displayName: 'Can Read', width: '200', cellClass: 'text-center', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ (row.getProperty(col.field)==1?\'Ya\' : \'Tidak\') }}</span></div>' },
          { field: 'can_edit', displayName: 'Can Update', width: '200', cellClass: 'text-center', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ (row.getProperty(col.field)==1?\'Ya\' : \'Tidak\') }}</span></div>' },
          { field: 'can_delete', displayName: 'Can Delete', width: '200', cellClass: 'text-center', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ (row.getProperty(col.field)==1?\'Ya\' : \'Tidak\') }}</span></div>' },
          { field: 'is_active', displayName: 'Active', width: '200', cellClass: 'text-center', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ (row.getProperty(col.field)==1?\'Ya\' : \'Tidak\') }}</span></div>' },
      ]
    };

  //--------------------------------------------------------------------------------------------------------------------------------------

}])

.controller('RoleEditCtrl', ['$window', '$timeout', '$scope', '$location', 'Restangular', 'item', 'i18nNotifications', '$dialog', 'security', 'simpleDeleteConfirm', function ($window, $timeout, $scope, $location, Restangular, item, i18nNotifications, $dialog, security, simpleDeleteConfirm) {

  //debugger;
  var original = item;

  // checkbox must be in boolean format
  original.can_create = !(!original.can_create);
  original.can_read = !(!original.can_read);
  original.can_edit = !(!original.can_edit);
  original.can_delete = !(!original.can_delete);
  original.can_approve = !(!original.can_approve);

  $scope.role = Restangular.copy(original);

  Restangular.all("routes").getList({orderby:'name asc'}).then(function(routes) {
    $scope.routes = routes.records;
  });

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;
  $scope.isAuthorizedRoute = security.isAuthorizedRoute;

  $scope.back = function() {
    $window.history.back();
  }

  $scope.canSave = function() {
    return $scope.roleForm.$valid && 
      !angular.equals($scope.role, original);
  }
  $scope.save = function() {
    $scope.role.put().then(function() {
      i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : $scope.role.no_nppr});
      $location.path('/home/role');
    });
  };

  $scope.canRemove = function() {
    return $scope.canDelete;
  }
  $scope.remove = function() {
    simpleDeleteConfirm.openDialog(original, '/home/role');
  };

  $scope.canRevert = function() {
    return !angular.equals($scope.role, original);
  }
  $scope.revert = function() {
    $scope.role =  Restangular.copy(original);
    $scope.roleForm.$setPristine();
  }

}])

.controller('RoleCreateCtrl', ['$window', '$scope', '$location', 'Restangular', 'security', 'i18nNotifications', function ($window, $scope, $location, Restangular, security, i18nNotifications) {
  //debugger;
  var original = {
    is_active: 1
  };

  $scope.role =  Restangular.copy(original);

  Restangular.all("routes").getList({orderby: 'name asc'}).then(function(routes) {
    $scope.routes = routes.records;
  });

  // role permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;
  $scope.isAuthorizedRoute = security.isAuthorizedRoute;

  $scope.isRoleChecked = function(role_id) {
    debugger;

    for (var i = 0; i < $scope.role.roles.length; i++) {
      if($scope.role.roles[i].id === role_id)
        return true;
    };

    return false;
  }

  $scope.canRemove = function() {
    return false;
  }
  $scope.canSave = function() {
  	return $scope.roleForm.$valid && 
      !angular.equals($scope.role, original);
  };

  $scope.back = function() {
    $window.history.back();
  }

  $scope.save = function() {
    Restangular.all('roles').post($scope.role).then(function(role) {
      i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : null});
      $location.path('/home/role');
    });
  }

  $scope.canRevert = function() {
    return $scope.roleForm.$valid && 
      !angular.equals($scope.role, original);
  }
  $scope.revert = function() {
    $scope.role =  Restangular.copy(original);
    $scope.roleForm.$setPristine();
  }

}]);

