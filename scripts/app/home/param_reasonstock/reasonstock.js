angular.module('reasonstock', [
  'shared.dialog',
  'restangular',
  'services.i18nNotifications'
  ])

.config(['$routeProvider', 'RestangularProvider', function ($routeProvider, RestangularProvider) {

  $routeProvider
  .when('/home/reasonstock', {
    controller: 'ReasonstockListCtrl',
    templateUrl: 'scripts/app/home/param_reasonstock/list.tpl.html'
  })
  .when('/home/reasonstock/edit/:id', {
    controller: 'ReasonstockEditCtrl',
    templateUrl: 'scripts/app/home/param_reasonstock/detail.tpl.html',
    resolve: {
      item: function(Restangular, $route){
        return Restangular.one('reasonstocks', $route.current.params.id).get();
      }
    }
  })
  .when('/home/reasonstock/create', {
    controller: 'ReasonstockCreateCtrl',
    templateUrl: 'scripts/app/home/param_reasonstock/detail.tpl.html'
  })
  .otherwise({redirectTo:'/home'});

  RestangularProvider.setRestangularFields({
    id: 'id'
  });

  RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
    if (operation === 'put') {
      elem._id = undefined;
      return elem;
    }
    return elem;
  });
}])

.controller('ReasonstockListCtrl', ['$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function ($scope, $route, $location, Restangular, security, i18nNotifications) {

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.create = function() {
    $location.path('home/reasonstock/create');
  }

  $scope.canSelect = function() {
    return ($scope.gridOptions.selectedItems.length===0?false:true);
  }

  $scope.select = function () {
    $modalInstance.close($scope.gridOptions.selectedItems[0]);
  };

  $scope.back = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.detail = function(row){
    $location.path('home/reasonstock/edit/'+row);
  }

  $scope.filterOptions = {
    filterText: "",
    useExternalFilter: true
  };
  $scope.totalServerItems = 0;
  $scope.pagingOptions = {
    pageSizes: [20, 50, 100],
    pageSize: 50,
    currentPage: 1
  };  
  $scope.setPagingData = function(data, page, pageSize){  
    var pagedData = data.records;
    $scope.myData = pagedData;
    $scope.totalServerItems = data.total;
    if (!$scope.$$phase) {
      $scope.$apply();
    }
  };
  $scope.getPagedDataAsync = function (pageSize, page, searchText) {
    setTimeout(function () {
      var custom_get = {};

      if($route.current.params)
        custom_get = $route.current.params;

      custom_get.offset = (page - 1) * pageSize; 
      custom_get.limit = pageSize;

      if (searchText='$scope.filterOptions.filterText') {

        custom_get.filter = $scope.filterOptions.filterText;
        custom_get.filter_fields = 'id,name';

        Restangular.all("reasonstocks").getList(custom_get).then(function(reasonstocks) {
          $scope.setPagingData(reasonstocks,page,pageSize);
        });
      } 
      else {
        Restangular.all("reasonstocks").getList(custom_get).then(function(reasonstocks) {
          $scope.setPagingData(reasonstocks,page,pageSize);
        });
      }
    }, 100);
  };

  $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

  $scope.$watch('pagingOptions', function (newVal, oldVal) {
    if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
    }
  }, true);
  $scope.$watch('filterOptions', function (newVal, oldVal) {
    if (newVal !== oldVal) {
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
    }
  }, true);

  $scope.gridOptions = {
    data: 'myData',
    enablePaging: true,
    showFooter: true,
    multiSelect: false,
    selectedItems: [],
    totalServerItems:'totalServerItems',
    pagingOptions: $scope.pagingOptions,
    filterOptions: $scope.filterOptions,
    columnDefs: [
    { field: 'id', displayName: 'Code', width: 100, cellClass:'text-center' },
    { field: 'name', displayName: 'Name', width: 1000 },
    { field: 'created_at', displayName: 'Date', width: 200, cellClass:'text-center' },
    { field: 'is_active', displayName: 'Active', width: 150, cellClass:'text-center', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ (row.getProperty(col.field)===1?\'Yes\' : \'No\') }}</span></div>'},
    {displayName: 'Action', cellClass:'text-center',
    cellTemplate: 	'<div class="ngCellText" ng-class="col.colIndex()"><a class="btn btn-xs btn-default" ng-href="" ng-click="detail(row.entity.id)" data-toggle="tooltip" title="edit" data-original-title="Edit Client"><i class="fa fa-pencil"></i></a></div>'}
    ]
  };

}])

.controller('ReasonstockCreateCtrl', ['$window', '$scope', '$location', 'Restangular', 'security', 'i18nNotifications', '$modal', 'APP_CONFIG', '$upload', '$timeout', '$dialogs', function ($window, $scope, $location, Restangular, security, i18nNotifications, $modal, APP_CONFIG, $upload, $timeout, $dialogs) {

  //debugger;
  var original = {};
  original.is_active = true;

  $scope.reasonstock = Restangular.copy(original);

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.canRemove = function() {
    return false;
  };

  $scope.canSave = function() {
  	return $scope.ReasonstockForm.$valid && 
    !angular.equals($scope.reasonstock, original);
  };

  $scope.back = function() {
    $window.history.back();
  }

  $scope.save = function() {
    Restangular.all('reasonstocks').post($scope.reasonstock).then(function(reasonstock) {
      i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : reasonstock.id});
      $location.path('/home/reasonstock');
    });
  }

  $scope.canRevert = function() {
    return $scope.ReasonstockForm.$valid && 
    !angular.equals($scope.reasonstock, original);
  }

  $scope.revert = function() {
    var original = {};
    original.is_active = true;
    $scope.reasonstock = Restangular.copy(original);
    $scope.ReasonstockForm.$setPristine();
  }

}])

.controller('ReasonstockEditCtrl', ['$window', '$scope', '$location', 'Restangular', 'item', 'i18nNotifications', '$dialogs', 'security', 'simpleDeleteConfirm', '$modal', 'APP_CONFIG', '$upload', '$timeout', function ($window, $scope, $location, Restangular, item, i18nNotifications, $dialogs, security, simpleDeleteConfirm, $modal, APP_CONFIG, $upload, $timeout) {

  //debugger;
  var original = item;
  $scope.reasonstock = Restangular.copy(original);

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.back = function() {
    $window.history.back();
  }

  $scope.canSave = function() {
    return $scope.ReasonstockForm.$valid && 
    !angular.equals($scope.reasonstock, original);
  }
  
  $scope.save = function() {
    $scope.reasonstock.put().then(function() {
      i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : $scope.reasonstock.id});
      $location.path('/home/reasonstock');
    });
  };
  
  $scope.canRemove = function() {
    return true;
  }
  $scope.remove = function() {
    simpleDeleteConfirm.openDialog(original, '/home/reasonstock');
  };

  $scope.canRevert = function() {
    return !angular.equals($scope.reasonstock, original);
  }
  $scope.revert = function() {
    $scope.reasonstock =  Restangular.copy(original);
    $scope.ReasonstockForm.$setPristine();
  }

}]);

