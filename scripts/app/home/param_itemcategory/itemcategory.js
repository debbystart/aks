angular.module('itemcategory', [
  'shared.dialog',
  'restangular',
  'services.i18nNotifications'
  ])

.config(['$routeProvider', 'RestangularProvider', function ($routeProvider, RestangularProvider) {

  $routeProvider
  .when('/home/itemcategory', {
    controller: 'ItemcategoryListCtrl',
    templateUrl: 'scripts/app/home/param_itemcategory/list.tpl.html'
  })
  .when('/home/itemcategory/edit/:id', {
    controller: 'ItemcategoryEditCtrl',
    templateUrl: 'scripts/app/home/param_itemcategory/detail.tpl.html',
    resolve: {
      item: function(Restangular, $route){
        return Restangular.one('itemcategorys', $route.current.params.id).get();
      }
    }
  })
  .when('/home/itemcategory/create', {
    controller: 'ItemcategoryCreateCtrl',
    templateUrl: 'scripts/app/home/param_itemcategory/detail.tpl.html'
  })
  .otherwise({redirectTo:'/home'});

  RestangularProvider.setRestangularFields({
    id: 'id'
  });

  RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
    if (operation === 'put') {
      elem._id = undefined;
      return elem;
    }
    return elem;
  });
}])

.controller('ItemcategoryListCtrl', ['$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function ($scope, $route, $location, Restangular, security, i18nNotifications) {

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.create = function() {
    $location.path('home/itemcategory/create');
  }

  $scope.canSelect = function() {
    return ($scope.gridOptions.selectedItems.length===0?false:true);
  }

  $scope.select = function () {
    $modalInstance.close($scope.gridOptions.selectedItems[0]);
  };

  $scope.back = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.detail = function(row){
    $location.path('home/itemcategory/edit/'+row);
  }

  $scope.filterOptions = {
    filterText: "",
    useExternalFilter: true
  };
  $scope.totalServerItems = 0;
  $scope.pagingOptions = {
    pageSizes: [20, 50, 100],
    pageSize: 50,
    currentPage: 1
  };  
  $scope.setPagingData = function(data, page, pageSize){  
    var pagedData = data.records;
    $scope.myData = pagedData;
    $scope.totalServerItems = data.total;
    if (!$scope.$$phase) {
      $scope.$apply();
    }
  };
  $scope.getPagedDataAsync = function (pageSize, page, searchText) {
    setTimeout(function () {
      var custom_get = {};

      if($route.current.params)
        custom_get = $route.current.params;

      custom_get.offset = (page - 1) * pageSize; 
      custom_get.limit = pageSize;

      if (searchText='$scope.filterOptions.filterText') {

        custom_get.filter = $scope.filterOptions.filterText;
        custom_get.filter_fields = 'id,name';

        Restangular.all("itemcategorys").getList(custom_get).then(function(itemcategorys) {
          $scope.setPagingData(itemcategorys,page,pageSize);
        });
      } 
      else {
        Restangular.all("itemcategorys").getList(custom_get).then(function(itemcategorys) {
          $scope.setPagingData(itemcategorys,page,pageSize);
        });
      }
    }, 100);
  };

  $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

  $scope.$watch('pagingOptions', function (newVal, oldVal) {
    if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
    }
  }, true);
  $scope.$watch('filterOptions', function (newVal, oldVal) {
    if (newVal !== oldVal) {
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
    }
  }, true);

  $scope.gridOptions = {
    data: 'myData',
    enablePaging: true,
    showFooter: true,
    multiSelect: false,
    selectedItems: [],
    totalServerItems:'totalServerItems',
    pagingOptions: $scope.pagingOptions,
    filterOptions: $scope.filterOptions,
    columnDefs: [
    { field: 'id', displayName: 'Code', width: 100, cellClass:'text-center' },
    { field: 'name', displayName: 'Name', width: 600 },
    { field: 'group_name', displayName: 'Item Group', width: 400 },
    { field: 'created_at', displayName: 'Date', width: 200, cellClass:'text-center' },
    { field: 'is_active', displayName: 'Active', width: 150, cellClass:'text-center', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ (row.getProperty(col.field)===1?\'Yes\' : \'No\') }}</span></div>'},
    {displayName: 'Action', cellClass:'text-center',
    cellTemplate: 	'<div class="ngCellText" ng-class="col.colIndex()"><a class="btn btn-xs btn-default" ng-href="" ng-click="detail(row.entity.id)" data-toggle="tooltip" title="edit" data-original-title="Edit Client"><i class="fa fa-pencil"></i></a></div>'}
    ]
  };

}])

.controller('ItemcategoryCreateCtrl', ['$window', '$scope', '$location', 'Restangular', 'security', 'i18nNotifications', '$modal', 'APP_CONFIG', '$upload', '$timeout', '$dialogs', function ($window, $scope, $location, Restangular, security, i18nNotifications, $modal, APP_CONFIG, $upload, $timeout, $dialogs) {

  //debugger;
  var original = {};
  original.is_active = true;

  $scope.itemcategory = Restangular.copy(original);

  Restangular.all("itemgroups").getList({where:"is_active = 1"}).then(function(itemgroups) {
    $scope.itemgroups = itemgroups.records;
  });

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.canRemove = function() {
    return false;
  };

  $scope.canSave = function() {
  	return $scope.ItemcategoryForm.$valid && 
    !angular.equals($scope.itemcategory, original);
  };

  $scope.back = function() {
    $window.history.back();
  }

  $scope.save = function() {
    Restangular.all('itemcategorys').post($scope.itemcategory).then(function(itemcategory) {
      i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : itemcategory.id});
      $location.path('/home/itemcategory');
    });
  }

  $scope.canRevert = function() {
    return $scope.ItemcategoryForm.$valid && 
    !angular.equals($scope.itemcategory, original);
  }

  $scope.revert = function() {
    var original = {};
    original.is_active = true;
    $scope.itemcategory = Restangular.copy(original);
    $scope.ItemcategoryForm.$setPristine();
  }

}])

.controller('ItemcategoryEditCtrl', ['$window', '$scope', '$location', 'Restangular', 'item', 'i18nNotifications', '$dialogs', 'security', 'simpleDeleteConfirm', '$modal', 'APP_CONFIG', '$upload', '$timeout', function ($window, $scope, $location, Restangular, item, i18nNotifications, $dialogs, security, simpleDeleteConfirm, $modal, APP_CONFIG, $upload, $timeout) {

  //debugger;
  var original = item;

  $scope.itemcategory = Restangular.copy(original);

  Restangular.all("itemgroups").getList({where:"is_active = 1"}).then(function(itemgroups) {
    $scope.itemgroups = itemgroups.records;
  });

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.back = function() {
    $window.history.back();
  }

  $scope.canSave = function() {
    return $scope.ItemcategoryForm.$valid && 
    !angular.equals($scope.itemcategory, original);
  }
  
  $scope.save = function() {
    $scope.itemcategory.put().then(function() {
      i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : $scope.itemcategory.id});
      $location.path('/home/itemcategory');
    });
  };
  
  $scope.canRemove = function() {
    return true;
  }
  $scope.remove = function() {
    simpleDeleteConfirm.openDialog(original, '/home/itemcategory');
  };

  $scope.canRevert = function() {
    return !angular.equals($scope.itemcategory, original);
  }
  $scope.revert = function() {
    $scope.itemcategory =  Restangular.copy(original);
    $scope.ItemcategoryForm.$setPristine();
  }

}]);

