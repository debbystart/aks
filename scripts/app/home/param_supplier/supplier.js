angular.module('supplier', [
  'shared.dialog',
  'restangular',
  'services.i18nNotifications'
  ])

.config(['$routeProvider', 'RestangularProvider', function ($routeProvider, RestangularProvider) {

  $routeProvider
  .when('/home/supplier', {
    controller: 'SupplierListCtrl',
    templateUrl: 'scripts/app/home/param_supplier/list.tpl.html'
  })
  .when('/home/supplier/edit/:id', {
    controller: 'SupplierEditCtrl',
    templateUrl: 'scripts/app/home/param_supplier/detail.tpl.html',
    resolve: {
      item: function(Restangular, $route){
        return Restangular.one('suppliers', $route.current.params.id).get();
      }
    }
  })
  .when('/home/supplier/create', {
    controller: 'SupplierCreateCtrl',
    templateUrl: 'scripts/app/home/param_supplier/detail.tpl.html'
  })

  .otherwise({redirectTo:'/home'});
  RestangularProvider.setRestangularFields({
    id: 'id'
  });

  RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
    if (operation === 'put') {
      elem._id = undefined;
      return elem;
    }
    return elem;
  });
  
}])

.controller('SupplierListCtrl', ['$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function ($scope, $route, $location, Restangular, security, i18nNotifications) {

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.create = function() {
    $location.path('home/supplier/create');
  }

  $scope.canSelect = function() {
    return ($scope.gridOptions.selectedItems.length===0?false:true);
  }

  $scope.select = function () {
    $modalInstance.close($scope.gridOptions.selectedItems[0]);
  };

  $scope.back = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.detail = function(row){
    $location.path('home/supplier/edit/'+row);
  }

  $scope.change_filter = function(){
    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
  }

  $scope.filterOptions = {
    filterText: null,
    useExternalFilter: true
  };
  $scope.totalServerItems = 0;
  $scope.pagingOptions = {
    pageSizes: [20, 50, 100],
    pageSize: 50,
    currentPage: 1
  };  
  $scope.setPagingData = function(data, page, pageSize){  
    var pagedData = data.records;
    $scope.myData = pagedData;
    $scope.totalServerItems = data.total;
    if (!$scope.$$phase) {
      $scope.$apply();
    }
  };
  $scope.getPagedDataAsync = function (pageSize, page, searchText) {
    setTimeout(function () {
      var custom_get = {};

      if($route.current.params)
        custom_get = $route.current.params;

      custom_get.offset = (page - 1) * pageSize; 
      custom_get.limit = pageSize;

      if (searchText='$scope.filterOptions.filterText') {

        custom_get.filter = $scope.filterOptions.filterText;
        custom_get.filter_fields = 'id,name';

        Restangular.all("suppliers").getList(custom_get).then(function(suppliers) {
          $scope.setPagingData(suppliers,page,pageSize);
        });
      } 
      else {
        Restangular.all("suppliers").getList(custom_get).then(function(suppliers) {
          $scope.setPagingData(suppliers,page,pageSize);
        });
      }
    }, 100);
  };

  $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

  $scope.$watch('pagingOptions', function (newVal, oldVal) {
    if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
    }
  }, true);

  $scope.gridOptions = {
    data: 'myData',
    enablePaging: true,
    showFooter: true,
    multiSelect: false,
    selectedItems: [],
    totalServerItems:'totalServerItems',
    pagingOptions: $scope.pagingOptions,
    filterOptions: $scope.filterOptions,
    enableColumnResize: true,
    columnDefs: [
    { field: 'id', displayName: 'Code', cellClass: 'text-center', enableCellEdit:true, width: 120},
    { field: 'name', displayName: 'Nama', enableCellEdit:true, width: 300},
    { field: 'province', displayName: 'Provinsi', width: 195},
    { field: 'city', displayName: 'Kabupaten/Kota', width: 190},
    { field: 'districts', displayName: 'Kecamatan', width: 185},
    { field: 'village', displayName: 'Kelurahan', width: 180},
    { field: 'market', displayName: 'Market', width: 130},
    { field: 'mobile', displayName: 'Telephone', width: 130, cellClass: 'text-right'},
    {displayName: 'Detail', cellClass: 'text-center',
    cellTemplate: 	'<div class="ngCellText" ng-class="col.colIndex()"><a class="btn btn-xs btn-default" ng-href="" ng-click="detail(row.entity.id)" data-toggle="tooltip" title="edit" data-original-title="Edit Client"><i class="fa fa-pencil"></i></a></div>'}

    ]
  };

}])

.controller('SupplierCreateCtrl', ['$window', '$scope', '$location', 'Restangular', 'security', 'i18nNotifications', '$modal', 'APP_CONFIG', '$upload', '$timeout', '$dialogs', function ($window, $scope, $location, Restangular, security, i18nNotifications, $modal, APP_CONFIG, $upload, $timeout, $dialogs) {
  //debugger;
  var original = {};
  original.is_active = true;

  $scope.supplier = Restangular.copy(original);

  Restangular.all("regions/province").getList().then(function(regions) {
    $scope.provinces = regions.records;
  });

  Restangular.all("markets").getList({where:"is_active = 1"}).then(function(markets) {
    $scope.markets = markets.records;
  });

  $scope.changeprov = function() {
    Restangular.all("regions/city").getList({where:"param_region.name = '"+$scope.supplier.province+"'"}).then(function(regions) {
      $scope.citys = regions.records;
    });
    $scope.supplier.city = undefined;
    $scope.supplier.districts = undefined;
    $scope.supplier.village = undefined;
    $scope.supplier.postal = undefined;
  }

  $scope.changecity = function() {
    Restangular.all("regions/districts").getList({where:"param_region.name = '"+$scope.supplier.province+"' and param_region.city = '"+$scope.supplier.city+"'"}).then(function(regions) {
      $scope.districts = regions.records;
    });
    $scope.supplier.districts = undefined;
    $scope.supplier.village = undefined;
    $scope.supplier.postal = undefined;
  }

  $scope.changedist = function() {
    Restangular.all("regions/village").getList({where:"param_region.name = '"+$scope.supplier.province+"' and param_region.city = '"+$scope.supplier.city+"' and param_region.districts = '"+$scope.supplier.districts+"'"}).then(function(regions) {
      $scope.villages = regions.records;
    });
    $scope.supplier.village = undefined;
    $scope.supplier.postal = undefined;
  }

  $scope.changepos = function() {
    Restangular.all("regions/postal").getList({where:"param_region.name = '"+$scope.supplier.province+"' and param_region.city = '"+$scope.supplier.city+"' and param_region.districts = '"+$scope.supplier.districts+"' and param_region.village = '"+$scope.supplier.village+"'"}).then(function(regions) {
      $scope.postals = regions.records;
      $scope.supplier.postal = $scope.postals[0].postal;
    });
    Restangular.all("regions/code").getList({where:"param_region.name = '"+$scope.supplier.province+"' and param_region.city = '"+$scope.supplier.city+"' and param_region.districts = '"+$scope.supplier.districts+"' and param_region.village = '"+$scope.supplier.village+"'"}).then(function(regions) {
      $scope.codes = regions.records;
      $scope.supplier.code = $scope.codes[0].code;
    });
  }
  
  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.canRemove = function() {
    return false;
  }
  $scope.canSave = function() {
  	return $scope.SupplierForm.$valid && 
    !angular.equals($scope.supplier, original);
  };

  $scope.back = function() {
    $window.history.back();
  }

  $scope.save = function() {
    Restangular.all('suppliers').post($scope.supplier).then(function(supplier) {
      i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : supplier.id});
      $location.path('/home/supplier');
    });
  }

  $scope.canRevert = function() {
    return $scope.SupplierForm.$valid && 
    !angular.equals($scope.supplier, original);
  }
  $scope.revert = function() {
    $scope.supplier =  {};
    $scope.SupplierForm.$setPristine();
  }

}])

.controller('SupplierEditCtrl', ['localizedMessages', '$window', '$scope', '$location', 'Restangular', 'item', 'i18nNotifications', '$dialogs', 'security', 'simpleDeleteConfirm', '$modal', 'APP_CONFIG', '$upload', '$timeout', function (localizedMessages, $window, $scope, $location, Restangular, item, i18nNotifications, $dialogs, security, simpleDeleteConfirm, $modal, APP_CONFIG, $upload, $timeout) {

  //debugger;
  var original = item;
  $scope.supplier = Restangular.copy(original);

  Restangular.all("regions/province").getList().then(function(regions) {
    $scope.provinces = regions.records;
  });

  Restangular.all("regions/city").getList({where:"param_region.name = '"+$scope.supplier.province+"'"}).then(function(regions) {
    $scope.citys = regions.records;
  });

  Restangular.all("regions/districts").getList({where:"param_region.city = '"+$scope.supplier.city+"'"}).then(function(regions) {
    $scope.districts = regions.records;
  });

  Restangular.all("regions/village").getList({where:"param_region.districts = '"+$scope.supplier.districts+"'"}).then(function(regions) {
    $scope.villages = regions.records;
  });

  Restangular.all("markets").getList({where:"is_active = 1"}).then(function(markets) {
    $scope.markets = markets.records;
  });

  $scope.changeprov = function() {
    Restangular.all("regions/city").getList({where:"param_region.name = '"+$scope.supplier.province+"'"}).then(function(regions) {
      $scope.citys = regions.records;
    });
    $scope.supplier.city = undefined;
    $scope.supplier.districts = undefined;
    $scope.supplier.village = undefined;
    $scope.supplier.postal = undefined;
  }

  $scope.changecity = function() {
    Restangular.all("regions/districts").getList({where:"param_region.name = '"+$scope.supplier.province+"' and param_region.city = '"+$scope.supplier.city+"'"}).then(function(regions) {
      $scope.districts = regions.records;
    });
    $scope.supplier.districts = undefined;
    $scope.supplier.village = undefined;
    $scope.supplier.postal = undefined;
  }

  $scope.changedist = function() {
    Restangular.all("regions/village").getList({where:"param_region.name = '"+$scope.supplier.province+"' and param_region.city = '"+$scope.supplier.city+"' and param_region.districts = '"+$scope.supplier.districts+"'"}).then(function(regions) {
      $scope.villages = regions.records;
    });
    $scope.supplier.village = undefined;
    $scope.supplier.postal = undefined;
  }

  $scope.changepos = function() {
    Restangular.all("regions/postal").getList({where:"param_region.name = '"+$scope.supplier.province+"' and param_region.city = '"+$scope.supplier.city+"' and param_region.districts = '"+$scope.supplier.districts+"' and param_region.village = '"+$scope.supplier.village+"'"}).then(function(regions) {
      $scope.postals = regions.records;
      $scope.supplier.postal = $scope.postals[0].postal;
    });
  }

  $scope.item = function() {
    $scope.supplier.total = parseInt($scope.supplier.price) * parseInt($scope.supplier.amount);
  }

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.back = function() {
    $window.history.back();
  }

  $scope.canSave = function() {
    return !angular.equals($scope.supplier, original);
  }
  
  $scope.save = function() {
    $scope.supplier.put().then(function() {
      i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : $scope.supplier.id});
      $location.path('/home/supplier');
    });
  };

  $scope.canRevert = function() {
    return !angular.equals($scope.supplier, original);
  }
  $scope.revert = function() {
    $scope.supplier =  Restangular.copy(original);
  }
  
  $scope.canRemove = function() {
    return true;
  }

  $scope.remove = function() {
    simpleDeleteConfirm.openDialog(original, '/home/buy');
  };

  $scope.browseBuy = function() {
    var modalInstance = $modal.open({
      templateUrl: 'scripts/app/home/param_supplier/supplier.list.modal.tpl.html',
      controller: ModalInstanceBrowseSupplierCtrl,
      resolve: {
        buy_id: function () {
          return $scope.supplier;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {

      var name = Restangular.copy(selectedItem);
      var supplier = Restangular.copy($scope.supplier);

      supplier.buy_id = name.id;
      supplier.buy = name.name;

      $scope.supplier = supplier;

    }, function () {
    });
  };

  var ModalInstanceBrowseSupplierCtrl = function ($scope, $dialog, $modalInstance, buy_id, Restangular, security, i18nNotifications, $route) {

    $scope.item = buy_id;

    $scope.canCreate    = security.canCreate;
    $scope.canUpdate    = security.canUpdate;
    $scope.canRead      = security.canRead;
    $scope.canDelete    = security.canDelete;
    $scope.isAuthorizedRoute = security.isAuthorizedRoute;

    $scope.filterOptions = {
      filterText: "",
      useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
      pageSizes: [20, 50, 100],
      pageSize: 20,
      currentPage: 1
    };
    $scope.setPagingData = function(data, page, pageSize){
      var pagedData = data.records;
      $scope.myData = pagedData;
      $scope.totalServerItems = data.total;
      if (!$scope.$$phase) {
        $scope.$apply();
      }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
      setTimeout(function () {
        var custom_get = {};

        if($route.current.params)
          custom_get = $route.current.params;

        custom_get.offset = (page - 1) * pageSize;
        custom_get.limit = pageSize;

        var ft = searchText;

        custom_get.filter = ft;
        custom_get.filter_fields = 'code, name';

        Restangular.all("buys").getList(custom_get).then(function(buys) {
          $scope.setPagingData(buys,page,pageSize);
        });
      }, 100);
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
      if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
      }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
      if (newVal !== oldVal) {
        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
      }
    }, true);

    $scope.gridOptions = {
      data: 'myData',
      enablePaging: true,
      showFooter: true,
      multiSelect: false,
      selectedItems: [],
      totalServerItems:'totalServerItems',
      pagingOptions: $scope.pagingOptions,
      filterOptions: $scope.filterOptions,
      columnDefs: [
      { field: 'code', displayName: 'Code', enableCellEdit:true, cellClass: 'text-center', width: 150},
      { field: 'name', displayName: 'Item', enableCellEdit:true}
      ]
    };

    $scope.canSelect = function() {
      return ($scope.gridOptions.selectedItems.length===0?false:true);
    }

    $scope.select = function () {
      $modalInstance.close($scope.gridOptions.selectedItems[0]);
    };

    $scope.back = function () {
      $modalInstance.dismiss('cancel');
    };

  };

}]);

