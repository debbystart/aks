angular.module('paymentlazada', [
  'shared.dialog',
  'restangular',
  'services.i18nNotifications'
  ])

.config(['$routeProvider', 'RestangularProvider', function ($routeProvider, RestangularProvider) {

  $routeProvider
  .when('/home/paymentlazada', {
    controller: 'PaymentlazadaListCtrl',
    templateUrl: 'scripts/app/home/data_paymentlazada/list.tpl.html'
  })
  .when('/home/paymentlazada/pay', {
    controller: 'PaymentlazadaPayListCtrl',
    templateUrl: 'scripts/app/home/data_paymentlazada/listpay.tpl.html'
  })
  .when('/home/paymentlazada/payed', {
    controller: 'PaymentlazadaPaidListCtrl',
    templateUrl: 'scripts/app/home/data_paymentlazada/listpayed.tpl.html'
  })
  .when('/home/paymentlazada/edit/:id/:facility_id', {
    controller: 'PaymentlazadaEditCtrl',
    templateUrl: 'scripts/app/home/data_paymentlazada/detail.tpl.html',
    resolve: {
      item: function(Restangular, $route){
        return Restangular.one('paymentlazadas', $route.current.params.id).get({expands: 'details'});
      },
      facility: function(Restangular, $route){
        return Restangular.one('facilitys', $route.current.params.facility_id).get();
      }
    }
  })
  .when('/home/paymentlazada/generate', {
    controller: 'PaymentlazadaGenerateCtrl',
    templateUrl: 'scripts/app/home/data_paymentlazada/generate.tpl.html'
  })
  .when('/home/paymentlazada/location/:location/:period', {
    controller: 'PaymentlazadaLocationCtrl',
    templateUrl: 'scripts/app/home/data_paymentlazada/location.tpl.html',
    resolve: {
      location: function(Restangular, $route){
        return $route.current.params.location;
      },
      period: function(Restangular, $route){
        return $route.current.params.period;
      }
    }
  })
  .when('/home/paymentlazada/asam/:location/:period', {
    controller: 'PaymentlazadaAsamCtrl',
    templateUrl: 'scripts/app/home/data_paymentlazada/asam.tpl.html',
    resolve: {
      location: function(Restangular, $route){
        return $route.current.params.location;
      },
      period: function(Restangular, $route){
        return $route.current.params.period;
      }
    }
  })
  .when('/home/paymentlazada/btotal/:location/:period', {
    controller: 'PaymentlazadaBtotalCtrl',
    templateUrl: 'scripts/app/home/data_paymentlazada/btotal.tpl.html',
    resolve: {
      location: function(Restangular, $route){
        return $route.current.params.location;
      },
      period: function(Restangular, $route){
        return $route.current.params.period;
      }
    }
  })
  .when('/home/paymentlazada/ksup/:location/:period', {
    controller: 'PaymentlazadaKsupCtrl',
    templateUrl: 'scripts/app/home/data_paymentlazada/ksup.tpl.html',
    resolve: {
      location: function(Restangular, $route){
        return $route.current.params.location;
      },
      period: function(Restangular, $route){
        return $route.current.params.period;
      }
    }
  })
  .when('/home/paymentlazada/office/:location/:period/:approved_by/:knowing_by/:submitted_by', {
    controller: 'PaymentlazadaOfficeCtrl',
    templateUrl: 'scripts/app/home/data_paymentlazada/office.tpl.html',
    resolve: {
      location: function(Restangular, $route){
        return $route.current.params.location;
      },
      period: function(Restangular, $route){
        return $route.current.params.period;
      },
      approved: function(Restangular, $route){
        return $route.current.params.approved_by;
      },
      knowing: function(Restangular, $route){
        return $route.current.params.knowing_by;
      },
      submitted: function(Restangular, $route){
        return $route.current.params.submitted_by;
      }
    }
  })
  .when('/home/paymentlazada/bbk/:period/:approved_by/:knowing_by/:submitted_by', {
    controller: 'PaymentlazadaBbkCtrl',
    templateUrl: 'scripts/app/home/data_paymentlazada/bbk.tpl.html',
    resolve: {
      period: function(Restangular, $route){
        return $route.current.params.period;
      },
      approved: function(Restangular, $route){
        return $route.current.params.approved_by;
      },
      knowing: function(Restangular, $route){
        return $route.current.params.knowing_by;
      },
      submitted: function(Restangular, $route){
        return $route.current.params.submitted_by;
      }
    }
  })
  .when('/home/paymentlazada/ppl/:period/:approved_by/:knowing_by/:submitted_by', {
    controller: 'PaymentlazadaPplCtrl',
    templateUrl: 'scripts/app/home/data_paymentlazada/ppl.tpl.html',
    resolve: {
      period: function(Restangular, $route){
        return $route.current.params.period;
      },
      approved: function(Restangular, $route){
        return $route.current.params.approved_by;
      },
      knowing: function(Restangular, $route){
        return $route.current.params.knowing_by;
      },
      submitted: function(Restangular, $route){
        return $route.current.params.submitted_by;
      }
    }
  })
  .when('/home/paymentlazada/print/:facility_id/:period/:approved_by/:knowing_by/:submitted_by', {
    controller: 'PaymentlazadaPrintCtrl',
    templateUrl: 'scripts/app/home/data_paymentlazada/print.tpl.html',
    resolve: {
      facility_id: function(Restangular, $route){
        return $route.current.params.facility_id;
      },
      period: function(Restangular, $route){
        return $route.current.params.period;
      },
      approved: function(Restangular, $route){
        return $route.current.params.approved_by;
      },
      knowing: function(Restangular, $route){
        return $route.current.params.knowing_by;
      },
      submitted: function(Restangular, $route){
        return $route.current.params.submitted_by;
      }
    }
  })

  .when('/home/paymentlazada/upload', {
    controller: 'PaymentlazadaUploadCtrl',
    templateUrl: 'scripts/app/home/data_paymentlazada/upload.tpl.html'
  })

  .otherwise({redirectTo:'/home'});
  RestangularProvider.setRestangularFields({
    id: 'id'
  });

  RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
    if (operation === 'put') {
      elem._id = undefined;
      return elem;
    }
    return elem;
  });
  
}])

.controller('PaymentlazadaPrintCtrl', ['approved', 'knowing', 'submitted', 'period', 'facility_id', '$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function (approved, knowing, submitted, period, facility_id, $scope, $route, $location, Restangular, security, i18nNotifications) {

  var get_data = {};
  var original = {};

  $scope.paymentlazada = Restangular.copy(original);

  Restangular.all("facilitys/active").getList({where:"id = "+facility_id}).then(function(facilitys) {
    $scope.facilitys = facilitys.records;
    $scope.paymentlazada.facility = $scope.facilitys[0].name;
  });

  var where_id = "data_paymentlazada.facility_id = "+facility_id+" and data_paymentlazada.is_active = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
  get_data.where = where_id;

  Restangular.all("paymentlazadadetails/active").getList(get_data).then(function(paymentlazadadetails) {
    $scope.paymentlazadadetails = paymentlazadadetails.records;
    $scope.paymentlazadadetails.totaljumlah = 0;
    $scope.paymentlazadadetails.totaljumlah_m = 0;
    $scope.paymentlazadadetails.totaljumlah_t = 0;
    $scope.paymentlazadadetails.totaljumlah_p = 0;
    $scope.paymentlazadadetails.totaljumlah_f = 0;
    $scope.paymentlazadadetails.totaljumlah_u = 0;
    $scope.paymentlazadadetails.totaljumlah_am = 0;
    $scope.paymentlazadadetails.totaljumlah_at = 0;
    $scope.paymentlazadadetails.totaljumlah_ap = 0;
    $scope.paymentlazadadetails.totaljumlah_af = 0;
    $scope.paymentlazadadetails.totaljumlah_au = 0;
    for(var i=0; i<$scope.paymentlazadadetails.length; i++){
      $scope.paymentlazadadetails[i].jumlah = parseInt($scope.paymentlazadadetails[i].total_meal_allowance) + parseInt($scope.paymentlazadadetails[i].total_transport_allowance) + parseInt($scope.paymentlazadadetails[i].total_presence) + parseInt($scope.paymentlazadadetails[i].total_fasting) + parseInt($scope.paymentlazadadetails[i].total_uml);
      $scope.paymentlazadadetails[i].jumlah_m = parseInt($scope.paymentlazadadetails[i].total_meal_allowance);
      $scope.paymentlazadadetails[i].jumlah_t = parseInt($scope.paymentlazadadetails[i].total_transport_allowance);
      $scope.paymentlazadadetails[i].jumlah_p = parseInt($scope.paymentlazadadetails[i].total_presence);
      $scope.paymentlazadadetails[i].jumlah_f = parseInt($scope.paymentlazadadetails[i].total_fasting);
      $scope.paymentlazadadetails[i].jumlah_u = parseInt($scope.paymentlazadadetails[i].total_uml);
      $scope.paymentlazadadetails[i].jumlah_am = parseInt($scope.paymentlazadadetails[i].amount_meal_allowance);
      $scope.paymentlazadadetails[i].jumlah_at = parseInt($scope.paymentlazadadetails[i].amount_transport_allowance);
      $scope.paymentlazadadetails[i].jumlah_ap = parseInt($scope.paymentlazadadetails[i].amount_presence);
      $scope.paymentlazadadetails[i].jumlah_af = parseInt($scope.paymentlazadadetails[i].amount_fasting);
      $scope.paymentlazadadetails[i].jumlah_au = parseInt($scope.paymentlazadadetails[i].amount_uml);
      $scope.paymentlazadadetails.totaljumlah = parseInt($scope.paymentlazadadetails.totaljumlah) + parseInt($scope.paymentlazadadetails[i].jumlah);
      $scope.paymentlazadadetails.totaljumlah_m = parseInt($scope.paymentlazadadetails.totaljumlah_m) + parseInt($scope.paymentlazadadetails[i].jumlah_m);
      $scope.paymentlazadadetails.totaljumlah_t = parseInt($scope.paymentlazadadetails.totaljumlah_t) + parseInt($scope.paymentlazadadetails[i].jumlah_t);
      $scope.paymentlazadadetails.totaljumlah_p = parseInt($scope.paymentlazadadetails.totaljumlah_p) + parseInt($scope.paymentlazadadetails[i].jumlah_p);
      $scope.paymentlazadadetails.totaljumlah_f = parseInt($scope.paymentlazadadetails.totaljumlah_f) + parseInt($scope.paymentlazadadetails[i].jumlah_f);
      $scope.paymentlazadadetails.totaljumlah_u = parseInt($scope.paymentlazadadetails.totaljumlah_u) + parseInt($scope.paymentlazadadetails[i].jumlah_u);
      $scope.paymentlazadadetails.totaljumlah_am = parseInt($scope.paymentlazadadetails.totaljumlah_am) + parseInt($scope.paymentlazadadetails[i].jumlah_am);
      $scope.paymentlazadadetails.totaljumlah_at = parseInt($scope.paymentlazadadetails.totaljumlah_at) + parseInt($scope.paymentlazadadetails[i].jumlah_at);
      $scope.paymentlazadadetails.totaljumlah_ap = parseInt($scope.paymentlazadadetails.totaljumlah_ap) + parseInt($scope.paymentlazadadetails[i].jumlah_ap);
      $scope.paymentlazadadetails.totaljumlah_af = parseInt($scope.paymentlazadadetails.totaljumlah_af) + parseInt($scope.paymentlazadadetails[i].jumlah_af);
      $scope.paymentlazadadetails.totaljumlah_au = parseInt($scope.paymentlazadadetails.totaljumlah_au) + parseInt($scope.paymentlazadadetails[i].jumlah_au);

      Restangular.all("employees/active").getList({where:"param_employee.id="+$scope.paymentlazadadetails[i].employee_id}).then(function(employees) {
        $scope.employees = employees.records;
      });
    }
  });

  //
  if(period.toString().substr(0, 2) == '01'){
    $scope.paymentlazada.month = 'JANUARI';
  }else if(period.toString().substr(0, 2) == '02'){
    $scope.paymentlazada.month = 'FEBRUARI';
  }if(period.toString().substr(0, 2) == '03'){
    $scope.paymentlazada.month = 'MARET';
  }if(period.toString().substr(0, 2) == '04'){
    $scope.paymentlazada.month = 'APRIL';
  }if(period.toString().substr(0, 2) == '05'){
    $scope.paymentlazada.month = 'MEI';
  }if(period.toString().substr(0, 2) == '06'){
    $scope.paymentlazada.month = 'JUNI';
  }if(period.toString().substr(0, 2) == '07'){
    $scope.paymentlazada.month = 'JULI';
  }if(period.toString().substr(0, 2) == '08'){
    $scope.paymentlazada.month = 'AGUSTUS';
  }if(period.toString().substr(0, 2) == '09'){
    $scope.paymentlazada.month = 'SEPTEMBER';
  }if(period.toString().substr(0, 2) == '10'){
    $scope.paymentlazada.month = 'OKTOBER';
  }if(period.toString().substr(0, 2) == '11'){
    $scope.paymentlazada.month = 'NOVEMBER';
  }if(period.toString().substr(0, 2) == '12'){
    $scope.paymentlazada.month = 'DESEMBER';
  }
  $scope.paymentlazada.year = period.toString().substr(-4);

  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1;
  var yyyy = today.getFullYear();
  if(dd<10){
    dd='0'+dd
  } 
  if(mm<10){
    mm='0'+mm
  }
  $scope.paymentlazada.dd = dd;
  $scope.paymentlazada.yyyy = yyyy;
  if(mm == '01'){
    $scope.paymentlazada.monthl = 'Januari';
  }else if(mm == '02'){
    $scope.paymentlazada.monthl = 'Februari';
  }if(mm == '03'){
    $scope.paymentlazada.monthl = 'Maret';
  }if(mm == '04'){
    $scope.paymentlazada.monthl = 'April';
  }if(mm == '05'){
    $scope.paymentlazada.monthl = 'Mei';
  }if(mm == '06'){
    $scope.paymentlazada.monthl = 'Juni';
  }if(mm == '07'){
    $scope.paymentlazada.monthl = 'Juli';
  }if(mm == '08'){
    $scope.paymentlazada.monthl = 'Agustus';
  }if(mm == '09'){
    $scope.paymentlazada.monthl = 'September';
  }if(mm == '10'){
    $scope.paymentlazada.monthl = 'Oktober';
  }if(mm == '11'){
    $scope.paymentlazada.monthl = 'November';
  }if(mm == '12'){
    $scope.paymentlazada.monthl = 'Desember';
  }

  Restangular.all("leaders/active").getList({where: "param_leader.id="+approved}).then(function(leaders) {
    $scope.leaders1 = leaders.records;
  });

  Restangular.all("leaders/active").getList({where: "param_leader.id="+knowing}).then(function(leaders) {
    $scope.leaders2 = leaders.records;
  });

  Restangular.all("leaders/active").getList({where: "param_leader.id="+submitted}).then(function(leaders) {
    $scope.leaders3 = leaders.records;
  });

  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.$watch(function() {
    return security.currentUser;
  }, function(currentUser) {
    $scope.currentUser = currentUser;
  });

  $scope.exportData = function () {
    var blob = new Blob([document.getElementById('exportable').innerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    saveAs(blob, "Report.xls");
  };

}])

.controller('PaymentlazadaUploadCtrl', ['$rootScope', '$window', '$scope', '$location', 'Restangular', 'security', 'i18nNotifications', '$modal', 'APP_CONFIG', '$upload', '$timeout', '$dialogs', function ($rootScope, $window, $scope, $location, Restangular, security, i18nNotifications, $modal, APP_CONFIG, $upload, $timeout, $dialogs) {

  var original = {};
  original.is_active = true;
  original.date = new Date();
  
  $scope.paymentlazada = Restangular.copy(original);

  $scope.format = 'dd-MMM-yyyy';
  
  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  var progress = 10;
  var msgs = [
  'Processing...',
  'Done!'
  ];
  var i = 0;    
  var fakeProgress = function(){
    $timeout(function(){
      if(progress < 95){
        progress += 5;
        $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': progress});
        fakeProgress();
      }else{
      }
    },1000);
  };

  $scope.fileReaderSupported = window.FileReader != null;
  $scope.uploadRightAway = false;
  $scope.howToSend = 1;

  $scope.hasUploader = function(index) {
    return $scope.upload[index] != null;
  };

  $scope.abort = function(index) {
    $scope.upload[index].abort(); 
    $scope.upload[index] = null;
  };
  
  $scope.onFileSelect = function($files) {
    $scope.selectedFiles = [];
    $scope.progress = [];
    if ($scope.upload && $scope.upload.length > 0) {
      for (var i = 0; i < $scope.upload.length; i++) {
        if ($scope.upload[i] != null) {
          $scope.upload[i].abort();
        }
      }
    }
    $scope.upload = [];
    $scope.uploadResult = [];
    $scope.selectedFiles = $files;
    $scope.dataUrls = [];
    for ( var i = 0; i < $files.length; i++) {
      var $file = $files[i];
      $scope.progress[i] = -1;
      if ($scope.uploadRightAway) {
        $scope.start(i);
      }
    }
  };

  $scope.start = function(index) {
    $scope.progress[index] = 0;
    if ($scope.howToSend == 1) {
      progress = 10;
      i = 0;
      $dialogs.wait(msgs[0],0);
      fakeProgress();
      $scope.upload[index] = $upload.upload({
        url : APP_CONFIG.appBaseUrl + '/api/paymentlazadas/exams/excels/'+moment($scope.paymentlazada.date).format('YYYY-MM-DD'),
        method: $scope.httpMethod,
        data : {
          myModel : $scope.price_list
        },
        file: $scope.selectedFiles[index],
        fileFormDataName: 'uploads'
      }).then(function(response) {

        $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
        $rootScope.$broadcast('dialogs.wait.complete');

        $scope.uploadResult.push(response.data);
        i18nNotifications.pushForCurrentRoute('import.data.success', 'success', {});
        $location.path('/home/paymentlazada');

      }, 
      function (response) {
        $rootScope.$broadcast('dialogs.wait.complete');
        $dialogs.error('Error', response.data.error);
      }, 
      function(evt) {
        $scope.progress[index] = parseInt(100.0 * evt.loaded / evt.total);
      });

    }else{
      var fileReader = new FileReader();
      fileReader.readAsArrayBuffer($scope.selectedFiles[index]);
      fileReader.onload = function(e) {
        $scope.upload[index] = $upload.http({
          url: 'upload',
          headers: {'Content-Type': $scope.selectedFiles[index].type},
          data: e.target.result
        }).then(function(response) {
          $scope.uploadResult.push(response.data.result);
        }, null, function(evt) {
          $scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
      }
    }
  };

  $scope.uploadSuccess = function () {
    if($scope.uploadResult===undefined)
      return false;
    if($scope.uploadResult.length===0)
      return false;
    return true;
  };

  $scope.canRemove = function() {
    return false;
  }
  $scope.canSave = function() {
    return $scope.PaymentlazadaForm.$valid && 
    !angular.equals($scope.paymentlazada, original);
  };

  $scope.back = function() {
    $window.history.back();
  }

  $scope.save = function() {
    Restangular.all('paymentlazadas').post($scope.paymentlazada).then(function(paymentlazada) {
      i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : paymentlazada.id});
      $location.path('/home/paymentlazada');
    });
  }

  $scope.canRevert = function() {
    return $scope.PaymentlazadaForm.$valid && 
    !angular.equals($scope.paymentlazada, original);
  }
  $scope.revert = function() {
    $scope.paymentlazada =  {};
    $scope.PaymentlazadaForm.$setPristine();
  }

}])

.controller('PaymentlazadaPplCtrl', ['approved', 'knowing', 'submitted', 'period', '$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function (approved, knowing, submitted, period, $scope, $route, $location, Restangular, security, i18nNotifications) {

  var get_data = {};
  var original = {};

  $scope.paymentlazada = Restangular.copy(original);

  var where_id = "data_paymentlazada.facility_id between 2 and 9 and data_paymentlazada.is_active = 4 and data_paymentlazada.month='"+period.toString().substr(0, 2)+"' and data_paymentlazada.year='"+period.toString().substr(-4)+"'";
  get_data.where = where_id;

  Restangular.all("paymentlazadas/facility/ppl").getList(get_data).then(function(paymentlazadas) {
    $scope.paymentlazadadetails = paymentlazadas.records;
    $scope.paymentlazadadetails.total_jml_gaji = 0;
    $scope.paymentlazadadetails.total_jml = 0;
    $scope.paymentlazadadetails.total_jml_bni = 0;
    $scope.paymentlazadadetails.total_bni = 0;
    $scope.paymentlazadadetails.total_jml_mandiri = 0;
    $scope.paymentlazadadetails.total_mandiri = 0;
    $scope.paymentlazadadetails.total_bri = 0;
    $scope.paymentlazadadetails.total_jml_bri = 0;
    $scope.paymentlazadadetails.total_bca = 0;
    $scope.paymentlazadadetails.total_jml_bca = 0;
    $scope.paymentlazadadetails.total_other = 0;
    $scope.paymentlazadadetails.total_jml_other = 0;
    for(var i=0; i<$scope.paymentlazadadetails.length; i++){
      if(Number($scope.paymentlazadadetails[i].jml_bni) == 0){
        $scope.paymentlazadadetails[i].jml_bni = null;
      }
      if(Number($scope.paymentlazadadetails[i].bni) == 0){
        $scope.paymentlazadadetails[i].bni = null;
      }
      if(Number($scope.paymentlazadadetails[i].jml_mandiri) == 0){
        $scope.paymentlazadadetails[i].jml_mandiri = null;
      }
      if(Number($scope.paymentlazadadetails[i].mandiri) == 0){
        $scope.paymentlazadadetails[i].mandiri = null;
      }
      if(Number($scope.paymentlazadadetails[i].jml_bri) == 0){
        $scope.paymentlazadadetails[i].jml_bri = null;
      }
      if(Number($scope.paymentlazadadetails[i].bri) == 0){
        $scope.paymentlazadadetails[i].bri = null;
      }
      if(Number($scope.paymentlazadadetails[i].jml_bca) == 0){
        $scope.paymentlazadadetails[i].jml_bca = null;
      }
      if(Number($scope.paymentlazadadetails[i].bca) == 0){
        $scope.paymentlazadadetails[i].bca = null;
      }
      if(Number($scope.paymentlazadadetails[i].jml_other) == 0){
        $scope.paymentlazadadetails[i].jml_other = null;
      }
      if(Number($scope.paymentlazadadetails[i].other) == 0){
        $scope.paymentlazadadetails[i].other = null;
      }
      $scope.paymentlazadadetails[i].jml = Number($scope.paymentlazadadetails[i].jml_bni) + Number($scope.paymentlazadadetails[i].jml_mandiri) + Number($scope.paymentlazadadetails[i].jml_bri) + Number($scope.paymentlazadadetails[i].jml_bca) + Number($scope.paymentlazadadetails[i].jml_other);
      $scope.paymentlazadadetails[i].jml_gaji = Number($scope.paymentlazadadetails[i].bni) + Number($scope.paymentlazadadetails[i].mandiri) + Number($scope.paymentlazadadetails[i].bri) + Number($scope.paymentlazadadetails[i].bca) + Number($scope.paymentlazadadetails[i].other);
      $scope.paymentlazadadetails.total_jml_gaji = $scope.paymentlazadadetails.total_jml_gaji + Number($scope.paymentlazadadetails[i].jml_gaji);
      $scope.paymentlazadadetails.total_jml = $scope.paymentlazadadetails.total_jml + Number($scope.paymentlazadadetails[i].jml);
      $scope.paymentlazadadetails.total_jml_bni = $scope.paymentlazadadetails.total_jml_bni + Number($scope.paymentlazadadetails[i].jml_bni);
      $scope.paymentlazadadetails.total_bni = $scope.paymentlazadadetails.total_bni + Number($scope.paymentlazadadetails[i].bni);
      $scope.paymentlazadadetails.total_jml_mandiri = $scope.paymentlazadadetails.total_jml_mandiri + Number($scope.paymentlazadadetails[i].jml_mandiri);
      $scope.paymentlazadadetails.total_mandiri = $scope.paymentlazadadetails.total_mandiri + Number($scope.paymentlazadadetails[i].mandiri);
      $scope.paymentlazadadetails.total_jml_bri = $scope.paymentlazadadetails.total_jml_bri + Number($scope.paymentlazadadetails[i].jml_bri);
      $scope.paymentlazadadetails.total_bri = $scope.paymentlazadadetails.total_bri + Number($scope.paymentlazadadetails[i].bri);
      $scope.paymentlazadadetails.total_jml_bca = $scope.paymentlazadadetails.total_jml_bca + Number($scope.paymentlazadadetails[i].jml_bca);
      $scope.paymentlazadadetails.total_bca = $scope.paymentlazadadetails.total_bca + Number($scope.paymentlazadadetails[i].bca);
      $scope.paymentlazadadetails.total_jml_other = $scope.paymentlazadadetails.total_jml_other + Number($scope.paymentlazadadetails[i].jml_other);
      $scope.paymentlazadadetails.total_other = $scope.paymentlazadadetails.total_other + Number($scope.paymentlazadadetails[i].other);
    }
  });

  //
  if(period.toString().substr(0, 2) == '01'){
    $scope.paymentlazada.month = 'JANUARI';
  }else if(period.toString().substr(0, 2) == '02'){
    $scope.paymentlazada.month = 'FEBRUARI';
  }if(period.toString().substr(0, 2) == '03'){
    $scope.paymentlazada.month = 'MARET';
  }if(period.toString().substr(0, 2) == '04'){
    $scope.paymentlazada.month = 'APRIL';
  }if(period.toString().substr(0, 2) == '05'){
    $scope.paymentlazada.month = 'MEI';
  }if(period.toString().substr(0, 2) == '06'){
    $scope.paymentlazada.month = 'JUNI';
  }if(period.toString().substr(0, 2) == '07'){
    $scope.paymentlazada.month = 'JULI';
  }if(period.toString().substr(0, 2) == '08'){
    $scope.paymentlazada.month = 'AGUSTUS';
  }if(period.toString().substr(0, 2) == '09'){
    $scope.paymentlazada.month = 'SEPTEMBER';
  }if(period.toString().substr(0, 2) == '10'){
    $scope.paymentlazada.month = 'OKTOBER';
  }if(period.toString().substr(0, 2) == '11'){
    $scope.paymentlazada.month = 'NOVEMBER';
  }if(period.toString().substr(0, 2) == '12'){
    $scope.paymentlazada.month = 'DESEMBER';
  }
  $scope.paymentlazada.year = period.toString().substr(-4);

  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1;
  var yyyy = today.getFullYear();
  if(dd<10){
    dd='0'+dd
  } 
  if(mm<10){
    mm='0'+mm
  }
  $scope.paymentlazada.dd = dd;
  $scope.paymentlazada.yyyy = yyyy;
  if(mm == '01'){
    $scope.paymentlazada.monthl = 'Januari';
  }else if(mm == '02'){
    $scope.paymentlazada.monthl = 'Februari';
  }if(mm == '03'){
    $scope.paymentlazada.monthl = 'Maret';
  }if(mm == '04'){
    $scope.paymentlazada.monthl = 'April';
  }if(mm == '05'){
    $scope.paymentlazada.monthl = 'Mei';
  }if(mm == '06'){
    $scope.paymentlazada.monthl = 'Juni';
  }if(mm == '07'){
    $scope.paymentlazada.monthl = 'Juli';
  }if(mm == '08'){
    $scope.paymentlazada.monthl = 'Agustus';
  }if(mm == '09'){
    $scope.paymentlazada.monthl = 'September';
  }if(mm == '10'){
    $scope.paymentlazada.monthl = 'Oktober';
  }if(mm == '11'){
    $scope.paymentlazada.monthl = 'November';
  }if(mm == '12'){
    $scope.paymentlazada.monthl = 'Desember';
  }

  Restangular.all("leaders/active").getList({where: "param_leader.id="+approved}).then(function(leaders) {
    $scope.leaders1 = leaders.records;
  });

  Restangular.all("leaders/active").getList({where: "param_leader.id="+knowing}).then(function(leaders) {
    $scope.leaders2 = leaders.records;
  });

  Restangular.all("leaders/active").getList({where: "param_leader.id="+submitted}).then(function(leaders) {
    $scope.leaders3 = leaders.records;
  });

  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.$watch(function() {
    return security.currentUser;
  }, function(currentUser) {
    $scope.currentUser = currentUser;
  });

  $scope.exportData = function () {
    var blob = new Blob([document.getElementById('exportable').innerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    saveAs(blob, "Report.xls");
  };

}])

.controller('PaymentlazadaBbkCtrl', ['approved', 'knowing', 'submitted', 'period', '$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function (approved, knowing, submitted, period, $scope, $route, $location, Restangular, security, i18nNotifications) {

  var get_data = {};
  var original = {};

  $scope.paymentlazada = Restangular.copy(original);

  var where_id = "data_paymentlazada.facility_id between 10 and 16 and data_paymentlazada.is_active = 4 and data_paymentlazada.month='"+period.toString().substr(0, 2)+"' and data_paymentlazada.year='"+period.toString().substr(-4)+"'";
  get_data.where = where_id;

  Restangular.all("paymentlazadas/facility/bbk").getList(get_data).then(function(paymentlazadas) {
    $scope.paymentlazadadetails = paymentlazadas.records;
    $scope.paymentlazadadetails.total_jml_gaji = 0;
    $scope.paymentlazadadetails.total_jml = 0;
    $scope.paymentlazadadetails.total_jml_bni = 0;
    $scope.paymentlazadadetails.total_bni = 0;
    $scope.paymentlazadadetails.total_jml_mandiri = 0;
    $scope.paymentlazadadetails.total_mandiri = 0;
    $scope.paymentlazadadetails.total_bri = 0;
    $scope.paymentlazadadetails.total_jml_bri = 0;
    $scope.paymentlazadadetails.total_other = 0;
    $scope.paymentlazadadetails.total_jml_other = 0;
    for(var i=0; i<$scope.paymentlazadadetails.length; i++){
      if(Number($scope.paymentlazadadetails[i].jml_bni) == 0){
        $scope.paymentlazadadetails[i].jml_bni = null;
      }
      if(Number($scope.paymentlazadadetails[i].bni) == 0){
        $scope.paymentlazadadetails[i].bni = null;
      }
      if(Number($scope.paymentlazadadetails[i].jml_mandiri) == 0){
        $scope.paymentlazadadetails[i].jml_mandiri = null;
      }
      if(Number($scope.paymentlazadadetails[i].mandiri) == 0){
        $scope.paymentlazadadetails[i].mandiri = null;
      }
      if(Number($scope.paymentlazadadetails[i].jml_bri) == 0){
        $scope.paymentlazadadetails[i].jml_bri = null;
      }
      if(Number($scope.paymentlazadadetails[i].bri) == 0){
        $scope.paymentlazadadetails[i].bri = null;
      }
      if(Number($scope.paymentlazadadetails[i].jml_other) == 0){
        $scope.paymentlazadadetails[i].jml_other = null;
      }
      if(Number($scope.paymentlazadadetails[i].other) == 0){
        $scope.paymentlazadadetails[i].other = null;
      }
      $scope.paymentlazadadetails[i].jml = Number($scope.paymentlazadadetails[i].jml_bni) + Number($scope.paymentlazadadetails[i].jml_mandiri) + Number($scope.paymentlazadadetails[i].jml_bri) + Number($scope.paymentlazadadetails[i].jml_other);
      $scope.paymentlazadadetails[i].jml_gaji = Number($scope.paymentlazadadetails[i].bni) + Number($scope.paymentlazadadetails[i].mandiri) + Number($scope.paymentlazadadetails[i].bri) + Number($scope.paymentlazadadetails[i].other);
      $scope.paymentlazadadetails.total_jml_gaji = $scope.paymentlazadadetails.total_jml_gaji + Number($scope.paymentlazadadetails[i].jml_gaji);
      $scope.paymentlazadadetails.total_jml = $scope.paymentlazadadetails.total_jml + Number($scope.paymentlazadadetails[i].jml);
      $scope.paymentlazadadetails.total_jml_bni = $scope.paymentlazadadetails.total_jml_bni + Number($scope.paymentlazadadetails[i].jml_bni);
      $scope.paymentlazadadetails.total_bni = $scope.paymentlazadadetails.total_bni + Number($scope.paymentlazadadetails[i].bni);
      $scope.paymentlazadadetails.total_jml_mandiri = $scope.paymentlazadadetails.total_jml_mandiri + Number($scope.paymentlazadadetails[i].jml_mandiri);
      $scope.paymentlazadadetails.total_mandiri = $scope.paymentlazadadetails.total_mandiri + Number($scope.paymentlazadadetails[i].mandiri);
      $scope.paymentlazadadetails.total_jml_bri = $scope.paymentlazadadetails.total_jml_bri + Number($scope.paymentlazadadetails[i].jml_bri);
      $scope.paymentlazadadetails.total_bri = $scope.paymentlazadadetails.total_bri + Number($scope.paymentlazadadetails[i].bri);
      $scope.paymentlazadadetails.total_jml_other = $scope.paymentlazadadetails.total_jml_other + Number($scope.paymentlazadadetails[i].jml_other);
      $scope.paymentlazadadetails.total_other = $scope.paymentlazadadetails.total_other + Number($scope.paymentlazadadetails[i].other);
    }
  });

  //
  if(period.toString().substr(0, 2) == '01'){
    $scope.paymentlazada.month = 'JANUARI';
  }else if(period.toString().substr(0, 2) == '02'){
    $scope.paymentlazada.month = 'FEBRUARI';
  }if(period.toString().substr(0, 2) == '03'){
    $scope.paymentlazada.month = 'MARET';
  }if(period.toString().substr(0, 2) == '04'){
    $scope.paymentlazada.month = 'APRIL';
  }if(period.toString().substr(0, 2) == '05'){
    $scope.paymentlazada.month = 'MEI';
  }if(period.toString().substr(0, 2) == '06'){
    $scope.paymentlazada.month = 'JUNI';
  }if(period.toString().substr(0, 2) == '07'){
    $scope.paymentlazada.month = 'JULI';
  }if(period.toString().substr(0, 2) == '08'){
    $scope.paymentlazada.month = 'AGUSTUS';
  }if(period.toString().substr(0, 2) == '09'){
    $scope.paymentlazada.month = 'SEPTEMBER';
  }if(period.toString().substr(0, 2) == '10'){
    $scope.paymentlazada.month = 'OKTOBER';
  }if(period.toString().substr(0, 2) == '11'){
    $scope.paymentlazada.month = 'NOVEMBER';
  }if(period.toString().substr(0, 2) == '12'){
    $scope.paymentlazada.month = 'DESEMBER';
  }
  $scope.paymentlazada.year = period.toString().substr(-4);

  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1;
  var yyyy = today.getFullYear();
  if(dd<10){
    dd='0'+dd
  } 
  if(mm<10){
    mm='0'+mm
  }
  $scope.paymentlazada.dd = dd;
  $scope.paymentlazada.yyyy = yyyy;
  if(mm == '01'){
    $scope.paymentlazada.monthl = 'Januari';
  }else if(mm == '02'){
    $scope.paymentlazada.monthl = 'Februari';
  }if(mm == '03'){
    $scope.paymentlazada.monthl = 'Maret';
  }if(mm == '04'){
    $scope.paymentlazada.monthl = 'April';
  }if(mm == '05'){
    $scope.paymentlazada.monthl = 'Mei';
  }if(mm == '06'){
    $scope.paymentlazada.monthl = 'Juni';
  }if(mm == '07'){
    $scope.paymentlazada.monthl = 'Juli';
  }if(mm == '08'){
    $scope.paymentlazada.monthl = 'Agustus';
  }if(mm == '09'){
    $scope.paymentlazada.monthl = 'September';
  }if(mm == '10'){
    $scope.paymentlazada.monthl = 'Oktober';
  }if(mm == '11'){
    $scope.paymentlazada.monthl = 'November';
  }if(mm == '12'){
    $scope.paymentlazada.monthl = 'Desember';
  }

  Restangular.all("leaders/active").getList({where: "param_leader.id="+approved}).then(function(leaders) {
    $scope.leaders1 = leaders.records;
  });

  Restangular.all("leaders/active").getList({where: "param_leader.id="+knowing}).then(function(leaders) {
    $scope.leaders2 = leaders.records;
  });

  Restangular.all("leaders/active").getList({where: "param_leader.id="+submitted}).then(function(leaders) {
    $scope.leaders3 = leaders.records;
  });

  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.$watch(function() {
    return security.currentUser;
  }, function(currentUser) {
    $scope.currentUser = currentUser;
  });

  $scope.exportData = function () {
    var blob = new Blob([document.getElementById('exportable').innerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    saveAs(blob, "Report.xls");
  };

}])

.controller('PaymentlazadaOfficeCtrl', ['approved', 'knowing', 'submitted', 'period', 'location', '$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function (approved, knowing, submitted, period, location, $scope, $route, $location, Restangular, security, i18nNotifications) {

  var get_data = {};
  var original = {};

  $scope.paymentlazada = Restangular.copy(original);

  if(location == 1){
    var where_id = "data_paymentlazada.facility_id = 1 and data_paymentlazada.is_active = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data.where = where_id;
  }else if(location == 2){
    var where_id = "data_paymentlazada.facility_id between 2 and 9 and data_paymentlazada.is_active = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data.where = where_id;
  }else if(location == 3){
    var where_id = "data_paymentlazada.facility_id = 10 and data_paymentlazada.is_active = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data.where = where_id;
  }else if(location == 4){
    var where_id = "data_paymentlazada.facility_id = 15 and data_paymentlazada.is_active = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data.where = where_id;
  }else if(location == 5){
    var where_id = "data_paymentlazada.facility_id between 11 and 16 and data_paymentlazada.is_active = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data.where = where_id;
  }

  Restangular.all("paymentlazadadetails/active").getList(get_data).then(function(paymentlazadadetails) {
    $scope.paymentlazadadetails = paymentlazadadetails.records;
    $scope.paymentlazadadetails.totaljumlah = 0;
    $scope.paymentlazadadetails.totaljumlah_m = 0;
    $scope.paymentlazadadetails.totaljumlah_t = 0;
    $scope.paymentlazadadetails.totaljumlah_p = 0;
    $scope.paymentlazadadetails.totaljumlah_f = 0;
    $scope.paymentlazadadetails.totaljumlah_u = 0;
    $scope.paymentlazadadetails.totaljumlah_am = 0;
    $scope.paymentlazadadetails.totaljumlah_at = 0;
    $scope.paymentlazadadetails.totaljumlah_ap = 0;
    $scope.paymentlazadadetails.totaljumlah_af = 0;
    $scope.paymentlazadadetails.totaljumlah_au = 0;
    for(var i=0; i<$scope.paymentlazadadetails.length; i++){
      $scope.paymentlazadadetails[i].jumlah = parseInt($scope.paymentlazadadetails[i].total_meal_allowance) + parseInt($scope.paymentlazadadetails[i].total_transport_allowance) + parseInt($scope.paymentlazadadetails[i].total_presence) + parseInt($scope.paymentlazadadetails[i].total_fasting) + parseInt($scope.paymentlazadadetails[i].total_uml);
      $scope.paymentlazadadetails[i].jumlah_m = parseInt($scope.paymentlazadadetails[i].total_meal_allowance);
      $scope.paymentlazadadetails[i].jumlah_t = parseInt($scope.paymentlazadadetails[i].total_transport_allowance);
      $scope.paymentlazadadetails[i].jumlah_p = parseInt($scope.paymentlazadadetails[i].total_presence);
      $scope.paymentlazadadetails[i].jumlah_f = parseInt($scope.paymentlazadadetails[i].total_fasting);
      $scope.paymentlazadadetails[i].jumlah_u = parseInt($scope.paymentlazadadetails[i].total_uml);
      $scope.paymentlazadadetails[i].jumlah_am = parseInt($scope.paymentlazadadetails[i].amount_meal_allowance);
      $scope.paymentlazadadetails[i].jumlah_at = parseInt($scope.paymentlazadadetails[i].amount_transport_allowance);
      $scope.paymentlazadadetails[i].jumlah_ap = parseInt($scope.paymentlazadadetails[i].amount_presence);
      $scope.paymentlazadadetails[i].jumlah_af = parseInt($scope.paymentlazadadetails[i].amount_fasting);
      $scope.paymentlazadadetails[i].jumlah_au = parseInt($scope.paymentlazadadetails[i].amount_uml);
      $scope.paymentlazadadetails.totaljumlah = parseInt($scope.paymentlazadadetails.totaljumlah) + parseInt($scope.paymentlazadadetails[i].jumlah);
      $scope.paymentlazadadetails.totaljumlah_m = parseInt($scope.paymentlazadadetails.totaljumlah_m) + parseInt($scope.paymentlazadadetails[i].jumlah_m);
      $scope.paymentlazadadetails.totaljumlah_t = parseInt($scope.paymentlazadadetails.totaljumlah_t) + parseInt($scope.paymentlazadadetails[i].jumlah_t);
      $scope.paymentlazadadetails.totaljumlah_p = parseInt($scope.paymentlazadadetails.totaljumlah_p) + parseInt($scope.paymentlazadadetails[i].jumlah_p);
      $scope.paymentlazadadetails.totaljumlah_f = parseInt($scope.paymentlazadadetails.totaljumlah_f) + parseInt($scope.paymentlazadadetails[i].jumlah_f);
      $scope.paymentlazadadetails.totaljumlah_u = parseInt($scope.paymentlazadadetails.totaljumlah_u) + parseInt($scope.paymentlazadadetails[i].jumlah_u);
      $scope.paymentlazadadetails.totaljumlah_am = parseInt($scope.paymentlazadadetails.totaljumlah_am) + parseInt($scope.paymentlazadadetails[i].jumlah_am);
      $scope.paymentlazadadetails.totaljumlah_at = parseInt($scope.paymentlazadadetails.totaljumlah_at) + parseInt($scope.paymentlazadadetails[i].jumlah_at);
      $scope.paymentlazadadetails.totaljumlah_ap = parseInt($scope.paymentlazadadetails.totaljumlah_ap) + parseInt($scope.paymentlazadadetails[i].jumlah_ap);
      $scope.paymentlazadadetails.totaljumlah_af = parseInt($scope.paymentlazadadetails.totaljumlah_af) + parseInt($scope.paymentlazadadetails[i].jumlah_af);
      $scope.paymentlazadadetails.totaljumlah_au = parseInt($scope.paymentlazadadetails.totaljumlah_au) + parseInt($scope.paymentlazadadetails[i].jumlah_au);

      Restangular.all("employees/active").getList({where:"param_employee.id="+$scope.paymentlazadadetails[i].employee_id}).then(function(employees) {
        $scope.employees = employees.records;
      });
    }
  });

  //
  if(period.toString().substr(0, 2) == '01'){
    $scope.paymentlazada.month = 'JANUARI';
  }else if(period.toString().substr(0, 2) == '02'){
    $scope.paymentlazada.month = 'FEBRUARI';
  }if(period.toString().substr(0, 2) == '03'){
    $scope.paymentlazada.month = 'MARET';
  }if(period.toString().substr(0, 2) == '04'){
    $scope.paymentlazada.month = 'APRIL';
  }if(period.toString().substr(0, 2) == '05'){
    $scope.paymentlazada.month = 'MEI';
  }if(period.toString().substr(0, 2) == '06'){
    $scope.paymentlazada.month = 'JUNI';
  }if(period.toString().substr(0, 2) == '07'){
    $scope.paymentlazada.month = 'JULI';
  }if(period.toString().substr(0, 2) == '08'){
    $scope.paymentlazada.month = 'AGUSTUS';
  }if(period.toString().substr(0, 2) == '09'){
    $scope.paymentlazada.month = 'SEPTEMBER';
  }if(period.toString().substr(0, 2) == '10'){
    $scope.paymentlazada.month = 'OKTOBER';
  }if(period.toString().substr(0, 2) == '11'){
    $scope.paymentlazada.month = 'NOVEMBER';
  }if(period.toString().substr(0, 2) == '12'){
    $scope.paymentlazada.month = 'DESEMBER';
  }
  $scope.paymentlazada.year = period.toString().substr(-4);

  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1;
  var yyyy = today.getFullYear();
  if(dd<10){
    dd='0'+dd
  } 
  if(mm<10){
    mm='0'+mm
  }
  $scope.paymentlazada.dd = dd;
  $scope.paymentlazada.yyyy = yyyy;
  if(mm == '01'){
    $scope.paymentlazada.monthl = 'Januari';
  }else if(mm == '02'){
    $scope.paymentlazada.monthl = 'Februari';
  }if(mm == '03'){
    $scope.paymentlazada.monthl = 'Maret';
  }if(mm == '04'){
    $scope.paymentlazada.monthl = 'April';
  }if(mm == '05'){
    $scope.paymentlazada.monthl = 'Mei';
  }if(mm == '06'){
    $scope.paymentlazada.monthl = 'Juni';
  }if(mm == '07'){
    $scope.paymentlazada.monthl = 'Juli';
  }if(mm == '08'){
    $scope.paymentlazada.monthl = 'Agustus';
  }if(mm == '09'){
    $scope.paymentlazada.monthl = 'September';
  }if(mm == '10'){
    $scope.paymentlazada.monthl = 'Oktober';
  }if(mm == '11'){
    $scope.paymentlazada.monthl = 'November';
  }if(mm == '12'){
    $scope.paymentlazada.monthl = 'Desember';
  }

  Restangular.all("leaders/active").getList({where: "param_leader.id="+approved}).then(function(leaders) {
    $scope.leaders1 = leaders.records;
  });

  Restangular.all("leaders/active").getList({where: "param_leader.id="+knowing}).then(function(leaders) {
    $scope.leaders2 = leaders.records;
  });

  Restangular.all("leaders/active").getList({where: "param_leader.id="+submitted}).then(function(leaders) {
    $scope.leaders3 = leaders.records;
  });

  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.$watch(function() {
    return security.currentUser;
  }, function(currentUser) {
    $scope.currentUser = currentUser;
  });

  $scope.exportData = function () {
    var blob = new Blob([document.getElementById('exportable').innerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    saveAs(blob, "Report.xls");
  };

}])

.controller('PaymentlazadaKsupCtrl', ['period', 'location', '$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function (period, location, $scope, $route, $location, Restangular, security, i18nNotifications) {

  var get_data = {};
  var original = {};

  $scope.paymentlazada = Restangular.copy(original);

  if(location == 1){
    var where_id = "data_paymentlazada.facility_id = 1 and data_paymentlazada.is_active = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data.where = where_id;
  }else if(location == 2){
    var where_id = "data_paymentlazada.facility_id between 2 and 9 and data_paymentlazada.is_active = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data.where = where_id;
  }else if(location == 3){
    var where_id = "data_paymentlazada.facility_id = 10 and data_paymentlazada.is_active = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data.where = where_id;
  }else if(location == 4){
    var where_id = "data_paymentlazada.facility_id = 15 and data_paymentlazada.is_active = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data.where = where_id;
  }else if(location == 5){
    var where_id = "data_paymentlazada.facility_id between 11 and 16 and data_paymentlazada.is_active = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data.where = where_id;
  }

  Restangular.all("paymentlazadadetails/active").getList(get_data).then(function(paymentlazadadetails) {
    $scope.paymentlazadadetails = paymentlazadadetails.records;
    $scope.paymentlazadadetails.totaljumlah_i = 0;
    $scope.paymentlazadadetails.totaljumlah = 0;
    for(var i=0; i<$scope.paymentlazadadetails.length; i++){
      $scope.paymentlazadadetails[i].jumlah = parseInt($scope.paymentlazadadetails[i].total_incentive);
      $scope.paymentlazadadetails[i].jumlah_i = parseInt($scope.paymentlazadadetails[i].total_incentive);
      $scope.paymentlazadadetails.totaljumlah = parseInt($scope.paymentlazadadetails.totaljumlah) + parseInt($scope.paymentlazadadetails[i].jumlah);
      $scope.paymentlazadadetails.totaljumlah_i = parseInt($scope.paymentlazadadetails.totaljumlah_i) + parseInt($scope.paymentlazadadetails[i].jumlah_i);
    }
  });

  if(period.toString().substr(0, 2) == '01'){
    $scope.paymentlazada.month = 'Januari';
  }else if(period.toString().substr(0, 2) == '02'){
    $scope.paymentlazada.month = 'Februari';
  }if(period.toString().substr(0, 2) == '03'){
    $scope.paymentlazada.month = 'Maret';
  }if(period.toString().substr(0, 2) == '04'){
    $scope.paymentlazada.month = 'April';
  }if(period.toString().substr(0, 2) == '05'){
    $scope.paymentlazada.month = 'Mei';
  }if(period.toString().substr(0, 2) == '06'){
    $scope.paymentlazada.month = 'Juni';
  }if(period.toString().substr(0, 2) == '07'){
    $scope.paymentlazada.month = 'Juli';
  }if(period.toString().substr(0, 2) == '08'){
    $scope.paymentlazada.month = 'Agustus';
  }if(period.toString().substr(0, 2) == '09'){
    $scope.paymentlazada.month = 'September';
  }if(period.toString().substr(0, 2) == '10'){
    $scope.paymentlazada.month = 'Oktober';
  }if(period.toString().substr(0, 2) == '11'){
    $scope.paymentlazada.month = 'November';
  }if(period.toString().substr(0, 2) == '12'){
    $scope.paymentlazada.month = 'Desember';
  }

  $scope.paymentlazada.year = period.toString().substr(-4);

  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.$watch(function() {
    return security.currentUser;
  }, function(currentUser) {
    $scope.currentUser = currentUser;
  });

  $scope.exportData = function () {
    var blob = new Blob([document.getElementById('exportable').innerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    saveAs(blob, "Report.xls");
  };

}])

.controller('PaymentlazadaBtotalCtrl', ['period', 'location', '$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function (period, location, $scope, $route, $location, Restangular, security, i18nNotifications) {

  var get_data = {};
  var original = {};

  $scope.paymentlazada = Restangular.copy(original);

  if(location == 1){
    var where_id = "data_paymentlazada.facility_id = 1 and data_paymentlazada.is_active = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data.where = where_id;
  }else if(location == 2){
    var where_id = "data_paymentlazada.facility_id between 2 and 9 and data_paymentlazada.is_active = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data.where = where_id;
  }else if(location == 3){
    var where_id = "data_paymentlazada.facility_id = 10 and data_paymentlazada.is_active = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data.where = where_id;
  }else if(location == 4){
    var where_id = "data_paymentlazada.facility_id = 15 and data_paymentlazada.is_active = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data.where = where_id;
  }else if(location == 5){
    var where_id = "data_paymentlazada.facility_id between 11 and 16 and data_paymentlazada.is_active = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data.where = where_id;
  }

  Restangular.all("paymentlazadadetails/active").getList(get_data).then(function(paymentlazadadetails) {
    $scope.paymentlazadadetails = paymentlazadadetails.records;
    $scope.paymentlazadadetails.totaljumlah_m = 0;
    $scope.paymentlazadadetails.totaljumlah_t = 0;
    $scope.paymentlazadadetails.totaljumlah = 0;
    for(var i=0; i<$scope.paymentlazadadetails.length; i++){
      $scope.paymentlazadadetails[i].jumlah = parseInt($scope.paymentlazadadetails[i].total_meal_allowance) + parseInt($scope.paymentlazadadetails[i].total_transport_allowance);
      $scope.paymentlazadadetails[i].jumlah_m = parseInt($scope.paymentlazadadetails[i].total_meal_allowance);
      $scope.paymentlazadadetails[i].jumlah_t = parseInt($scope.paymentlazadadetails[i].total_transport_allowance);
      $scope.paymentlazadadetails.totaljumlah = parseInt($scope.paymentlazadadetails.totaljumlah) + parseInt($scope.paymentlazadadetails[i].jumlah);
      $scope.paymentlazadadetails.totaljumlah_m = parseInt($scope.paymentlazadadetails.totaljumlah_m) + parseInt($scope.paymentlazadadetails[i].jumlah_m);
      $scope.paymentlazadadetails.totaljumlah_t = parseInt($scope.paymentlazadadetails.totaljumlah_t) + parseInt($scope.paymentlazadadetails[i].jumlah_t);
    }
  });

  if(period.toString().substr(0, 2) == '01'){
    $scope.paymentlazada.month = 'Januari';
  }else if(period.toString().substr(0, 2) == '02'){
    $scope.paymentlazada.month = 'Februari';
  }if(period.toString().substr(0, 2) == '03'){
    $scope.paymentlazada.month = 'Maret';
  }if(period.toString().substr(0, 2) == '04'){
    $scope.paymentlazada.month = 'April';
  }if(period.toString().substr(0, 2) == '05'){
    $scope.paymentlazada.month = 'Mei';
  }if(period.toString().substr(0, 2) == '06'){
    $scope.paymentlazada.month = 'Juni';
  }if(period.toString().substr(0, 2) == '07'){
    $scope.paymentlazada.month = 'Juli';
  }if(period.toString().substr(0, 2) == '08'){
    $scope.paymentlazada.month = 'Agustus';
  }if(period.toString().substr(0, 2) == '09'){
    $scope.paymentlazada.month = 'September';
  }if(period.toString().substr(0, 2) == '10'){
    $scope.paymentlazada.month = 'Oktober';
  }if(period.toString().substr(0, 2) == '11'){
    $scope.paymentlazada.month = 'November';
  }if(period.toString().substr(0, 2) == '12'){
    $scope.paymentlazada.month = 'Desember';
  }

  $scope.paymentlazada.year = period.toString().substr(-4);

  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.$watch(function() {
    return security.currentUser;
  }, function(currentUser) {
    $scope.currentUser = currentUser;
  });

  $scope.exportData = function () {
    var blob = new Blob([document.getElementById('exportable').innerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    saveAs(blob, "Report.xls");
  };

}])

.controller('PaymentlazadaAsamCtrl', ['period', 'location', '$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function (period, location, $scope, $route, $location, Restangular, security, i18nNotifications) {

  var get_data = {};
  var get_data_mandiri = {};
  var original = {};

  $scope.paymentlazada = Restangular.copy(original);

  if(location == 1){
    var where_id = "data_paymentlazada.facility_id = 1 and data_paymentlazada.is_active = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data.where = where_id;
  }else if(location == 2){
    var where_id = "data_paymentlazada.facility_id between 2 and 9 and data_paymentlazada.is_active = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data.where = where_id;
  }else if(location == 3){
    var where_id = "data_paymentlazada.facility_id = 10 and data_paymentlazada.is_active = 4 and data_paymentlazada_detail.bank_id = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data.where = where_id;
    var where_id_mandiri = "data_paymentlazada.facility_id = 10 and data_paymentlazada.is_active = 4 and data_paymentlazada_detail.bank_id = 3 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data_mandiri.where = where_id_mandiri;
  }else if(location == 4){
    var where_id = "data_paymentlazada.facility_id = 15 and data_paymentlazada.is_active = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data.where = where_id;
  }else if(location == 5){
    var where_id = "data_paymentlazada.facility_id between 11 and 16 and data_paymentlazada.is_active = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data.where = where_id;
  }

  Restangular.all("paymentlazadadetails/active").getList(get_data).then(function(paymentlazadadetails) {
    $scope.paymentlazadadetails = paymentlazadadetails.records;
    $scope.paymentlazadadetails.totaljumlah = 0;
    for(var i=0; i<$scope.paymentlazadadetails.length; i++){
      $scope.paymentlazadadetails[i].jumlah = parseInt($scope.paymentlazadadetails[i].total_meal_allowance) + parseInt($scope.paymentlazadadetails[i].total_transport_allowance) + parseInt($scope.paymentlazadadetails[i].total_presence) + parseInt($scope.paymentlazadadetails[i].total_fasting) + parseInt($scope.paymentlazadadetails[i].total_uml) + parseInt($scope.paymentlazadadetails[i].total_incentive);
      $scope.paymentlazadadetails.totaljumlah = parseInt($scope.paymentlazadadetails.totaljumlah) + parseInt($scope.paymentlazadadetails[i].jumlah);
      $scope.paymentlazadadetails.company = $scope.paymentlazadadetails[i].company;
      $scope.paymentlazadadetails.fname = $scope.paymentlazadadetails[i].fname;
    }
  });

  Restangular.all("paymentlazadadetails/active").getList(get_data_mandiri).then(function(paymentlazadadetails) {
    $scope.paymentlazadadetails_mandiri = paymentlazadadetails.records;
    $scope.paymentlazadadetails_mandiri.totaljumlah = 0;
    for(var i=0; i<$scope.paymentlazadadetails_mandiri.length; i++){
      $scope.paymentlazadadetails_mandiri[i].jumlah = parseInt($scope.paymentlazadadetails_mandiri[i].total_meal_allowance) + parseInt($scope.paymentlazadadetails_mandiri[i].total_transport_allowance) + parseInt($scope.paymentlazadadetails_mandiri[i].total_presence) + parseInt($scope.paymentlazadadetails_mandiri[i].total_fasting) + parseInt($scope.paymentlazadadetails_mandiri[i].total_uml) + parseInt($scope.paymentlazadadetails_mandiri[i].total_incentive);
      $scope.paymentlazadadetails_mandiri.totaljumlah = parseInt($scope.paymentlazadadetails_mandiri.totaljumlah) + parseInt($scope.paymentlazadadetails_mandiri[i].jumlah);
      $scope.paymentlazadadetails_mandiri.company = $scope.paymentlazadadetails_mandiri[i].company;
      $scope.paymentlazadadetails_mandiri.fname = $scope.paymentlazadadetails_mandiri[i].fname;
    }
  });

  if(period.toString().substr(0, 2) == '01'){
    $scope.paymentlazada.month = 'JANUARI';
  }else if(period.toString().substr(0, 2) == '02'){
    $scope.paymentlazada.month = 'FEBRUARI';
  }if(period.toString().substr(0, 2) == '03'){
    $scope.paymentlazada.month = 'MARET';
  }if(period.toString().substr(0, 2) == '04'){
    $scope.paymentlazada.month = 'APRIL';
  }if(period.toString().substr(0, 2) == '05'){
    $scope.paymentlazada.month = 'MEI';
  }if(period.toString().substr(0, 2) == '06'){
    $scope.paymentlazada.month = 'JUNI';
  }if(period.toString().substr(0, 2) == '07'){
    $scope.paymentlazada.month = 'JULI';
  }if(period.toString().substr(0, 2) == '08'){
    $scope.paymentlazada.month = 'AGUSTUS';
  }if(period.toString().substr(0, 2) == '09'){
    $scope.paymentlazada.month = 'SEPTEMBER';
  }if(period.toString().substr(0, 2) == '10'){
    $scope.paymentlazada.month = 'OKTOBER';
  }if(period.toString().substr(0, 2) == '11'){
    $scope.paymentlazada.month = 'NOVEMBER';
  }if(period.toString().substr(0, 2) == '12'){
    $scope.paymentlazada.month = 'DESEMBER';
  }

  $scope.paymentlazada.year = period.toString().substr(-4);

  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.$watch(function() {
    return security.currentUser;
  }, function(currentUser) {
    $scope.currentUser = currentUser;
  });

  $scope.exportData = function () {
    var blob = new Blob([document.getElementById('exportable').innerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    saveAs(blob, "Report.xls");
  };

}])

.controller('PaymentlazadaLocationCtrl', ['period', 'location', '$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function (period, location, $scope, $route, $location, Restangular, security, i18nNotifications) {

  var get_data = {};
  var original = {};

  $scope.paymentlazada = Restangular.copy(original);

  if(location == 1){
    var where_id = "data_paymentlazada.facility_id = 1 and data_paymentlazada.is_active = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data.where = where_id;
  }else if(location == 2){
    var where_id = "data_paymentlazada.facility_id between 2 and 9 and data_paymentlazada.is_active = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data.where = where_id;
  }else if(location == 3){
    var where_id = "data_paymentlazada.facility_id = 10 and data_paymentlazada.is_active = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data.where = where_id;
  }else if(location == 4){
    var where_id = "data_paymentlazada.facility_id = 15 and data_paymentlazada.is_active = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data.where = where_id;
  }else if(location == 5){
    var where_id = "data_paymentlazada.facility_id between 11 and 16 and data_paymentlazada.is_active = 4 and month='"+period.toString().substr(0, 2)+"' and year='"+period.toString().substr(-4)+"'";
    get_data.where = where_id;
  }

  Restangular.all("paymentlazadadetails/active").getList(get_data).then(function(paymentlazadadetails) {
    $scope.paymentlazadadetails = paymentlazadadetails.records;
    $scope.paymentlazadadetails.totaljumlah = 0;
    for(var i=0; i<$scope.paymentlazadadetails.length; i++){
      $scope.paymentlazadadetails[i].jumlah = parseInt($scope.paymentlazadadetails[i].total_meal_allowance) + parseInt($scope.paymentlazadadetails[i].total_transport_allowance) + parseInt($scope.paymentlazadadetails[i].total_presence) + parseInt($scope.paymentlazadadetails[i].total_fasting) + parseInt($scope.paymentlazadadetails[i].total_uml) + parseInt($scope.paymentlazadadetails[i].total_incentive);
      $scope.paymentlazadadetails.totaljumlah = parseInt($scope.paymentlazadadetails.totaljumlah) + parseInt($scope.paymentlazadadetails[i].jumlah);
    }
  });

  if(period.toString().substr(0, 2) == '01'){
    $scope.paymentlazada.month = 'JANUARI';
  }else if(period.toString().substr(0, 2) == '02'){
    $scope.paymentlazada.month = 'FEBRUARI';
  }if(period.toString().substr(0, 2) == '03'){
    $scope.paymentlazada.month = 'MARET';
  }if(period.toString().substr(0, 2) == '04'){
    $scope.paymentlazada.month = 'APRIL';
  }if(period.toString().substr(0, 2) == '05'){
    $scope.paymentlazada.month = 'MEI';
  }if(period.toString().substr(0, 2) == '06'){
    $scope.paymentlazada.month = 'JUNI';
  }if(period.toString().substr(0, 2) == '07'){
    $scope.paymentlazada.month = 'JULI';
  }if(period.toString().substr(0, 2) == '08'){
    $scope.paymentlazada.month = 'AGUSTUS';
  }if(period.toString().substr(0, 2) == '09'){
    $scope.paymentlazada.month = 'SEPTEMBER';
  }if(period.toString().substr(0, 2) == '10'){
    $scope.paymentlazada.month = 'OKTOBER';
  }if(period.toString().substr(0, 2) == '11'){
    $scope.paymentlazada.month = 'NOVEMBER';
  }if(period.toString().substr(0, 2) == '12'){
    $scope.paymentlazada.month = 'DESEMBER';
  }

  $scope.paymentlazada.year = period.toString().substr(-4);

  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.$watch(function() {
    return security.currentUser;
  }, function(currentUser) {
    $scope.currentUser = currentUser;
  });

  $scope.exportData = function () {
    var blob = new Blob([document.getElementById('exportable').innerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    saveAs(blob, "Report.xls");
  };

}])

.controller('PaymentlazadaPaidListCtrl', ['$rootScope', '$timeout', 'localizedMessages', 'APP_CONFIG', '$window', '$dialogs', '$modal', '$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function ($rootScope, $timeout, localizedMessages, APP_CONFIG, $window, $dialogs, $modal, $scope, $route, $location, Restangular, security, i18nNotifications) {

  var get_mt = {};

  Restangular.all("unitlocations/active").getList({orderby: 'name'}).then(function(unitlocations) {
    $scope.unitlocations = unitlocations.records;
  });

  Restangular.all("unitsmallests/active").getList({orderby: 'name'}).then(function(unitsmallests) {
    $scope.unitsmallests = unitsmallests.records;
  });

  Restangular.all("leaders/active").getList().then(function(leaders) {
    $scope.leaders = leaders.records; 
  });

  Restangular.all("facilitys/active").getList().then(function(facilitys) {
    $scope.facilitys = facilitys.records; 
  });

  $scope.format = 'MMM-yyyy'

  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.$watch(function() {
    return security.currentUser;
  }, function(currentUser) {
    $scope.currentUser = currentUser;
  });

  $scope.change_filter = function(){
    $scope.filterOptions.period = moment($scope.filterOptions.filterText).format('MM-YYYY');
    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
  }

  $scope.back = function() {
    $window.history.back();
  };

  $scope.detail = function(row, facility){
    $location.path('home/paymentlazada/edit/'+row+'/'+facility);
  }

  $scope.changeagree = function () {
    if($scope.filterOptions.agree==true){
      $scope.filterOptions.submitted_by = 1;
      $scope.filterOptions.approved_by = 3;
      $scope.filterOptions.knowing_by = 2;
    }else{
      $scope.filterOptions.submitted_by = undefined;
      $scope.filterOptions.approved_by = undefined;
      $scope.filterOptions.knowing_by = undefined;
    }
  };

  $scope.excel1 = function(){
    var windowObjectReference = null;
    if($scope.filterOptions.filterText == undefined){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.period'));
    }else if($scope.filterOptions.location == undefined){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.location'));
    }else{
      if($scope.filterOptions.location==1){
        if($scope.filterOptions.submitted_by == undefined){
          $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.submitted_by'));
        }else if($scope.filterOptions.knowing_by == undefined){
          $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.knowing_by'));
        }else if($scope.filterOptions.approved_by == undefined){
          $dialogs.error('Warning', localizedMessages.get('outpatient.error.approved_by'));
        }else{
          $window.open('#/home/paymentlazada/office/'+$scope.filterOptions.location+'/'+$scope.filterOptions.period+'/'+$scope.filterOptions.approved_by+'/'+$scope.filterOptions.knowing_by+'/'+$scope.filterOptions.submitted_by, '_blank');
        }
      }else if($scope.filterOptions.location==2){
        $window.open('#/home/paymentlazada/location/'+$scope.filterOptions.location+'/'+$scope.filterOptions.period, '_blank');
      }else if($scope.filterOptions.location==3){
        $window.open('#/home/paymentlazada/asam/'+$scope.filterOptions.location+'/'+$scope.filterOptions.period, '_blank');
      }else if($scope.filterOptions.location==4){
        $window.open('#/home/paymentlazada/btotal/'+$scope.filterOptions.location+'/'+$scope.filterOptions.period, '_blank');
      }else if($scope.filterOptions.location==5){
        $window.open('#/home/paymentlazada/ksup/'+$scope.filterOptions.location+'/'+$scope.filterOptions.period, '_blank');
      }
      
    }
  }

  $scope.excel2 = function(){
    var windowObjectReference = null;
    if($scope.filterOptions.filterText == undefined){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.period'));
    }else if($scope.filterOptions.submitted_by == undefined){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.submitted_by'));
    }else if($scope.filterOptions.knowing_by == undefined){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.knowing_by'));
    }else if($scope.filterOptions.approved_by == undefined){
      $dialogs.error('Warning', localizedMessages.get('outpatient.error.approved_by'));
    }else{
      $window.open('#/home/paymentlazada/ppl/'+$scope.filterOptions.period+'/'+$scope.filterOptions.approved_by+'/'+$scope.filterOptions.knowing_by+'/'+$scope.filterOptions.submitted_by, '_blank');
    }
  }

  $scope.excel3 = function(){
    var windowObjectReference = null;
    if($scope.filterOptions.filterText == undefined){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.period'));
    }else if($scope.filterOptions.submitted_by == undefined){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.submitted_by'));
    }else if($scope.filterOptions.knowing_by == undefined){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.knowing_by'));
    }else if($scope.filterOptions.approved_by == undefined){
      $dialogs.error('Warning', localizedMessages.get('outpatient.error.approved_by'));
    }else{
      $window.open('#/home/paymentlazada/bbk/'+$scope.filterOptions.period+'/'+$scope.filterOptions.approved_by+'/'+$scope.filterOptions.knowing_by+'/'+$scope.filterOptions.submitted_by, '_blank');
    }
  }

  $scope.print1 = function () {
    var windowObjectReference = null;
    if($scope.filterOptions.filterText == undefined){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.period'));
    }else if($scope.filterOptions.submitted_by == undefined){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.submitted_by'));
    }else if($scope.filterOptions.knowing_by == undefined){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.knowing_by'));
    }else if($scope.filterOptions.approved_by == undefined){
      $dialogs.error('Warning', localizedMessages.get('outpatient.error.approved_by'));
    }else if($scope.filterOptions.location == undefined){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.location'));
    }else{
      windowObjectReference = $window.open(APP_CONFIG.appBaseUrl + '/api/paymentlazadas/payment/pdfs/'+$scope.filterOptions.location+'/'+$scope.filterOptions.period+'/'+$scope.filterOptions.submitted_by+'/'+$scope.filterOptions.knowing_by+'/'+$scope.filterOptions.approved_by, '_blank');
      windowObjectReference.focus();
    }
  };

  $scope.print2 = function () {
    var windowObjectReference = null;
    if($scope.filterOptions.filterText == undefined){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.period'));
    }else if($scope.filterOptions.submitted_by == undefined){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.submitted_by'));
    }else if($scope.filterOptions.knowing_by == undefined){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.knowing_by'));
    }else if($scope.filterOptions.approved_by == undefined){
      $dialogs.error('Warning', localizedMessages.get('outpatient.error.approved_by'));
    }else if($scope.filterOptions.location == undefined){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.location'));
    }else{
      windowObjectReference = $window.open(APP_CONFIG.appBaseUrl + '/api/paymentlazadas/requestpayment/pdfs/'+$scope.filterOptions.location+'/'+$scope.filterOptions.period+'/'+$scope.filterOptions.submitted_by+'/'+$scope.filterOptions.knowing_by+'/'+$scope.filterOptions.approved_by, '_blank');
      windowObjectReference.focus();
    }
  };

  $scope.print3 = function () {
    var windowObjectReference = null;
    if($scope.filterOptions.filterText == undefined){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.period'));
    }else if($scope.filterOptions.submitted_by == undefined){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.submitted_by'));
    }else if($scope.filterOptions.knowing_by == undefined){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.knowing_by'));
    }else if($scope.filterOptions.approved_by == undefined){
      $dialogs.error('Warning', localizedMessages.get('outpatient.error.approved_by'));
    }else if($scope.filterOptions.facility_id == undefined){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.location'));
    }else{
      $window.open('#/home/paymentlazada/print/'+$scope.filterOptions.facility_id+'/'+$scope.filterOptions.period+'/'+$scope.filterOptions.approved_by+'/'+$scope.filterOptions.knowing_by+'/'+$scope.filterOptions.submitted_by, '_blank');
    }
  };

  var progress = 10;
  var msgs = [
  'Processing...',
  'Done!'
  ];
  var i = 0;    
  var fakeProgress = function(){
    $timeout(function(){
      if(progress < 95){
        progress += 5;
        $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': progress});
        fakeProgress();
      }else{
      }
    },1000);
  };

  $scope.payed = function() {
    var selectedItem = $scope.gridOptions.selectedItems;
    if(selectedItem.length == 0){
      $dialogs.error('Warning', localizedMessages.get('outpatient.error.pay'));
    }else{
      var dlg = $dialogs.confirm('Konfirmasikan','Apakah Anda yakin ingin menyetujui catatan ini?');
      dlg.result.then(function(btn){
        progress = 10;
        i = 0;
        $dialogs.wait(msgs[0],0);
        fakeProgress();

        for(let i=0; i<selectedItem.length; i++){
          setTimeout( function(){
            var pay = selectedItem[i];
            var where_id = "data_paymentlazada.id = '"+pay.id+"'";
            get_mt.where = where_id;
            Restangular.all("paymentlazadas/active").getList(get_mt).then(function(paymentlazadas) {
              $scope.meal = paymentlazadas.records; 
              $scope.paymentlazadas = Restangular.one('paymentlazadas/'+$scope.meal[0].id);
              $scope.paymentlazadas.save = 2;
              $scope.paymentlazadas.is_active = 5;
              $scope.paymentlazadas.put().then(function() {
                if(selectedItem.length==(i+1)){
                  $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
                  $rootScope.$broadcast('dialogs.wait.complete');
                  $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
                }
              });
            });
          }, i*3000 );
        }
      },function(btn){
      });
    }
  };

  $scope.filterOptions = {
    filterText: null,
    useExternalFilter: true
  };

  $scope.totalServerItems = 0;
  $scope.pagingOptions = {
    pageSizes: [20, 50, 100],
    pageSize: 50,
    currentPage: 1
  };  
  $scope.setPagingData = function(data, page, pageSize){  
    var pagedData = data.records;
    $scope.myData = pagedData;
    $scope.totalServerItems = data.total;
    if (!$scope.$$phase) {
      $scope.$apply();
    }
  };
  $scope.getPagedDataAsync = function (pageSize, page, searchText) {
    setTimeout(function () {
      var custom_get = {};

      if($route.current.params)
        custom_get = $route.current.params;

      custom_get.offset = (page - 1) * pageSize; 
      custom_get.limit = pageSize;

      var where_id = "1 = 1";

      if($scope.filterOptions.filterText != null){
        where_id = where_id+ " and (data_paymentlazada.month = '"+$scope.filterOptions.period.toString().substr(0, 2)+"' and data_paymentlazada.year = '"+$scope.filterOptions.period.toString().substr(-4)+"')";
      }

      if($scope.filterOptions.facility_id != undefined){
        where_id = where_id+ " and data_paymentlazada.facility_id = '"+$scope.filterOptions.facility_id+"'";
      }

      custom_get.where = where_id;

      Restangular.all("paymentlazadas/payed").getList(custom_get).then(function(paymentlazadas) {
        $scope.setPagingData(paymentlazadas,page,pageSize);
      });
    }, 100);
  };

  $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

  $scope.$watch('pagingOptions', function (newVal, oldVal) {
    if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
    }
  }, true);

  $scope.gridOptions = {
    data: 'myData',
    enablePaging: true,
    showFooter: true,
    multiSelect: true,
    selectedItems: [],
    totalServerItems:'totalServerItems',
    pagingOptions: $scope.pagingOptions,
    filterOptions: $scope.filterOptions,
    enableColumnResize: true,
    showSelectionCheckbox: true,
    checkboxHeaderTemplate: '<input class="ngSelectionHeader" type="checkbox" ng-model="allSelected" ng-change="toggleSelectAll(allSelected, true)"/>',
    columnDefs: [
    {field: 'is_active', displayName: 'Status', cellClass: 'text-center', width: 170,
    cellTemplate:   '<div class="ngCellText" style="text-align:center" ng-class="{\'blue\' : row.getProperty(\'is_active\') == 0 , \'orangeyellow\' : row.getProperty(\'is_active\') == 1, \'green\' : row.getProperty(\'is_active\') == 2, \'red\' : row.getProperty(\'is_active\') == 3, \'yellow\' : row.getProperty(\'is_active\') == 4, \'orange\' : row.getProperty(\'is_active\') == 5}">'+
    '<span ng-cell-text ng-show="row.getProperty(col.field)==null">Generated</span>'+
    '<span ng-cell-text ng-show="row.getProperty(col.field)==0">Saved</span>'+
    '<span ng-cell-text ng-show="row.getProperty(col.field)==1">Posted</span>'+
    '<span ng-cell-text ng-show="row.getProperty(col.field)==2">Approved</span>'+
    '<span ng-cell-text ng-show="row.getProperty(col.field)==3">Rejected</span>'+
    '<span ng-cell-text ng-show="row.getProperty(col.field)==4">Finance Process</span>'+
    '<span ng-cell-text ng-show="row.getProperty(col.field)==5">Paid</span></div>'},
    { field: 'code', displayName: 'No Trx', cellClass: 'text-center', width: 150},
    { field: 'facilitycode', displayName: 'Kode Lokasi', width: 150, cellClass: 'text-center'},
    { field: 'facility', displayName: 'Lokasi', width: 300},
    { field: 'work', displayName: 'Jumlah Hari Kerja', cellClass: 'text-right', width: 180, cellFilter: 'noFractionCurrency'},
    { field: 'month', displayName: 'Bulan', cellClass: 'text-center', width: 150},
    { field: 'year', displayName: 'Tahun', cellClass: 'text-center', width: 150},
    { field: 'alltotal', displayName: 'Total', cellClass: 'text-right', width: 180, cellFilter: 'noFractionCurrency'},
    {displayName: 'Detail', cellClass: 'text-center',
    cellTemplate:   '<div class="ngCellText" ng-class="col.colIndex()"><a class="btn btn-xs btn-default" ng-href="" ng-click="detail(row.entity.id, row.entity.facility_id)" data-toggle="tooltip" title="edit" data-original-title="Edit Client"><i class="fa fa-pencil"></i></a></div>'}
    ]
  };

}])

.controller('PaymentlazadaPayListCtrl', ['localizedMessages', 'APP_CONFIG', '$window', '$dialogs', '$modal', '$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function (localizedMessages, APP_CONFIG, $window, $dialogs, $modal, $scope, $route, $location, Restangular, security, i18nNotifications) {

  $scope.paymentlazadas = {};

  Restangular.all("unitlocations/active").getList({orderby: 'name'}).then(function(unitlocations) {
    $scope.unitlocations = unitlocations.records;
  });

  Restangular.all("unitsmallests/active").getList({orderby: 'name'}).then(function(unitsmallests) {
    $scope.unitsmallests = unitsmallests.records;
  });

  $scope.$watch(function() {
    return security.currentUser;
  }, function(currentUser) {
    $scope.currentUser = currentUser;
  });

  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.back = function() {
    $window.history.back();
  };

  $scope.payed = function() {
    $location.path('home/paymentlazada/payed');
  }

  $scope.detail = function(row, facility){
    $location.path('home/paymentlazada/edit/'+row+'/'+facility);
  }

  $scope.change_filter = function(){
    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
  }

  $scope.payment = function () {
    var selectedItem = $scope.gridOptions.selectedItems;
    if(selectedItem.length == 0){
      $dialogs.error('Warning', localizedMessages.get('outpatient.error.pay'));
    }else{
      var dlg = $dialogs.confirm('Konfirmasikan','Apakah Anda yakin ingin melakukkan pembayaran ini?');
      dlg.result.then(function(btn){
        for(var i = 0; i <  selectedItem.length; i++){
          var pay = selectedItem[i];
          $scope.paymentlazadas = Restangular.one('paymentlazadas/'+pay.id);
          $scope.paymentlazadas.save = 2;
          $scope.paymentlazadas.is_active = 4;
          $scope.paymentlazadas.put().then(function() {
            i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : $scope.paymentlazadas.id});
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
          });
        }
      });
    }
  };

  $scope.filterOptions = {
    filterText: null,
    useExternalFilter: true
  };

  $scope.totalServerItems = 0;
  $scope.pagingOptions = {
    pageSizes: [20, 50, 100],
    pageSize: 50,
    currentPage: 1
  };  
  $scope.setPagingData = function(data, page, pageSize){  
    var pagedData = data.records;
    $scope.myData = pagedData;
    $scope.totalServerItems = data.total;
    if (!$scope.$$phase) {
      $scope.$apply();
    }
  };
  $scope.getPagedDataAsync = function (pageSize, page, searchText) {
    setTimeout(function () {
      var custom_get = {};

      if($route.current.params)
        custom_get = $route.current.params;

      custom_get.offset = (page - 1) * pageSize; 
      custom_get.limit = pageSize;

      if (searchText='$scope.filterOptions.filterText') {
        custom_get.filter = $scope.filterOptions.filterText;
        custom_get.filter_fields = 'code,name,nip';

        Restangular.all("paymentlazadas/payment").getList(custom_get).then(function(paymentlazadas) {
          $scope.setPagingData(paymentlazadas,page,pageSize);
        });
      } 
      else {
        Restangular.all("paymentlazadas/payment").getList(custom_get).then(function(paymentlazadas) {
          $scope.setPagingData(paymentlazadas,page,pageSize);
        });
      }
    }, 100);
  };

  $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

  $scope.$watch('pagingOptions', function (newVal, oldVal) {
    if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
    }
  }, true);

  $scope.gridOptions = {
    data: 'myData',
    enablePaging: true,
    showFooter: true,
    multiSelect: true,
    selectedItems: [],
    totalServerItems:'totalServerItems',
    pagingOptions: $scope.pagingOptions,
    filterOptions: $scope.filterOptions,
    enableColumnResize: true,
    showSelectionCheckbox: true,
    checkboxHeaderTemplate: '<input class="ngSelectionHeader" type="checkbox" ng-model="allSelected" ng-change="toggleSelectAll(allSelected, true)"/>',
    columnDefs: [
    {field: 'is_active', displayName: 'Status', cellClass: 'text-center', width: 170,
    cellTemplate:   '<div class="ngCellText" style="text-align:center" ng-class="{\'blue\' : row.getProperty(\'is_active\') == 0 , \'orangeyellow\' : row.getProperty(\'is_active\') == 1, \'green\' : row.getProperty(\'is_active\') == 2, \'red\' : row.getProperty(\'is_active\') == 3, \'yellow\' : row.getProperty(\'is_active\') == 4, \'orange\' : row.getProperty(\'is_active\') == 5}">'+
    '<span ng-cell-text ng-show="row.getProperty(col.field)==null">Generated</span>'+
    '<span ng-cell-text ng-show="row.getProperty(col.field)==0">Saved</span>'+
    '<span ng-cell-text ng-show="row.getProperty(col.field)==1">Posted</span>'+
    '<span ng-cell-text ng-show="row.getProperty(col.field)==2">Approved</span>'+
    '<span ng-cell-text ng-show="row.getProperty(col.field)==3">Rejected</span>'+
    '<span ng-cell-text ng-show="row.getProperty(col.field)==4">Finance Process</span>'+
    '<span ng-cell-text ng-show="row.getProperty(col.field)==5">Paid</span></div>'},
    { field: 'code', displayName: 'No Trx', cellClass: 'text-center', width: 150},
    { field: 'facilitycode', displayName: 'Kode Lokasi', width: 150, cellClass: 'text-center'},
    { field: 'facility', displayName: 'Lokasi', width: 300},
    { field: 'work', displayName: 'Jumlah Hari Kerja', cellClass: 'text-right', width: 180, cellFilter: 'noFractionCurrency'},
    { field: 'month', displayName: 'Bulan', cellClass: 'text-center', width: 150},
    { field: 'year', displayName: 'Tahun', cellClass: 'text-center', width: 150},
    { field: 'alltotal', displayName: 'Total', cellClass: 'text-right', width: 180, cellFilter: 'noFractionCurrency'},
    {displayName: 'Detail', cellClass: 'text-center',
    cellTemplate:   '<div class="ngCellText" ng-class="col.colIndex()"><a class="btn btn-xs btn-default" ng-href="" ng-click="detail(row.entity.id, row.entity.facility_id)" data-toggle="tooltip" title="edit" data-original-title="Edit Client"><i class="fa fa-pencil"></i></a></div>'}
    ]
  };

}])

.controller('PaymentlazadaListCtrl', ['$modal', '$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function ($modal, $scope, $route, $location, Restangular, security, i18nNotifications) {


  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.$watch(function() {
    return security.currentUser;
  }, function(currentUser) {
    $scope.currentUser = currentUser;
  });

  $scope.upload = function() {
    $location.path('home/paymentlazada/upload');
  }

  $scope.canSelect = function() {
    return ($scope.gridOptions.selectedItems.length===0?false:true);
  }

  $scope.select = function () {
    $modalInstance.close($scope.gridOptions.selectedItems[0]);
  };

  $scope.back = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.detail = function(row, facility){
    $location.path('home/paymentlazada/edit/'+row+'/'+facility);
  }

  $scope.change_filter = function(){
    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
  }

  $scope.change_reset = function(){
    $scope.filterOptions.filterText = undefined;
    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
  }

  $scope.filterOptions = {
    filterText: null,
    useExternalFilter: true
  };
  $scope.totalServerItems = 0;
  $scope.pagingOptions = {
    pageSizes: [20, 50, 100],
    pageSize: 50,
    currentPage: 1
  };  
  $scope.setPagingData = function(data, page, pageSize){  
    var pagedData = data.records;
    $scope.myData = pagedData;
    $scope.totalServerItems = data.total;
    if (!$scope.$$phase) {
      $scope.$apply();
    }
  };
  $scope.getPagedDataAsync = function (pageSize, page, searchText) {
    setTimeout(function () {
      var custom_get = {};

      if($route.current.params)
        custom_get = $route.current.params;

      custom_get.offset = (page - 1) * pageSize; 
      custom_get.limit = pageSize;

      if (searchText='$scope.filterOptions.filterText') {
        custom_get.filter = $scope.filterOptions.filterText;
        custom_get.filter_fields = 'data_paymentlazada_detail.tracking_number';

        Restangular.all("paymentlazadas").getList(custom_get).then(function(paymentlazadas) {
          $scope.setPagingData(paymentlazadas,page,pageSize);
        });
      } 
      else {
        Restangular.all("paymentlazadas").getList(custom_get).then(function(paymentlazadas) {
          $scope.setPagingData(paymentlazadas,page,pageSize);
        });
      }
    }, 100);
  };

  $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

  $scope.$watch('pagingOptions', function (newVal, oldVal) {
    if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
    }
  }, true);

  $scope.gridOptions = {
    data: 'myData',
    enablePaging: true,
    showFooter: true,
    multiSelect: false,
    selectedItems: [],
    totalServerItems:'totalServerItems',
    pagingOptions: $scope.pagingOptions,
    filterOptions: $scope.filterOptions,
    enableColumnResize: true,
    enableCellEdit: true,
    columnDefs: [
    {field: 'is_save', displayName: 'Status', cellClass: 'text-center', width: 120,
    cellTemplate:   '<div class="ngCellText" style="text-align:center" ng-class="{\'blue\' : row.getProperty(\'is_save\') == 0 , \'orangeyellow\' : row.getProperty(\'is_save\') == 1, \'green\' : row.getProperty(\'status\') == 2, \'red\' : row.getProperty(\'status\') == 3}">'+
    '<span ng-cell-text ng-show="row.getProperty(col.field)==null">Generated</span>'+
    '<span ng-cell-text ng-show="row.getProperty(col.field)==0">Belum Dibayar</span>'+
    '<span ng-cell-text ng-show="row.getProperty(col.field)==1">Dibayar</span></div>'},
    { field: 'store', displayName: 'Toko', cellClass: 'text-left', width: 130},
    { field: 'date', displayName: 'Tanggal Jual', cellFilter: 'date:\'dd-MMM-yyyy\'', cellClass: 'text-center', width: 100},
    { field: 'order_no', displayName: 'Nomor Order', cellClass: 'text-left', width: 130},
    { field: 'tracking_number', displayName: 'Nomor Resi', cellClass: 'text-left', width: 150},
    { field: 'receiver_name', displayName: 'Nama Pembeli', cellClass: 'text-left', width: 130},
    { field: 'city', displayName: 'Kota', cellClass: 'text-left', width: 150},
    { field: 'product_name', displayName: 'Nama Barang', cellClass: 'text-center', width: 280},
    { field: 'sku', displayName: 'SKU', cellClass: 'text-left', width: 120},
    { field: 'quantity', displayName: 'QTY', cellClass: 'text-right', width: 60, cellFilter: 'noFractionCurrency'},
    { field: 'price', displayName: 'Harga', cellClass: 'text-right', width: 100, cellFilter: 'noFractionCurrency'},
    {displayName: 'Detail', cellClass: 'text-center',
    cellTemplate:   '<div class="ngCellText" ng-class="col.colIndex()"><a class="btn btn-xs btn-default" ng-href="" ng-click="detail(row.entity.id, row.entity.facility_id)" data-toggle="tooltip" title="edit" data-original-title="Edit Client"><i class="fa fa-pencil"></i></a></div>'}
    ]
  };

}])

.controller('PaymentlazadaGenerateCtrl', ['$rootScope', '$route', 'localizedMessages', '$window', '$scope', '$location', 'Restangular', 'security', 'i18nNotifications', '$modal', 'APP_CONFIG', '$upload', '$timeout', '$dialogs', function ($rootScope, $route, localizedMessages, $window, $scope, $location, Restangular, security, i18nNotifications, $modal, APP_CONFIG, $upload, $timeout, $dialogs) {

  var original = {};
  var get_emp = {};
  var get_fac = {};
  var get_his = {};
  var get_meal = {};
  original.family_1 = true;

  $scope.paymentlazada = Restangular.copy(original);

  Restangular.all("facilitys/active").getList().then(function(facilitys) {
    $scope.facilitys = facilitys.records;
  });

  $scope.change_date = function(){
    var today = $scope.paymentlazada.addyear;
    var dd = today.getDate();
    var mm = today.getMonth()+1; 
    var yyyy = today.getFullYear();
    if(dd<10){
      dd='0'+dd
    } 
    if(mm<10){
      mm='0'+mm
    }
    $scope.paymentlazada.year = yyyy;
    $scope.paymentlazada.month = mm;

    $scope.paymentlazada.mydate = yyyy+'-'+mm;

    var where_his = "to_char(data_employeechange.input_date, 'YYYY-MM') = '"+$scope.paymentlazada.mydate+"'";
    get_his.where = where_his;
    Restangular.all("employeechanges/history/join").getList(get_his).then(function(history) {
      $scope.hist(history.records);
    });

  };

  var date = new Date(), y = date.getFullYear(), m = date.getMonth()-1;
  var firstDay = new Date(y, m, 1);
  var lastDay = new Date(y, m + 1, 0);
  $scope.paymentlazada.mydatemax = lastDay; 
  
  $scope.paymentlazada.firstDay = moment(firstDay).format('YYYY-MM-DD');

  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.$watch(function() {
    return security.currentUser;
  }, function(currentUser) {
    $scope.currentUser = currentUser;
  });

  $scope.revert = function() {
    var date = new Date(), y = date.getFullYear(), m = date.getMonth()-1;
    var firstDay = new Date(y, m, 1);
    var lastDay = new Date(y, m + 1, 0);
    $scope.paymentlazada.mydatemax = lastDay; 
    $scope.paymentlazada.month = undefined;
    $scope.paymentlazada.year = undefined;
    $scope.paymentlazada.addyear = undefined;
  };

  $scope.canSave = function() {
    return $scope.PaymentlazadaForm.$valid && !angular.equals($scope.paymentlazada, original);
  };

  $scope.back = function() {
    $window.history.back();
  }

  var progress = 10;
  var msgs = [
  'Processing...',
  'Done!'
  ];
  var i = 0;    
  var fakeProgress = function(){
    $timeout(function(){
      if(progress < 95){
        progress += 5;
        $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': progress});
        fakeProgress();
      }else{
      }
    },1000);
  };

  $scope.hist = function(hist) {
    $scope.hist = hist;
  };

  $scope.generate = function() {
    var where_meal = "data_paymentlazada.month = '"+$scope.paymentlazada.month+"' and data_paymentlazada.year = '"+$scope.paymentlazada.year+"' and data_paymentlazada.facility_id = "+$scope.paymentlazada.facility_id;
    get_meal.where = where_meal;
    Restangular.all("paymentlazadas").getList(get_meal).then(function(paymentlazadas) {
      $scope.paymentlazadas = paymentlazadas.records;
      if($scope.paymentlazadas.length > 0){
        $dialogs.error('Warning', localizedMessages.get('crud.save.paymentlazada.generate'));
      }else{
        var dlg = $dialogs.confirm('Konfirmasikan','Apakah Anda yakin ingin generate catatan ini?');
        dlg.result.then(function(btn){
          progress = 10;
          i = 0;
          $dialogs.wait(msgs[0],0);
          fakeProgress();

          $scope.paymentlazada.company_id = $scope.currentUser.company_id;
          var where_fac = "param_facility.end_date > '"+$scope.paymentlazada.firstDay+"' and param_facility.id = "+$scope.paymentlazada.facility_id;
          get_fac.where = where_fac;

          Restangular.all("facilitys").getList(get_fac).then(function(facilitys) {
            $scope.facilitys = facilitys.records;
            for(let i=0; i<$scope.facilitys.length; i++){
              setTimeout( function(){
                if ($scope.facilitys[i].unitsmallest_id == null){
                  if($scope.hist.length>0){
                    for(var k=0; k<$scope.hist.length; k++){
                      if($scope.hist[k].unitlocation_id == $scope.facilitys[i].unitlocation_id){
                        var where_id = "param_employee.unitlocation_id = '"+$scope.facilitys[i].unitlocation_id+"' or param_employee.id = '"+$scope.hist[k].employee_id+"'";
                        get_emp.where = where_id;
                      }else{
                        var where_id = "param_employee.unitlocation_id = '"+$scope.facilitys[i].unitlocation_id+"'";
                        get_emp.where = where_id;
                      }
                    }
                  }else{
                    var where_id = "param_employee.unitlocation_id = '"+$scope.facilitys[i].unitlocation_id+"'";
                    get_emp.where = where_id;
                  }
                }else{
                  if($scope.hist.length>0){
                    for(var k=0; k<$scope.hist.length; k++){
                      if($scope.hist[k].unitlocation_id == $scope.facilitys[i].unitlocation_id && $scope.hist[k].unitsmallest_id == $scope.facilitys[i].unitsmallest_id){
                        var where_id = "param_employee.unitlocation_id = '"+$scope.facilitys[i].unitlocation_id+"' and param_employee.unitsmallest_id = '"+$scope.facilitys[i].unitsmallest_id+"' or param_employee.id = '"+$scope.hist[k].employee_id+"'";
                        get_emp.where = where_id;
                      }else{
                        var where_id = "param_employee.unitlocation_id = '"+$scope.facilitys[i].unitlocation_id+"' and param_employee.unitsmallest_id = '"+$scope.facilitys[i].unitsmallest_id+"'";
                        get_emp.where = where_id;
                      }
                    }
                  }else{
                    var where_id = "param_employee.unitlocation_id = '"+$scope.facilitys[i].unitlocation_id+"' and param_employee.unitsmallest_id = '"+$scope.facilitys[i].unitsmallest_id+"'";
                    get_emp.where = where_id;
                  }
                }
                $scope.paymentlazada.facility_id = $scope.facilitys[i].id;
                $scope.paymentlazada.save = 1;
                Restangular.all('paymentlazadas').post($scope.paymentlazada).then(function(paymentlazada) {
                  i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : paymentlazada.id});
                  Restangular.all("employees/paymentlazada/active").getList(get_emp).then(function(employees) {
                    $scope.employees = employees.records;
                    for(let j=0; j<$scope.employees.length; j++){
                      setTimeout( function(){
                        $scope.paymentlazada.paymentlazada_id = paymentlazada.id;
                        $scope.paymentlazada.employee_id = $scope.employees[j].employee_id;
                        $scope.paymentlazada.position_id = $scope.employees[j].position_id;
                        $scope.paymentlazada.level_id    = $scope.employees[j].level_id;
                        $scope.paymentlazada.nip         = $scope.employees[j].nip;
                        $scope.paymentlazada.name        = $scope.employees[j].name;
                        $scope.paymentlazada.bank_id     = $scope.employees[j].bank_id;
                        $scope.paymentlazada.bank_name   = $scope.employees[j].bank_name;
                        $scope.paymentlazada.bank_no     = $scope.employees[j].bank_no;
                        $scope.paymentlazada.rp_meal_allowance       = $scope.employees[j].rp_meal_allowance;
                        $scope.paymentlazada.rp_transport_allowance  = $scope.employees[j].rp_transport_allowance;
                        $scope.paymentlazada.rp_presence             = $scope.employees[j].rp_presence;
                        $scope.paymentlazada.rp_fasting              = $scope.employees[j].rp_fasting;
                        $scope.paymentlazada.rp_uml                  = $scope.employees[j].rp_uml;
                        $scope.paymentlazada.rp_incentive            = $scope.employees[j].rp_incentive;
                        $scope.paymentlazada.save         = 2;
                        Restangular.all('paymentlazadas').post($scope.paymentlazada).then(function(paymentlazada) {
                          if(($scope.facilitys.length==(i+1))&&($scope.employees.length==(j+1))){
                            $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
                            $rootScope.$broadcast('dialogs.wait.complete');
                            i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : paymentlazada.id});
                            $location.path('/home/paymentlazada');
                          }
                        });
                      }, j*3000 );
                    }
                  });
                });
              }, i*6000 );
              //
            }
          });
          //
        },function(btn){
        });
        //
      }
    });
    //
  };

}])

.controller('PaymentlazadaEditCtrl', ['$route', 'localizedMessages','$window', '$scope', '$location', 'Restangular', 'item', 'facility', 'i18nNotifications', '$dialogs', 'security', 'simpleDeleteConfirm', '$modal', 'APP_CONFIG', '$upload', '$timeout', function ($route, localizedMessages, $window, $scope, $location, Restangular, item, facility, i18nNotifications, $dialogs, security, simpleDeleteConfirm, $modal, APP_CONFIG, $upload, $timeout) {

  var original = item;
  var facilitys = facility;
  var get_fac = {};

  $scope.paymentlazada = Restangular.copy(original);
  $scope.facility = Restangular.copy(facilitys);

  Restangular.all("facilitys").getList().then(function(facilitys) {
    $scope.facilitys = facilitys.records;
  });

  Restangular.all("positions/active").getList({orderby: 'name'}).then(function(positions) {
    $scope.positions = positions.records;
  });

  Restangular.all("levels/active").getList().then(function(levels) {
    $scope.levels = levels.records;
  });

  Restangular.all("banks/active").getList().then(function(banks) {
    $scope.banks = banks.records;
  });

  Restangular.all("departments/active").getList({orderby: 'name'}).then(function(departments) {
    $scope.departments = departments.records;
  });

  $scope.changemeal = function(row){
    row.entity.total_meal_allowance = parseInt(row.entity.amount_meal_allowance) * parseInt(row.entity.meal_allowance);
    row.entity.alltotal = parseInt(row.entity.total_meal_allowance) + parseInt(row.entity.total_transport_allowance) + parseInt(row.entity.total_presence) + parseInt(row.entity.total_fasting) + parseInt(row.entity.total_uml) + parseInt(row.entity.total_incentive);
    $scope.alltotal();
  };

  $scope.changetransport = function(row){
    if(row.entity.transport_allowance == 0){
      $dialogs.error('Warning', localizedMessages.get('inpatient.error.notchange'));
      row.entity.amount_transport_allowance = 0;
    }else{
      row.entity.total_transport_allowance = parseInt(row.entity.amount_transport_allowance) * parseInt(row.entity.transport_allowance);
    }
    row.entity.alltotal = parseInt(row.entity.total_meal_allowance) + parseInt(row.entity.total_transport_allowance) + parseInt(row.entity.total_presence) + parseInt(row.entity.total_fasting) + parseInt(row.entity.total_uml) + parseInt(row.entity.total_incentive);
    $scope.alltotal();
  };

  $scope.changepresence = function(row){
    if(row.entity.presence == 0){
      $dialogs.error('Warning', localizedMessages.get('inpatient.error.notchange'));
      row.entity.amount_presence = 0;
    }else{
      row.entity.total_presence = parseInt(row.entity.amount_presence) * parseInt(row.entity.presence);
    }
    row.entity.alltotal = parseInt(row.entity.total_meal_allowance) + parseInt(row.entity.total_transport_allowance) + parseInt(row.entity.total_presence) + parseInt(row.entity.total_fasting) + parseInt(row.entity.total_uml) + parseInt(row.entity.total_incentive);
    $scope.alltotal();
  };

  $scope.changeincentive = function(row){
    if(row.entity.incentive == 0){
      $dialogs.error('Warning', localizedMessages.get('inpatient.error.notchange'));
      row.entity.amount_incentive = 0;
    }else{
      row.entity.total_incentive = parseInt(row.entity.amount_incentive) * parseInt(row.entity.incentive);
    }
    row.entity.alltotal = parseInt(row.entity.total_meal_allowance) + parseInt(row.entity.total_transport_allowance) + parseInt(row.entity.total_presence) + parseInt(row.entity.total_fasting) + parseInt(row.entity.total_uml) + parseInt(row.entity.total_incentive);
    $scope.alltotal();
  };

  $scope.changefast = function(row){
    row.entity.total_fasting = parseInt(row.entity.amount_fasting) * parseInt(row.entity.fasting);
    row.entity.alltotal = parseInt(row.entity.total_meal_allowance) + parseInt(row.entity.total_transport_allowance) + parseInt(row.entity.total_presence) + parseInt(row.entity.total_fasting) + parseInt(row.entity.total_uml) + parseInt(row.entity.total_incentive);
    $scope.alltotal();
  };

  $scope.changeuml = function(row){
    row.entity.total_uml = parseInt(row.entity.amount_uml) * parseInt(row.entity.uml);
    row.entity.alltotal = parseInt(row.entity.total_meal_allowance) + parseInt(row.entity.total_transport_allowance) + parseInt(row.entity.total_presence) + parseInt(row.entity.total_fasting) + parseInt(row.entity.total_uml) + parseInt(row.entity.total_incentive);
    $scope.alltotal();
  };

  $scope.alltotal = function (){
    $scope.paymentlazada.totalmeal = 0;
    $scope.paymentlazada.totaltransport = 0;
    $scope.paymentlazada.totalpresence = 0;
    $scope.paymentlazada.totalfasting = 0;
    $scope.paymentlazada.totaluml = 0;
    $scope.paymentlazada.totalincentive = 0;
    $scope.paymentlazada.alltotal = 0;
    for(var i=0; i<$scope.paymentlazada.details.length; i++){
      $scope.paymentlazada.totalmeal = parseInt($scope.paymentlazada.totalmeal) + parseInt($scope.paymentlazada.details[i].total_meal_allowance);
      $scope.paymentlazada.totaltransport = parseInt($scope.paymentlazada.totaltransport) + parseInt($scope.paymentlazada.details[i].total_transport_allowance);
      $scope.paymentlazada.totalpresence = parseInt($scope.paymentlazada.totalpresence) + parseInt($scope.paymentlazada.details[i].total_presence);
      $scope.paymentlazada.totalfasting = parseInt($scope.paymentlazada.totalfasting) + parseInt($scope.paymentlazada.details[i].total_fasting);
      $scope.paymentlazada.totaluml = parseInt($scope.paymentlazada.totaluml) + parseInt($scope.paymentlazada.details[i].total_uml);
      $scope.paymentlazada.totalincentive = parseInt($scope.paymentlazada.totalincentive) + parseInt($scope.paymentlazada.details[i].total_incentive);
      $scope.paymentlazada.alltotal = parseInt($scope.paymentlazada.alltotal) + parseInt($scope.paymentlazada.details[i].alltotal);
    }
  };

  $scope.changelevel = function(row){
    if(row.entity.level_id > 10){
      row.entity.transport_allowance = 0;
    }else{
      row.entity.transport_allowance = $scope.facility.rp_transport_allowance;
    }
    if(row.entity.level_id > 16){
      row.entity.presence = 0;
    }else{
      row.entity.presence = $scope.facility.rp_presence;
    }
    if(row.entity.level_id < 11){
      row.entity.incentive = 0;
    }else{
      row.entity.incentive = $scope.facility.rp_incentive;
    }
  };

  if($scope.facility.meal_allowance == '1'){
    var ma = true;
  }else{
    var ma = false;
  }

  if($scope.facility.transport_allowance == '1'){
    var ta = true;
  }else{
    var ta = false;
  }

  if($scope.facility.presence_allowance == '1'){
    var prs = true;
  }else{
    var prs = false;
  }

  if($scope.facility.fasting_allowance == '1'){
    var fst = true;
  }else{
    var fst = false;
  }

  if($scope.facility.uml_allowance == '1'){
    var um = true;
  }else{
    var um = false;
  }

  if($scope.facility.incentive_allowance == '1'){
    var inc = true;
  }else{
    var inc = false;
  }

  $scope.add = function() {
    var modalInstance = $modal.open({
      templateUrl: 'scripts/app/home/data_paymentlazada/paymentlazada.list.modal.tpl.html',
      controller: ModalInstanceBrowseEmployeeCtrl,
      resolve: {
        paymentlazada: function(Restangular, $route){
          return Restangular.copy($scope.paymentlazada);
        },
        unitlocation_id: function () {
          return $scope.facility.unitlocation_id;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {

      for (var i = 0; i < selectedItem.length; i++){
        var paymentlazada = {};
        paymentlazada.pivot = {};
        paymentlazada.employee_id = selectedItem[i].id;
        paymentlazada.nip = selectedItem[i].nip;
        paymentlazada.name = selectedItem[i].name;
        paymentlazada.department_id = selectedItem[i].department_id;
        paymentlazada.department = selectedItem[i].department;
        paymentlazada.position_id = selectedItem[i].position_id;
        paymentlazada.position = selectedItem[i].position;
        paymentlazada.level_id = selectedItem[i].level_id;
        paymentlazada.bank_id = selectedItem[i].bank_id;
        paymentlazada.bank_name = selectedItem[i].bank_name;
        paymentlazada.bank_no = selectedItem[i].bank_no;
        paymentlazada.meal_allowance = selectedItem[i].rp_meal_allowance;
        paymentlazada.transport_allowance = selectedItem[i].rp_transport_allowance;
        paymentlazada.presence = selectedItem[i].rp_presence;
        paymentlazada.fasting = selectedItem[i].rp_fasting;
        paymentlazada.uml = selectedItem[i].rp_uml;
        paymentlazada.incentive = selectedItem[i].rp_incentive;
        paymentlazada.amount_meal_allowance = 0;
        paymentlazada.amount_transport_allowance = 0;
        paymentlazada.amount_presence = 0;
        paymentlazada.amount_fasting = 0;
        paymentlazada.amount_uml = 0;
        paymentlazada.amount_incentive = 0;
        paymentlazada.total_meal_allowance = 0;
        paymentlazada.total_transport_allowance = 0;
        paymentlazada.total_presence = 0;
        paymentlazada.total_fasting = 0;
        paymentlazada.total_uml = 0;
        paymentlazada.total_incentive = 0;
        paymentlazada.alltotal = 0;

        $scope.paymentlazada.details.splice($scope.paymentlazada.details.length+1,0,paymentlazada);
      }

      $scope.alltotal();

    }, function () {
    });
  };

  var ModalInstanceBrowseEmployeeCtrl = function (paymentlazada, unitlocation_id, $route, $scope, $dialog, $modalInstance, simpleDeleteConfirm, Restangular, security, i18nNotifications, $upload, APP_CONFIG) {

    var original = angular.copy(paymentlazada);

    $scope.paymentlazada = original;

    Restangular.all("positions/active").getList().then(function(positions) {
      $scope.positions = positions.records;
    });

    Restangular.all("departments/active").getList().then(function(departments) {
      $scope.departments = departments.records;
    });

    $scope.change_filter = function(){
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    }

    $scope.$watch(function() {
      return security.currentUser;
    }, function(currentUser) {
      $scope.currentUser = currentUser;
    });

    $scope.filterOptions = {
      filterText: "",
      useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
      pageSizes: [20, 50, 100, 200],
      pageSize: 200,
      currentPage: 1
    };
    $scope.setPagingData = function(data, page, pageSize){
      var pagedData = data.records;
      $scope.myData = pagedData;
      $scope.totalServerItems = data.total;
      if (!$scope.$$phase) {
        $scope.$apply();
      }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
      setTimeout(function () {
        var custom_get = {};

        if($route.current.params)
          custom_get = $route.current.params;

        custom_get.offset = (page - 1) * pageSize;
        custom_get.limit = pageSize;

        var where_id = "param_employee.unitlocation_id = "+unitlocation_id;

        if($scope.filterOptions.filterText != null){
          where_id = where_id+ " and (param_employee.nip like '%"+$scope.filterOptions.filterText+"%' or param_employee.name like '%"+$scope.filterOptions.filterText+"%')";
        }

        if($scope.filterOptions.position_id != undefined){
          where_id = where_id+ " and param_employee.position_id = "+$scope.filterOptions.position_id;
        }

        if($scope.filterOptions.department_id != undefined){
          where_id = where_id+ " and param_employee.department_id = "+$scope.filterOptions.department_id;
        }

        custom_get.where = where_id;

        Restangular.all("employees/paymentlazada/active").getList(custom_get).then(function(employees) {
          $scope.setPagingData(employees,page,pageSize);
        });
      }, 100);
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
      if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
      }
    }, true);

    $scope.gridOptions = {
      data: 'myData',
      enablePaging: true,
      showFooter: true,
      multiSelect: true,
      selectedItems: [],
      enableColumnResize: true,
      totalServerItems:'totalServerItems',
      pagingOptions: $scope.pagingOptions,
      filterOptions: $scope.filterOptions,
      showSelectionCheckbox: true,
      checkboxHeaderTemplate: '<input class="ngSelectionHeader" type="checkbox" ng-model="allSelected" ng-change="toggleSelectAll(allSelected, true)"/>',
      columnDefs: [
      { field: 'nip', displayName: 'Nip', width: 200, cellClass: "text-center"},
      { field: 'name', displayName: 'Name'},
      { field: 'position', displayName: 'Jabatan', width: 300},
      { field: 'department', displayName: 'Department', width: 250},
      { field: 'unitlocation', displayName: 'Lokasi Unit', width: 250}
      ]
    };

    $scope.canSelect = function() {
      return ($scope.gridOptions.selectedItems.length===0?false:true);
    }

    $scope.select = function () {
      var selectedItem = $scope.gridOptions.selectedItems;
      var isExist = false;
      for(var j = 0; j <$scope.paymentlazada.details.length; j++){
        for(var i = 0; i <  selectedItem.length; i++){
          var as = selectedItem[i];
          if((as.nip==$scope.paymentlazada.details[j].nip)){
            selectedItem.splice(i, 1);
          }
        }
      }

      for(var i = 0; i <  selectedItem.length; i++){
        var as = selectedItem[i];
        for(var j=0; j <  selectedItem.length; j++){
          var sa = selectedItem[j];
          if(i != j){
            if(as.nip==sa.nip){
              selectedItem.splice(j, 1);
            }
          }
        }
      }
      $modalInstance.close(selectedItem);
    };

    $scope.back = function () {
      $modalInstance.dismiss('cancel');
    };
  };

  if($scope.paymentlazada.facility_id == 17 && $scope.paymentlazada.company_id == 2){
    $scope.paymentlazada.cek_dept = true;
  }else{
    $scope.paymentlazada.cek_dept = false;
  }
  
  $scope.detailCellTpl = '<div class="ngCellText" ng-class="col.colIndex()"><a class="btn btn-xs btn-default" ng-href="" ng-click="removeQuotation(row.entity.id, row.rowIndex)" data-toggle="tooltip" title="hapus" data-original-title="Hapus Client"><i class="icon-trash"></i></a></div>';
  
  $scope.details = {
    data: 'paymentlazada.details',
    enablePaging: true,
    showFooter: true,
    multiSelect: false,
    selectedItems: [],
    totalServerItems:'totalServerItems',
    pagingOptions: $scope.pagingOptions,
    filterOptions: $scope.filterOptions,
    enableCellSelection: true,
    enableColumnResize: true,
    columnDefs: [
    { field: 'delete', displayName: 'Delete', cellTemplate: $scope.detailCellTpl, cellClass:'text-center', width: 70},
    { field: 'nip', displayName: 'NIP', cellClass: 'text-center', width: 110, enableCellEdit: true, editableCellTemplate: '<input type="text" ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="9" ng-disabled="row.entity.id!=undefined"/>'},
    { field: 'name', displayName: 'Name', width: 250, enableCellEdit: true, editableCellTemplate: '<input type="text" ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" onkeyup="convertToUppercase(this)" ng-disabled="row.entity.id!=undefined"/>'},
    { field: 'employee_dept', displayName: 'Department', width:200,  visible: $scope.paymentlazada.cek_dept,
    cellTemplate: '<select ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-options="department.id as department.name for department in departments" disabled></select>'},
    { field: 'position_id', displayName: 'Position', width:200, visible: false,
    cellTemplate: '<select ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-options="position.id as position.name for position in positions" disabled></select>'},
    { field: 'level_id', displayName: 'Grade', width:80, cellClass: 'text-center', visible: false,
    cellTemplate: '<select ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-options="level.id as level.name for level in levels" ng-change="changelevel(row)" disabled></select>'},
    { field: 'amount_meal_allowance', displayName: 'Jml Makan', visible:ma, cellClass: 'text-center', width: 100, enableCellEdit: true, cellFilter: 'noFractionCurrency', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-change="changemeal(row)" format="number" maxlength="2"/>'},
    { field: 'amount_transport_allowance', displayName: 'Jml Transport', visible:ta, cellClass: 'text-center', width: 120, enableCellEdit: true, cellFilter: 'noFractionCurrency', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-change="changetransport(row)" format="number" maxlength="2"/>'},
    { field: 'amount_presence', displayName: 'Jml Kehadiran', visible:prs, cellClass: 'text-center', width: 120, enableCellEdit: true, cellFilter: 'noFractionCurrency', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-change="changepresence(row)" format="number" maxlength="2"/>'},
    { field: 'amount_fasting', displayName: 'Jml Puasa', visible:fst, cellClass: 'text-center', width: 100, enableCellEdit: true, cellFilter: 'noFractionCurrency', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-change="changefast(row)" format="number" maxlength="2"/>'},
    { field: 'amount_uml', displayName: 'Jml Lembur', visible:um, cellClass: 'text-center', width: 100, enableCellEdit: true, cellFilter: 'noFractionCurrency', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-change="changeuml(row)" format="number" maxlength="2"/>'},
    { field: 'amount_incentive', displayName: 'Jml Insentif', visible:inc, cellClass: 'text-center', width: 100, enableCellEdit: true, cellFilter: 'noFractionCurrency', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-change="changeincentive(row)" format="number" maxlength="2"/>'},
    { field: 'meal_allowance', displayName: 'Uang Makan', visible:ma, cellClass: 'text-right', width: 100, cellFilter: 'noFractionCurrency'},
    { field: 'transport_allowance', displayName: 'Uang Transport', visible:ta, cellClass: 'text-right', width: 120, cellFilter: 'noFractionCurrency'},
    { field: 'presence', displayName: 'Uang Kehadiran', visible:prs, cellClass: 'text-right', width: 120, cellFilter: 'noFractionCurrency'},
    { field: 'fasting', displayName: 'Uang Puasa', visible:fst, cellClass: 'text-right', width: 100, cellFilter: 'noFractionCurrency'},
    { field: 'uml', displayName: 'Uang Lembur', visible:um, cellClass: 'text-right', width: 120, cellFilter: 'noFractionCurrency'},
    { field: 'incentive', displayName: 'Uang Insentif', visible:inc, cellClass: 'text-right', width: 100, cellFilter: 'noFractionCurrency'},
    { field: 'total_meal_allowance', displayName: 'Total Makan', visible:ma, cellClass: 'text-right', width: 100, cellFilter: 'noFractionCurrency'},
    { field: 'total_transport_allowance', displayName: 'Total Transport', visible:ta, cellClass: 'text-right', width: 120, cellFilter: 'noFractionCurrency'},
    { field: 'total_presence', displayName: 'Total Kehadiran', visible:prs, cellClass: 'text-right', width: 120, cellFilter: 'noFractionCurrency'},
    { field: 'total_fasting', displayName: 'Total Puasa', visible:fst, cellClass: 'text-right', width: 100, cellFilter: 'noFractionCurrency'},
    { field: 'total_uml', displayName: 'Total Lembur', visible:um, cellClass: 'text-right', width: 100, cellFilter: 'noFractionCurrency'},
    { field: 'total_incentive', displayName: 'Total Insentif', visible:inc, cellClass: 'text-right', width: 100, cellFilter: 'noFractionCurrency'},
    { field: 'alltotal', displayName: 'Total', cellClass: 'text-right', width: 100, cellFilter: 'noFractionCurrency'},
    { field: 'employee_id', displayName: 'employee_id', visible:false},
    ]
  };

  Restangular.all("paymentlazadas/comment").getList().then(function(comment) {
    $scope.comment = comment.records;
    for(var i=0 ; i<$scope.comment.length; i++){
      if($scope.comment[i].paymentlazada_id==$scope.paymentlazada.id && $scope.paymentlazada.is_active==3){
        $scope.paymentlazada.comment = $scope.comment[i].comment;
      }
    }
  });

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.$watch(function() {
    return security.currentUser;
  }, function(currentUser) {
    $scope.currentUser = currentUser;
  });

  $scope.removeQuotation = function(id, index) {
    if(id==undefined){
      $scope.paymentlazada.details.splice(index, 1);
    }else if($scope.paymentlazada.is_active > 1){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.delete'));
    }else{
      var dlg = $dialogs.confirm('Konfirmasi','Apakah Anda yakin ingin menghapus data ini?');
      dlg.result.then(function(btn){
        Restangular.one('paymentlazadas/delete/'+id).remove().then(function() {
          $scope.paymentlazada.details.splice(index, 1);
        });
      },function(btn){
      });
    }
  };

  $scope.back = function() {
    $window.history.back();
  }

  $scope.canSave = function() {
    return !angular.equals($scope.paymentlazada, original);
  }
  
  $scope.save = function() {
    var mealtrans = [];
    for(var i=0; i<$scope.paymentlazada.details.length; i++){
      if($scope.paymentlazada.details[i].name == undefined || $scope.paymentlazada.details[i].name == ''){
        var x = 1;
      }else if($scope.paymentlazada.details[i].bank_id == undefined || $scope.paymentlazada.details[i].bank_id == ''){
        var x = 2;
      }else if($scope.paymentlazada.details[i].bank_name == undefined || $scope.paymentlazada.details[i].bank_name == ''){
        var x = 3;
      }else if($scope.paymentlazada.details[i].bank_no == undefined || $scope.paymentlazada.details[i].bank_no == ''){
        var x = 4;
      }
      mealtrans.push($scope.paymentlazada.details[i].nip);
    }
    for(var m=0; m<$scope.paymentlazada.details.length; m++){
      for(var n=0; n<mealtrans.length; n++){
        if(m != n){
          if($scope.paymentlazada.details[m].nip == mealtrans[n]){
            var x = 5;
          }
        }
      }
    }
    if(x == 1){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.name'));
    }else if(x == 2){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.bank_id'));
    }else if(x == 3){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.bank_name'));
    }else if(x == 4){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.bank_no'));
    }else if(x == 5){
      $dialogs.error('Warning', localizedMessages.get('paymentlazada.error.samenip'));
    }else{
      if($scope.paymentlazada.work > 0){
        $scope.paymentlazada.save = 1;
        $scope.paymentlazada.is_active = 0;
        $scope.paymentlazada.put().then(function() {
          i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : $scope.paymentlazada.id});
          $location.path('/home/paymentlazada');
        });
      }else{
        $dialogs.error('Warning', localizedMessages.get('operational.error.work'));
      }
    }
  };

  $scope.post = function() {
    if($scope.paymentlazada.work > 0){
      var dlg = $dialogs.confirm('Konfirmasikan','Apakah Anda yakin ingin mengeposkan catatan ini?');
      dlg.result.then(function(btn){
        $scope.paymentlazada.save = 2;
        $scope.paymentlazada.is_active = 1;
        $scope.paymentlazada.put().then(function() {
          i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : $scope.paymentlazada.id});
          $location.path('/home/paymentlazada');
        });
      },function(btn){
      });
    }else{
      $dialogs.error('Warning', localizedMessages.get('operational.error.work'));
    }
  };

  $scope.approve = function() {
    var dlg = $dialogs.confirm('Konfirmasikan','Apakah Anda yakin ingin menyetujui catatan ini?');
    dlg.result.then(function(btn){
      $scope.paymentlazada.save = 1;
      $scope.paymentlazada.is_active = 2;
      $scope.paymentlazada.put().then(function() {
        i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : $scope.paymentlazada.id});
        $location.path('/home/paymentlazada');
      });
    },function(btn){
    });
  };

  $scope.canRemove = function() {
    return true;
  }

  $scope.remove = function() {
    simpleDeleteConfirm.openDialog(original, '/home/paymentlazada');
  };

  $scope.reject = function() {
    var modalInstance = $modal.open({
      templateUrl: 'scripts/app/home/data_paymentlazada/comment.list.modal.tpl.html',
      controller: PaymentlazadaCommentCtrl,
      resolve: {
        item: function(Restangular, $route){
          return $route.current.params.id;
        }
      }
    });
    modalInstance.result.then(function () {
      $scope.paymentlazada.save = 2;
      $scope.paymentlazada.is_active = 3;
      $scope.paymentlazada.put().then(function() {
        i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : $scope.paymentlazada.id});
        $location.path('/home/paymentlazada');
      });
    }, function () {
    });
  };

  var PaymentlazadaCommentCtrl = function ($scope, $modalInstance, security, Restangular, item, i18nNotifications, $dialogs) {

    var original = {};

    $scope.comment = Restangular.copy(original);

    $scope.canCreate    = security.canCreate;
    $scope.canUpdate    = security.canUpdate;
    $scope.canRead      = security.canRead;
    $scope.canDelete    = security.canDelete;

    $scope.canSave = function() {
      return $scope.CommentForm.$valid && !angular.equals($scope.comment, original);
    };

    $scope.back = function() {
      $modalInstance.dismiss('canceled');
    };

    $scope.save = function() {
      $scope.comment.paymentlazada_id = item;
      Restangular.all('paymentlazadas/comment').post($scope.comment).then(function(comment) {
        $modalInstance.close(comment);
      });
    };

    $scope.cancel = function(){
      $modalInstance.dismiss('canceled');  
    };
  }

}]);