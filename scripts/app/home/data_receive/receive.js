angular.module('receive', [
    'shared.dialog',
    'ui.bootstrap.datepicker',
    'ui.bootstrap.typeahead',
    'ui.bootstrap.modal',
    'restangular', 
    'services.i18nNotifications'
    ])

.config(['$routeProvider', 'RestangularProvider', function ($routeProvider, RestangularProvider) {

    $routeProvider

    .when('/home/receive', {
        controller: 'ReceiveCtrl',
        templateUrl: 'scripts/app/home/data_receive/openreceive.tpl.html'
    })

    .when('/home/receive/list', {
        controller: 'ReceiveListCtrl',
        templateUrl: 'scripts/app/home/data_receive/listreceive.tpl.html'
    })

    .when('/home/receive/edit/:id', {
        controller: 'ReceiveEditCtrl',
        templateUrl: 'scripts/app/home/data_receive/detail.tpl.html',
        resolve: {
            item: function(Restangular, $route){
                return Restangular.one('purchaseorders', $route.current.params.id).get();
            }
        }
    })
    
    .otherwise({redirectTo:'/home'});

    RestangularProvider.setRestangularFields({
        id: 'id'
    });

    RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
        if (operation === 'put') {
            elem._id = undefined;
            return elem;
        }
        return elem;
    });
}])

.controller('ReceiveCtrl', ['$window', '$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function ($window, $scope, $route, $location, Restangular, security, i18nNotifications) {

    // user permissions
    $scope.canCreate    = security.canCreate;
    $scope.canUpdate    = security.canUpdate;
    $scope.canRead      = security.canRead;
    $scope.canDelete    = security.canDelete;

    $scope.create = function() {
        $location.path('home/receive/create');
    }

    $scope.canSelect = function() {
        return ($scope.gridOptions.selectedItems.length===0?false:true);
    }

    $scope.select = function () {
        $modalInstance.close($scope.gridOptions.selectedItems[0]);
    };

    $scope.back = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.detail = function(row){
        $location.path('home/receive/edit/'+row);
    }

    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [20, 50, 100],
        pageSize: 50,
        currentPage: 1
    };  
    $scope.setPagingData = function(data, page, pageSize){  
        var pagedData = data.records;
        $scope.myData = pagedData;
        $scope.totalServerItems = data.total;
        if (!$scope.$$phase) {
          $scope.$apply();
      }
        //
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        setTimeout(function () {
          var custom_get = {};

          if($route.current.params)
            custom_get = $route.current.params;

        custom_get.offset = (page - 1) * pageSize; 
        custom_get.limit = pageSize;

        if (searchText='$scope.filterOptions.filterText') {

            custom_get.filter = $scope.filterOptions.filterText;
            custom_get.filter_fields = 'id';

            Restangular.all("purchaseorders/openreceive").getList(custom_get).then(function(purchaseorders) {
              $scope.setPagingData(purchaseorders,page,pageSize);
          });
        } 
        else {
            Restangular.all("purchaseorders/openreceive").getList(custom_get).then(function(purchaseorders) {
              $scope.setPagingData(purchaseorders,page,pageSize);
          });
        }
    }, 100);
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
          $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
      }
  }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
          $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
      }
  }, true);

    $scope.gridOptions = {
        data: 'myData',
        enablePaging: true,
        showFooter: true,
        multiSelect: false,
        selectedItems: [],
        totalServerItems:'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        enableColumnResize: true,
        columnDefs: [
        { field: 'is_save', displayName: 'Status', width: 170,
        cellTemplate:   '<div class="ngCellText" style="text-align:center" ng-class="{\'red\' : row.getProperty(\'is_save\')==1}">'+
        '<span ng-cell-text ng-show="row.getProperty(col.field)==1">Open</span></div>'},
        { field: 'id', displayName: 'Code', width: 150, cellClass:'text-center', enableCellEdit: true },
        { field: 'supplier', displayName: 'Supplier', width: 250, enableCellEdit: true },
        { field: 'shipping', displayName: 'Shipping', width: 150, enableCellEdit: true },
        { field: 'to_name', displayName: 'Kepada', width: 200, enableCellEdit: true },
        { field: 'to_ship', displayName: 'Alamat', width: 350, enableCellEdit: true },
        { field: 'ongkir', displayName: 'Ongkos Kirim', width: 150, enableCellEdit: true, cellFilter: 'number', cellClass: 'text-right'},
        {displayName: 'Action', cellClass:'text-center',
        cellTemplate:   '<div class="ngCellText" ng-class="col.colIndex()"><a class="btn btn-xs btn-default" ng-href="" ng-click="detail(row.entity.id)" data-toggle="tooltip" title="edit" data-original-title="Edit Client"><i class="fa fa-pencil"></i></a></div>'}
        ]
    };

}])

.controller('ReceiveListCtrl', ['$window', '$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function ($window, $scope, $route, $location, Restangular, security, i18nNotifications) {

    // user permissions
    $scope.canCreate    = security.canCreate;
    $scope.canUpdate    = security.canUpdate;
    $scope.canRead      = security.canRead;
    $scope.canDelete    = security.canDelete;

    $scope.create = function() {
        $location.path('home/receive/create');
    }

    $scope.canSelect = function() {
        return ($scope.gridOptions.selectedItems.length===0?false:true);
    }

    $scope.select = function () {
        $modalInstance.close($scope.gridOptions.selectedItems[0]);
    };

    $scope.back = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.detail = function(row){
        $location.path('home/receive/edit/'+row);
    }

    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [20, 50, 100],
        pageSize: 50,
        currentPage: 1
    };  
    $scope.setPagingData = function(data, page, pageSize){  
        var pagedData = data.records;
        $scope.myData = pagedData;
        $scope.totalServerItems = data.total;
        if (!$scope.$$phase) {
          $scope.$apply();
      }
        //
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        setTimeout(function () {
          var custom_get = {};

          if($route.current.params)
            custom_get = $route.current.params;

        custom_get.offset = (page - 1) * pageSize; 
        custom_get.limit = pageSize;

        if (searchText='$scope.filterOptions.filterText') {

            custom_get.filter = $scope.filterOptions.filterText;
            custom_get.filter_fields = 'id';

            Restangular.all("purchaseorders/listreceive").getList(custom_get).then(function(purchaseorders) {
              $scope.setPagingData(purchaseorders,page,pageSize);
          });
        } 
        else {
            Restangular.all("purchaseorders/listreceive").getList(custom_get).then(function(purchaseorders) {
              $scope.setPagingData(purchaseorders,page,pageSize);
          });
        }
    }, 100);
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
          $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
      }
  }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
          $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
      }
  }, true);

    $scope.gridOptions = {
        data: 'myData',
        enablePaging: true,
        showFooter: true,
        multiSelect: false,
        selectedItems: [],
        totalServerItems:'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        enableColumnResize: true,
        columnDefs: [
        { field: 'receive_save', displayName: 'Status', width: 170,
        cellTemplate:   '<div class="ngCellText" style="text-align:center" ng-class="{\'green\' : row.getProperty(\'receive_save\')==2}">'+
        '<span ng-cell-text ng-show="row.getProperty(col.field)==2">Receive</span></div>'},
        { field: 'date',  cellFilter: 'date:\'dd-MMM-yyyy\'', displayName: 'Date', cellClass:"text-center", width: 150},
        { field: 'code', displayName: 'Code', width: 150, cellClass:'text-center', enableCellEdit: true },
        { field: 'po_id', displayName: 'PO Code', width: 150, cellClass:'text-center', enableCellEdit: true },
        { field: 'supplier', displayName: 'Supplier', width: 300, enableCellEdit: true },
        { field: 'shipping', displayName: 'Shipping', width: 150, enableCellEdit: true },
        { field: 'to_name', displayName: 'Kepada', width: 200, enableCellEdit: true },
        { field: 'ongkir_receive', displayName: 'Ongkos Kirim', width: 150, enableCellEdit: true, cellFilter: 'number', cellClass: 'text-right'},
        {displayName: 'Action', cellClass:'text-center',
        cellTemplate:   '<div class="ngCellText" ng-class="col.colIndex()"><a class="btn btn-xs btn-default" ng-href="" ng-click="detail(row.entity.po_id)" data-toggle="tooltip" title="edit" data-original-title="Edit Client"><i class="fa fa-pencil"></i></a></div>'}
        ]
    };

}])

.controller('ReceiveEditCtrl', ['APP_CONFIG','$timeout', '$rootScope', '$http', '$window', '$scope', '$route', '$location', 'Restangular', 'item', 'i18nNotifications', '$dialog', 'security', 'simpleDeleteConfirm', '$modal', '$dialogs', function (APP_CONFIG,$timeout, $rootScope, $http, $window, $scope, $route, $location, Restangular, item, i18nNotifications, $dialog, security, simpleDeleteConfirm, $modal, $dialogs) {

    //debugger;
    var original = item;

    original.date_receive = new Date();
    original.transaction_type = '1';

    $scope.receive = Restangular.copy(original);

    $scope.receive.to_name_receive = $scope.receive.to_name;

    Restangular.all("shippings").getList({where:"is_active = 1"}).then(function(shippings) {
        $scope.shippings = shippings.records;
    });

    Restangular.all("transactions").getList({where: "po_id = '"+$scope.receive.id+"'"}).then(function(transactions) {
        $scope.transactions = transactions.records;
        if($scope.transactions.length > 0)
            $scope.receive.receive_code = $scope.transactions[0].code;
    });

    $scope.$watch(function() {
        return security.currentUser;
    }, function(currentUser) {
        $scope.currentUser = currentUser;
    });

    $scope.format = 'dd-MMM-yyyy';

    var transaction_detail = Restangular.copy($scope.receive);

    Restangular.all("purchaseorders_detail/"+$scope.receive.id).getList().then(function(purchaseorders_detail) {
        transaction_detail.purchaseorders_detail = purchaseorders_detail.records;
    })

    $scope.receive = transaction_detail;
    $scope.receive.purchaseorders_detail = transaction_detail.purchaseorders_detail;

    $scope.detailCellTpl = '<div class="ngCellText" ng-class="col.colIndex()"><a ng-href="" ng-click="removeQuotationItem(row.rowIndex, row)"><i class="icon-trash"></i></a></div>';

    $scope.gridOptions = {
        data: 'receive.purchaseorders_detail',
        enablePaging: true,
        showFooter: true,
        multiSelect: false,
        selectedItems: [],
        totalServerItems:'totalServerItems',
        enableColumnResize: true,
        enableCellSelection: true,
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        columnDefs: [
        { displayName: 'Delete', width: 80, cellClass: "text-center", cellTemplate: $scope.detailCellTpl},
        { field: 'item_id', displayName: 'Code', width: 120, cellClass: "text-center"},
        { field: 'item_name', displayName: 'Item'},
        { field: 'item_unit', displayName: 'Unit', width:60, cellClass: "text-center"},
        { field: 'stock', displayName: 'Stok', width:70, cellClass: "text-right", cellFilter: 'noFractionCurrency'},
        { field: 'mystock', displayName: 'MyStok', width:120, cellClass: "text-right", cellFilter: 'noFractionCurrency', visible: false},
        { field: 'item_weight', displayName: 'Berat (gram)', width:100, cellClass: "text-right", cellFilter: 'number'},
        { field: 'total_item_weight', displayName: 'Toral Berat (gram)', width:120, cellClass: "text-right", cellFilter: 'number', visible: false},
        { field: 'qty_receive', displayName: 'Qty Receive*', width: 100, cellClass: "text-right" , enableCellEdit: true, cellFilter: 'noFractionCurrency', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-change="calculate(row)" format="number"/>'},
        { field: 'qty', displayName: 'Qty PO', width: 100, cellClass: "text-right", cellFilter: 'number'},
        { field: 'price', displayName: 'Price', width: 120, cellClass: "text-right", cellFilter: 'number'},
        { field: 'ongkir', displayName: 'Ongkos Kirim' , width: 120, cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'bruto', displayName: 'Bruto' , width: 120, cellClass: "text-right", cellFilter: 'number'},
        { field: 'total_price_item', displayName: 'Total /item' , width: 120, cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'total_price', displayName: 'Total' , width: 120, cellClass: "text-right", cellFilter: 'number:2'}
        ]
    }

    $scope.receive.detail_delete = new Array();
    /*$scope.removeQuotationItem = function(index, row) {
        $scope.receive.purchaseorders_detail.splice(index, 1);
        $scope.receive.detail_delete.push(row.entity.id);
        $scope.calculate();
    };*/
    
    $scope.calculate = function(row) {
        if(row.entity.mystock == 0){
            row.entity.stock = parseInt(row.entity.qty_receive);
        }else{
            row.entity.stock = parseInt(row.entity.mystock) + parseInt(row.entity.qty_receive);
        }

        $scope.receive.qty_receive = 0;
        var x = 0;
        for(var i=0; i<$scope.receive.purchaseorders_detail.length; i++){
            $scope.receive.qty_receive = parseInt($scope.receive.qty_receive) + parseInt($scope.receive.purchaseorders_detail[i].qty_receive);
        }
    };

    $scope.canCreate    = security.canCreate;
    $scope.canUpdate    = security.canUpdate;
    $scope.canRead      = security.canRead;
    $scope.canDelete    = security.canDelete;

    var progress = 10;
    var msgs = [
    'Processing...',
    'Done!'
    ];
    var i = 0;    
    var fakeProgress = function(){
        $timeout(function(){
            if(progress < 95){
                progress += 5;
                $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': progress});
                fakeProgress();
            }else{
            }
        },1000);
    };

    $scope.back = function() {
        $window.history.back();
    }
    $scope.canSave = function() {
        return $scope.ReceiveForm1.$valid && $scope.ReceiveForm2.$valid &&
        !angular.equals($scope.receive, original);
    };

    $scope.received = function(){
        var dlg = $dialogs.confirm('Konfirmasi','Apakah Anda ingin yakin mereceive data ini?');
        dlg.result.then(function(btn){
            $scope.receive.is_save = 2;
            $scope.saved();
        },function(btn){
        });
    };

    $scope.saved = function() { 
        for(var i=0; i<$scope.receive.purchaseorders_detail.length; i++){
            if($scope.receive.purchaseorders_detail[i].qty_receive == undefined || $scope.receive.purchaseorders_detail[i].qty_receive == '' || $scope.receive.purchaseorders_detail[i].qty_receive == 0){
                var x = 1;
            }
        }

        if(x==1){
            $dialogs.error('Warning', 'Nilai Qty Receive tidak boleh kosong !!!')
        }else{
            progress = 10;
            i = 0;
            $dialogs.wait(msgs[0],0);
            fakeProgress();

            $scope.receive.date_receive = moment($scope.receive.date_receive).format('YYYY-MM-DD');

            $scope.receive.put().then(function() {
              i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : $scope.receive.id});
              $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
              $rootScope.$broadcast('dialogs.wait.complete');
              $location.path('/home/receive/list');
          });
        }
    };

}]);

