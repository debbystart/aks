angular.module('route', [
  'shared.dialog',

  'restangular',

  'services.i18nNotifications'
])

.config(['$routeProvider', 'RestangularProvider', function ($routeProvider, RestangularProvider) {

  $routeProvider
    .when('/home/route', {
      controller: 'RouteListCtrl',
      templateUrl: 'scripts/app/home/use_route/list.tpl.html'
    })
    .when('/home/route/edit/:id', {
      controller: 'RouteEditCtrl',
      templateUrl: 'scripts/app/home/use_route/detail.tpl.html',
      resolve: {
        item: function(Restangular, $route){
          return Restangular.one('routes', $route.current.params.id).get();
        }
      }
    })
    .when('/home/route/create', {
      controller: 'RouteCreateCtrl',
      templateUrl: 'scripts/app/home/use_route/detail.tpl.html'
    })
    .otherwise({redirectTo:'/home'});

    RestangularProvider.setRestangularFields({
      id: 'id'
    });

    RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
      if (operation === 'put') {
        elem._id = undefined;
            return elem;
      }
      return elem;
    });
}])

.controller('RouteListCtrl', ['$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function ($scope, $route, $location, Restangular, security, i18nNotifications) {

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.create = function() {
    $location.path('home/route/create');
  }

  $scope.canSelect = function() {
    return ($scope.gridOptions.selectedItems.length===0?false:true);
  }

  $scope.select = function () {
    $modalInstance.close($scope.gridOptions.selectedItems[0]);
  };

  $scope.back = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.detail = function(){

    var selectedItem = $scope.gridOptions.selectedItems[0];

    $location.path('home/route/edit/'+selectedItem.id);

  }

    //--------------------------------------------------------------------------------------------------------------------------------------
    // ng-Grid  

    $scope.filterOptions = {
          filterText: "",
          useExternalFilter: true
      };
      $scope.totalServerItems = 0;
      $scope.pagingOptions = {
          pageSizes: [20, 50, 100],
          pageSize: 50,
          currentPage: 1
      };  
      $scope.setPagingData = function(data, page, pageSize){  
          var pagedData = data.records;
          $scope.myData = pagedData;
          $scope.totalServerItems = data.total;
          if (!$scope.$$phase) {
              $scope.$apply();
          }
      };
      $scope.getPagedDataAsync = function (pageSize, page, searchText) {
          setTimeout(function () {
              var custom_get = {};

              if($route.current.params)
                custom_get = $route.current.params;

              custom_get.offset = (page - 1) * pageSize; 
              custom_get.limit = pageSize;

              if (searchText) {
                  var ft = searchText.toUpperCase();

                  custom_get.filter = ft;
                  custom_get.filter_fields = 'name,label';

                  Restangular.all("routes").getList(custom_get).then(function(routes) {
                    $scope.setPagingData(routes,page,pageSize);
                  });
              } 
              else {
                  Restangular.all("routes").getList(custom_get).then(function(routes) {
                    $scope.setPagingData(routes,page,pageSize);
                  });
              }
          }, 100);
      };
    
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    
      $scope.$watch('pagingOptions', function (newVal, oldVal) {
          if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
          }
      }, true);
      $scope.$watch('filterOptions', function (newVal, oldVal) {
          if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
          }
      }, true);
   
      $scope.gridOptions = {
        data: 'myData',
        enablePaging: true,
        showFooter: true,
        multiSelect: false,
        selectedItems: [],
        totalServerItems:'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        enableColumnResize: true,
        columnDefs: [
            { field: 'name', displayName: 'Nama', width: 250 },
            { field: 'label', displayName: 'Label'}
      ]
      };

    //--------------------------------------------------------------------------------------------------------------------------------------

}])

.controller('RouteEditCtrl', ['$window', '$scope', '$location', 'Restangular', 'item', 'i18nNotifications', '$dialog', 'security', 'simpleDeleteConfirm', function ($window, $scope, $location, Restangular, item, i18nNotifications, $dialog, security, simpleDeleteConfirm) {

  //debugger;

  var original = item;
  $scope.route = Restangular.copy(original);

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.back = function() {
    $window.history.back();
  }

  $scope.canSave = function() {
    return !angular.equals($scope.route, original);
  }
  $scope.save = function() {
    $scope.route.put().then(function() {
      i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : $scope.route.id});
      $location.path('/home/route');
    });
  };

  $scope.canRemove = function() {
    return true;
  }
  $scope.remove = function() {
    simpleDeleteConfirm.openDialog(original, '/home/route');
  };

  $scope.canRevert = function() {
    return !angular.equals($scope.route, original);
  }
  $scope.revert = function() {
    $scope.route =  Restangular.copy(original);
    $scope.routeForm.$setPristine();
  }

}])

.controller('RouteCreateCtrl', ['$window', '$scope', '$location', 'Restangular', 'security', 'i18nNotifications', function ($window, $scope, $location, Restangular, security, i18nNotifications) {
  //debugger;
  var original = {};
  original.is_active = true;

  $scope.route = Restangular.copy(original);

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.canRemove = function() {
    return false;
  }
  $scope.canSave = function() {
    return $scope.routeForm.$valid && 
      !angular.equals($scope.route, original);
  };

  $scope.back = function() {
    $window.history.back();
  }

  $scope.save = function() {
    Restangular.all('routes').post($scope.route).then(function(route) {
      i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : route.id});
      $location.path('/home/route');
    });
  }

  $scope.canRevert = function() {
    return $scope.routeForm.$valid && 
      !angular.equals($scope.route, original);
  }
  $scope.revert = function() {
    $scope.route =  {};
    $scope.routeForm.$setPristine();
  }

}]);