angular.module('owner', [
  'shared.dialog',
  'restangular',
  'services.i18nNotifications'
  ])

.config(['$routeProvider', 'RestangularProvider', function ($routeProvider, RestangularProvider) {

  $routeProvider
  .when('/home/owner', {
    controller: 'OwnerListCtrl',
    templateUrl: 'scripts/app/home/param_owner/list.tpl.html'
  })
  .when('/home/owner/edit/:id', {
    controller: 'OwnerEditCtrl',
    templateUrl: 'scripts/app/home/param_owner/detail.tpl.html',
    resolve: {
      item: function(Restangular, $route){
        return Restangular.one('owners', $route.current.params.id).get();
      }
    }
  })
  .when('/home/owner/create', {
    controller: 'OwnerCreateCtrl',
    templateUrl: 'scripts/app/home/param_owner/detail.tpl.html'
  })

  .otherwise({redirectTo:'/home'});
  RestangularProvider.setRestangularFields({
    id: 'id'
  });

  RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
    if (operation === 'put') {
      elem._id = undefined;
      return elem;
    }
    return elem;
  });
  
}])

.controller('OwnerListCtrl', ['$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function ($scope, $route, $location, Restangular, security, i18nNotifications) {

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.create = function() {
    $location.path('home/owner/create');
  }

  $scope.canSelect = function() {
    return ($scope.gridOptions.selectedItems.length===0?false:true);
  }

  $scope.select = function () {
    $modalInstance.close($scope.gridOptions.selectedItems[0]);
  };

  $scope.back = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.detail = function(row){
    $location.path('home/owner/edit/'+row);
  }

  $scope.change_filter = function(){
    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
  }

  $scope.filterOptions = {
    filterText: null,
    useExternalFilter: true
  };
  $scope.totalServerItems = 0;
  $scope.pagingOptions = {
    pageSizes: [20, 50, 100],
    pageSize: 50,
    currentPage: 1
  };  
  $scope.setPagingData = function(data, page, pageSize){  
    var pagedData = data.records;
    $scope.myData = pagedData;
    $scope.totalServerItems = data.total;
    if (!$scope.$$phase) {
      $scope.$apply();
    }
  };
  $scope.getPagedDataAsync = function (pageSize, page, searchText) {
    setTimeout(function () {
      var custom_get = {};

      if($route.current.params)
        custom_get = $route.current.params;

      custom_get.offset = (page - 1) * pageSize; 
      custom_get.limit = pageSize;

      if (searchText='$scope.filterOptions.filterText') {

        custom_get.filter = $scope.filterOptions.filterText;
        custom_get.filter_fields = 'id,name';

        Restangular.all("owners").getList(custom_get).then(function(owners) {
          $scope.setPagingData(owners,page,pageSize);
        });
      } 
      else {
        Restangular.all("owners").getList(custom_get).then(function(owners) {
          $scope.setPagingData(owners,page,pageSize);
        });
      }
    }, 100);
  };

  $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

  $scope.$watch('pagingOptions', function (newVal, oldVal) {
    if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
    }
  }, true);

  $scope.gridOptions = {
    data: 'myData',
    enablePaging: true,
    showFooter: true,
    multiSelect: false,
    selectedItems: [],
    totalServerItems:'totalServerItems',
    pagingOptions: $scope.pagingOptions,
    filterOptions: $scope.filterOptions,
    enableColumnResize: true,
    columnDefs: [
    { field: 'id', displayName: 'Code', cellClass: 'text-center', enableCellEdit:true, width: 150},
    { field: 'name', displayName: 'Nama', enableCellEdit:true},
    { field: 'mobile', displayName: 'Telephone', width: 200, cellClass: 'text-right'},
    {displayName: 'Detail', cellClass: 'text-center', width: 170,
    cellTemplate: 	'<div class="ngCellText" ng-class="col.colIndex()"><a class="btn btn-xs btn-default" ng-href="" ng-click="detail(row.entity.id)" data-toggle="tooltip" title="edit" data-original-title="Edit Client"><i class="fa fa-pencil"></i></a></div>'}
    ]
  };

}])

.controller('OwnerCreateCtrl', ['$window', '$scope', '$location', 'Restangular', 'security', 'i18nNotifications', '$modal', 'APP_CONFIG', '$upload', '$timeout', '$dialogs', function ($window, $scope, $location, Restangular, security, i18nNotifications, $modal, APP_CONFIG, $upload, $timeout, $dialogs) {
  //debugger;
  var original = {};
  original.is_active = true;

  $scope.owner = Restangular.copy(original);

  Restangular.all("regions/province").getList().then(function(regions) {
    $scope.provinces = regions.records;
  });

  Restangular.all("markets").getList({where:"is_active = 1"}).then(function(markets) {
    $scope.markets = markets.records;
  });

  $scope.changeprov = function() {
    Restangular.all("regions/city").getList({where:"param_region.name = '"+$scope.owner.province+"'"}).then(function(regions) {
      $scope.citys = regions.records;
    });
    $scope.owner.city = undefined;
    $scope.owner.districts = undefined;
    $scope.owner.village = undefined;
    $scope.owner.postal = undefined;
  }

  $scope.changecity = function() {
    Restangular.all("regions/districts").getList({where:"param_region.name = '"+$scope.owner.province+"' and param_region.city = '"+$scope.owner.city+"'"}).then(function(regions) {
      $scope.districts = regions.records;
    });
    $scope.owner.districts = undefined;
    $scope.owner.village = undefined;
    $scope.owner.postal = undefined;
  }

  $scope.changedist = function() {
    Restangular.all("regions/village").getList({where:"param_region.name = '"+$scope.owner.province+"' and param_region.city = '"+$scope.owner.city+"' and param_region.districts = '"+$scope.owner.districts+"'"}).then(function(regions) {
      $scope.villages = regions.records;
    });
    $scope.owner.village = undefined;
    $scope.owner.postal = undefined;
  }

  $scope.changepos = function() {
    Restangular.all("regions/postal").getList({where:"param_region.name = '"+$scope.owner.province+"' and param_region.city = '"+$scope.owner.city+"' and param_region.districts = '"+$scope.owner.districts+"' and param_region.village = '"+$scope.owner.village+"'"}).then(function(regions) {
      $scope.postals = regions.records;
      $scope.owner.postal = $scope.postals[0].postal;
    });
    Restangular.all("regions/code").getList({where:"param_region.name = '"+$scope.owner.province+"' and param_region.city = '"+$scope.owner.city+"' and param_region.districts = '"+$scope.owner.districts+"' and param_region.village = '"+$scope.owner.village+"'"}).then(function(regions) {
      $scope.codes = regions.records;
      $scope.owner.code = $scope.codes[0].code;
    });
  }
  
  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.canRemove = function() {
    return false;
  }
  $scope.canSave = function() {
  	return $scope.OwnerForm.$valid && 
    !angular.equals($scope.owner, original);
  };

  $scope.back = function() {
    $window.history.back();
  }

  $scope.save = function() {
    Restangular.all('owners').post($scope.owner).then(function(owner) {
      i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : owner.id});
      $location.path('/home/owner');
    });
  }

  $scope.canRevert = function() {
    return $scope.OwnerForm.$valid && 
    !angular.equals($scope.owner, original);
  }
  $scope.revert = function() {
    $scope.owner =  {};
    $scope.OwnerForm.$setPristine();
  }

}])

.controller('OwnerEditCtrl', ['localizedMessages', '$window', '$scope', '$location', 'Restangular', 'item', 'i18nNotifications', '$dialogs', 'security', 'simpleDeleteConfirm', '$modal', 'APP_CONFIG', '$upload', '$timeout', function (localizedMessages, $window, $scope, $location, Restangular, item, i18nNotifications, $dialogs, security, simpleDeleteConfirm, $modal, APP_CONFIG, $upload, $timeout) {

  //debugger;
  var original = item;
  $scope.owner = Restangular.copy(original);

  Restangular.all("regions/province").getList().then(function(regions) {
    $scope.provinces = regions.records;
  });

  Restangular.all("regions/city").getList({where:"param_region.name = '"+$scope.owner.province+"'"}).then(function(regions) {
    $scope.citys = regions.records;
  });

  Restangular.all("regions/districts").getList({where:"param_region.city = '"+$scope.owner.city+"'"}).then(function(regions) {
    $scope.districts = regions.records;
  });

  Restangular.all("regions/village").getList({where:"param_region.districts = '"+$scope.owner.districts+"'"}).then(function(regions) {
    $scope.villages = regions.records;
  });

  Restangular.all("markets").getList({where:"is_active = 1"}).then(function(markets) {
    $scope.markets = markets.records;
  });

  $scope.changeprov = function() {
    Restangular.all("regions/city").getList({where:"param_region.name = '"+$scope.owner.province+"'"}).then(function(regions) {
      $scope.citys = regions.records;
    });
    $scope.owner.city = undefined;
    $scope.owner.districts = undefined;
    $scope.owner.village = undefined;
    $scope.owner.postal = undefined;
  }

  $scope.changecity = function() {
    Restangular.all("regions/districts").getList({where:"param_region.name = '"+$scope.owner.province+"' and param_region.city = '"+$scope.owner.city+"'"}).then(function(regions) {
      $scope.districts = regions.records;
    });
    $scope.owner.districts = undefined;
    $scope.owner.village = undefined;
    $scope.owner.postal = undefined;
  }

  $scope.changedist = function() {
    Restangular.all("regions/village").getList({where:"param_region.name = '"+$scope.owner.province+"' and param_region.city = '"+$scope.owner.city+"' and param_region.districts = '"+$scope.owner.districts+"'"}).then(function(regions) {
      $scope.villages = regions.records;
    });
    $scope.owner.village = undefined;
    $scope.owner.postal = undefined;
  }

  $scope.changepos = function() {
    Restangular.all("regions/postal").getList({where:"param_region.name = '"+$scope.owner.province+"' and param_region.city = '"+$scope.owner.city+"' and param_region.districts = '"+$scope.owner.districts+"' and param_region.village = '"+$scope.owner.village+"'"}).then(function(regions) {
      $scope.postals = regions.records;
      $scope.owner.postal = $scope.postals[0].postal;
    });
  }

  $scope.item = function() {
    $scope.owner.total = parseInt($scope.owner.price) * parseInt($scope.owner.amount);
  }

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.back = function() {
    $window.history.back();
  }

  $scope.canSave = function() {
    return !angular.equals($scope.owner, original);
  }
  
  $scope.save = function() {
    $scope.owner.put().then(function() {
      i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : $scope.owner.id});
      $location.path('/home/owner');
    });
  };

  $scope.canRevert = function() {
    return !angular.equals($scope.owner, original);
  }
  $scope.revert = function() {
    $scope.owner =  Restangular.copy(original);
  }
  
  $scope.canRemove = function() {
    return true;
  }

  $scope.remove = function() {
    simpleDeleteConfirm.openDialog(original, '/home/buy');
  };

  $scope.browseBuy = function() {
    var modalInstance = $modal.open({
      templateUrl: 'scripts/app/home/param_owner/owner.list.modal.tpl.html',
      controller: ModalInstanceBrowseOwnerCtrl,
      resolve: {
        buy_id: function () {
          return $scope.owner;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {

      var name = Restangular.copy(selectedItem);
      var owner = Restangular.copy($scope.owner);

      owner.buy_id = name.id;
      owner.buy = name.name;

      $scope.owner = owner;

    }, function () {
    });
  };

  var ModalInstanceBrowseOwnerCtrl = function ($scope, $dialog, $modalInstance, buy_id, Restangular, security, i18nNotifications, $route) {

    $scope.item = buy_id;

    $scope.canCreate    = security.canCreate;
    $scope.canUpdate    = security.canUpdate;
    $scope.canRead      = security.canRead;
    $scope.canDelete    = security.canDelete;
    $scope.isAuthorizedRoute = security.isAuthorizedRoute;

    $scope.filterOptions = {
      filterText: "",
      useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
      pageSizes: [20, 50, 100],
      pageSize: 20,
      currentPage: 1
    };
    $scope.setPagingData = function(data, page, pageSize){
      var pagedData = data.records;
      $scope.myData = pagedData;
      $scope.totalServerItems = data.total;
      if (!$scope.$$phase) {
        $scope.$apply();
      }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
      setTimeout(function () {
        var custom_get = {};

        if($route.current.params)
          custom_get = $route.current.params;

        custom_get.offset = (page - 1) * pageSize;
        custom_get.limit = pageSize;

        var ft = searchText;

        custom_get.filter = ft;
        custom_get.filter_fields = 'code, name';

        Restangular.all("buys").getList(custom_get).then(function(buys) {
          $scope.setPagingData(buys,page,pageSize);
        });
      }, 100);
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
      if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
      }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
      if (newVal !== oldVal) {
        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
      }
    }, true);

    $scope.gridOptions = {
      data: 'myData',
      enablePaging: true,
      showFooter: true,
      multiSelect: false,
      selectedItems: [],
      totalServerItems:'totalServerItems',
      pagingOptions: $scope.pagingOptions,
      filterOptions: $scope.filterOptions,
      columnDefs: [
      { field: 'code', displayName: 'Code', enableCellEdit:true, cellClass: 'text-center', width: 150},
      { field: 'name', displayName: 'Item', enableCellEdit:true}
      ]
    };

    $scope.canSelect = function() {
      return ($scope.gridOptions.selectedItems.length===0?false:true);
    }

    $scope.select = function () {
      $modalInstance.close($scope.gridOptions.selectedItems[0]);
    };

    $scope.back = function () {
      $modalInstance.dismiss('cancel');
    };

  };

}]);

