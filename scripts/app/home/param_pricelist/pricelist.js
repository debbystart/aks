angular.module('pricelist', [
	'shared.dialog',
	'ui.bootstrap.datepicker',
	'ui.bootstrap.typeahead',
	'ui.bootstrap.modal',
	'restangular',
	'services.i18nNotifications'
	])

.config(['$routeProvider', 'RestangularProvider', function ($routeProvider, RestangularProvider) {

	$routeProvider
	.when('/home/pricelist', {
		controller: 'PricelistListCtrl',
		templateUrl: 'scripts/app/home/param_pricelist/list.tpl.html'
	})
	.when('/home/pricelist/edit/:id', {
		controller: 'PricelistEditCtrl',
		templateUrl: 'scripts/app/home/param_pricelist/detail.tpl.html',
		resolve: {
			item: function(Restangular, $route){
				return Restangular.one('pricelists', $route.current.params.id).get({expands: ['pricelists_detail,operationals']});
			}
		}
	})
	.when('/home/pricelist/create', {
		controller: 'PricelistCreateCtrl',
		templateUrl: 'scripts/app/home/param_pricelist/create.tpl.html'
	})

	.otherwise({redirectTo:'/home'});

	RestangularProvider.setRestangularFields({
		id: 'id'
	});

	RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
		if (operation === 'put') {
			elem._id = undefined;
			return elem;
		}
		return elem;
	});
}])

.controller('PricelistListCtrl', ['$window','$filter','APP_CONFIG','$modal','$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function ($window,$filter,APP_CONFIG,$modal,$scope, $route, $location, Restangular, security, i18nNotifications) {

  //debugger;

  Restangular.all("markets").getList({where:'is_active=1'}).then(function(markets) {
    $scope.markets = markets.records;
  });

  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;
  $scope.isAuthorizedRoute = security.isAuthorizedRoute;

  $scope.$watch(function() {
  	return security.currentUser;
  }, function(currentUser) {
  	$scope.currentUser = currentUser;
  });

  $scope.create = function() {
  	$location.path('/home/pricelist/create');
  }

  $scope.detail = function(row){
  	$location.path('home/pricelist/edit/'+row);
  }

  $scope.print =  function (row) {
  	windowObjectReference = $window.open(APP_CONFIG.appBaseUrl + '/api/pricelists/pdf/'+ row.entity.id, '_blank');
  	windowObjectReference.focus();
  }

  $scope.filterOptions = {
    filterText: null,
    filterTextFrom: null,
    filterTextTo: null,
    useExternalFilter: true
  };
  $scope.totalServerItems = 0;
  $scope.pagingOptions = {
    pageSizes: [50, 100, 200],
    pageSize: 50,
    currentPage: 1
  }
  $scope.setPagingData = function(data, page, pageSize){
    var pagedData = data.records;
    $scope.myData = pagedData;
    $scope.totalServerItems = data.total;
    if (!$scope.$$phase) {
      $scope.$apply();
    }
  };
  $scope.getPagedDataAsync = function (pageSize, page, searchText, fromText, toText) {
    setTimeout(function () {
      var custom_get = {};

      if($route.current.params){
        custom_get = $route.current.params;
      }
      custom_get.offset = (page - 1) * pageSize;
      custom_get.limit = pageSize;

      if ($scope.filterOptions.market_id == undefined || $scope.filterOptions.market_id == null) {
        custom_get.where = "1 = 1";
      }else{
        custom_get.where = "market_id = " + $scope.filterOptions.market_id;
      }

      if (searchText='$scope.filterOptions.filterText') {
        custom_get.filter = $scope.filterOptions.filterText;
        custom_get.filter_fields = 'name';

        Restangular.all("pricelists").getList(custom_get).then(function(pricelists) {
          $scope.setPagingData(pricelists,page,pageSize);
        });
      }else{
        Restangular.all("pricelists").getList(custom_get).then(function(pricelists) {
          $scope.setPagingData(pricelists,page,pageSize);
        });
      }

    }, 100);
  };

  $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

  $scope.$watch('pagingOptions', function (newVal, oldVal) {
    if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
    }
  }, true);
  $scope.$watch('filterOptions', function (newVal, oldVal) {
    if (newVal !== oldVal) {
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
    }
  }, true);

  $scope.gridOptions = {
    data: 'myData',
    enablePaging: true,
    showFooter: true,
    multiSelect: false,
    selectedItems: [],
    totalServerItems:'totalServerItems',
    pagingOptions: $scope.pagingOptions,
    filterOptions: $scope.filterOptions,
    enableColumnResize: true,
    enableCellSelection: true,
    enableCellEdit:true,
    columnDefs:
    [{field: 'is_active', displayName: 'Status', width: 200,
    cellTemplate: 	'<div class="ngCellText" style="text-align:center" ng-class="{\'blue\' : row.getProperty(\'is_active\') == 0, \'green\' : row.getProperty(\'is_active\') == 1}">'+
    '<span ng-cell-text ng-show="row.getProperty(col.field)==0">Saved</span>'+
    '<span ng-cell-text ng-show="row.getProperty(col.field)==1">Actived</span></div>'},
    {field: 'id', displayName: 'Code', width: 150, cellClass: "text-center"},
    {field: 'name', displayName: 'Name'},
    {field: 'market', displayName: 'Market', width: 250},
    {field: 'start_date', displayName: 'Start Date', width: 200, cellClass: "text-center",  cellFilter: 'date:\'dd-MMM-yyyy\''},
    {field: 'end_date', displayName: 'End Date', width: 200, cellClass: "text-center",  cellFilter: 'date:\'dd-MMM-yyyy\''},
    {displayName: 'Action', width: 200,
    cellTemplate: 	'<div style="text-align:center" class="ngCellText" ng-class="col.colIndex()"><a class="btn btn-xs btn-default" ng-href="" ng-click="detail(row.entity.id)" data-toggle="tooltip" title="edit" data-original-title="Edit Client"><i class="fa fa-pencil"></i></a> | <a class="btn btn-xs btn-default" ng-href="" ng-click="print(row)" data-toggle="tooltip" title="Export To PDF" data-original-title="Export To Pdf File"><i class="fa fa-file-pdf-o"></i></a></div>'}

    ]
  }

}])

.controller('PricelistCreateCtrl', ['APP_CONFIG','$upload','$timeout', '$rootScope', '$http','$window', '$route', '$scope', '$location', 'Restangular', 'security', 'i18nNotifications', '$modal', '$dialogs', function (APP_CONFIG, $upload, $timeout, $rootScope, $http ,$window, $route, $scope, $location, Restangular, security, i18nNotifications, $modal, $dialogs) {

  //debugger;

  var original = {};
  original.is_active = 1;
  original.operational = 0;
  original.totalnominal = 0;
  original.operationals = new Array();
  original.pricelists_detail = new Array();

  $scope.pricelist = Restangular.copy(original);

  Restangular.all("markets").getList({where:"is_active = 1"}).then(function(markets) {
  	$scope.markets = markets.records;
  });

  $scope.format = 'dd-MMM-yyyy';

  $scope.minDate = new Date();

  $scope.changeStart = function(){
  	$scope.minDateEnd = moment($scope.pricelist.start_date).format('YYYY-MM-DD');
  	$scope.pricelist.end_date = undefined;
  };

  $scope.searchItem = function(val){
  	$scope.filterOptionsItem.filterText = 'item_name:'+val;
  };

  $scope.searchItemCode = function(val){
  	$scope.filterOptionsItem.filterText = 'item_id:'+val;
  };

  $scope.operationals = {
  	data: 'pricelist.operationals',
  	enablePaging: true,
  	showFooter: true,
  	multiSelect: false,
  	selectedItems: [],
  	totalServerItems:'totalServerItems',
  	pagingOptions: $scope.pagingOptions,
  	filterOptions: $scope.filterOptions,
  	enableCellSelection: true,
  	enableColumnResize: true,
  	columnDefs: [
  	{displayName: 'Delete', cellClass: 'text-center', width: 150, cellTemplate: '<div class="ngCellText col8 colt8 text-center" ng-class="col.colIndex()"><a class="btn btn-xs btn-default" ng-click="deleteRow(row)" data-toggle="tooltip" title="hapus" data-original-title="Hapus Client"><i class="icon-trash"></i></a></div>'},
  	{ field: 'name', displayName: 'Nama', enableCellEdit: true, editableCellTemplate: '<input type="text" ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD"  onkeyup="convertToUppercase(this)" required/>'},
  	{ field: 'nominal', displayName: 'Nominal', enableCellEdit: true, cellClass: 'text-right', width: 250, cellFilter: 'noFractionCurrency', editableCellTemplate: '<input type="text" ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number" ng-change="nominal()" required/>'}
  	]
  };

  $scope.deleteRow = function(row) {
  	var index = $scope.pricelist.operationals.indexOf(row.entity);
  	$scope.pricelist.operationals.splice(index, 1);
  	$scope.nominal();
  };

  $scope.add = function (){
  	var y = 0;
  	for(var i=0; i<$scope.pricelist.operationals.length; i++){
  		if($scope.pricelist.operationals[i].name == undefined || $scope.pricelist.operationals[i].nominal == 0){
  			y = 1;
      }
    }
    if(y==0){
      var operationals = {};
      operationals.pivot = {};
      operationals.nominal = 0;
      $scope.pricelist.operationals.push(operationals);
    }else{
      $dialogs.error('Warning', 'Nama dan nominal tidak boleh kosong !!!')
    }
  };

  $scope.nominal = function (){
    $scope.pricelist.totalnominal = 0;
    for(var i=0; i<$scope.pricelist.operationals.length; i++){
      $scope.pricelist.totalnominal = parseInt($scope.pricelist.totalnominal) + parseInt($scope.pricelist.operationals[i].nominal);
    }
    $scope.pricelist.totalnominal = parseInt($scope.pricelist.totalnominal) + parseInt($scope.pricelist.operational)
    $scope.count();
  };

  $scope.filterOptionsItem = {};
  $scope.filterOptionsItem.filterText = "";

  $scope.deleteItemList = '<div class="ngCellText" ng-class="col.colIndex()"><a ng-href="" ng-click="removeQuotationItem(row.entity)"><i class="icon-trash"></i></a></div>';

  $scope.gridOptionsItem = {
    data: 'pricelist.pricelists_detail',
    enablePaging: true,
    showFooter: true,
    multiSelect: false,
    enableSorting: true,
    selectedItems: [],
    totalServerItems:'totalServerItems',
    enableColumnResize: true,
    enableCellSelection: true,
    pagingOptions: $scope.pagingOptions,
    enableFiltering: true,
    filterOptions: $scope.filterOptionsItem,
    columnDefs: [
    { displayName: 'Delete', width: 80, cellTemplate: $scope.deleteItemList, cellClass: "text-center"},
    { field: 'item_id', displayName: 'Code', width: 120, cellClass:'text-center' },
    { field: 'item_name', displayName: 'Name'},
    { field: 'item_unit', displayName: 'Unit', width: 80, cellClass:'text-center' },
    { field: 'stock', displayName: 'Stock', width: 80, cellClass:'text-right', cellFilter: 'number'},
    { field: 'hpp', displayName: 'HPP', width: 100, cellClass:'text-right', cellFilter: 'number:2'},
    { field: 'operational', displayName: 'Operational', width: 100, cellClass:'text-right', cellFilter: 'number:2'},
    { field: 'amounthpp', displayName: 'Jumlah HPP', width: 100, cellClass:'text-right', cellFilter: 'number:2'},
    { field: 'margin', displayName: 'Margin(%)*', width: 100, cellClass: "text-right", cellFilter: 'number:2', enableCellEdit: true, editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number" ng-change="margin()" required>'},
    { field: 'marginrp', displayName: 'Margin(Rp)', width: 100, cellClass: "text-right", cellFilter: 'number:2', enableCellEdit: true, editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number" ng-change="marginrp()" required>'},
    { field: 'item_price', displayName: 'Price', width: 100, cellClass: "text-right", cellFilter: 'number:2', enableCellEdit: true, editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number" ng-change="pricerp()" required>'},
    { field: 'admin', displayName: 'Admin(%)*', width: 100, cellClass: "text-right", cellFilter: 'number', enableCellEdit: true, editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number" ng-change="margin()" required>'},
    { field: 'adminrp', displayName: 'Admin(Rp)', width: 100, cellClass: "text-right", cellFilter: 'number:2'}
    ]
  }

  $scope.removeQuotationItem = function(item) {
    var index = $scope.pricelist.pricelists_detail.indexOf(item);
    $scope.pricelist.pricelists_detail.splice(index, 1);
    $scope.nominal();
  };

  $scope.count = function() {
    $scope.pricelist.totalhpp = 0;
    var x = 0;
    for(var i=0; i<$scope.pricelist.pricelists_detail.length; i++){
      $scope.pricelist.totalhpp = parseFloat($scope.pricelist.totalhpp) + parseFloat($scope.pricelist.pricelists_detail[i].hpp);
    }

    for(var i=0; i<$scope.pricelist.pricelists_detail.length; i++){
      x = parseFloat($scope.pricelist.pricelists_detail[i].hpp / $scope.pricelist.totalhpp) * 100;
      $scope.pricelist.pricelists_detail[i].operational = parseFloat($scope.pricelist.totalnominal) * parseFloat(x / 100);
      $scope.pricelist.pricelists_detail[i].amounthpp = parseFloat($scope.pricelist.pricelists_detail[i].hpp) + parseFloat($scope.pricelist.pricelists_detail[i].operational);
      $scope.pricelist.pricelists_detail[i].adminrp = parseFloat($scope.pricelist.pricelists_detail[i].item_price) * parseFloat($scope.pricelist.pricelists_detail[i].admin / 100);
    }
  };

  $scope.margin = function() {
    for(var i=0; i<$scope.pricelist.pricelists_detail.length; i++){
      $scope.pricelist.pricelists_detail[i].marginrp = parseFloat(parseFloat($scope.pricelist.pricelists_detail[i].hpp) * parseFloat($scope.pricelist.pricelists_detail[i].margin / 100));
      $scope.pricelist.pricelists_detail[i].item_price = parseFloat($scope.pricelist.pricelists_detail[i].marginrp) + parseFloat($scope.pricelist.pricelists_detail[i].amounthpp);
    }
    $scope.count();
  };

  $scope.marginrp = function() {
    for(var i=0; i<$scope.pricelist.pricelists_detail.length; i++){
      $scope.pricelist.pricelists_detail[i].margin = parseFloat(parseFloat($scope.pricelist.pricelists_detail[i].marginrp / $scope.pricelist.pricelists_detail[i].hpp) * 100);
      $scope.pricelist.pricelists_detail[i].item_price = parseFloat($scope.pricelist.pricelists_detail[i].marginrp) + parseFloat($scope.pricelist.pricelists_detail[i].amounthpp);
    }
    $scope.count();
  };

  $scope.pricerp = function() {
    for(var i=0; i<$scope.pricelist.pricelists_detail.length; i++){
      $scope.pricelist.pricelists_detail[i].marginrp = parseFloat($scope.pricelist.pricelists_detail[i].item_price) - parseFloat($scope.pricelist.pricelists_detail[i].amounthpp);
      $scope.pricelist.pricelists_detail[i].margin = parseFloat(parseFloat($scope.pricelist.pricelists_detail[i].marginrp / $scope.pricelist.pricelists_detail[i].hpp) * 100)
    }
    $scope.count();
  };

  $scope.addItem = function(row) {
    var modalInstance = $modal.open({
      templateUrl: 'scripts/app/home/param_pricelist/item.list.modal.tpl.html',
      controller: ModalInstanceAddQuotationItemCreateCtrl,
      resolve: {
        pricelist_month: function(Restangular, $route){
          return Restangular.copy($scope.pricelist);
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {

      for (var i = 0; i < selectedItem.length; i++){
        var item = {};
        item.pivot = {};
        item.item_id = selectedItem[i].item_id;
        item.item_name = selectedItem[i].item_name;
        item.item_unit = selectedItem[i].item_unit;
        item.hpp = selectedItem[i].hpp;
        item.stock = selectedItem[i].qty;
        item.stock_id = selectedItem[i].id;
        item.item_weight = selectedItem[i].item_weight;
        item.margin = 0;
        item.marginrp = 0;
        item.item_price = 0;
        item.operational = 0;
        item.amounthpp = 0;
        item.admin = 0;
        item.adminrp = 0;
        $scope.pricelist.pricelists_detail.push(item);
      }

    }, function () {
    });
  };

  var ModalInstanceAddQuotationItemCreateCtrl = function ($scope, $dialog, $modalInstance, pricelist_month, simpleDeleteConfirm, Restangular, security, i18nNotifications, $upload, APP_CONFIG) {

    var original = angular.copy(pricelist_month);

    $scope.pricelist_month = original;
    
    $scope.filterOptions = {
      filterText: "",
      useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
      pageSizes: [300, 400, 500],
      pageSize: 300,
      currentPage: 1
    };
    $scope.setPagingData = function(data, page, pageSize){
      var pagedData = data.records;
      $scope.myData = pagedData;
      $scope.totalServerItems = data.total;
      if (!$scope.$$phase) {
        $scope.$apply();
      }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
      $dialogs.wait(msgs[0],0);
      fakeProgress();
      setTimeout(function () {
        var custom_get_items = new Array();

        if($route.current.params){
          custom_get_items = $route.current.params;
        }

        custom_get_items.offset = (page - 1) * pageSize;
        custom_get_items.limit = pageSize;

        if (searchText='$scope.filterOptions.filterText') {
          custom_get_items.filter = $scope.filterOptions.filterText;
          custom_get_items.filter_fields = 'item_id, item_name';
          Restangular.all("pricelists/item").getList(custom_get_items).then(function(price_list_locations) {
            $scope.setPagingData(price_list_locations,page,pageSize);
            $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
            $rootScope.$broadcast('dialogs.wait.complete');
          });
        }
        else {
          Restangular.all("pricelists/item").getList(custom_get_items).then(function(price_list_locations) {
            $scope.setPagingData(price_list_locations,page,pageSize);
            $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
            $rootScope.$broadcast('dialogs.wait.complete');
          });
        }
      }, 100);
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
      if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
      }
    }, true);

    var timeoutPromise;
    $scope.$watch('filterOptions', function (newVal, oldVal) {
      $timeout.cancel(timeoutPromise);
      timeoutPromise = $timeout(function(){
        if (newVal !== oldVal) {
          $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
      },1000);
    }, true);

    $scope.gridOptions = {
      data: 'myData',
      enablePaging: true,
      showFooter: true,
      multiSelect: true,
      selectedItems: [],
      enableColumnResize: true,
      totalServerItems:'totalServerItems',
      pagingOptions: $scope.pagingOptions,
      filterOptions: $scope.filterOptions,
      showSelectionCheckbox: true,
      checkboxHeaderTemplate: '<input class="ngSelectionHeader" type="checkbox" ng-model="allSelected" ng-change="toggleSelectAll(allSelected, true)"/>',
      columnDefs: [
      { field: 'item_id', displayName: 'Code', width: 150, cellClass:'text-center' },
      { field: 'item_name', displayName: 'Name'},
      { field: 'item_unit', displayName: 'Unit', width: 110, cellClass:'text-center' },
      { field: 'qty', displayName: 'Stock', width: 110, cellClass:'text-right', cellFilter: 'number', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number"/>'},
      { field: 'hpp', displayName: 'HPP', width: 180, cellClass:'text-right', cellFilter: 'number:2', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number"/>'},
      { field: 'total_price', displayName: 'Total HPP', width: 180, cellClass:'text-right', cellFilter: 'number:2', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number"/>'},
      { field: 'created_at', displayName: 'Date', width: 200, cellClass:'text-center' }
      ]
    };

    $scope.canSelect = function() {
      return ($scope.gridOptions.selectedItems.length===0?false:true);
    }

    $scope.select = function () {
      var selectedItem = $scope.gridOptions.selectedItems;
      var isExist = false;
      for(var j = 0; j <$scope.pricelist_month.pricelists_detail.length; j++){
        for(var i = 0; i <  selectedItem.length; i++){
          var as = selectedItem[i];
          if(as.item_id==$scope.pricelist_month.pricelists_detail[j].item_id){
            selectedItem.splice(i, 1);
          }
        }
      }

      for(var i = 0; i <  selectedItem.length; i++){
        var as = selectedItem[i];
        for(var j=0; j <  selectedItem.length; j++){
          var sa = selectedItem[j];
          if(i != j){
            if(as.item_id==sa.item_id){
              selectedItem.splice(j, 1);
            }
          }
        }
      }
      $modalInstance.close(selectedItem);
    };

    $scope.back = function () {
      $modalInstance.dismiss('cancel');
    };

  };

  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.$watch(function() {
    return security.currentUser;
  }, function(currentUser) {
    $scope.currentUser = currentUser;
  });

  var progress = 10;
  var msgs = [
  'Processing...',
  'Done!'
  ];
  var i = 0;
  var fakeProgress = function(){
    $timeout(function(){
      if(progress < 95){
        progress += 5;
        $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': progress});
        fakeProgress();
      }else{
      }
    },1000);
  };

  $scope.back = function() {
    $window.history.back();
  }

  $scope.canRevert = function() {
    return !angular.equals($scope.pricelist, original);
  }

  $scope.revert = function() {
    $scope.pricelist =  Restangular.copy(original);
  }

  $scope.canSave = function() {
    return $scope.PricelistForm.$valid && !angular.equals($scope.pricelist, original);
  };

  $scope.save = function() {
    if($scope.pricelist.pricelists_detail.length == 0){
      $dialogs.error('Warning', "Item tidak boleh kosong !!!")
    }else{
      progress = 10;
      i = 0;
      $dialogs.wait(msgs[0],0);
      fakeProgress();

      $scope.pricelist.start_date = moment($scope.pricelist.start_date).format('YYYY-MM-DD');
      $scope.pricelist.end_date = moment($scope.pricelist.end_date).format('YYYY-MM-DD')
      Restangular.all('pricelists').post($scope.pricelist).then(function(pricelist) {
        i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : pricelist.id});
        $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
        $rootScope.$broadcast('dialogs.wait.complete');
        $location.path('/home/pricelist');
      });
    }
  }

}])

.controller('PricelistEditCtrl', ['APP_CONFIG','$upload','$timeout', '$rootScope', '$http','$window', '$scope', '$route', '$location', 'Restangular', 'item', 'i18nNotifications', '$dialog', 'security', 'simpleDeleteConfirm', '$modal', '$dialogs', function (APP_CONFIG,$upload,$timeout, $rootScope, $http, $window, $scope, $route, $location, Restangular, item, i18nNotifications, $dialog, security, simpleDeleteConfirm, $modal, $dialogs) {

  //debugger;

  var original = item;
  $scope.pricelist = Restangular.copy(original);

  $scope.pricelist.detail_delete = new Array();
  $scope.pricelist.operational_delete = new Array();

  Restangular.all("markets").getList({where:"is_active = 1"}).then(function(markets) {
    $scope.markets = markets.records;
  });

  $scope.format = 'dd-MMM-yyyy';

  $scope.minDate = new Date();

  $scope.refresh = function(){
    for(var i=0; i<$scope.pricelist.pricelists_detail.length; i++){
      $scope.pricelist.pricelists_detail[i].hpp = $scope.pricelist.pricelists_detail[i].hppstock;
    }
    $scope.nominal();
  };

  $scope.changeStart = function(){
    $scope.minDateEnd = moment($scope.pricelist.start_date).format('YYYY-MM-DD');
    $scope.pricelist.end_date = undefined;
  };

  $scope.searchItem = function(val){
    $scope.filterOptionsItem.filterText = 'item_name:'+val;
  };

  $scope.searchItemCode = function(val){
    $scope.filterOptionsItem.filterText = 'item_id:'+val;
  };

  $scope.operationals = {
    data: 'pricelist.operationals',
    enablePaging: true,
    showFooter: true,
    multiSelect: false,
    selectedItems: [],
    totalServerItems:'totalServerItems',
    pagingOptions: $scope.pagingOptions,
    filterOptions: $scope.filterOptions,
    enableCellSelection: true,
    enableColumnResize: true,
    columnDefs: [
    {displayName: 'Delete', cellClass: 'text-center', width: 150, cellTemplate: '<div class="ngCellText col8 colt8 text-center" ng-class="col.colIndex()"><a class="btn btn-xs btn-default" ng-click="deleteRow(row)" data-toggle="tooltip" title="hapus" data-original-title="Hapus Client"><i class="icon-trash"></i></a></div>'},
    { field: 'name', displayName: 'Nama', enableCellEdit: true, editableCellTemplate: '<input type="text" ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD"  onkeyup="convertToUppercase(this)" required/>'},
    { field: 'nominal', displayName: 'Nominal', enableCellEdit: true, cellClass: 'text-right', width: 250, cellFilter: 'noFractionCurrency', editableCellTemplate: '<input type="text" ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number" ng-change="nominal()" required/>'}
    ]
  };

  $scope.deleteRow = function(row) {
    var index = $scope.pricelist.operationals.indexOf(row.entity);
    $scope.pricelist.operationals.splice(index, 1);
    if(row.entity.id != undefined){
      $scope.pricelist.operational_delete.push(row.entity.id);
    }
    $scope.nominal();
  };

  $scope.add = function (){
    var y = 0;
    for(var i=0; i<$scope.pricelist.operationals.length; i++){
      if($scope.pricelist.operationals[i].name == undefined || $scope.pricelist.operationals[i].nominal == 0){
        y = 1;
      }
    }
    if(y==0){
      var operationals = {};
      operationals.pivot = {};
      operationals.nominal = 0;
      $scope.pricelist.operationals.push(operationals);
    }else{
      $dialogs.error('Warning', 'Nama dan nominal tidak boleh kosong !!!')
    }
  };

  $scope.nominal = function (){
    $scope.pricelist.totalnominal = 0;
    for(var i=0; i<$scope.pricelist.operationals.length; i++){
      $scope.pricelist.totalnominal = parseInt($scope.pricelist.totalnominal) + parseInt($scope.pricelist.operationals[i].nominal);
    }
    $scope.pricelist.totalnominal = parseInt($scope.pricelist.totalnominal) + parseInt($scope.pricelist.operational)
    $scope.count();
  };

  $scope.filterOptionsItem = {};
  $scope.filterOptionsItem.filterText = "";

  $scope.deleteItemList = '<div class="ngCellText" ng-class="col.colIndex()"><a ng-href="" ng-click="removeQuotationItem(row.entity)"><i class="icon-trash"></i></a></div>';

  $scope.gridOptionsItem = {
    data: 'pricelist.pricelists_detail',
    enablePaging: true,
    showFooter: true,
    multiSelect: false,
    enableSorting: true,
    selectedItems: [],
    totalServerItems:'totalServerItems',
    enableColumnResize: true,
    enableCellSelection: true,
    pagingOptions: $scope.pagingOptions,
    enableFiltering: true,
    filterOptions: $scope.filterOptionsItem,
    columnDefs: [
    { displayName: 'Delete', width: 80, cellTemplate: $scope.deleteItemList, cellClass: "text-center"},
    { field: 'item_id', displayName: 'Code', width: 120, cellClass:'text-center' },
    { field: 'item_name', displayName: 'Name'},
    { field: 'item_unit', displayName: 'Unit', width: 80, cellClass:'text-center' },
    { field: 'stock', displayName: 'Stock', width: 80, cellClass:'text-right', cellFilter: 'number'},
    { field: 'hppstock', displayName: 'HPP Stock', width: 100, cellClass:'text-right', cellFilter: 'number:2'},
    { field: 'hpp', displayName: 'HPP', width: 100, cellClass:'text-right', cellFilter: 'number:2'},
    { field: 'operational', displayName: 'Operational', width: 100, cellClass:'text-right', cellFilter: 'number:2'},
    { field: 'amounthpp', displayName: 'Jumlah HPP', width: 100, cellClass:'text-right', cellFilter: 'number:2'},
    { field: 'margin', displayName: 'Margin(%)*', width: 100, cellClass: "text-right", cellFilter: 'number:2', enableCellEdit: true, editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number" ng-change="margin()" required>'},
    { field: 'marginrp', displayName: 'Margin(Rp)', width: 100, cellClass: "text-right", cellFilter: 'number:2', enableCellEdit: true, editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number" ng-change="marginrp()" required>'},
    { field: 'item_price', displayName: 'Price', width: 100, cellClass: "text-right", cellFilter: 'number:2', enableCellEdit: true, editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number" ng-change="pricerp()" required>'},
    { field: 'admin', displayName: 'Admin(%)*', width: 100, cellClass: "text-right", cellFilter: 'number', enableCellEdit: true, editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number" ng-change="margin()" required>'},
    { field: 'adminrp', displayName: 'Admin(Rp)', width: 100, cellClass: "text-right", cellFilter: 'number:2'}
    ]
  }

  $scope.removeQuotationItem = function(item) {
    if($scope.pricelist.pricelists_detail.length == 1){
      $dialogs.error('Warning', "Item tidak boleh kosong !!!")
    }else{
      var index = $scope.pricelist.pricelists_detail.indexOf(item);
      $scope.pricelist.pricelists_detail.splice(index, 1);
      if(item.id != undefined){
        $scope.pricelist.detail_delete.push(item.id);
      }
      $scope.nominal();
    }
  };

  $scope.count = function() {
    $scope.pricelist.totalhpp = 0;
    var x = 0;
    for(var i=0; i<$scope.pricelist.pricelists_detail.length; i++){
      $scope.pricelist.totalhpp = parseFloat($scope.pricelist.totalhpp) + parseFloat($scope.pricelist.pricelists_detail[i].hpp);
    }

    for(var i=0; i<$scope.pricelist.pricelists_detail.length; i++){
      x = parseFloat($scope.pricelist.pricelists_detail[i].hpp / $scope.pricelist.totalhpp) * 100;
      $scope.pricelist.pricelists_detail[i].operational = parseFloat($scope.pricelist.totalnominal) * parseFloat(x / 100);
      $scope.pricelist.pricelists_detail[i].amounthpp = parseFloat($scope.pricelist.pricelists_detail[i].hpp) + parseFloat($scope.pricelist.pricelists_detail[i].operational);
      $scope.pricelist.pricelists_detail[i].adminrp = parseFloat($scope.pricelist.pricelists_detail[i].item_price) * parseFloat($scope.pricelist.pricelists_detail[i].admin / 100);
    }
  };

  $scope.margin = function() {
    for(var i=0; i<$scope.pricelist.pricelists_detail.length; i++){
      $scope.pricelist.pricelists_detail[i].marginrp = parseFloat(parseFloat($scope.pricelist.pricelists_detail[i].hpp) * parseFloat($scope.pricelist.pricelists_detail[i].margin / 100));
      $scope.pricelist.pricelists_detail[i].item_price = parseFloat($scope.pricelist.pricelists_detail[i].marginrp) + parseFloat($scope.pricelist.pricelists_detail[i].amounthpp);
    }
    $scope.count();
  };

  $scope.marginrp = function() {
    for(var i=0; i<$scope.pricelist.pricelists_detail.length; i++){
      $scope.pricelist.pricelists_detail[i].margin = parseFloat(parseFloat($scope.pricelist.pricelists_detail[i].marginrp / $scope.pricelist.pricelists_detail[i].hpp) * 100);
      $scope.pricelist.pricelists_detail[i].item_price = parseFloat($scope.pricelist.pricelists_detail[i].marginrp) + parseFloat($scope.pricelist.pricelists_detail[i].amounthpp);
    }
    $scope.count();
  };

  $scope.pricerp = function() {
    for(var i=0; i<$scope.pricelist.pricelists_detail.length; i++){
      $scope.pricelist.pricelists_detail[i].marginrp = parseFloat($scope.pricelist.pricelists_detail[i].item_price) - parseFloat($scope.pricelist.pricelists_detail[i].amounthpp);
      $scope.pricelist.pricelists_detail[i].margin = parseFloat(parseFloat($scope.pricelist.pricelists_detail[i].marginrp / $scope.pricelist.pricelists_detail[i].hpp) * 100)
    }
    $scope.count();
  };

  $scope.addItem = function(row) {
    var modalInstance = $modal.open({
      templateUrl: 'scripts/app/home/param_pricelist/item.list.modal.tpl.html',
      controller: ModalInstanceAddQuotationItemCreateCtrl,
      resolve: {
        pricelist_month: function(Restangular, $route){
          return Restangular.copy($scope.pricelist);
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {

      for (var i = 0; i < selectedItem.length; i++){
        var item = {};
        item.pivot = {};
        item.item_id = selectedItem[i].item_id;
        item.item_name = selectedItem[i].item_name;
        item.item_unit = selectedItem[i].item_unit;
        item.hpp = selectedItem[i].hpp;
        item.stock = selectedItem[i].qty;
        item.stock_id = selectedItem[i].id;
        item.item_weight = selectedItem[i].item_weight;
        item.margin = 0;
        item.marginrp = 0;
        item.item_price = 0;
        item.operational = 0;
        item.amounthpp = 0;
        item.admin = 0;
        item.adminrp = 0;
        $scope.pricelist.pricelists_detail.push(item);
      }

    }, function () {
    });
  };

  var ModalInstanceAddQuotationItemCreateCtrl = function ($scope, $dialog, $modalInstance, pricelist_month, simpleDeleteConfirm, Restangular, security, i18nNotifications, $upload, APP_CONFIG) {

    var original = angular.copy(pricelist_month);

    $scope.pricelist_month = original;

    $scope.filterOptions = {
      filterText: "",
      useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
      pageSizes: [300, 400, 500],
      pageSize: 300,
      currentPage: 1
    };
    $scope.setPagingData = function(data, page, pageSize){
      var pagedData = data.records;
      $scope.myData = pagedData;
      $scope.totalServerItems = data.total;
      if (!$scope.$$phase) {
        $scope.$apply();
      }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
      $dialogs.wait(msgs[0],0);
      fakeProgress();
      setTimeout(function () {
        var custom_get_items = new Array();

        if($route.current.params){
          custom_get_items = $route.current.params;
        }

        custom_get_items.offset = (page - 1) * pageSize;
        custom_get_items.limit = pageSize;

        if (searchText='$scope.filterOptions.filterText') {
          custom_get_items.filter = $scope.filterOptions.filterText;
          custom_get_items.filter_fields = 'item_id, item_name';
          Restangular.all("pricelists/item").getList(custom_get_items).then(function(price_list_locations) {
            $scope.setPagingData(price_list_locations,page,pageSize);
            $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
            $rootScope.$broadcast('dialogs.wait.complete');
          });
        }
        else {
          Restangular.all("pricelists/item").getList(custom_get_items).then(function(price_list_locations) {
            $scope.setPagingData(price_list_locations,page,pageSize);
            $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
            $rootScope.$broadcast('dialogs.wait.complete');
          });
        }
      }, 100);
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
      if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
      }
    }, true);

    var timeoutPromise;
    $scope.$watch('filterOptions', function (newVal, oldVal) {
      $timeout.cancel(timeoutPromise);
      timeoutPromise = $timeout(function(){
        if (newVal !== oldVal) {
          $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
      },1000);
    }, true);

    $scope.gridOptions = {
      data: 'myData',
      enablePaging: true,
      showFooter: true,
      multiSelect: true,
      selectedItems: [],
      enableColumnResize: true,
      totalServerItems:'totalServerItems',
      pagingOptions: $scope.pagingOptions,
      filterOptions: $scope.filterOptions,
      showSelectionCheckbox: true,
      checkboxHeaderTemplate: '<input class="ngSelectionHeader" type="checkbox" ng-model="allSelected" ng-change="toggleSelectAll(allSelected, true)"/>',
      columnDefs: [
      { field: 'item_id', displayName: 'Code', width: 150, cellClass:'text-center' },
      { field: 'item_name', displayName: 'Name'},
      { field: 'item_unit', displayName: 'Unit', width: 110, cellClass:'text-center' },
      { field: 'qty', displayName: 'Stock', width: 110, cellClass:'text-right', cellFilter: 'number', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number"/>'},
      { field: 'hpp', displayName: 'HPP', width: 180, cellClass:'text-right', cellFilter: 'number:2', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number"/>'},
      { field: 'total_price', displayName: 'Total HPP', width: 180, cellClass:'text-right', cellFilter: 'number:2', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number"/>'},
      { field: 'created_at', displayName: 'Date', width: 200, cellClass:'text-center' }
      ]
    };

    $scope.canSelect = function() {
      return ($scope.gridOptions.selectedItems.length===0?false:true);
    }

    $scope.select = function () {
      var selectedItem = $scope.gridOptions.selectedItems;
      var isExist = false;
      for(var j = 0; j <$scope.pricelist_month.pricelists_detail.length; j++){
        for(var i = 0; i <  selectedItem.length; i++){
          var as = selectedItem[i];
          if(as.item_id==$scope.pricelist_month.pricelists_detail[j].item_id){
            selectedItem.splice(i, 1);
          }
        }
      }

      for(var i = 0; i <  selectedItem.length; i++){
        var as = selectedItem[i];
        for(var j=0; j <  selectedItem.length; j++){
          var sa = selectedItem[j];
          if(i != j){
            if(as.item_id==sa.item_id){
              selectedItem.splice(j, 1);
            }
          }
        }
      }
      $modalInstance.close(selectedItem);
    };

    $scope.back = function () {
      $modalInstance.dismiss('cancel');
    };

  };

  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.$watch(function() {
    return security.currentUser;
  }, function(currentUser) {
    $scope.currentUser = currentUser;
  });

  var progress = 10;
  var msgs = [
  'Processing...',
  'Done!'
  ];
  var i = 0;
  var fakeProgress = function(){
    $timeout(function(){
      if(progress < 95){
        progress += 5;
        $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': progress});
        fakeProgress();
      }else{
      }
    },1000);
  };

  $scope.back = function() {
    $window.history.back();
  }

  $scope.canRevert = function() {
    return !angular.equals($scope.pricelist, original);
  }

  $scope.revert = function() {
    $scope.pricelist =  Restangular.copy(original);
  }

  $scope.canSave = function() {
    return $scope.PricelistForm.$valid && 
    !angular.equals($scope.pricelist, original);
  }

  $scope.save = function() {
    var dlg = $dialogs.confirm('Konfirmasi','Apakah Anda ingin yakin menyimpan data ini?');
    dlg.result.then(function(btn){
      progress = 10;
      i = 0;
      $dialogs.wait(msgs[0],0);
      fakeProgress();
      $scope.pricelist.put().then(function(pricelist) {
        $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
        $rootScope.$broadcast('dialogs.wait.complete');
        $location.path('/home/pricelist');
      });
    },function(btn){
    });
  };

}]);