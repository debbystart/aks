angular.module('profile', [

  'ui.bootstrap.datepicker',
  'ui.bootstrap.typeahead',
  'ui.bootstrap.modal',

	'restangular',

	'services.i18nNotifications'
])

.config(['$routeProvider', 'RestangularProvider', function ($routeProvider, RestangularProvider) {

  $routeProvider
    .when('/home/profile', {
      controller: 'ProfileCtrl',
      templateUrl: 'scripts/app/home/use_profile/detail.tpl.html'
    })
    .otherwise({redirectTo:'/home'});

    RestangularProvider.setRestangularFields({
      id: 'id'
    });

    RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
      if (operation === 'put') {
        elem._id = undefined;
            return elem;
      }
      return elem;
    });
}])

.controller('ProfileCtrl', ['$window', '$timeout', '$scope', '$location', 'Restangular', 'i18nNotifications', '$dialog', 'security', 'simpleDeleteConfirm', function ($window, $timeout, $scope, $location, Restangular, i18nNotifications, $dialog, security, simpleDeleteConfirm) {

  //debugger;
  
  
}])

.controller('ProfileEditCtrl', ['$window', '$timeout', '$scope', '$location', 'Restangular', 'i18nNotifications', '$dialog', 'security', 'simpleDeleteConfirm', function ($window, $timeout, $scope, $location, Restangular, i18nNotifications, $dialog, security, simpleDeleteConfirm) {

  //debugger;
  
  //----------------------------------------------------------------------------------------------
  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.original = {};

  $scope.$watch(function() {
        return security.currentUser;
      }, function(currentUser) {
      if(currentUser!==null){
          Restangular.one('users', currentUser['id']).get().then(function(curr_user) {
            $scope.original = curr_user;
            $scope.profile = Restangular.copy($scope.original);
          });
        }
  });


  $scope.back = function() {
    $window.history.back();
  }

  $scope.canSave = function() {
    return $scope.profileForm.$valid && 
      !angular.equals($scope.profile, $scope.original);
  }
  $scope.save = function() {
    $scope.profile.put().then(function() {
      i18nNotifications.pushForCurrentRoute('crud.save.success', 'success', {id : $scope.profile.id});

        Restangular.one('users', $scope.profile.id).get().then(function(curr_user) {
          $scope.original = curr_user;
          $scope.profile = Restangular.copy($scope.original);
          $scope.profileForm.$setPristine();
        });

    });
  };

  $scope.canRemove = function() {
    return false;
  }

  $scope.canRevert = function() {
    return !angular.equals($scope.profile, $scope.original);
  }
  $scope.revert = function() {
    $scope.profile =  Restangular.copy($scope.original);
    $scope.profileForm.$setPristine();
  }

}])

.controller('ChangePasswordCtrl', ['$dialogs','localizedMessages', '$window', '$timeout', '$scope', '$location', 'Restangular', 'i18nNotifications', '$dialog', 'security', 'simpleDeleteConfirm', function ($dialogs, localizedMessages, $window, $timeout, $scope, $location, Restangular, i18nNotifications, $dialog, security, simpleDeleteConfirm) {

  //debugger;
  
  //----------------------------------------------------------------------------------------------
  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.original = {};

  $scope.$watch(function() {
      return security.currentUser;
    }, function(currentUser) {
      if(currentUser!==null){
          Restangular.one('users', currentUser['id']).get().then(function(curr_user) {
            $scope.original = curr_user;
            $scope.change_pwd = Restangular.copy($scope.original);
          });
      }
  });


  $scope.back = function() {
    $window.history.back();
  }

  $scope.canSave = function() {
    return $scope.change_pwdForm.$valid && 
      !angular.equals($scope.change_pwd, $scope.original);
  }
/*  $scope.save = function() {
    $scope.change_pwd.put().then(function(response) {

      if(response)
        i18nNotifications.pushForCurrentRoute('crud.changePwd.error', 'error', {error : response.error});
      else {

        i18nNotifications.pushForCurrentRoute('crud.changePwd.success', 'success', {id : $scope.change_pwd.id});

        $scope.change_pwd.old_pwd = null;
        $scope.change_pwd.new_pwd = null;
        $scope.change_pwd.retype_new_pwd = null;

        Restangular.one('users', $scope.change_pwd.id).get().then(function(curr_user) {
          $scope.original = curr_user;
          $scope.change_pwd = Restangular.copy($scope.original);
          $scope.change_pwdForm.$setPristine();
        });        
      }
    });
  };*/

  $scope.save = function () {
      $scope.change_pwd.put().then(function() {

        i18nNotifications.pushForCurrentRoute('crud.changePwd.success', 'success', {id : $scope.change_pwd.id});

        $scope.change_pwd.old_pwd = null;
        $scope.change_pwd.new_pwd = null;
        $scope.change_pwd.retype_new_pwd = null;

        Restangular.one('users', $scope.change_pwd.id).get().then(function(curr_user) {
          $scope.original = curr_user;
          $scope.change_pwd = Restangular.copy($scope.original);
          $scope.change_pwdForm.$setPristine();
        });        

      }, function(respond) {
        $dialogs.error('Error', localizedMessages.get(respond.data.error));
      });
    };

  $scope.canRemove = function() {
    return false;
  }

  $scope.canRevert = function() {
    return !angular.equals($scope.change_pwd, $scope.original);
  }
  $scope.revert = function() {
    $scope.change_pwd =  Restangular.copy($scope.original);
    $scope.change_pwdForm.$setPristine();
  }

}]);

