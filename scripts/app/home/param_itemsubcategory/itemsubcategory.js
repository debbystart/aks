angular.module('itemsubcategory', [
  'shared.dialog',
  'restangular',
  'services.i18nNotifications'
  ])

.config(['$routeProvider', 'RestangularProvider', function ($routeProvider, RestangularProvider) {

  $routeProvider
  .when('/home/itemsubcategory', {
    controller: 'ItemsubcategoryListCtrl',
    templateUrl: 'scripts/app/home/param_itemsubcategory/list.tpl.html'
  })
  .when('/home/itemsubcategory/edit/:id', {
    controller: 'ItemsubcategoryEditCtrl',
    templateUrl: 'scripts/app/home/param_itemsubcategory/detail.tpl.html',
    resolve: {
      item: function(Restangular, $route){
        return Restangular.one('itemsubcategorys', $route.current.params.id).get();
      }
    }
  })
  .when('/home/itemsubcategory/create', {
    controller: 'ItemsubcategoryCreateCtrl',
    templateUrl: 'scripts/app/home/param_itemsubcategory/detail.tpl.html'
  })
  .otherwise({redirectTo:'/home'});

  RestangularProvider.setRestangularFields({
    id: 'id'
  });

  RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
    if (operation === 'put') {
      elem._id = undefined;
      return elem;
    }
    return elem;
  });
}])

.controller('ItemsubcategoryListCtrl', ['$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function ($scope, $route, $location, Restangular, security, i18nNotifications) {

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.create = function() {
    $location.path('home/itemsubcategory/create');
  }

  $scope.canSelect = function() {
    return ($scope.gridOptions.selectedItems.length===0?false:true);
  }

  $scope.select = function () {
    $modalInstance.close($scope.gridOptions.selectedItems[0]);
  };

  $scope.back = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.detail = function(row){
    $location.path('home/itemsubcategory/edit/'+row);
  }

  $scope.filterOptions = {
    filterText: "",
    useExternalFilter: true
  };
  $scope.totalServerItems = 0;
  $scope.pagingOptions = {
    pageSizes: [20, 50, 100],
    pageSize: 50,
    currentPage: 1
  };  
  $scope.setPagingData = function(data, page, pageSize){  
    var pagedData = data.records;
    $scope.myData = pagedData;
    $scope.totalServerItems = data.total;
    if (!$scope.$$phase) {
      $scope.$apply();
    }
  };
  $scope.getPagedDataAsync = function (pageSize, page, searchText) {
    setTimeout(function () {
      var custom_get = {};

      if($route.current.params)
        custom_get = $route.current.params;

      custom_get.offset = (page - 1) * pageSize; 
      custom_get.limit = pageSize;

      if (searchText='$scope.filterOptions.filterText') {

        custom_get.filter = $scope.filterOptions.filterText;
        custom_get.filter_fields = 'id,name';

        Restangular.all("itemsubcategorys").getList(custom_get).then(function(itemsubcategorys) {
          $scope.setPagingData(itemsubcategorys,page,pageSize);
        });
      } 
      else {
        Restangular.all("itemsubcategorys").getList(custom_get).then(function(itemsubcategorys) {
          $scope.setPagingData(itemsubcategorys,page,pageSize);
        });
      }
    }, 100);
  };

  $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

  $scope.$watch('pagingOptions', function (newVal, oldVal) {
    if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
    }
  }, true);
  $scope.$watch('filterOptions', function (newVal, oldVal) {
    if (newVal !== oldVal) {
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
    }
  }, true);

  $scope.gridOptions = {
    data: 'myData',
    enablePaging: true,
    showFooter: true,
    multiSelect: false,
    selectedItems: [],
    totalServerItems:'totalServerItems',
    pagingOptions: $scope.pagingOptions,
    filterOptions: $scope.filterOptions,
    columnDefs: [
    { field: 'id', displayName: 'Code', width: 100, cellClass:'text-center' },
    { field: 'name', displayName: 'Name', width: 460 },
    { field: 'group_name', displayName: 'Item Group', width: 300 },
    { field: 'category_name', displayName: 'Item Category', width: 260 },
    { field: 'created_at', displayName: 'Date', width: 180, cellClass:'text-center' },
    { field: 'is_active', displayName: 'Active', width: 130, cellClass:'text-center', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ (row.getProperty(col.field)===1?\'Yes\' : \'No\') }}</span></div>'},
    {displayName: 'Action', cellClass:'text-center',
    cellTemplate: 	'<div class="ngCellText" ng-class="col.colIndex()"><a class="btn btn-xs btn-default" ng-href="" ng-click="detail(row.entity.id)" data-toggle="tooltip" title="edit" data-original-title="Edit Client"><i class="fa fa-pencil"></i></a></div>'}
    ]
  };

}])

.controller('ItemsubcategoryCreateCtrl', ['$window', '$scope', '$location', 'Restangular', 'security', 'i18nNotifications', '$modal', 'APP_CONFIG', '$upload', '$timeout', '$dialogs', function ($window, $scope, $location, Restangular, security, i18nNotifications, $modal, APP_CONFIG, $upload, $timeout, $dialogs) {

  //debugger;
  var original = {};
  original.is_active = true;

  $scope.itemsubcategory = Restangular.copy(original);

  Restangular.all("itemgroups").getList({where:"is_active = 1"}).then(function(itemgroups) {
    $scope.itemgroups = itemgroups.records;
  });

  $scope.changeitemgroup = function() {
    Restangular.all("itemcategorys").getList({where:"is_active = 1 and itemgroup_id = "+$scope.itemsubcategory.itemgroup_id}).then(function(itemcategorys) {
      $scope.itemcategorys = itemcategorys.records;
    });
  };

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.canRemove = function() {
    return false;
  };

  $scope.canSave = function() {
  	return $scope.ItemsubcategoryForm.$valid && 
    !angular.equals($scope.itemsubcategory, original);
  };

  $scope.back = function() {
    $window.history.back();
  }

  $scope.save = function() {
    Restangular.all('itemsubcategorys').post($scope.itemsubcategory).then(function(itemsubcategory) {
      i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : itemsubcategory.id});
      $location.path('/home/itemsubcategory');
    });
  }

  $scope.canRevert = function() {
    return $scope.ItemsubcategoryForm.$valid && 
    !angular.equals($scope.itemsubcategory, original);
  }

  $scope.revert = function() {
    var original = {};
    original.is_active = true;
    $scope.itemsubcategory = Restangular.copy(original);
    $scope.ItemsubcategoryForm.$setPristine();
  }

}])

.controller('ItemsubcategoryEditCtrl', ['$window', '$scope', '$location', 'Restangular', 'item', 'i18nNotifications', '$dialogs', 'security', 'simpleDeleteConfirm', '$modal', 'APP_CONFIG', '$upload', '$timeout', function ($window, $scope, $location, Restangular, item, i18nNotifications, $dialogs, security, simpleDeleteConfirm, $modal, APP_CONFIG, $upload, $timeout) {

  //debugger;
  var original = item;

  $scope.itemsubcategory = Restangular.copy(original);

  Restangular.all("itemgroups").getList({where:"is_active = 1"}).then(function(itemgroups) {
    $scope.itemgroups = itemgroups.records;
  });

  Restangular.all("itemcategorys").getList({where:"is_active = 1 and itemgroup_id = "+$scope.itemsubcategory.itemgroup_id}).then(function(itemcategorys) {
    $scope.itemcategorys = itemcategorys.records;
  });

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.back = function() {
    $window.history.back();
  }

  $scope.canSave = function() {
    return $scope.ItemsubcategoryForm.$valid && 
    !angular.equals($scope.itemsubcategory, original);
  }
  
  $scope.save = function() {
    $scope.itemsubcategory.put().then(function() {
      i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : $scope.itemsubcategory.id});
      $location.path('/home/itemsubcategory');
    });
  };
  
  $scope.canRemove = function() {
    return true;
  }
  $scope.remove = function() {
    simpleDeleteConfirm.openDialog(original, '/home/itemsubcategory');
  };

  $scope.canRevert = function() {
    return !angular.equals($scope.itemsubcategory, original);
  }
  $scope.revert = function() {
    $scope.itemsubcategory =  Restangular.copy(original);
    $scope.ItemsubcategoryForm.$setPristine();
  }

}]);

