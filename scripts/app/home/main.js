angular.module('app', [

  'checklist-model',

  'ui.bootstrap',
  'ui.bootstrap.tpls',
  'ui.bootstrap.tooltip',
  'ui.bootstrap.tabs',
  'ui.bootstrap.collapse',
  'ui.bootstrap.datetimepicker',
  'ui.bootstrap.popover',
  'ui.bootstrap.progressbar',

  'ngRoute',
  'restangular',  
  'ngCookies',

  'services.GeoUTM',
  'services.breadcrumbs',
  'services.i18nNotifications',
  'services.httpRequestTracker',
  'security',
  'textAngular',

  'ngGrid',
  'dialogs',
  'angularFileUpload',

  'directives.customValidations',
  'directives.crud',
  'directives.barcodeGenerator',
  'shared.dialog.delete-factory',

  'home',
  'user_manager',
  'profile',
  'route',
  'directive',
  'company',
  'item',
  'customer',
  'itemgroup',
  'itemcategory',
  'unit',
  'brand',
  'itemsubcategory',
  'purchaseorder',
  'supplier',
  'market',
  'shipping',
  'receive',
  'stock',
  'salesman',
  'salesorder',
  'delivery',
  'adjustment',
  'reasonstock',
  'owner',
  'pricelist',
  'salesorderall',
  'paymentlazada'

  ]);

angular.module('app').constant('APP_CONFIG', {
 appName: 'AKS',
 //appBaseUrl: 'http://pufd.pangansari.co.id'
 appBaseUrl: 'http://localhost/aks'
});

angular.module('app').config(function(RestangularProvider) {
  RestangularProvider
  .setBaseUrl("api");
  RestangularProvider.setRequestInterceptor(
    function(elem, operation, what) {
      if (operation === 'put') {
        elem.id = undefined;
        return elem;
      }
      return elem;
    });      

});

//TODO: move those messages to a separate module

angular.module('app').constant('I18N.MESSAGES', {
  'errors.route.changeError':'Route change error',
  'service.checkTagihan.error':"Pembayaran dari database dispenda belum tersedia! Silahkan coba di lain waktu.",
  'crud.changePwd.success':"Password berhasil disimpan.",
  'crud.changePwd.error':"Password gagal disimpan: {{error}}",
  'crud.oldPwd.error':"Password lama salah !",
  'crud.save.success':"Data berhasil disimpan.",
  'crud.save.inactive':"Master masih digunakan, tidak dapat dinonaktifkan !",
  'crud.save.remove':"Master masih digunakan, tidak dapat dihapus !",

  'crud.remove.success':"Data berhasil dihapus.",
  'crud.remove.error':"Ada yang salah ketika menghapus data dengan id '{{id}}'.",
  'login.reason.notAuthorized':"Anda tidak memiliki izin akses yang diperlukan. Apakah Anda ingin login sebagai orang lain?",
  'login.reason.notAuthenticated':"Anda harus login untuk mengakses bagian dari aplikasi.",
  'login.error.invalidCredentials': "Invalid username or password!",
  'login.error.serverError': "Ada masalah dengan otentikasi: {{exception}}."

});

angular.module('app').constant('I18N.MONTH', {
  'January': 'Januari',
  'February': 'Februari',
  'March': 'Maret',
  'April': 'April',
  'May': 'Mei',
  'June': 'Juni',
  'July': 'Juli',
  'August': 'Agustus',
  'September': 'September',
  'October': 'Oktober',
  'November': 'Nopember',
  'December': 'Desember'
});

angular.module('app').config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider, security) {
  $routeProvider
  .when('/home', {
    templateUrl: 'scripts/app/home/home/home.tpl.html', 
    controller: 'HomeCtrl'})
  .otherwise({redirectTo: '/home'});
  $locationProvider.html5Mode = true;
}]);

angular.module('app').run(['security', function(security) {

  // Get the current user when the application starts

  // (in case they are still logged in from a previous session)
  security.requestCurrentUser();
  //console.log(security.requestCurrentUser)

}]);

angular.module('app').controller('AppCtrl', ['$rootScope','$scope', 'i18nNotifications', 'localizedMessages', '$timeout', 'security', '$cookies',

  function($rootScope, $scope, i18nNotifications, $timeout, security, $window, $cookies) {

    var parent = $rootScope;
    $scope.date = new Date();

    $scope.tes = $cookies.client_id;
  //console.log($scope.tes)
  
  $scope.isAuthenticated = function () {
    if(parent.security===undefined)
      return false;

    if(parent.security.currentUser!==undefined)
      return parent.security.currentUser;

    if(typeof(parent.security.isAuthenticated)===undefined)
      return false;

    if(!parent.security.currentUser) { return false; }
    return parent.security.isAuthenticated;    
  };

  $scope.notifications = i18nNotifications;

  $scope.removeNotification = function (notification) {
    i18nNotifications.remove(notification);
  };



  $scope.$on('$routeChangeError', function(event, current, previous, rejection){
    i18nNotifications.pushForCurrentRoute('errors.route.changeError', 'error', {}, {rejection: rejection});
  });
  
  // Bound to the output display
  $scope.output = "0";

    // Used to evaluate whether to start a new number
    // in the display and when to concatenate
    $scope.newNumber = true;

    // Holds the pending operation so calculate knows
    // what to do
    $scope.pendingOperation = null;

    // Bound to the view to display a token indicating
    // the current operation
    $scope.operationToken = "";

    // Holds the running total as numbers are added/subtracted
    $scope.runningTotal = null;

    // Holds the number value of the string in the display output
    $scope.pendingValue = null;

    // Tells calculate what to do when the equals buttons is clicked repeatedly
    $scope.lastOperation = null;

    // Constants
    var ADD = "adding";
    var SUBTRACT = "subtracting";
    var CROSS = "crossing";
    var DEVIDE = "deviding";
    var ADD_TOKEN = "+";
    var SUBTRACT_TOKEN = "-";
    var CROSS_TOKEN = "*";
    var DEVIDE_TOKEN ="/";

    /*
     * Runs every time a number button is clicked.
     * Updates the output display and sets 
     * newNumber flag
     */
     $scope.updateOutput = function (btn) {
      if ($scope.output == "0" || $scope.newNumber) {
        $scope.output = btn;
        $scope.newNumber = false;
      } else {
        $scope.output += String(btn);
      }
      $scope.pendingValue = toNumber($scope.output);
    };

    /*
     * Runs every time the add button is clicked.
     * If a number has been entered before the add
     * button was clicked we set the number as a pendingValue,
     * set ADD as a pendingOperation, and set the token. 
     * If no number was entered but an existing calculated
     * number is in the output display we add the last added
     * value on to the total again.
     */
     $scope.add = function () {
      if ($scope.pendingValue) {
        if ($scope.runningTotal && $scope.pendingOperation == ADD) {
          $scope.runningTotal += $scope.pendingValue;
        } else if ($scope.runningTotal && $scope.pendingOperation == SUBTRACT) {
          $scope.runningTotal -= $scope.pendingValue;
        } else if ($scope.runningTotal && $scope.pendingOperation == CROSS) {
          $scope.runningTotal *= $scope.pendingValue;
        } else if ($scope.runningTotal && $scope.pendingOperation == DEVIDE) {
          $scope.runningTotal /= $scope.pendingValue;
        } else {
          $scope.runningTotal = $scope.pendingValue;
        }
      }
      setOperationToken(ADD);
      setOutput(String($scope.runningTotal));
      $scope.pendingOperation = ADD;
      $scope.newNumber = true;
      $scope.pendingValue = null;
    };

    /*
     * Runs every time the subtract button is clicked.
     * If a number has been entered before the subtract
     * button was clicked we set the number as a pendingValue,
     * set subtract as a pendingOperation, and set the token. 
     * If no number was entered but an existing calculated
     * number is in the output display we subtract the last added
     * value from the total.
     */
     $scope.subtract = function () {
      if ($scope.pendingValue) {
        if ($scope.runningTotal && ($scope.pendingOperation == SUBTRACT)) {
          $scope.runningTotal -= $scope.pendingValue;
        } else if ($scope.runningTotal && $scope.pendingOperation == ADD) {
          $scope.runningTotal += $scope.pendingValue;
        } else if ($scope.runningTotal && $scope.pendingOperation == CROSS) {
          $scope.runningTotal *= $scope.pendingValue;
        } else if ($scope.runningTotal && $scope.pendingOperation == DEVIDE) {
          $scope.runningTotal /= $scope.pendingValue;
        } else {
          $scope.runningTotal = $scope.pendingValue;
        }
      }
      setOperationToken(SUBTRACT);
      setOutput(String($scope.runningTotal));
      $scope.pendingOperation = SUBTRACT;
      $scope.newNumber = true;
      $scope.pendingValue = null;
    };

    $scope.cross = function () {
      if ($scope.pendingValue) {
        if ($scope.runningTotal && ($scope.pendingOperation == CROSS)) {
          $scope.runningTotal *= $scope.pendingValue;
        } else if ($scope.runningTotal && $scope.pendingOperation == ADD) {
          $scope.runningTotal += $scope.pendingValue;
        } else if ($scope.runningTotal && $scope.pendingOperation == SUBTRACT) {
          $scope.runningTotal -= $scope.pendingValue;
        } else if ($scope.runningTotal && $scope.pendingOperation == DEVIDE) {
          $scope.runningTotal /= $scope.pendingValue;
        } else {
          $scope.runningTotal = $scope.pendingValue;
        }
      }
      setOperationToken(CROSS);
      setOutput(String($scope.runningTotal));
      $scope.pendingOperation = CROSS;
      $scope.newNumber = true;
      $scope.pendingValue = null;
    };

    $scope.devide = function () {
      if ($scope.pendingValue) {
        if ($scope.runningTotal && ($scope.pendingOperation == DEVIDE)) {
          $scope.runningTotal /= $scope.pendingValue;
        } else if ($scope.runningTotal && $scope.pendingOperation == ADD) {
          $scope.runningTotal += $scope.pendingValue;
        } else if ($scope.runningTotal && $scope.pendingOperation == CROSS) {
          $scope.runningTotal *= $scope.pendingValue;
        } else if ($scope.runningTotal && $scope.pendingOperation == SUBTRACT) {
          $scope.runningTotal -= $scope.pendingValue;
        } else {
          $scope.runningTotal = $scope.pendingValue;
        }
      }
      setOperationToken(DEVIDE);
      setOutput(String($scope.runningTotal));
      $scope.pendingOperation = DEVIDE;
      $scope.newNumber = true;
      $scope.pendingValue = null;
    };

    /*
     * Runs when the equals (=) button is clicked.
     * If a number has been entered before the equals
     * button was clicked we perform the calculation
     * based on the pendingOperation.
     * If no number was entered but an existing calculated
     * number is in the output display we repeat the last
     * operation. For example, if 8+2 was entered we will
     * continue to add 2 every time the equals button is clicked.
     */
     $scope.calculate = function () {
      if (!$scope.newNumber) {
        $scope.pendingValue = toNumber($scope.output);
        $scope.lastValue = $scope.pendingValue;
      }
      if ($scope.pendingOperation == ADD) {
        $scope.runningTotal += $scope.pendingValue;
        $scope.lastOperation = ADD;
      } else if ($scope.pendingOperation == SUBTRACT) {
        $scope.runningTotal -= $scope.pendingValue;
        $scope.lastOperation = SUBTRACT;
      } else if ($scope.pendingOperation == CROSS) {
        $scope.runningTotal *= $scope.pendingValue;
        $scope.lastOperation = CROSS;
      } else if ($scope.pendingOperation == DEVIDE) {
        $scope.runningTotal /= $scope.pendingValue;
        $scope.lastOperation = DEVIDE;
      } else {
        if ($scope.lastOperation) {
          if ($scope.lastOperation == ADD) {
            if ($scope.runningTotal) {
              $scope.runningTotal += $scope.lastValue;
            } else {
              $scope.runningTotal = 0;
            }
          } else if ($scope.lastOperation == SUBTRACT) {
            if ($scope.runningTotal) {
              $scope.runningTotal -= $scope.lastValue;
            } else {
              $scope.runningTotal = 0;
            }
          }
          else if ($scope.lastOperation == CROSS) {
            if ($scope.runningTotal) {
              $scope.runningTotal *= $scope.lastValue;
            } else {
              $scope.runningTotal = 0;
            }
          }
          else if ($scope.lastOperation == DEVIDE) {
            if ($scope.runningTotal) {
              $scope.runningTotal /= $scope.lastValue;
            } else {
              $scope.runningTotal = 0;
            }
          }
        } else {
          $scope.runningTotal = 0;
        }
      }
      setOutput($scope.runningTotal);
      setOperationToken();
      $scope.pendingOperation = null;
      $scope.pendingValue = null;
    };

    /* 
     * Initializes the appropriate values
     * when the clear button is clicked.
     */
     $scope.clear = function () {
      $scope.runningTotal = null;
      $scope.pendingValue = null;
      $scope.pendingOperation = null;
      setOutput("0");
    };

    /* 
     * Updates the display output and resets the
     * newNumber flag.
     */
     setOutput = function (outputString) {
      $scope.output = outputString;
      $scope.newNumber = true;
    };

    /* 
     * Sets the operation token to let the user know
     * what the pendingOperation is
     */
     setOperationToken = function (operation) {
      if (operation == ADD) {
        $scope.operationToken = ADD_TOKEN;
      } else if (operation == SUBTRACT) {
        $scope.operationToken = SUBTRACT_TOKEN;
      } else if (operation == CROSS) {
        $scope.operationToken = CROSS_TOKEN;
      } else if (operation == DEVIDE) {
        $scope.operationToken = DEVIDE_TOKEN;
      } else {
        $scope.operationToken = "";
      }
    };

    /* Converts a string to a number so we can
     * perform calculations. Simply multiplies
     * by one to do so
     */
     toNumber = function (numberString) {
      var result = 0;
      if (numberString) {
        result = numberString * 1;
      }
      return result;
    };

  }]);

angular.module('app').controller('HeaderCtrl', ['$rootScope','$tooltip','APP_CONFIG','$scope', '$location', '$route', 'security', 'breadcrumbs', 'notifications', 'httpRequestTracker', 'Restangular',

  function ($rootScope, $tooltip,APP_CONFIG, $scope, $location, $route, security, breadcrumbs, notifications, httpRequestTracker, Restangular) {

    $scope.location = $location;
    $scope.breadcrumbs = breadcrumbs;
    $scope.logout = security.logout;
    $scope.isAuthenticated = security.isAuthenticated;
    $rootScope.security = security;
    $scope.isAdmin = security.isAdmin;
    $scope.isAuthorizedRoles  = security.isAuthorizedRoles;
    $scope.canRead      = security.canRead;

    $scope.profile = function () { $location.path('/home/profile'); }

    $scope.calculator = function() {
      window.open("calculator.modal.tpl.html", "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, top=500, left=500, width=255, height=300");
    }

    $scope.$watch(function() {
      return security.currentUser;
    }, function(currentUser) {

      $scope.currentUser = currentUser;	   
      $scope.rejected_non_cash = 0;
      $scope.open = 0;
      $scope.need_approval_non_cash = 0;
      $scope.reject_price_list = 0;
      $scope.need_approval_price_list = 0;
      $scope.rejected_cash = 0;
      $scope.need_approval_cash = 0;
      $scope.sumnotif = 0;

    });

    $scope.refresh = function() {
     $scope.rejected_non_cash = 0;
     $scope.open = 0;
     $scope.need_approval_non_cash = 0;
     $scope.reject_price_list = 0;
     $scope.need_approval_price_list = 0;
     $scope.rejected_cash = 0;
     $scope.need_approval_cash = 0;
     $scope.sumnotif = 0;
   }  

   $scope.APP_CONFIG = APP_CONFIG;

   $scope.isNavbarActive = function (navBarPath) {
    return navBarPath === breadcrumbs.getFirst().name;
  };

  $scope.hasPendingRequests = function () {
    return httpRequestTracker.hasPendingRequests();
  };
  $scope.breadcumbsLabel = [];

  $scope.breadcrumbLabel = function(name) {
    for (var i = 0; i < $scope.breadcumbsLabel.length; i++) {
      if($scope.breadcumbsLabel[i].name === name)
        return $scope.breadcumbsLabel[i].label;
    };
    return name;
  }

}])

angular.module('app').controller('LeftColMenuCtrl', ['$routeParams', '$tooltip','APP_CONFIG','$scope', '$location', '$route', 'security', 'breadcrumbs', 'notifications', 'httpRequestTracker', 'Restangular',

  function ($routeParams, $tooltip,APP_CONFIG, $scope, $location, $route, security, breadcrumbs, notifications, httpRequestTracker, Restangular) {

    $scope.$route = $route;

    $scope.location = $location;
    $scope.breadcrumbs = breadcrumbs;

    $scope.isAuthenticated = security.isAuthenticated;
    $scope.isAuthorizedRoles = security.isAuthorizedRoles;
    $scope.isAdmin = security.isAdmin;
    $scope.canRead      = security.canRead;
    $scope.APP_CONFIG = APP_CONFIG;

    $scope.isActive = function (viewLocation) { 
      return viewLocation === $location.path();
    };

    $scope.$location = $location;
    $scope.$routeParams = $routeParams;

    $scope.isNavbarActive = function (navBarPath) {
      return navBarPath === breadcrumbs.getFirst().name;
    };

    $scope.isMatchedRoute = breadcrumbs.isMatchedRoute;

    $scope.hasPendingRequests = function () {
      return httpRequestTracker.hasPendingRequests();
    };

    $scope.hoverInPr = function(){
      this.hoverEditPr = true;
      $scope.personColour = {'background-color':'rgba(129, 127, 127, 0.82)'};
      $scope.personColour2 = {'color':'white'};
    };

    $scope.hoverOutPr = function(){
      this.hoverEditPr = false;
      $scope.personColour = {'background-color': "#"};
      $scope.personColour2 = {'color':'#'};
    };
    $scope.hoverInPo = function(){
      this.hoverEditPo = true;
      $scope.personColour1 = {'background-color':'rgba(129, 127, 127, 0.82)'};
      $scope.personColour3 = {'color':'white'};
    };

    $scope.hoverOutPo = function(){
      this.hoverEditPo = false;
      $scope.personColour1 = {'background-color': "#"};
      $scope.personColour3 = {'color':'#'};
    };

  }]);