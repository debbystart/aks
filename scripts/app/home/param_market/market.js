angular.module('market', [
  'shared.dialog',
  'restangular',
  'services.i18nNotifications'
  ])

.config(['$routeProvider', 'RestangularProvider', function ($routeProvider, RestangularProvider) {

  $routeProvider
  .when('/home/market', {
    controller: 'MarketListCtrl',
    templateUrl: 'scripts/app/home/param_market/list.tpl.html'
  })
  .when('/home/market/edit/:id', {
    controller: 'MarketEditCtrl',
    templateUrl: 'scripts/app/home/param_market/detail.tpl.html',
    resolve: {
      item: function(Restangular, $route){
        return Restangular.one('markets', $route.current.params.id).get();
      }
    }
  })
  .when('/home/market/create', {
    controller: 'MarketCreateCtrl',
    templateUrl: 'scripts/app/home/param_market/detail.tpl.html'
  })

  .otherwise({redirectTo:'/home'});
  RestangularProvider.setRestangularFields({
    id: 'id'
  });

  RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
    if (operation === 'put') {
      elem._id = undefined;
      return elem;
    }
    return elem;
  });
  
}])

.controller('MarketListCtrl', ['$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function ($scope, $route, $location, Restangular, security, i18nNotifications) {

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.create = function() {
    $location.path('home/market/create');
  }

  $scope.canSelect = function() {
    return ($scope.gridOptions.selectedItems.length===0?false:true);
  }

  $scope.select = function () {
    $modalInstance.close($scope.gridOptions.selectedItems[0]);
  };

  $scope.back = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.detail = function(row){
    $location.path('home/market/edit/'+row);
  }

  $scope.change_filter = function(){
    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
  }

  $scope.filterOptions = {
    filterText: null,
    useExternalFilter: true
  };
  $scope.totalServerItems = 0;
  $scope.pagingOptions = {
    pageSizes: [20, 50, 100],
    pageSize: 50,
    currentPage: 1
  };  
  $scope.setPagingData = function(data, page, pageSize){  
    var pagedData = data.records;
    $scope.myData = pagedData;
    $scope.totalServerItems = data.total;
    if (!$scope.$$phase) {
      $scope.$apply();
    }
  };
  $scope.getPagedDataAsync = function (pageSize, page, searchText) {
    setTimeout(function () {
      var custom_get = {};

      if($route.current.params)
        custom_get = $route.current.params;

      custom_get.offset = (page - 1) * pageSize; 
      custom_get.limit = pageSize;

      if (searchText='$scope.filterOptions.filterText') {

        custom_get.filter = $scope.filterOptions.filterText;
        custom_get.filter_fields = 'id,name';

        Restangular.all("markets").getList(custom_get).then(function(markets) {
          $scope.setPagingData(markets,page,pageSize);
        });
      } 
      else {
        Restangular.all("markets").getList(custom_get).then(function(markets) {
          $scope.setPagingData(markets,page,pageSize);
        });
      }
    }, 100);
  };

  $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

  $scope.$watch('pagingOptions', function (newVal, oldVal) {
    if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
    }
  }, true);

  $scope.gridOptions = {
    data: 'myData',
    enablePaging: true,
    showFooter: true,
    multiSelect: false,
    selectedItems: [],
    totalServerItems:'totalServerItems',
    pagingOptions: $scope.pagingOptions,
    filterOptions: $scope.filterOptions,
    enableColumnResize: true,
    columnDefs: [
    { field: 'id', displayName: 'Code', enableCellEdit:true, cellClass: 'text-center', width: 100},
    { field: 'name', displayName: 'Market', enableCellEdit:true, width: 1200},
    { field: 'is_active', displayName: 'Aktif', width: 150, cellClass: 'text-center', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ (row.getProperty(col.field)===1?\'Ya\' : \'Tidak\') }}</span></div>'},
    {displayName: 'Detail', cellClass: 'text-center',
    cellTemplate: 	'<div class="ngCellText" ng-class="col.colIndex()"><a class="btn btn-xs btn-default" ng-href="" ng-click="detail(row.entity.id)" data-toggle="tooltip" title="edit" data-original-title="Edit Client"><i class="fa fa-pencil"></i></a></div>'}

    ]
  };
}])

.controller('MarketCreateCtrl', ['$window', '$scope', '$location', 'Restangular', 'security', 'i18nNotifications', '$modal', 'APP_CONFIG', '$upload', '$timeout', '$dialogs', function ($window, $scope, $location, Restangular, security, i18nNotifications, $modal, APP_CONFIG, $upload, $timeout, $dialogs) {
  //debugger;
  var original = {};
  original.is_active = true;

  $scope.market = Restangular.copy(original);
  
  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.canRemove = function() {
    return false;
  }
  $scope.canSave = function() {
  	return $scope.MarketForm.$valid && 
    !angular.equals($scope.market, original);
  };

  $scope.back = function() {
    $window.history.back();
  }

  $scope.save = function() {
    Restangular.all('markets').post($scope.market).then(function(market) {
      i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : market.id});
      $location.path('/home/market');
    });
  }

  $scope.canRevert = function() {
    return $scope.MarketForm.$valid && 
    !angular.equals($scope.market, original);
  }
  $scope.revert = function() {
    $scope.market =  {};
    $scope.MarketForm.$setPristine();
  }
}])

.controller('MarketEditCtrl', ['$window', '$scope', '$location', 'Restangular', 'item', 'i18nNotifications', '$dialogs', 'security', 'simpleDeleteConfirm', '$modal', 'APP_CONFIG', '$upload', '$timeout', function ($window, $scope, $location, Restangular, item, i18nNotifications, $dialogs, security, simpleDeleteConfirm, $modal, APP_CONFIG, $upload, $timeout) {

  //debugger;
  var original = item;
  
  $scope.market = Restangular.copy(original);

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.back = function() {
    $window.history.back();
  }

  $scope.canSave = function() {
    return !angular.equals($scope.market, original);
  }
  
  $scope.save = function() {
    $scope.market.put().then(function() {
      i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : $scope.market.id});
      $location.path('/home/market');
    });
  };
  
  $scope.canRemove = function() {
    return true;
  }

  $scope.remove = function() {
    simpleDeleteConfirm.openDialog(original, '/home/market');
  };

  $scope.canRevert = function() {
    return !angular.equals($scope.market, original);
  }
  $scope.revert = function() {
    $scope.market =  Restangular.copy(original);
    $scope.MarketForm.$setPristine();
  }

}]);

