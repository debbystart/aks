angular.module('company', [
  'shared.dialog',
  'restangular',
  'services.i18nNotifications'
  ])

.config(['$routeProvider', 'RestangularProvider', function ($routeProvider, RestangularProvider) {

  $routeProvider
  .when('/home/company', {
    controller: 'CompanyListCtrl',
    templateUrl: 'scripts/app/home/param_company/list.tpl.html'
  })
  .when('/home/company/edit/:id', {
    controller: 'CompanyEditCtrl',
    templateUrl: 'scripts/app/home/param_company/detail.tpl.html',
    resolve: {
      item: function(Restangular, $route){
        return Restangular.one('companys', $route.current.params.id).get();
      }
    }
  })
  .when('/home/company/create', {
    controller: 'CompanyCreateCtrl',
    templateUrl: 'scripts/app/home/param_company/detail.tpl.html'
  })

  .otherwise({redirectTo:'/home'});
  RestangularProvider.setRestangularFields({
    id: 'id'
  });

  RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
    if (operation === 'put') {
      elem._id = undefined;
      return elem;
    }
    return elem;
  });
  
}])

.controller('CompanyListCtrl', ['$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function ($scope, $route, $location, Restangular, security, i18nNotifications) {

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.create = function() {
    $location.path('home/company/create');
  }

  $scope.canSelect = function() {
    return ($scope.gridOptions.selectedItems.length===0?false:true);
  }

  $scope.select = function () {
    $modalInstance.close($scope.gridOptions.selectedItems[0]);
  };

  $scope.back = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.detail = function(row){
    $location.path('home/company/edit/'+row);
  }

  $scope.change_filter = function(){
    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
  }

  $scope.filterOptions = {
    filterText: null,
    useExternalFilter: true
  };
  $scope.totalServerItems = 0;
  $scope.pagingOptions = {
    pageSizes: [20, 50, 100],
    pageSize: 50,
    currentPage: 1
  };  
  $scope.setPagingData = function(data, page, pageSize){  
    var pagedData = data.records;
    $scope.myData = pagedData;
    $scope.totalServerItems = data.total;
    if (!$scope.$$phase) {
      $scope.$apply();
    }
  };
  $scope.getPagedDataAsync = function (pageSize, page, searchText) {
    setTimeout(function () {
      var custom_get = {};

      if($route.current.params)
        custom_get = $route.current.params;

      custom_get.offset = (page - 1) * pageSize; 
      custom_get.limit = pageSize;

      if (searchText='$scope.filterOptions.filterText') {

        custom_get.filter = $scope.filterOptions.filterText;
        custom_get.filter_fields = 'code,name';

        Restangular.all("companys").getList(custom_get).then(function(companys) {
          $scope.setPagingData(companys,page,pageSize);
        });
      } 
      else {
        Restangular.all("companys").getList(custom_get).then(function(companys) {
          $scope.setPagingData(companys,page,pageSize);
        });
      }
    }, 100);
  };

  $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

  $scope.$watch('pagingOptions', function (newVal, oldVal) {
    if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
    }
  }, true);

  $scope.gridOptions = {
    data: 'myData',
    enablePaging: true,
    showFooter: true,
    multiSelect: false,
    selectedItems: [],
    totalServerItems:'totalServerItems',
    pagingOptions: $scope.pagingOptions,
    filterOptions: $scope.filterOptions,
    enableColumnResize: true,
    columnDefs: [
    { field: 'code', displayName: 'Code', cellClass: 'text-center', enableCellEdit:true, width: 100},
    { field: 'name', displayName: 'Nama', enableCellEdit:true, width: 380},
    { field: 'address', displayName: 'Alamat', width: 400},
    { field: 'phone', displayName: 'Telepon', width: 150 },
    { field: 'email', displayName: 'Email', width: 250 },
    { field: 'is_active', displayName: 'Aktif', width: 150, cellClass: 'text-center', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ (row.getProperty(col.field)===1?\'Yes\' : \'No\') }}</span></div>'},
    {displayName: 'Detil', cellClass: 'text-center',
    cellTemplate: 	'<div class="ngCellText" ng-class="col.colIndex()"><a class="btn btn-xs btn-default" ng-href="" ng-click="detail(row.entity.id)" data-toggle="tooltip" title="edit" data-original-title="Edit Client"><i class="fa fa-pencil"></i></a></div>'}

    ]
  };

}])

.controller('CompanyCreateCtrl', ['$window', '$scope', '$location', 'Restangular', 'security', 'i18nNotifications', '$modal', 'APP_CONFIG', '$upload', '$timeout', '$dialogs', function ($window, $scope, $location, Restangular, security, i18nNotifications, $modal, APP_CONFIG, $upload, $timeout, $dialogs) {
  //debugger;
  var original = {};
  original.is_active = true;

  $scope.company = Restangular.copy(original);

  document.getElementById("phone").onkeypress = validate_int;
  document.getElementById("phone").onkeyup = phone_number_mask;
  
  $scope.appBaseUrl = APP_CONFIG.appBaseUrl;

  $scope.company.attachments = [];   
  if($scope.company.file_path) {
    var att = {
      'file_path': $scope.company.file_path,
      'file_type': $scope.company.file_type
    };
    $scope.company.attachments.push(att);
  }
  
  //----------------------------------------------------------------------------------------------
  // multiple file-upload setup

  $scope.fileReaderSupported = window.FileReader != null;
  $scope.uploadRightAway = true;
  $scope.howToSend = 1; //Multupart/form-data upload using $upload.upload() service cross browser 
  //$scope.angularVersion = window.location.hash.length > 1 ? window.location.hash.substring(1) : '1.2.0';
  
  $scope.abort = function(index) {
    $scope.upload[index].abort(); 
    $scope.upload[index] = null;
  };
  
  $scope.onFileSelect = function($files) {
    $scope.selectedFiles = [];
    $scope.progress = [];
    if ($scope.upload && $scope.upload.length > 0) {
      for (var i = 0; i < $scope.upload.length; i++) {
        if ($scope.upload[i] != null) {
          $scope.upload[i].abort();
        }
      }
    }
    $scope.upload = [];
    $scope.uploadResult = [];
    $scope.selectedFiles = $files;
    $scope.dataUrls = [];
    for ( var i = 0; i < $files.length; i++) {
      var $file = $files[i];
      if (window.FileReader && $file.type.indexOf('image') > -1) {
        var fileReader = new FileReader();
        fileReader.readAsDataURL($files[i]);
        var loadFile = function(fileReader, index) {
          fileReader.onload = function(e) {
            $timeout(function() {
              $scope.dataUrls[index] = e.target.result;
            });
          }
        }(fileReader, i);
      }
      $scope.progress[i] = -1;
      if ($scope.uploadRightAway) {
        $scope.start(i);
      }
    }
  };

  $scope.start = function(index) {
    $scope.progress[index] = 0;
    $scope.errorMsg = null;
    if ($scope.howToSend == 1) {
      $scope.upload[index] = $upload.upload({
        url : APP_CONFIG.appBaseUrl + '/api/companys/files/upload',
        method: $scope.httpMethod,
        headers: {'upload': 'file_path'},
        data : {
          myModel : $scope.company
        },
        
        file: $scope.selectedFiles[index],
        fileFormDataName: 'uploads'
      }).then(function(response) {
        $scope.company.attachments.push(response.data);
      }, function(response) {
        if (response.status > 0) $scope.errorMsg = response.status + ': ' + response.data;
      }, function(evt) {
        // Math.min is to fix IE which reports 200% sometimes
        $scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
      });/*.xhr(function(xhr){
        xhr.upload.addEventListener('abort', function() {console.log('abort complete')}, false);
      });*/
    } else {
      var fileReader = new FileReader();
      fileReader.onload = function(e) {
        $scope.upload[index] = $upload.http({
          url: 'upload',
          headers: {'Content-Type': $scope.selectedFiles[index].type},
          data: e.target.result
        }).then(function(response) {
          $scope.company.attachments.push(response.data);
        }, function(response) {
          if (response.status > 0) $scope.errorMsg = response.status + ': ' + response.data;
        }, function(evt) {
          // Math.min is to fix IE which reports 200% sometimes
          $scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
      }
      fileReader.readAsArrayBuffer($scope.selectedFiles[index]);
    }
  };

  $scope.removeAttachment = function (index) {

    if($scope.company.attachments.length===0)
      return false;

    var attachment = $scope.company.attachments[index];

    if(attachment.file_path) {

      var dlg = $dialogs.confirm('Please Confirm','Are you sure want to delete this attachment?');
      dlg.result.then(function(btn){

        Restangular.all('company/file').post(attachment).then(function() {
          i18nNotifications.pushForCurrentRoute('crud.remove.success', 'success', {id : attachment.file_path});        
          $scope.company.attachments.splice(index, 1);
        });

      },function(btn){

      });

    }
    else {
      $scope.company.attachments.splice(index, 1);
    }

  };
  
  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.canRemove = function() {
    return false;
  }
  $scope.canSave = function() {
  	return $scope.CompanyForm.$valid && 
    !angular.equals($scope.company, original);
  };

  $scope.back = function() {
    $window.history.back();
  }

  $scope.save = function() {
    Restangular.all('companys').post($scope.company).then(function(company) {
      i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : company.id});
      $location.path('/home/company');
    });
  }

  $scope.canRevert = function() {
    return $scope.CompanyForm.$valid && 
    !angular.equals($scope.company, original);
  }
  $scope.revert = function() {
    $scope.company =  {};
    $scope.CompanyForm.$setPristine();
  }

}])

.controller('CompanyEditCtrl', ['localizedMessages', '$window', '$scope', '$location', 'Restangular', 'item', 'i18nNotifications', '$dialogs', 'security', 'simpleDeleteConfirm', '$modal', 'APP_CONFIG', '$upload', '$timeout', function (localizedMessages, $window, $scope, $location, Restangular, item, i18nNotifications, $dialogs, security, simpleDeleteConfirm, $modal, APP_CONFIG, $upload, $timeout) {

  //debugger;
  var original = item;
  $scope.company = Restangular.copy(original);

  document.getElementById("phone").onkeypress = validate_int;
  document.getElementById("phone").onkeyup = phone_number_mask;

  $scope.appBaseUrl = APP_CONFIG.appBaseUrl;

  $scope.company.attachments = [];   
  if($scope.company.file_path) {
    var att = {
      'file_path': $scope.company.file_path,
      'file_type': $scope.company.file_type
    };
    $scope.company.attachments.push(att);
  }
  
  //----------------------------------------------------------------------------------------------
  // multiple file-upload setup

  $scope.fileReaderSupported = window.FileReader != null;
  $scope.uploadRightAway = true;
  $scope.howToSend = 1; //Multupart/form-data upload using $upload.upload() service cross browser 
  //$scope.angularVersion = window.location.hash.length > 1 ? window.location.hash.substring(1) : '1.2.0';

  $scope.hasUploader = function(index) {
    return $scope.upload[index] != null;
  };
  
  $scope.abort = function(index) {
    $scope.upload[index].abort(); 
    $scope.upload[index] = null;
  };
  
  $scope.onFileSelect = function($files) {
    $scope.selectedFiles = [];
    $scope.progress = [];
    if ($scope.upload && $scope.upload.length > 0) {
      for (var i = 0; i < $scope.upload.length; i++) {
        if ($scope.upload[i] != null) {
          $scope.upload[i].abort();
        }
      }
    }
    $scope.upload = [];
    $scope.uploadResult = [];
    $scope.selectedFiles = $files;
    $scope.dataUrls = [];
    for ( var i = 0; i < $files.length; i++) {
      var $file = $files[i];
      if (window.FileReader && $file.type.indexOf('image') > -1) {
        var fileReader = new FileReader();
        fileReader.readAsDataURL($files[i]);
        var loadFile = function(fileReader, index) {
          fileReader.onload = function(e) {
            $timeout(function() {
              $scope.dataUrls[index] = e.target.result;
            });
          }
        }(fileReader, i);
      }
      $scope.progress[i] = -1;
      if ($scope.uploadRightAway) {
        $scope.start(i);
      }
    }
  };

  $scope.start = function(index) {
    $scope.progress[index] = 0;
    $scope.errorMsg = null;
    if ($scope.howToSend == 1) {
      $scope.upload[index] = $upload.upload({
        url : APP_CONFIG.appBaseUrl + '/api/companys/files/upload',
        method: $scope.httpMethod,
        headers: {'upload': 'file_path'},
        data : {
          myModel : $scope.company
        },
        
        file: $scope.selectedFiles[index],
        fileFormDataName: 'uploads'
      }).then(function(response) {
        $scope.company.attachments.push(response.data);
      }, function(response) {
        if (response.status > 0) $scope.errorMsg = response.status + ': ' + response.data;
      }, function(evt) {
        // Math.min is to fix IE which reports 200% sometimes
        $scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
      });/*.xhr(function(xhr){
        xhr.upload.addEventListener('abort', function() {console.log('abort complete')}, false);
      });*/
    } else {
      var fileReader = new FileReader();
      fileReader.onload = function(e) {
        $scope.upload[index] = $upload.http({
          url: 'upload',
          headers: {'Content-Type': $scope.selectedFiles[index].type},
          data: e.target.result
        }).then(function(response) {
          $scope.company.attachments.push(response.data);
        }, function(response) {
          if (response.status > 0) $scope.errorMsg = response.status + ': ' + response.data;
        }, function(evt) {
          // Math.min is to fix IE which reports 200% sometimes
          $scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
      }
      fileReader.readAsArrayBuffer($scope.selectedFiles[index]);
    }
  };

  $scope.removeAttachment = function (index) {

    if($scope.company.attachments.length===0)
      return false;

    var attachment = $scope.company.attachments[index];

    if(attachment.file_path) {

      var dlg = $dialogs.confirm('Konfirmasi','Apakah Anda yakin ingin menghapus lampiran ini?');
      dlg.result.then(function(btn){

        Restangular.all('company/file').post(attachment).then(function() {
          i18nNotifications.pushForCurrentRoute('crud.remove.success', 'success', {id : attachment.file_path});        
          $scope.company.attachments.splice(index, 1);
        });

      },function(btn){

      });

    }
    else {
      $scope.company.attachments.splice(index, 1);
    }

  };

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.back = function() {
    $window.history.back();
  }

  $scope.canSave = function() {
    return !angular.equals($scope.company, original);
  }
  
  $scope.save = function() {
    if($scope.company.is_active==1){
      $scope.company.put().then(function() {
        i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : $scope.company.id});
        $location.path('/home/company');
      });
    }else{
      $dialogs.error('Warning', localizedMessages.get('crud.save.inactive'));
    }
  };

}]);

