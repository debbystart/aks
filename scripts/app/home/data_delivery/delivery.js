angular.module('delivery', [
    'shared.dialog',
    'ui.bootstrap.datepicker',
    'ui.bootstrap.typeahead',
    'ui.bootstrap.modal',
    'restangular', 
    'services.i18nNotifications'
    ])

.config(['$routeProvider', 'RestangularProvider', function ($routeProvider, RestangularProvider) {

    $routeProvider

    .when('/home/delivery', {
        controller: 'DeliveryCtrl',
        templateUrl: 'scripts/app/home/data_delivery/opendelivery.tpl.html'
    })

    .when('/home/delivery/list', {
        controller: 'DeliverytListCtrl',
        templateUrl: 'scripts/app/home/data_delivery/listdelivery.tpl.html'
    })

    .when('/home/delivery/edit/:id', {
        controller: 'DeliverytEditCtrl',
        templateUrl: 'scripts/app/home/data_delivery/detail.tpl.html',
        resolve: {
            item: function(Restangular, $route){
                return Restangular.one('salesorders', $route.current.params.id).get();
            }
        }
    })

    .when('/home/delivery/transaction/:id', {
        controller: 'SalesorderTransactionCtrl',
        templateUrl: 'scripts/app/home/data_delivery/detail_list.tpl.html',
        resolve: {
            item: function(Restangular, $route){
                return Restangular.one('salesorders/listdelivery', $route.current.params.id).get({expands: 'inventorys_detail'});
            }
        }
    })
    
    .otherwise({redirectTo:'/home'});

    RestangularProvider.setRestangularFields({
        id: 'id'
    });

    RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
        if (operation === 'put') {
            elem._id = undefined;
            return elem;
        }
        return elem;
    });
}])

.controller('DeliveryCtrl', ['$window','$filter','APP_CONFIG','$modal','$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function ($window,$filter,APP_CONFIG,$modal,$scope, $route, $location, Restangular, security, i18nNotifications) {

    $scope.format = 'dd/MM/yyyy';

    // user permissions
    $scope.canCreate    = security.canCreate;
    $scope.canUpdate    = security.canUpdate;
    $scope.canRead      = security.canRead;
    $scope.canDelete    = security.canDelete;
    $scope.isAuthorizedRoute = security.isAuthorizedRoute;

    $scope.$watch(function() {
        return security.currentUser;
    }, function(currentUser) {
        $scope.currentUser = currentUser;
    });

    $scope.canSelect = function() {
      return ($scope.gridOptions.selectedItems.length===0?false:true);
        //
    }

    $scope.select = function () {
      $modalInstance.close($scope.gridOptions.selectedItems[0]);
        //
    };

    $scope.back = function () {
      $modalInstance.dismiss('cancel');
        //
    };

    $scope.detail = function(row){
      $location.path('/home/delivery/edit/'+row);
        //
    }

    $scope.change_filter = function(){
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        //
    }

    // ng-Grid
    $scope.filterOptions = {
        //filterText: "",
        filterText: null,
        filterTextFrom: null,
        filterTextTo: null,
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [20, 25, 50],
        pageSize: 20,
        currentPage: 1
    };
    $scope.setPagingData = function(data, page, pageSize){
        var pagedData = data.records;

        var oneDay = 24*60*60*1000;

        for(var i=0; i<pagedData.length; i++){
            var firstDate = new Date();
            var secondDate = new Date(pagedData[i].expired_selling_date);
            pagedData[i].range_date = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)))+" hari";
        }

        $scope.myData = pagedData;
        $scope.totalServerItems = data.total;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText, fromText, toText) {
        setTimeout(function () {
            var custom_get = {};

            if($route.current.params)
                custom_get = $route.current.params;
            custom_get.offset = (page - 1) * pageSize;
            custom_get.limit = pageSize;

            if (searchText='$scope.filterOptions.filterText') {

                custom_get.filter = $scope.filterOptions.filterText;
                custom_get.filter_fields = 'id, customer_id, selling';

                Restangular.all("salesorders/opendelivery").getList(custom_get).then(function(salesorders) {
                  $scope.setPagingData(salesorders,page,pageSize);
              });
            } 
            else {
                Restangular.all("salesorders/opendelivery").getList(custom_get).then(function(salesorders) {
                  $scope.setPagingData(salesorders,page,pageSize);
              });
            }

        }, 100);
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.gridOptions = {
        data: 'myData',
        enablePaging: true,
        showFooter: true,
        multiSelect: false,
        selectedItems: [],
        totalServerItems:'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        enableColumnResize: true,
        enableCellSelection: true,
        enableCellEdit:true,
        columnDefs:
        [
        { field: 'is_save', displayName: 'Status', width: 170,
        cellTemplate:   '<div class="ngCellText" style="text-align:center" ng-class="{\'red\' : row.getProperty(\'is_save\') == 1 || row.getProperty(\'is_save\') == 3}">'+
        '<span ng-cell-text ng-show="row.getProperty(col.field)==1 || row.getProperty(col.field)==3">Open</span></div>'},
        { field: 'id', displayName: 'Nomor SO', width: 120, cellClass:"text-center"},
        { field: 'selling', displayName: 'Selling', width: 150},
        { field: 'customer_id', displayName: 'Cust Code', width: 100, cellClass:"text-center"},
        { field: 'customer', displayName: 'Customer', width: 230},
        { field: 'salesman', displayName: 'Salesman', width: 180},
        { field: 'selling_date',  cellFilter: 'date:\'dd-MMM-yyyy\'', displayName: 'Tanggal Penjualan', cellClass:"text-center", width: 140},
        { field: 'expired_selling_date',  cellFilter: 'date:\'dd-MMM-yyyy\'', displayName: 'Akhir Penjualan', cellClass:"text-center", width: 140},
        { field: 'range_date', displayName: 'Sisa Waktu', width: 100, cellClass: "text-center"},
        { field: 'subtotal', displayName: 'Total Price', width: 120, cellClass: "text-right", cellFilter: 'number'},
        { displayName: 'Action', cellClass: 'text-center',
        cellTemplate:   '<div class="ngCellText" ng-class="col.colIndex()"><a class="btn btn-xs btn-default" ng-href="" ng-click="detail(row.entity.id)" data-toggle="tooltip" title="edit" data-original-title="Edit Client"><i class="fa fa-pencil"></i></a></div>'}
        ]
    }

}])

.controller('DeliverytListCtrl', ['$window','$filter','APP_CONFIG','$modal','$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function ($window,$filter,APP_CONFIG,$modal,$scope, $route, $location, Restangular, security, i18nNotifications) {

    //debugger;
    var original = {};

    $scope.delivery = Restangular.copy(original);

    $scope.format = 'dd/MM/yyyy';

    // user permissions
    $scope.canCreate    = security.canCreate;
    $scope.canUpdate    = security.canUpdate;
    $scope.canRead      = security.canRead;
    $scope.canDelete    = security.canDelete;
    $scope.isAuthorizedRoute = security.isAuthorizedRoute;

    $scope.$watch(function() {
        return security.currentUser;
    }, function(currentUser) {
        $scope.currentUser = currentUser;
    });

    $scope.canSelect = function() {
      return ($scope.gridOptions.selectedItems.length===0?false:true);
        //
    }

    $scope.select = function () {
      $modalInstance.close($scope.gridOptions.selectedItems[0]);
        //
    };

    $scope.back = function () {
      $modalInstance.dismiss('cancel');
        //
    };

    $scope.detail = function(row){
      $location.path('/home/delivery/transaction/'+row);
        //
    }

    $scope.change_filter = function(){
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        //
    }

    // ng-Grid
    $scope.filterOptions = {
        //filterText: "",
        filterText: null,
        filterTextFrom: null,
        filterTextTo: null,
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [20, 25, 50],
        pageSize: 20,
        currentPage: 1
    };
    $scope.setPagingData = function(data, page, pageSize){
        var pagedData = data.records;

        for(let i=0; i<pagedData.length; i++){
            setTimeout( function(){
                $scope.delivery.i = i;
                Restangular.all("customers").getList({where:"id = '"+pagedData[i].customer+"'"}).then(function(customers) {
                    $scope.customers = customers.records;
                    pagedData[$scope.delivery.i].customer_name = $scope.customers[0].name;
                });
            }, i*1000 );
        }

        $scope.myData = pagedData;
        $scope.totalServerItems = data.total;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText, fromText, toText) {
        setTimeout(function () {
            var custom_get = {};

            if($route.current.params)
                custom_get = $route.current.params;
            custom_get.offset = (page - 1) * pageSize;
            custom_get.limit = pageSize;

            if (searchText='$scope.filterOptions.filterText') {

                custom_get.filter = $scope.filterOptions.filterText;
                custom_get.filter_fields = 'id';

                Restangular.all("salesorders/listdelivery").getList(custom_get).then(function(salesorders) {
                  $scope.setPagingData(salesorders,page,pageSize);
              });
            } 
            else {
                Restangular.all("salesorders/listdelivery").getList(custom_get).then(function(salesorders) {
                  $scope.setPagingData(salesorders,page,pageSize);
              });
            }

        }, 100);
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.gridOptions = {
        data: 'myData',
        enablePaging: true,
        showFooter: true,
        multiSelect: false,
        selectedItems: [],
        totalServerItems:'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        enableColumnResize: true,
        enableCellSelection: true,
        enableCellEdit:true,
        columnDefs:
        [
        { field: 'delivery_save', displayName: 'Status', width: 170,
        cellTemplate:   '<div class="ngCellText" style="text-align:center" ng-class="{\'green\' : row.getProperty(\'delivery_save\') == 2 || row.getProperty(\'delivery_save\') == 3}">'+
        '<span ng-cell-text ng-show="row.getProperty(col.field)==2 || row.getProperty(col.field)==3">Delivery</span></div>'},
        { field: 'date',  cellFilter: 'date:\'dd-MMM-yyyy\'', displayName: 'Date', cellClass:"text-center", width: 120},
        { field: 'code', displayName: 'Code', width: 150, cellClass:"text-center"},
        { field: 'so_id', displayName: 'Nomor SO', width: 150, cellClass:"text-center"},
        { field: 'ongkir', displayName: 'Ongkir', width: 100, cellFilter: 'number', cellClass: 'text-right'},
        { field: 'shipping_delivery', displayName: 'Pengiriman', width: 130},
        { field: 'selling_delivery', displayName: 'Resi Penjualan', width: 130},
        { field: 'customer', displayName: 'Cust Code', width: 100, cellClass:"text-center"},
        { field: 'customer_name', displayName: 'Customer', width: 230},
        { field: 'city', displayName: 'Kabupaten/Kota', width: 170},
        { displayName: 'Action', cellClass: 'text-center',
        cellTemplate:   '<div class="ngCellText" ng-class="col.colIndex()"><a class="btn btn-xs btn-default" ng-href="" ng-click="detail(row.entity.id)" data-toggle="tooltip" title="edit" data-original-title="Edit Client"><i class="fa fa-pencil"></i></a></div>'}
        ]
    }

}])

.controller('DeliverytEditCtrl', ['APP_CONFIG','$timeout', '$rootScope', '$http', '$window', '$scope', '$route', '$location', 'Restangular', 'item', 'i18nNotifications', '$dialog', 'security', 'simpleDeleteConfirm', '$modal', '$dialogs', function (APP_CONFIG,$timeout, $rootScope, $http, $window, $scope, $route, $location, Restangular, item, i18nNotifications, $dialog, security, simpleDeleteConfirm, $modal, $dialogs) {

    //debugger;
    var original = item;
    original.transaction_type = 2;
    original.ongkir_delivery = original.ongkir;
    original.shipping_id_delivery = original.shipping_id;
    original.tot_price = original.total;
    original.total_item_weight_delivery = original.total_item_weight;
    original.qty_save = original.qty;
    original.qty_trans = 0;

    $scope.delivery = Restangular.copy(original);

    $scope.$watch(function() {
        return security.currentUser;
    }, function(currentUser) {
        $scope.currentUser = currentUser;
    });

    Restangular.all("markets").getList().then(function(markets) {
        $scope.markets = markets.records;
    });

    Restangular.all("shippings").getList().then(function(shippings) {
        $scope.shippings = shippings.records;
    });

    Restangular.all("regions/province").getList().then(function(regions) {
        $scope.provinces = regions.records;
    });

    Restangular.all("regions/city").getList({where:"param_region.name = '"+$scope.delivery.province+"'"}).then(function(regions) {
        $scope.citys = regions.records;
    });

    Restangular.all("regions/districts").getList({where:"param_region.city = '"+$scope.delivery.city+"'"}).then(function(regions) {
        $scope.districts = regions.records;
    });

    Restangular.all("regions/village").getList({where:"param_region.districts = '"+$scope.delivery.districts+"'"}).then(function(regions) {
        $scope.villages = regions.records;
    });

    $scope.format = 'dd-MMM-yyyy';
    $scope.minDate = new Date();
    $scope.maxDate = new Date().setDate(new Date().getDate() + 60);

    var transaction_detail = Restangular.copy($scope.delivery);

    Restangular.all("salesorders_detail/"+$scope.delivery.id).getList().then(function(salesorders_detail) {
        transaction_detail.salesorders_detail = salesorders_detail.records;
        for(var i=0; i<transaction_detail.salesorders_detail.length; i++){
           transaction_detail.salesorders_detail[i].is_delivery = 1;
            //
        }
    })

    $scope.delivery = transaction_detail;
    $scope.delivery.salesorders_detail = transaction_detail.salesorders_detail;

    $scope.detailCellTpl = '<div class="ngCellText" ng-class="col.colIndex()"><a ng-href="" ng-click="removeQuotationItem(row.rowIndex, row)"><i class="icon-trash"></i></a></div>';

    $scope.gridOptions = {
        data: 'delivery.salesorders_detail',
        enablePaging: true,
        showFooter: true,
        multiSelect: false,
        selectedItems: [],
        totalServerItems:'totalServerItems',
        enableColumnResize: true,
        enableCellSelection: true,
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        columnDefs: [
        { displayName: 'Delete', width: 80, cellClass: "text-center", cellTemplate: $scope.detailCellTpl},
        { field: 'item_id', displayName: 'Code', width: 120, cellClass: "text-center"},
        { field: 'item_name', displayName: 'Item'},
        { field: 'item_unit', displayName: 'Unit', width:80, cellClass: "text-center"},
        { field: 'stock', displayName: 'Stok', width:100, cellClass: "text-right", cellFilter: 'number'},
        { field: 'item_weight', displayName: 'Berat (g)', width:120, cellClass: "text-right", cellFilter: 'number'},
        { field: 'qty_trans', displayName: 'Qty Delivery*', width: 120, cellClass: "text-right" , enableCellEdit: true, cellFilter: 'number', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-change="calculate(row)" format="number"/>'},
        { field: 'qty', displayName: 'Qty SO', width: 100, cellClass: "text-right" , cellFilter: 'number'},
        { field: 'price', displayName: 'Price (Rp)*', width: 120, cellClass: "text-right", cellFilter: 'number'},
        { field: 'total_item_weight', displayName: 'Total Berat (g)', width:120, cellClass: "text-right", cellFilter: 'number'},
        { field: 'bruto', displayName: 'Bruto' , width: 120, cellClass: "text-right", cellFilter: 'number'},
        { field: 'disc_percent', displayName: 'Disc (%)', width: 100, visible: false,cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'disc_rupiah', displayName: 'Disc (Rp)', width: 120, cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'total_price', displayName: 'Total' , width: 120, cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'is_delivery', displayName: 'Is Delivery' , width: 120, cellClass: "text-right", cellFilter: 'number:2', visible: false}
        ]
    }

    $scope.delivery.detail_delete = new Array();
    $scope.removeQuotationItem = function(index, row) {
        $scope.delivery.salesorders_detail.splice(index, 1);
        $scope.calculate(row);
    };

    $scope.calculate = function(row) {
        if(row.entity.qty_trans != ''){
            if(row.entity.qty_trans <= row.entity.qty){
                if(row.entity.mystock == 0){
                    row.entity.stock = parseInt(row.entity.qty_trans);
                }else{
                    row.entity.stock = parseInt(row.entity.mystock) - parseInt(row.entity.qty_trans);
                }
            }else{
                $dialogs.error('Warning', 'Qty delivery tidak boleh lebih besar dari Qty SO');
                row.entity.qty_trans = 0;
            }
        }else{
            row.entity.qty_trans = 0;
            row.entity.stock = row.entity.mystock;
        }

        $scope.delivery.qty_trans = 0;
        $scope.delivery.tot_price = 0;
        $scope.delivery.total_item_weight_delivery = 0;
        $scope.delivery.qty_save = 0;
        var x = 0;
        for(var i=0; i<$scope.delivery.salesorders_detail.length; i++){
            $scope.delivery.qty_trans = parseInt($scope.delivery.qty_trans) + parseInt($scope.delivery.salesorders_detail[i].qty_trans);
            $scope.delivery.tot_price = parseInt($scope.delivery.tot_price) + parseInt($scope.delivery.salesorders_detail[i].total_price);
            $scope.delivery.total_item_weight_delivery = parseInt($scope.delivery.total_item_weight_delivery) + parseInt($scope.delivery.salesorders_detail[i].total_item_weight);
            $scope.delivery.qty_save = parseInt($scope.delivery.qty_save) + parseInt($scope.delivery.salesorders_detail[i].qty);
        }
    };

    $scope.ongkir = function() {
        $scope.delivery.subtotal = parseFloat($scope.delivery.ongkir_delivery) + parseFloat($scope.delivery.total);
    };

    $scope.canCreate    = security.canCreate;
    $scope.canUpdate    = security.canUpdate;
    $scope.canRead      = security.canRead;
    $scope.canDelete    = security.canDelete;

    var progress = 10;
    var msgs = [
    'Processing...',
    'Done!'
    ];
    var i = 0;    
    var fakeProgress = function(){
        $timeout(function(){
            if(progress < 95){
                progress += 5;
                $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': progress});
                fakeProgress();
            }else{
            }
        },1000);
    };

    $scope.back = function() {
        $window.history.back();
    }
    $scope.canSave = function() {
        return $scope.DeliveryForm1.$valid && $scope.DeliveryForm2.$valid &&
        !angular.equals($scope.delivery, original);
    };

    $scope.delivered = function(){
        var dlg = $dialogs.confirm('Konfirmasi','Apakah Anda ingin yakin mendeliver data ini?');
        dlg.result.then(function(btn){
            $scope.delivery.is_save = 2;
            $scope.saved();
        },function(btn){
            $scope.delivery.is_save = 1;
        });
    };

    $scope.saved = function() { 
        for(var i=0; i<$scope.delivery.salesorders_detail.length; i++){
            if($scope.delivery.salesorders_detail[i].qty_trans == undefined || $scope.delivery.salesorders_detail[i].qty_trans == '' || $scope.delivery.salesorders_detail[i].qty_trans == 0){
                var x = 1;
            }
        }

        if(x==1){
            $dialogs.error('Warning', 'Nilai Qty Delivery tidak boleh kosong !!!')
        }else{
            progress = 10;
            i = 0;
            $dialogs.wait(msgs[0],0);
            fakeProgress();

            $scope.delivery.date_delivery = moment($scope.delivery.date_delivery).format('YYYY-MM-DD');

            $scope.delivery.put().then(function() {
              i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : $scope.delivery.id});
              $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
              $rootScope.$broadcast('dialogs.wait.complete');
              $location.path('/home/delivery/list');
          });
        }
    };

}])

.controller('SalesorderTransactionCtrl', ['APP_CONFIG','$timeout', '$rootScope', '$http', '$window', '$scope', '$route', '$location', 'Restangular', 'item', 'i18nNotifications', '$dialog', 'security', 'simpleDeleteConfirm', '$modal', '$dialogs', function (APP_CONFIG,$timeout, $rootScope, $http, $window, $scope, $route, $location, Restangular, item, i18nNotifications, $dialog, security, simpleDeleteConfirm, $modal, $dialogs) {

    //debugger;
    var original = item;
    original.transaction_type = 2;
    original.ongkir_delivery = original.ongkir;
    original.shipping_id_delivery = original.shipping_id;

    $scope.delivery = Restangular.copy(original);

    Restangular.all("customers").getList({where: "id = '"+$scope.delivery.customer+"'"}).then(function(customers) {
        $scope.customers = customers.records;
        $scope.delivery.customer = $scope.customers[0].name;
    });

    Restangular.all("markets").getList({where:"is_active = 1"}).then(function(markets) {
        $scope.markets = markets.records;
    });

    Restangular.all("shippings").getList({where:"is_active = 1"}).then(function(shippings) {
        $scope.shippings = shippings.records;
    });

    Restangular.all("regions/province").getList().then(function(regions) {
        $scope.provinces = regions.records;
    });

    Restangular.all("regions/city").getList({where:"param_region.name = '"+$scope.delivery.province+"'"}).then(function(regions) {
        $scope.citys = regions.records;
    });

    Restangular.all("regions/districts").getList({where:"param_region.city = '"+$scope.delivery.city+"'"}).then(function(regions) {
        $scope.districts = regions.records;
    });

    Restangular.all("regions/village").getList({where:"param_region.districts = '"+$scope.delivery.districts+"'"}).then(function(regions) {
        $scope.villages = regions.records;
    });

    $scope.$watch(function() {
        return security.currentUser;
    }, function(currentUser) {
        $scope.currentUser = currentUser;
    });

    for(var i=0; i<$scope.delivery.inventorys_detail.length; i++){
        $scope.delivery.inventorys_detail[i].total_hpp = $scope.delivery.inventorys_detail[i].qty * $scope.delivery.inventorys_detail[i].stock_hpp;
    }

    $scope.detailCellTpl = '<div class="ngCellText" ng-class="col.colIndex()"><a ng-href="" ng-click="removeQuotationItem(row.rowIndex, row)"><i class="icon-trash"></i></a></div>';

    $scope.gridOptions = {
        data: 'delivery.inventorys_detail',
        enablePaging: true,
        showFooter: true,
        multiSelect: false,
        selectedItems: [],
        totalServerItems:'totalServerItems',
        enableColumnResize: true,
        enableCellSelection: true,
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        columnDefs: [
        { displayName: 'Delete', width: 80, cellClass: "text-center", cellTemplate: $scope.detailCellTpl},
        { field: 'item_id', displayName: 'Code', width: 120, cellClass: "text-center"},
        { field: 'item_name', displayName: 'Item'},
        { field: 'item_unit', displayName: 'Unit', width:80, cellClass: "text-center"},
        { field: 'stock_qty', displayName: 'Stok', width:100, cellClass: "text-right", cellFilter: 'number'},
        { field: 'qty', displayName: 'Qty SO', width: 100, cellClass: "text-right" , cellFilter: 'number'},
        { field: 'price', displayName: 'Price (Rp)*', width: 120, cellClass: "text-right", cellFilter: 'number'},
        { field: 'total_hpp', displayName: 'Price HPP (Rp)', width:120, cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'total_item_weight', displayName: 'Total Berat (g)', width:120, cellClass: "text-right", cellFilter: 'number'},
        { field: 'bruto', displayName: 'Bruto' , width: 120, cellClass: "text-right", cellFilter: 'number'},
        { field: 'disc_percent', displayName: 'Disc (%)', width: 100, visible: false,cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'disc_rupiah', displayName: 'Disc (Rp)', width: 120, cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'total_price', displayName: 'Total' , width: 120, cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'is_delivery', displayName: 'Is Delivery' , width: 120, cellClass: "text-right", cellFilter: 'number:2', visible: false}
        ]
    }

    $scope.delivery.detail_delete = new Array();
    $scope.removeQuotationItem = function(index, row) {
        $scope.delivery.salesorders_detail.splice(index, 1);
        $scope.calculate(row);
    };

    $scope.calculate = function(row) {
        if(row.entity.qty_trans != ''){
            if(row.entity.qty_trans <= row.entity.qty){
                if(row.entity.mystock == 0){
                    row.entity.stock = parseInt(row.entity.qty_trans);
                }else{
                    row.entity.stock = parseInt(row.entity.mystock) - parseInt(row.entity.qty_trans);
                }
            }else{
                $dialogs.error('Warning', 'Qty delivery tidak boleh lebih besar dari Qty SO');
                row.entity.qty_trans = 0;
            }
        }else{
            row.entity.qty_trans = 0;
            row.entity.stock = row.entity.mystock;
        }

        $scope.delivery.qty_trans = 0;
        $scope.delivery.tot_price = 0;
        var x = 0;
        for(var i=0; i<$scope.delivery.salesorders_detail.length; i++){
            $scope.delivery.qty_trans = parseInt($scope.delivery.qty_trans) + parseInt($scope.delivery.salesorders_detail[i].qty_trans);
        }
    };

    $scope.ongkir = function() {
        $scope.delivery.subtotal = parseFloat($scope.delivery.ongkir_delivery) + parseFloat($scope.delivery.total);
    };

    $scope.canCreate    = security.canCreate;
    $scope.canUpdate    = security.canUpdate;
    $scope.canRead      = security.canRead;
    $scope.canDelete    = security.canDelete;

    var progress = 10;
    var msgs = [
    'Processing...',
    'Done!'
    ];
    var i = 0;    
    var fakeProgress = function(){
        $timeout(function(){
            if(progress < 95){
                progress += 5;
                $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': progress});
                fakeProgress();
            }else{
            }
        },1000);
    };

    $scope.back = function() {
        $window.history.back();
    }
    $scope.canSave = function() {
        return $scope.DeliveryForm1.$valid && $scope.DeliveryForm2.$valid &&
        !angular.equals($scope.delivery, original);
    };

    $scope.delivered = function(){
        var dlg = $dialogs.confirm('Konfirmasi','Apakah Anda ingin yakin mendeliver data ini?');
        dlg.result.then(function(btn){
            $scope.saved();
        },function(btn){
        });
    };

    $scope.saved = function() { 
        for(var i=0; i<$scope.delivery.salesorders_detail.length; i++){
            if($scope.delivery.salesorders_detail[i].qty_trans == undefined || $scope.delivery.salesorders_detail[i].qty_trans == '' || $scope.delivery.salesorders_detail[i].qty_trans == 0){
                var x = 1;
            }
        }

        if(x==1){
            $dialogs.error('Warning', 'Nilai Qty Delivery tidak boleh kosong !!!')
        }else{
            progress = 10;
            i = 0;
            $dialogs.wait(msgs[0],0);
            fakeProgress();

            $scope.delivery.date_delivery = moment($scope.delivery.date_delivery).format('YYYY-MM-DD');

            $scope.delivery.is_save = 2;

            $scope.delivery.put().then(function() {
              i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : $scope.delivery.id});
              $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
              $rootScope.$broadcast('dialogs.wait.complete');
              $location.path('/home/delivery/list');
          });
        }
    };

}]);