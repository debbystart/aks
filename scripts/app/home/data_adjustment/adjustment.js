angular.module('adjustment', [
    'shared.dialog',
    'ui.bootstrap.datepicker',
    'ui.bootstrap.typeahead',
    'ui.bootstrap.modal',
    'restangular', 
    'services.i18nNotifications'
    ])

.config(['$routeProvider', 'RestangularProvider', function ($routeProvider, RestangularProvider) {

    $routeProvider
    .when('/home/adjustment', {
        controller: 'AdjustmentListCtrl',
        templateUrl: 'scripts/app/home/data_adjustment/list.tpl.html'
    })

    .when('/home/adjustment/edit/:id', {
        controller: 'AdjustmentEditCtrl',
        templateUrl: 'scripts/app/home/data_adjustment/detail.tpl.html',
        resolve: {
            item: function(Restangular, $route){
                return Restangular.one('adjustments', $route.current.params.id).get({expands: 'adjustments_detail'});
            }
        }
    })

    .when('/home/adjustment/create', {
        controller: 'AdjustmentCreateCtrl',
        templateUrl: 'scripts/app/home/data_adjustment/create.tpl.html'
    })
    
    .otherwise({redirectTo:'/home'});

    RestangularProvider.setRestangularFields({
        id: 'id'
    });

    RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
        if (operation === 'put') {
            elem._id = undefined;
            return elem;
        }
        return elem;
    });
}])

.controller('AdjustmentListCtrl', ['$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function ($scope, $route, $location, Restangular, security, i18nNotifications) {

    // user permissions
    $scope.canCreate    = security.canCreate;
    $scope.canUpdate    = security.canUpdate;
    $scope.canRead      = security.canRead;
    $scope.canDelete    = security.canDelete;

    $scope.create = function() {
        $location.path('home/adjustment/create');
    }

    $scope.canSelect = function() {
        return ($scope.gridOptions.selectedItems.length===0?false:true);
    }

    $scope.select = function () {
        $modalInstance.close($scope.gridOptions.selectedItems[0]);
    };

    $scope.back = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.detail = function(row){
        $location.path('home/adjustment/edit/'+row);
    }

    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [20, 50, 100],
        pageSize: 50,
        currentPage: 1
    };  
    $scope.setPagingData = function(data, page, pageSize){  
        var pagedData = data.records;
        $scope.myData = pagedData;
        $scope.totalServerItems = data.total;
        if (!$scope.$$phase) {
          $scope.$apply();
      }
        //
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        setTimeout(function () {
          var custom_get = {};

          if($route.current.params)
            custom_get = $route.current.params;

        custom_get.offset = (page - 1) * pageSize; 
        custom_get.limit = pageSize;

        if (searchText='$scope.filterOptions.filterText') {

            custom_get.filter = $scope.filterOptions.filterText;
            custom_get.filter_fields = 'id';

            Restangular.all("adjustments").getList(custom_get).then(function(adjustments) {
              $scope.setPagingData(adjustments,page,pageSize);
          });
        } 
        else {
            Restangular.all("adjustments").getList(custom_get).then(function(adjustments) {
              $scope.setPagingData(adjustments,page,pageSize);
          });
        }
    }, 100);
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
          $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
      }
  }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
          $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
      }
  }, true);

    $scope.gridOptions = {
        data: 'myData',
        enablePaging: true,
        showFooter: true,
        multiSelect: false,
        selectedItems: [],
        totalServerItems:'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        enableColumnResize: true,
        columnDefs: [
        { field: 'is_save', displayName: 'Status', width: 170,
        cellTemplate:   '<div class="ngCellText" style="text-align:center" ng-class="{\'blue\' : row.getProperty(\'is_save\') == 0, \'orange\' : row.getProperty(\'is_save\')==1, \'green\' : row.getProperty(\'is_save\')==2}">'+
        '<span ng-cell-text ng-show="row.getProperty(col.field)==0">Saved</span>'+
        '<span ng-cell-text ng-show="row.getProperty(col.field)==1">Posted</span>'+
        '<span ng-cell-text ng-show="row.getProperty(col.field)==2">Receive</span></div>'},
        { field: 'id', displayName: 'Code', width: 150, cellClass:'text-center', enableCellEdit: true },
        { field: 'supplier', displayName: 'Supplier', width: 450, enableCellEdit: true },
        { field: 'shipping', displayName: 'Shipping', width: 150, enableCellEdit: true },
        { field: 'to_name', displayName: 'Kepada', width: 200, enableCellEdit: true },
        { field: 'total', displayName: 'Total', width: 150, enableCellEdit: true, cellFilter: 'number', cellClass: 'text-right'},
        { field: 'ongkir', displayName: 'Ongkos Kirim', width: 150, enableCellEdit: true, cellFilter: 'number', cellClass: 'text-right'},
        {displayName: 'Action', cellClass:'text-center',
        cellTemplate:   '<div class="ngCellText" ng-class="col.colIndex()"><a class="btn btn-xs btn-default" ng-href="" ng-click="detail(row.entity.id)" data-toggle="tooltip" title="edit" data-original-title="Edit Client"><i class="fa fa-pencil"></i></a></div>'}
        ]
    };

}])

.controller('AdjustmentCreateCtrl', ['$timeout', '$rootScope', '$http','$window', '$route', '$scope', '$location', 'Restangular', 'security', 'i18nNotifications', '$modal', '$dialogs', function ($timeout, $rootScope, $http ,$window, $route, $scope, $location, Restangular, security, i18nNotifications, $modal, $dialogs) {

    var original = {};

    original.adjustments_detail = [];
    original.date = new Date();
    original.type = 2;

    $scope.adjustment = Restangular.copy(original);

    Restangular.all("reasonstocks").getList({where:"is_active = 1"}).then(function(reasonstocks) {
        $scope.reasonstocks = reasonstocks.records;
    });

    $scope.$watch(function() {
        return security.currentUser;
    }, function(currentUser) {
        $scope.currentUser = currentUser;
    });

    $scope.format = 'dd-MMM-yyyy';

    $scope.adjustment.adjustments_detail = original.adjustments_detail;

    $scope.detailCellTpl = '<div class="ngCellText" ng-class="col.colIndex()"><a ng-href="" ng-click="removeQuotationItem(row.rowIndex)"><i class="icon-trash"></i></a></div>';

    $scope.gridOptions = {
        data: 'adjustment.adjustments_detail',
        enablePaging: true,
        showFooter: true,
        multiSelect: false,
        selectedItems: [],
        totalServerItems:'totalServerItems',
        enableColumnResize: true,
        enableCellSelection: true,
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        columnDefs: [
        { displayName: 'Delete', width: 100, cellClass: "text-center", cellTemplate: $scope.detailCellTpl},
        { field: 'item_id', displayName: 'Code', width: 120, cellClass: "text-center"},
        { field: 'item_name', displayName: 'Item'},
        { field: 'item_unit', displayName: 'Unit', width:100, cellClass: "text-center"},
        { field: 'item_weight', displayName: 'Berat (gram)', width:120, cellClass: "text-right", cellFilter: 'number'},
        { field: 'total_item_weight', displayName: 'Toral Berat (gram)', width:120, cellClass: "text-right", cellFilter: 'number', visible: false},
        { field: 'stock_save', displayName: 'Stok Save' , width: 150, cellClass: "text-right", cellFilter: 'number', visible: false},
        { field: 'stock', displayName: 'Stok' , width: 150, cellClass: "text-right", cellFilter: 'number'},
        { field: 'qty', displayName: 'Qty*', width: 100, cellClass: "text-right" , enableCellEdit: true, cellFilter: 'number', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-change="qtycalculate(row)" format="number"/>'},
        { field: 'price', displayName: 'Price', width: 150, cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'bruto', displayName: 'Bruto' , width: 150, cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'total_price_item', displayName: 'Total /item' , width: 150, cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'total_price', displayName: 'Total' , width: 150, cellClass: "text-right", cellFilter: 'number:2'}
        ]
    }

    $scope.removeQuotationItem = function(index) {
        $scope.adjustment.adjustments_detail.splice(index, 1);
        $scope.calculate();
    };

    $scope.qtycalculate = function(row) {
        if($scope.adjustment.type == 1){
            row.entity.stock = row.entity.stock_save + row.entity.qty;
        }else{
            row.entity.stock = row.entity.stock_save - row.entity.qty;
            if(row.entity.stock < 0){
                $dialogs.error('Warning', 'Qty tidak boleh lebih besar dari Stock !!!')
                row.entity.qty = 0;
                row.entity.stock = row.entity.stock_save;
            }
        }
        $scope.calculate();
    };

    $scope.calculate = function() {
        $scope.adjustment.bruto = 0;
        $scope.adjustment.total = 0;
        $scope.adjustment.qty = 0;
        $scope.adjustment.total_item_weight = 0;
        var x = 0;
        for(var i=0; i<$scope.adjustment.adjustments_detail.length; i++){
            $scope.adjustment.adjustments_detail[i].total_item_weight = parseInt($scope.adjustment.adjustments_detail[i].qty) * parseInt($scope.adjustment.adjustments_detail[i].item_weight);
            $scope.adjustment.adjustments_detail[i].bruto = parseInt($scope.adjustment.adjustments_detail[i].qty) * parseInt($scope.adjustment.adjustments_detail[i].price);
            $scope.adjustment.qty = parseInt($scope.adjustment.qty) + parseInt($scope.adjustment.adjustments_detail[i].qty);
            $scope.adjustment.total_item_weight = parseInt($scope.adjustment.total_item_weight) + parseInt($scope.adjustment.adjustments_detail[i].total_item_weight);
        }

        for(var i=0; i<$scope.adjustment.adjustments_detail.length; i++){
            x = parseFloat($scope.adjustment.adjustments_detail[i].total_item_weight / $scope.adjustment.total_item_weight) * 100;
            $scope.adjustment.adjustments_detail[i].ongkir = parseFloat($scope.adjustment.ongkir) * parseFloat(x / 100);
            $scope.adjustment.adjustments_detail[i].total_price = parseInt($scope.adjustment.adjustments_detail[i].bruto) + parseFloat($scope.adjustment.adjustments_detail[i].ongkir);
            $scope.adjustment.adjustments_detail[i].total_price_item = parseFloat($scope.adjustment.adjustments_detail[i].total_price) / parseFloat($scope.adjustment.adjustments_detail[i].qty);
            $scope.adjustment.bruto = parseInt($scope.adjustment.bruto) + parseInt($scope.adjustment.adjustments_detail[i].bruto);
            $scope.adjustment.total = parseFloat($scope.adjustment.total) + parseFloat($scope.adjustment.adjustments_detail[i].total_price);
        }
    };
    
    $scope.addItem = function(row) {
        var modalInstance = $modal.open({
            templateUrl: 'scripts/app/home/data_adjustment/item.list.modal.tpl.html',
            controller: ModalInstanceAddQuotationItemCreateCtrl,
            resolve: {
                adjustment_detail: function(Restangular, $route){
                    return Restangular.copy($scope.adjustment);
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {

            $scope.adjustment.item_amount = $scope.adjustment.item_amount + selectedItem.length;

            for (var i = 0; i < selectedItem.length; i++){
            // reload table
            var adjustment_detail            = {};
            adjustment_detail.pivot          = {};
            adjustment_detail.item_id        = selectedItem[i].id;
            adjustment_detail.item_name      = selectedItem[i].name;
            adjustment_detail.item_unit      = selectedItem[i].unit;
            adjustment_detail.item_weight    = selectedItem[i].weight;
            adjustment_detail.stock          = selectedItem[i].qty;
            adjustment_detail.stock_save     = selectedItem[i].qty;
            adjustment_detail.qty            = 0;
            adjustment_detail.price          = selectedItem[i].hpp;
            adjustment_detail.bruto          = 0;
            adjustment_detail.total_price    = 0;
            adjustment_detail.total_price_item = 0;

            $scope.adjustment.adjustments_detail.splice($scope.adjustment.adjustments_detail.length+1,0,adjustment_detail);
        }

    }, function () {
    });
    };
    
    var ModalInstanceAddQuotationItemCreateCtrl = function ($scope, $dialog, $modalInstance, adjustment_detail, simpleDeleteConfirm, Restangular, security, i18nNotifications, $upload, APP_CONFIG) {

        var original = angular.copy(adjustment_detail);
        
        $scope.adjustment_detail = original;

        $scope.$watch(function() {
            return security.currentUser;
        }, function(currentUser) {
            $scope.currentUser = currentUser;
        });

        $scope.filterOptions = {
            filterText: "",
            useExternalFilter: true
        };
        $scope.totalServerItems = 0;
        $scope.pagingOptions = {
            pageSizes: [20, 50, 100],
            pageSize: 20,
            currentPage: 1
        };
        $scope.setPagingData = function(data, page, pageSize){
            var pagedData = data.records;
            $scope.myData = pagedData;
            $scope.totalServerItems = data.total;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        };
        $scope.getPagedDataAsync = function (pageSize, page, searchText) {
            setTimeout(function () {
                var custom_get = {};

                if($route.current.params)
                    custom_get = $route.current.params;

                custom_get.offset = (page - 1) * pageSize;
                custom_get.limit = pageSize;

                if (searchText='$scope.filterOptions.filterText') {

                    custom_get.filter = $scope.filterOptions.filterText;
                    custom_get.filter_fields = 'param_item.id, param_item.name';

                    Restangular.all("items/adjustment").getList(custom_get).then(function(items) {
                        $scope.setPagingData(items,page,pageSize);
                    });
                }
                else {
                    Restangular.all("items/adjustment").getList(custom_get).then(function(items) {
                        $scope.setPagingData(items,page,pageSize);
                    });
                }
            }, 100);
        };

        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

        $scope.$watch('pagingOptions', function (newVal, oldVal) {
            if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
            }
        }, true);
        $scope.$watch('filterOptions', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
            }
        }, true);


        $scope.gridOptions = {
            data: 'myData',
            enablePaging: true,
            showFooter: true,
            multiSelect: true,
            selectedItems: [],
            enableColumnResize: true,
            totalServerItems:'totalServerItems',
            pagingOptions: $scope.pagingOptions,
            filterOptions: $scope.filterOptions,
            showSelectionCheckbox: true,
            checkboxHeaderTemplate: '<input class="ngSelectionHeader" type="checkbox" ng-model="allSelected" ng-change="toggleSelectAll(allSelected, true)"/>',
            columnDefs: [
            { field: 'id', displayName: 'Code', width: 130, cellClass:'text-center', enableCellEdit: true },
            { field: 'name', displayName: 'Name', enableCellEdit: true },
            { field: 'unit', displayName: 'Unit', width: 100, cellClass:'text-center' },
            { field: 'qty', displayName: 'Stok', width: 100, cellClass:'text-right' },
            { field: 'brand', displayName: 'Merek', width: 150, cellClass:'text-center' },
            { field: 'group_name', displayName: 'Item Group', width: 240, enableCellEdit: true },
            { field: 'category_name', displayName: 'Item Category', width: 200, enableCellEdit: true },
            { field: 'subcategory_name', displayName: 'Item Subcategory', width: 190, enableCellEdit: true }
            ]
        };

        $scope.canSelect = function() {
            return ($scope.gridOptions.selectedItems.length===0?false:true);
        }

        $scope.select = function () {
            var selectedItem = $scope.gridOptions.selectedItems;
            var isExist = false;
            for(var j = 0; j <$scope.adjustment_detail.adjustments_detail.length; j++){
                for(var i = 0; i <  selectedItem.length; i++){
                    var as = selectedItem[i];
                    if(as.id==$scope.adjustment_detail.adjustments_detail[j].item_id){
                        selectedItem.splice(i, 1);
                    }
                }
            }

            for(var i = 0; i <  selectedItem.length; i++){
                var as = selectedItem[i];
                for(var j=0; j <  selectedItem.length; j++){
                    var sa = selectedItem[j];
                    if(i != j){
                        if(as.id==sa.id){
                            selectedItem.splice(j, 1);
                        }
                    }
                }
            }
            $modalInstance.close(selectedItem);
        };

        $scope.back = function () {
            $modalInstance.dismiss('cancel');
        };
    };

    $scope.canCreate    = security.canCreate;
    $scope.canUpdate    = security.canUpdate;
    $scope.canRead      = security.canRead;
    $scope.canDelete    = security.canDelete;

    var progress = 10;
    var msgs = [
    'Processing...',
    'Done!'
    ];
    var i = 0;    
    var fakeProgress = function(){
        $timeout(function(){
            if(progress < 95){
                progress += 5;
                $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': progress});
                fakeProgress();
            }else{
            }
        },1000);
    };

    $scope.canRemove = function() {
        return false;
    }

    $scope.revertprtype = function() {
        $scope.myData = original.adjustments_detail= [];
    }

    $scope.back = function() {
        $window.history.back();
    }
    $scope.canSave = function() {
        return $scope.AdjustmentForm.$valid &&
        !angular.equals($scope.adjustment, original);
    };

    $scope.canRevert = function() {
        return !angular.equals($scope.adjustment, original);
    }
    $scope.revert = function() {
        $scope.adjustment =  Restangular.copy(original);
        $scope.adjustmentForm.$setPristine();
    }

    $scope.save = function() { 
        for(var i=0; i<$scope.adjustment.adjustments_detail.length; i++){
            if($scope.adjustment.adjustments_detail[i].qty == undefined || $scope.adjustment.adjustments_detail[i].qty == '' || $scope.adjustment.adjustments_detail[i].qty == 0){
                var x = 1;
            }else if($scope.adjustment.adjustments_detail[i].price == undefined || $scope.adjustment.adjustments_detail[i].price == '' || $scope.adjustment.adjustments_detail[i].price == 0){
                var x = 2;
            }
        }

        if(x==1){
            $dialogs.error('Warning', 'Nilai Qty tidak boleh kosong !!!')
        }else if(x==2){
            $dialogs.error('Warning', 'Nilai Price tidak boleh kosong !!!')
        }else{
            progress = 10;
            i = 0;
            $dialogs.wait(msgs[0],0);
            fakeProgress();

            $scope.adjustment.date = moment($scope.adjustment.date).format('YYYY-MM-DD');

            Restangular.all('adjustments').post($scope.adjustment).then(function(adjustment) {
              i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : adjustment.id});
              $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
              $rootScope.$broadcast('dialogs.wait.complete');
              $location.path('/home/adjustment');
          });
        }
    };

}])

.controller('AdjustmentEditCtrl', ['APP_CONFIG','$timeout', '$rootScope', '$http', '$window', '$scope', '$route', '$location', 'Restangular', 'item', 'i18nNotifications', '$dialog', 'security', 'simpleDeleteConfirm', '$modal', '$dialogs', function (APP_CONFIG,$timeout, $rootScope, $http, $window, $scope, $route, $location, Restangular, item, i18nNotifications, $dialog, security, simpleDeleteConfirm, $modal, $dialogs) {

    //debugger;
    var original = item;

    $scope.adjustment = Restangular.copy(original);

    Restangular.all("markets").getList({where:"is_active = 1"}).then(function(markets) {
        $scope.markets = markets.records;
    });

    Restangular.all("shippings").getList({where:"is_active = 1"}).then(function(shippings) {
        $scope.shippings = shippings.records;
    });

    Restangular.all("regions/province").getList().then(function(regions) {
        $scope.provinces = regions.records;
    });

    Restangular.all("regions/city").getList({where:"param_region.name = '"+$scope.adjustment.province+"'"}).then(function(regions) {
        $scope.citys = regions.records;
    });

    Restangular.all("regions/districts").getList({where:"param_region.city = '"+$scope.adjustment.city+"'"}).then(function(regions) {
        $scope.districts = regions.records;
    });

    Restangular.all("regions/village").getList({where:"param_region.districts = '"+$scope.adjustment.districts+"'"}).then(function(regions) {
        $scope.villages = regions.records;
    });

    $scope.$watch(function() {
        return security.currentUser;
    }, function(currentUser) {
        $scope.currentUser = currentUser;
    });

    $scope.format = 'dd-MMM-yyyy';

    $scope.adjustment.adjustments_detail = original.adjustments_detail;

    $scope.detailCellTpl = '<div class="ngCellText" ng-class="col.colIndex()"><a ng-href="" ng-click="removeQuotationItem(row.rowIndex, row)"><i class="icon-trash"></i></a></div>';

    $scope.gridOptions = {
        data: 'adjustment.adjustments_detail',
        enablePaging: true,
        showFooter: true,
        multiSelect: false,
        selectedItems: [],
        totalServerItems:'totalServerItems',
        enableColumnResize: true,
        enableCellSelection: true,
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        columnDefs: [
        { displayName: 'Delete', width: 100, cellClass: "text-center", cellTemplate: $scope.detailCellTpl},
        { field: 'item_id', displayName: 'Code', width: 120, cellClass: "text-center"},
        { field: 'item_name', displayName: 'Item'},
        { field: 'item_unit', displayName: 'Unit', width:100, cellClass: "text-center"},
        { field: 'item_weight', displayName: 'Berat (gram)', width:120, cellClass: "text-right", cellFilter: 'number'},
        { field: 'total_item_weight', displayName: 'Toral Berat (gram)', width:120, cellClass: "text-right", cellFilter: 'number', visible: false},
        { field: 'qty', displayName: 'Qty*', width: 100, cellClass: "text-right" , enableCellEdit: true, cellFilter: 'number', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-change="calculate()" format="number"/>'},
        { field: 'price', displayName: 'Price*', width: 150, cellClass: "text-right", enableCellEdit: true, cellFilter: 'number', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-change="calculate()" format="number"/>'},
        { field: 'ongkir', displayName: 'Ongkos Kirim' , width: 150, cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'bruto', displayName: 'Bruto' , width: 150, cellClass: "text-right", cellFilter: 'number'},
        { field: 'total_price_item', displayName: 'Total /item' , width: 150, cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'total_price', displayName: 'Total' , width: 150, cellClass: "text-right", cellFilter: 'number:2'}
        ]
    }

    $scope.adjustment.detail_delete = new Array();
    $scope.removeQuotationItem = function(index, row) {
        $scope.adjustment.adjustments_detail.splice(index, 1);
        $scope.adjustment.detail_delete.push(row.entity.id);
        $scope.calculate();
    };

    $scope.change_ongkir = function() {
        for(var i=0; i<$scope.adjustment.adjustments_detail.length; i++){
            $scope.adjustment.adjustments_detail[i].qty = 0;
        }
    };

    $scope.calculate = function() {
        $scope.adjustment.bruto = 0;
        $scope.adjustment.total = 0;
        $scope.adjustment.qty = 0;
        $scope.adjustment.total_item_weight = 0;
        var x = 0;
        for(var i=0; i<$scope.adjustment.adjustments_detail.length; i++){
            $scope.adjustment.adjustments_detail[i].total_item_weight = parseInt($scope.adjustment.adjustments_detail[i].qty) * parseInt($scope.adjustment.adjustments_detail[i].item_weight);
            $scope.adjustment.adjustments_detail[i].bruto = parseInt($scope.adjustment.adjustments_detail[i].qty) * parseInt($scope.adjustment.adjustments_detail[i].price);
            $scope.adjustment.qty = parseInt($scope.adjustment.qty) + parseInt($scope.adjustment.adjustments_detail[i].qty);
            $scope.adjustment.total_item_weight = parseInt($scope.adjustment.total_item_weight) + parseInt($scope.adjustment.adjustments_detail[i].total_item_weight);
        }

        for(var i=0; i<$scope.adjustment.adjustments_detail.length; i++){
            x = parseFloat($scope.adjustment.adjustments_detail[i].total_item_weight / $scope.adjustment.total_item_weight) * 100;
            $scope.adjustment.adjustments_detail[i].ongkir = parseFloat($scope.adjustment.ongkir) * parseFloat(x / 100);
            $scope.adjustment.adjustments_detail[i].total_price = parseInt($scope.adjustment.adjustments_detail[i].bruto) + parseFloat($scope.adjustment.adjustments_detail[i].ongkir);
            $scope.adjustment.adjustments_detail[i].total_price_item = parseFloat($scope.adjustment.adjustments_detail[i].total_price) / parseFloat($scope.adjustment.adjustments_detail[i].qty);
            $scope.adjustment.bruto = parseInt($scope.adjustment.bruto) + parseInt($scope.adjustment.adjustments_detail[i].bruto);
            $scope.adjustment.total = parseFloat($scope.adjustment.total) + parseFloat($scope.adjustment.adjustments_detail[i].total_price);
        }
    };

    $scope.supplier = function() {

        var modalInstance = $modal.open({
            templateUrl: 'scripts/app/home/data_adjustment/supplier.list.modal.tpl.html',
            controller: ModalInstanceBrowseProjectCtrl,
            resolve: {
                supplier_id: function () {
                    return $scope.adjustment;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {

            var customer = Restangular.copy(selectedItem);
            var adjustment = Restangular.copy($scope.adjustment);

            Restangular.all("regions/province").getList().then(function(regions) {
                $scope.provinces = regions.records;
            });

            Restangular.all("regions/city").getList({where:"param_region.name = '"+customer.province+"'"}).then(function(regions) {
                $scope.citys = regions.records;
            });

            Restangular.all("regions/districts").getList({where:"param_region.city = '"+customer.city+"'"}).then(function(regions) {
                $scope.districts = regions.records;
            });

            Restangular.all("regions/village").getList({where:"param_region.districts = '"+customer.districts+"'"}).then(function(regions) {
                $scope.villages = regions.records;
            });

            adjustment.supplier_id = customer.id;
            adjustment.supplier = customer.name;
            adjustment.market_id = customer.market_id;
            adjustment.province = customer.province;
            adjustment.city = customer.city;
            adjustment.districts = customer.districts;
            adjustment.village = customer.village;
            adjustment.from_ship = customer.address;
            adjustment.postal = customer.postal;

            $scope.adjustment = adjustment;

        }, function () {

        });

    };

    var ModalInstanceBrowseProjectCtrl = function ($scope, $dialog, $modalInstance, supplier_id, Restangular, security, i18nNotifications, $route) {

        $scope.item = supplier_id;


        // user permissions
        $scope.canCreate    = security.canCreate;
        $scope.canUpdate    = security.canUpdate;
        $scope.canRead      = security.canRead;
        $scope.canDelete    = security.canDelete;
        $scope.isAuthorizedRoute = security.isAuthorizedRoute;

        $scope.change_filter = function(){
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }

        // ng-Grid
        $scope.filterOptions = {
            filterText: "",
            useExternalFilter: true
        };
        $scope.totalServerItems = 0;
        $scope.pagingOptions = {
            pageSizes: [20, 50, 100],
            pageSize: 50,
            currentPage: 1
        };
        $scope.setPagingData = function(data, page, pageSize){
            var pagedData = data.records;
            $scope.myData = pagedData;
            $scope.totalServerItems = data.total;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        };
        $scope.getPagedDataAsync = function (pageSize, page, searchText) {
            setTimeout(function () {
                var custom_get = {};

                if($route.current.params)
                    custom_get = $route.current.params;

                custom_get.offset = (page - 1) * pageSize;
                custom_get.limit = pageSize;

                if (searchText='$scope.filterOptions.filterText') {

                    custom_get.filter = $scope.filterOptions.filterText;
                    custom_get.filter_fields = 'id, name';

                    Restangular.all("suppliers").getList(custom_get).then(function(suppliers) {
                        $scope.setPagingData(suppliers,page,pageSize);
                    });
                }
                else {
                    Restangular.all("suppliers").getList(custom_get).then(function(suppliers) {
                        $scope.setPagingData(suppliers,page,pageSize);
                    });
                }
            }, 100);
        };

        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

        $scope.$watch('pagingOptions', function (newVal, oldVal) {
            if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
            }
        }, true);

        $scope.gridOptions = {
            data: 'myData',
            enablePaging: true,
            showFooter: true,
            multiSelect: false,
            selectedItems: [],
            totalServerItems:'totalServerItems',
            pagingOptions: $scope.pagingOptions,
            filterOptions: $scope.filterOptions,
            columnDefs: [
            { field: 'id', displayName: 'Code', cellClass: 'text-center', enableCellEdit:true, width: 120},
            { field: 'name', displayName: 'Nama', enableCellEdit:true},
            { field: 'province', displayName: 'Provinsi', width: 195},
            { field: 'city', displayName: 'Kabupaten/Kota', width: 190},
            { field: 'districts', displayName: 'Kecamatan', width: 185},
            { field: 'village', displayName: 'Kelurahan', width: 180},
            { field: 'market', displayName: 'Market', width: 130},
            { field: 'mobile', displayName: 'Telephone', width: 130, cellClass: 'text-right'},
            ]
        };

        $scope.canSelect = function() {
            return ($scope.gridOptions.selectedItems.length===0?false:true);
        }

        $scope.select = function () {
            $modalInstance.close($scope.gridOptions.selectedItems[0]);
        };

        $scope.back = function () {
            $modalInstance.dismiss('cancel');
        };

    };
    
    $scope.addItem = function(row) {
        var modalInstance = $modal.open({
            templateUrl: 'scripts/app/home/data_adjustment/item.list.modal.tpl.html',
            controller: ModalInstanceAddQuotationItemCreateCtrl,
            resolve: {
                adjustment_detail: function(Restangular, $route){
                    return Restangular.copy($scope.adjustment);
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {

            $scope.adjustment.item_amount = $scope.adjustment.item_amount + selectedItem.length;

            for (var i = 0; i < selectedItem.length; i++){
            // reload table
            var adjustment_detail            = {};
            adjustment_detail.pivot          = {};
            adjustment_detail.item_id        = selectedItem[i].id;
            adjustment_detail.item_name      = selectedItem[i].name;
            adjustment_detail.item_unit      = selectedItem[i].unit;
            adjustment_detail.item_weight    = selectedItem[i].weight;
            adjustment_detail.qty            = 0;
            adjustment_detail.price          = 0;
            adjustment_detail.ongkir         = 0;
            adjustment_detail.bruto          = 0;
            adjustment_detail.total_price    = 0;
            adjustment_detail.total_price_item = 0;

            $scope.adjustment.adjustments_detail.splice($scope.adjustment.adjustments_detail.length+1,0,adjustment_detail);
        }

    }, function () {
    });
    };
    
    var ModalInstanceAddQuotationItemCreateCtrl = function ($scope, $dialog, $modalInstance, adjustment_detail, simpleDeleteConfirm, Restangular, security, i18nNotifications, $upload, APP_CONFIG) {

        var original = angular.copy(adjustment_detail);
        
        $scope.adjustment_detail = original;

        $scope.$watch(function() {
            return security.currentUser;
        }, function(currentUser) {
            $scope.currentUser = currentUser;
        });

        $scope.filterOptions = {
            filterText: "",
            useExternalFilter: true
        };
        $scope.totalServerItems = 0;
        $scope.pagingOptions = {
            pageSizes: [20, 50, 100],
            pageSize: 20,
            currentPage: 1
        };
        $scope.setPagingData = function(data, page, pageSize){
            var pagedData = data.records;
            $scope.myData = pagedData;
            $scope.totalServerItems = data.total;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        };
        $scope.getPagedDataAsync = function (pageSize, page, searchText) {
            setTimeout(function () {
                var custom_get = {};

                if($route.current.params)
                    custom_get = $route.current.params;

                custom_get.offset = (page - 1) * pageSize;
                custom_get.limit = pageSize;

                if (searchText='$scope.filterOptions.filterText') {

                    custom_get.filter = $scope.filterOptions.filterText;
                    custom_get.filter_fields = 'param_item.id, param_item.name';

                    Restangular.all("items/po").getList(custom_get).then(function(price_list_locations) {
                        $scope.setPagingData(price_list_locations,page,pageSize);
                    });
                }
                else {
                    Restangular.all("items/po").getList(custom_get).then(function(price_list_locations) {
                        $scope.setPagingData(price_list_locations,page,pageSize);
                    });
                }
            }, 100);
        };

        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

        $scope.$watch('pagingOptions', function (newVal, oldVal) {
            if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
            }
        }, true);
        $scope.$watch('filterOptions', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
            }
        }, true);


        $scope.gridOptions = {
            data: 'myData',
            enablePaging: true,
            showFooter: true,
            multiSelect: true,
            selectedItems: [],
            enableColumnResize: true,
            totalServerItems:'totalServerItems',
            pagingOptions: $scope.pagingOptions,
            filterOptions: $scope.filterOptions,
            showSelectionCheckbox: true,
            checkboxHeaderTemplate: '<input class="ngSelectionHeader" type="checkbox" ng-model="allSelected" ng-change="toggleSelectAll(allSelected, true)"/>',
            columnDefs: [
            { field: 'id', displayName: 'Code', width: 130, cellClass:'text-center', enableCellEdit: true },
            { field: 'name', displayName: 'Name', enableCellEdit: true },
            { field: 'unit', displayName: 'Unit', width: 100, cellClass:'text-center' },
            { field: 'qty', displayName: 'Stok', width: 100, cellClass:'text-right' },
            { field: 'brand', displayName: 'Merek', width: 150, cellClass:'text-center' },
            { field: 'group_name', displayName: 'Item Group', width: 240, enableCellEdit: true },
            { field: 'category_name', displayName: 'Item Category', width: 200, enableCellEdit: true },
            { field: 'subcategory_name', displayName: 'Item Subcategory', width: 190, enableCellEdit: true }
            ]
        };

        $scope.canSelect = function() {
            return ($scope.gridOptions.selectedItems.length===0?false:true);
        }

        $scope.select = function () {
            var selectedItem = $scope.gridOptions.selectedItems;
            var isExist = false;
            for(var j = 0; j <$scope.adjustment_detail.adjustments_detail.length; j++){
                for(var i = 0; i <  selectedItem.length; i++){
                    var as = selectedItem[i];
                    if(as.id==$scope.adjustment_detail.adjustments_detail[j].item_id){
                        selectedItem.splice(i, 1);
                    }
                }
            }

            for(var i = 0; i <  selectedItem.length; i++){
                var as = selectedItem[i];
                for(var j=0; j <  selectedItem.length; j++){
                    var sa = selectedItem[j];
                    if(i != j){
                        if(as.id==sa.id){
                            selectedItem.splice(j, 1);
                        }
                    }
                }
            }
            $modalInstance.close(selectedItem);
        };

        $scope.back = function () {
            $modalInstance.dismiss('cancel');
        };
    };

    $scope.canCreate    = security.canCreate;
    $scope.canUpdate    = security.canUpdate;
    $scope.canRead      = security.canRead;
    $scope.canDelete    = security.canDelete;

    var progress = 10;
    var msgs = [
    'Processing...',
    'Done!'
    ];
    var i = 0;    
    var fakeProgress = function(){
        $timeout(function(){
            if(progress < 95){
                progress += 5;
                $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': progress});
                fakeProgress();
            }else{
            }
        },1000);
    };

    $scope.canRemove = function() {
        return false;
    }

    $scope.back = function() {
        $window.history.back();
    }
    $scope.canSave = function() {
        return $scope.AdjustmentForm1.$valid && $scope.AdjustmentForm2.$valid &&
        !angular.equals($scope.adjustment, original);
    };

    $scope.canRevert = function() {
        return !angular.equals($scope.adjustment, original);
    }
    $scope.revert = function() {
        $scope.adjustment =  Restangular.copy(original);
        $scope.adjustmentForm.$setPristine();
    }

    $scope.save = function(){
        $scope.adjustment.is_save = 0;
        $scope.saved();
    };

    $scope.post = function(){
        var dlg = $dialogs.confirm('Konfirmasi','Apakah Anda ingin yakin memposting data ini?');
        dlg.result.then(function(btn){
            $scope.adjustment.is_save = 1;
            $scope.saved();
        },function(btn){
        });
    };

    $scope.saved = function() { 
        for(var i=0; i<$scope.adjustment.adjustments_detail.length; i++){
            if($scope.adjustment.adjustments_detail[i].qty == undefined || $scope.adjustment.adjustments_detail[i].qty == '' || $scope.adjustment.adjustments_detail[i].qty == 0){
                var x = 1;
            }else if($scope.adjustment.adjustments_detail[i].price == undefined || $scope.adjustment.adjustments_detail[i].price == '' || $scope.adjustment.adjustments_detail[i].price == 0){
                var x = 2;
            }
        }

        if(x==1){
            $dialogs.error('Warning', 'Nilai Qty tidak boleh kosong !!!')
        }else if(x==2){
            $dialogs.error('Warning', 'Nilai Price tidak boleh kosong !!!')
        }else{
            progress = 10;
            i = 0;
            $dialogs.wait(msgs[0],0);
            fakeProgress();

            $scope.adjustment.date = moment($scope.adjustment.date).format('YYYY-MM-DD');

            $scope.adjustment.put().then(function() {
              i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : $scope.adjustment.id});
              $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
              $rootScope.$broadcast('dialogs.wait.complete');
              $location.path('/home/adjustment');
          });
        }
    };

}]);

