angular.module('stock', [
  'shared.dialog',
  'restangular',
  'services.i18nNotifications'
  ])

.config(['$routeProvider', 'RestangularProvider', function ($routeProvider, RestangularProvider) {

  $routeProvider
  .when('/home/stock', {
    controller: 'StockListCtrl',
    templateUrl: 'scripts/app/home/data_stock/list.tpl.html'
  })
  
  .otherwise({redirectTo:'/home'});

  RestangularProvider.setRestangularFields({
    id: 'id'
  });

  RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
    if (operation === 'put') {
      elem._id = undefined;
      return elem;
    }
    return elem;
  });
}])

.controller('StockListCtrl', ['$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function ($scope, $route, $location, Restangular, security, i18nNotifications) {

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.back = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.change_filter = function(){
    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
  }

  $scope.filterOptions = {
    filterText: "",
    useExternalFilter: true
  };
  $scope.totalServerItems = 0;
  $scope.pagingOptions = {
    pageSizes: [20, 50, 100],
    pageSize: 50,
    currentPage: 1
  };  
  $scope.setPagingData = function(data, page, pageSize){  
    var pagedData = data.records;
    $scope.myData = pagedData;
    $scope.totalServerItems = data.total;
    if (!$scope.$$phase) {
      $scope.$apply();
    }
  };
  $scope.getPagedDataAsync = function (pageSize, page, searchText) {
    setTimeout(function () {
      var custom_get = {};

      if($route.current.params)
        custom_get = $route.current.params;

      custom_get.offset = (page - 1) * pageSize; 
      custom_get.limit = pageSize;

      if (searchText='$scope.filterOptions.filterText') {

        custom_get.filter = $scope.filterOptions.filterText;
        custom_get.filter_fields = 'item_id ,item_name';

        Restangular.all("stocks").getList(custom_get).then(function(stocks) {
          $scope.setPagingData(stocks,page,pageSize);
        });
      } 
      else {
        Restangular.all("stocks").getList(custom_get).then(function(stocks) {
          $scope.setPagingData(stocks,page,pageSize);
        });
      }
    }, 100);
  };

  $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

  $scope.$watch('pagingOptions', function (newVal, oldVal) {
    if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
    }
  }, true);

  $scope.gridOptions = {
    data: 'myData',
    enablePaging: true,
    showFooter: true,
    multiSelect: false,
    selectedItems: [],
    totalServerItems:'totalServerItems',
    pagingOptions: $scope.pagingOptions,
    filterOptions: $scope.filterOptions,
    enableCellEdit : true,
    columnDefs: [
    { field: 'item_id', displayName: 'Code', width: 150, cellClass:'text-center' },
    { field: 'item_name', displayName: 'Name'},
    { field: 'item_unit', displayName: 'Unit', width: 110, cellClass:'text-center' },
    { field: 'qty', displayName: 'Stock', width: 110, cellClass:'text-right', cellFilter: 'number', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number"/>'},
    { field: 'hpp', displayName: 'HPP', width: 180, cellClass:'text-right', cellFilter: 'number:2', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number"/>'},
    { field: 'total_price', displayName: 'Total HPP', width: 180, cellClass:'text-right', cellFilter: 'number:2', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number"/>'},
    { field: 'created_at', displayName: 'Date', width: 200, cellClass:'text-center' }
    ]
  };

}]);

