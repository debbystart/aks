angular.module('item', [
  'shared.dialog',
  'restangular',
  'services.i18nNotifications'
  ])

.config(['$routeProvider', 'RestangularProvider', function ($routeProvider, RestangularProvider) {

  $routeProvider
  .when('/home/item', {
    controller: 'ItemListCtrl',
    templateUrl: 'scripts/app/home/param_item/list.tpl.html'
  })
  .when('/home/item/edit/:id', {
    controller: 'ItemEditCtrl',
    templateUrl: 'scripts/app/home/param_item/detail.tpl.html',
    resolve: {
      item: function(Restangular, $route){
        return Restangular.one('items', $route.current.params.id).get();
      }
    }
  })
  .when('/home/item/create', {
    controller: 'ItemCreateCtrl',
    templateUrl: 'scripts/app/home/param_item/detail.tpl.html'
  })
  .otherwise({redirectTo:'/home'});

  RestangularProvider.setRestangularFields({
    id: 'id'
  });

  RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
    if (operation === 'put') {
      elem._id = undefined;
      return elem;
    }
    return elem;
  });
}])

.controller('ItemListCtrl', ['$dialogs', '$timeout', '$rootScope', '$window', '$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function ($dialogs, $timeout, $rootScope, $window, $scope, $route, $location, Restangular, security, i18nNotifications) {

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  var progress = 10;
  var msgs = [
  'Processing...',
  'Done!'
  ];
  var i = 0;
  var fakeProgress = function(){
    $timeout(function(){
      if(progress < 95){
        progress += 5;
        $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': progress});
        fakeProgress();
      }else{
      }
    },1000);
  };

  $scope.create = function() {
    $location.path('home/item/create');
  }

  $scope.canSelect = function() {
    return ($scope.gridOptions.selectedItems.length===0?false:true);
  }

  $scope.select = function () {
    $modalInstance.close($scope.gridOptions.selectedItems[0]);
  };

  $scope.back = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.detail = function(row){
    $window.open('#/home/item/edit/'+row,'_blank');
  }

  $scope.filterOptions = {
    filterText: "",
    useExternalFilter: true
  };
  $scope.totalServerItems = 0;
  $scope.pagingOptions = {
    pageSizes: [20, 50, 100],
    pageSize: 50,
    currentPage: 1
  };  
  $scope.setPagingData = function(data, page, pageSize){  
    var pagedData = data.records;
    $scope.myData = pagedData;
    $scope.totalServerItems = data.total;
    if (!$scope.$$phase) {
      $scope.$apply();
    }
  };

  $scope.getPagedDataAsync = function (pageSize, page, searchText) {
    $dialogs.wait(msgs[0],0);
    fakeProgress();
    setTimeout(function () {
      var custom_get_items = new Array();

      if($route.current.params){
        custom_get_items = $route.current.params;
      }

      custom_get_items.offset = (page - 1) * pageSize;
      custom_get_items.limit = pageSize;

      if (searchText='$scope.filterOptions.filterText') {
        custom_get_items.filter = $scope.filterOptions.filterText;
        custom_get_items.filter_fields = 'id, name';
        Restangular.all("items").getList(custom_get_items).then(function(items) {
          $scope.setPagingData(items,page,pageSize);
          $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
          $rootScope.$broadcast('dialogs.wait.complete');
        });
      }
      else {
        Restangular.all("items").getList(custom_get_items).then(function(items) {
          $scope.setPagingData(items,page,pageSize);
          $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
          $rootScope.$broadcast('dialogs.wait.complete');
        });
      }
    }, 100);
  };

  $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

  $scope.$watch('pagingOptions', function (newVal, oldVal) {
    if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
    }
  }, true);
  var timeoutPromise;
  $scope.$watch('filterOptions', function (newVal, oldVal) {
    $timeout.cancel(timeoutPromise);
    timeoutPromise = $timeout(function(){
      if (newVal !== oldVal) {
        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
      }
    },1000);
  }, true);

  $scope.gridOptions = {
    data: 'myData',
    enablePaging: true,
    showFooter: true,
    multiSelect: false,
    selectedItems: [],
    totalServerItems:'totalServerItems',
    pagingOptions: $scope.pagingOptions,
    filterOptions: $scope.filterOptions,
    enableColumnResize: true,
    columnDefs: [
    { field: 'id', displayName: 'Code', width: 100, cellClass:'text-center', enableCellEdit: true },
    { field: 'name', displayName: 'Name', enableCellEdit: true },
    { field: 'unit', displayName: 'Unit', width: 80, cellClass:'text-center' },
    { field: 'brand', displayName: 'Merek', width: 120, cellClass:'text-center' },
    { field: 'group_name', displayName: 'Item Group', width: 170, enableCellEdit: true },
    { field: 'category_name', displayName: 'Item Category', width: 170, enableCellEdit: true },
    { field: 'subcategory_name', displayName: 'Item Subcategory', width: 170, enableCellEdit: true },
    { field: 'is_active', displayName: 'Active', width: 100, cellClass:'text-center', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ (row.getProperty(col.field)===1?\'Yes\' : \'No\') }}</span></div>'},
    {displayName: 'Action', width: 170, cellClass:'text-center',
    cellTemplate:   '<div class="ngCellText" ng-class="col.colIndex()"><a class="btn btn-xs btn-default" ng-href="" ng-click="detail(row.entity.id)" data-toggle="tooltip" title="edit" data-original-title="Edit Client"><i class="fa fa-pencil"></i></a></div>'}
    ]
  };

}])

.controller('ItemCreateCtrl', ['$rootScope', '$window', '$scope', '$location', 'Restangular', 'security', 'i18nNotifications', '$modal', 'APP_CONFIG', '$upload', '$timeout', '$dialogs', function ($rootScope, $window, $scope, $location, Restangular, security, i18nNotifications, $modal, APP_CONFIG, $upload, $timeout, $dialogs) {

  //debugger;
  var original = {};
  original.is_save = 0;
  original.is_active = true;
  original.weight = 0;
  original.long = 0;
  original.wide = 0;
  original.high = 0;
  original.check_barcode = false;
  original.owner_id = '01';

  $scope.item = Restangular.copy(original);

  Restangular.all("itemgroups").getList({where:"is_active = 1"}).then(function(itemgroups) {
    $scope.itemgroups = itemgroups.records;
  });

  $scope.changeitemgroup = function() {
    Restangular.all("itemcategorys").getList({where:"is_active = 1 and itemgroup_id = "+$scope.item.itemgroup_id}).then(function(itemcategorys) {
      $scope.itemcategorys = itemcategorys.records;
    });
  };

  $scope.changeitemcategory = function() {
    Restangular.all("itemsubcategorys").getList({where:"is_active = 1 and itemgroup_id = "+$scope.item.itemgroup_id+" and itemcategory_id = "+$scope.item.itemcategory_id}).then(function(itemsubcategorys) {
      $scope.itemsubcategorys = itemsubcategorys.records;
    });
  };

  Restangular.all("units").getList({where:"is_active = 1"}).then(function(units) {
    $scope.units = units.records;
  });

  Restangular.all("brands").getList({where:"is_active = 1"}).then(function(brands) {
    $scope.brands = brands.records;
  });

  Restangular.all("owners").getList({where:"is_active = 1"}).then(function(owners) {
    $scope.owners = owners.records;
  });

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  var progress = 10;
  var msgs = [
  'Processing...',
  'Done!'
  ];
  var i = 0;
  var fakeProgress = function(){
    $timeout(function(){
      if(progress < 95){
        progress += 5;
        $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': progress});
        fakeProgress();
      }else{
      }
    },1000);
  };

  $scope.canRemove = function() {
    return false;
  };

  $scope.canSave = function() {
    return $scope.ItemForm.$valid && 
    !angular.equals($scope.item, original);
  };

  $scope.back = function() {
    $window.history.back();
  }

  $scope.save = function() {
    progress = 10;
    i = 0;
    $dialogs.wait(msgs[0],0);
    fakeProgress();

    Restangular.all('items').post($scope.item).then(function(item) {
      i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : item.id});
      $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
      $rootScope.$broadcast('dialogs.wait.complete');
      $location.path('/home/item');
    });
  }

  $scope.canRevert = function() {
    return $scope.ItemForm.$valid && 
    !angular.equals($scope.item, original);
  }

  $scope.revert = function() {
    var original = {};
    original.is_active = true;
    $scope.item = Restangular.copy(original);
    $scope.ItemForm.$setPristine();
  }

}])

.controller('ItemEditCtrl', ['$rootScope', '$window', '$scope', '$location', 'Restangular', 'item', 'i18nNotifications', '$dialogs', 'security', 'simpleDeleteConfirm', '$modal', 'APP_CONFIG', '$upload', '$timeout', function ($rootScope, $window, $scope, $location, Restangular, item, i18nNotifications, $dialogs, security, simpleDeleteConfirm, $modal, APP_CONFIG, $upload, $timeout) {

  //debugger;
  var original = item;

  $scope.item = Restangular.copy(original);

  $scope.item.is_save = 1;
  $scope.item.check_barcode = false;
  $scope.item.name_save = $scope.item.name;

  Restangular.all("itemgroups").getList({where:"is_active = 1"}).then(function(itemgroups) {
    $scope.itemgroups = itemgroups.records;
  });

  Restangular.all("itemcategorys").getList({where:"is_active = 1 and itemgroup_id = "+$scope.item.itemgroup_id}).then(function(itemcategorys) {
    $scope.itemcategorys = itemcategorys.records;
  });

  Restangular.all("itemsubcategorys").getList({where:"is_active = 1 and itemgroup_id = "+$scope.item.itemgroup_id+" and itemcategory_id = "+$scope.item.itemcategory_id}).then(function(itemsubcategorys) {
    $scope.itemsubcategorys = itemsubcategorys.records;
  });

  Restangular.all("units").getList({where:"is_active = 1"}).then(function(units) {
    $scope.units = units.records;
  });

  Restangular.all("brands").getList({where:"is_active = 1"}).then(function(brands) {
    $scope.brands = brands.records;
  });

  Restangular.all("owners").getList({where:"is_active = 1"}).then(function(owners) {
    $scope.owners = owners.records;
  });

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  var progress = 10;
  var msgs = [
  'Processing...',
  'Done!'
  ];
  var i = 0;
  var fakeProgress = function(){
    $timeout(function(){
      if(progress < 95){
        progress += 5;
        $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': progress});
        fakeProgress();
      }else{
      }
    },1000);
  };

  $scope.back = function() {
    $window.history.back();
  }

  $scope.canSave = function() {
    return $scope.ItemForm.$valid && 
    !angular.equals($scope.item, original);
  }
  
  $scope.save = function() {
    $scope.item.put().then(function() {
      i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : $scope.item.id});
      $location.path('/home/item');
    });
  };
  
  $scope.canRemove = function() {
    return true;
  }
  $scope.remove = function() {
    simpleDeleteConfirm.openDialog(original, '/home/item');
  };

  $scope.canRevert = function() {
    return !angular.equals($scope.item, original);
  }
  $scope.revert = function() {
    $scope.item =  Restangular.copy(original);
    $scope.ItemForm.$setPristine();
  }

  $scope.duplicate = function() {
    if($scope.item.name == $scope.item.name_save){
      $dialogs.error('Warning', 'Nama item tidak boleh sama !!!')
    }else{
      var dlg = $dialogs.confirm('Konfirmasi','Apakah Anda ingin yakin menyimpan data ini?');
      dlg.result.then(function(btn){
        progress = 10;
        i = 0;
        $dialogs.wait(msgs[0],0);
        fakeProgress();
        if($scope.item.check_barcode == false){
          $scope.item.barcode = undefined;
        }
        Restangular.all('items').post($scope.item).then(function(item) {
          i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : item.id});
          $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
          $rootScope.$broadcast('dialogs.wait.complete');
          $location.path('/home/item');
        });
      },function(btn){
      });
    }
  };

}]);

