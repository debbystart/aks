angular.module('customer', [
  'shared.dialog',
  'restangular',
  'services.i18nNotifications'
  ])

.config(['$routeProvider', 'RestangularProvider', function ($routeProvider, RestangularProvider) {

  $routeProvider
  .when('/home/customer', {
    controller: 'CustomerListCtrl',
    templateUrl: 'scripts/app/home/param_customer/list.tpl.html'
  })
  .when('/home/customer/edit/:id', {
    controller: 'CustomerEditCtrl',
    templateUrl: 'scripts/app/home/param_customer/detail.tpl.html',
    resolve: {
      item: function(Restangular, $route){
        return Restangular.one('customers', $route.current.params.id).get();
      }
    }
  })
  .when('/home/customer/create', {
    controller: 'CustomerCreateCtrl',
    templateUrl: 'scripts/app/home/param_customer/detail.tpl.html'
  })

  .otherwise({redirectTo:'/home'});
  RestangularProvider.setRestangularFields({
    id: 'id'
  });

  RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
    if (operation === 'put') {
      elem._id = undefined;
      return elem;
    }
    return elem;
  });
  
}])

.controller('CustomerListCtrl', ['$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function ($scope, $route, $location, Restangular, security, i18nNotifications) {

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.create = function() {
    $location.path('home/customer/create');
  }

  $scope.canSelect = function() {
    return ($scope.gridOptions.selectedItems.length===0?false:true);
  }

  $scope.select = function () {
    $modalInstance.close($scope.gridOptions.selectedItems[0]);
  };

  $scope.back = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.detail = function(row){
    $location.path('home/customer/edit/'+row);
  }

  $scope.change_filter = function(){
    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
  }

  $scope.filterOptions = {
    filterText: null,
    useExternalFilter: true
  };
  $scope.totalServerItems = 0;
  $scope.pagingOptions = {
    pageSizes: [20, 50, 100],
    pageSize: 50,
    currentPage: 1
  };  
  $scope.setPagingData = function(data, page, pageSize){  
    var pagedData = data.records;
    $scope.myData = pagedData;
    $scope.totalServerItems = data.total;
    if (!$scope.$$phase) {
      $scope.$apply();
    }
  };
  $scope.getPagedDataAsync = function (pageSize, page, searchText) {
    setTimeout(function () {
      var custom_get = {};

      if($route.current.params)
        custom_get = $route.current.params;

      custom_get.offset = (page - 1) * pageSize; 
      custom_get.limit = pageSize;

      if (searchText='$scope.filterOptions.filterText') {

        custom_get.filter = $scope.filterOptions.filterText;
        custom_get.filter_fields = 'id,name';

        Restangular.all("customers").getList(custom_get).then(function(customers) {
          $scope.setPagingData(customers,page,pageSize);
        });
      } 
      else {
        Restangular.all("customers").getList(custom_get).then(function(customers) {
          $scope.setPagingData(customers,page,pageSize);
        });
      }
    }, 100);
  };

  $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

  $scope.$watch('pagingOptions', function (newVal, oldVal) {
    if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
    }
  }, true);

  $scope.gridOptions = {
    data: 'myData',
    enablePaging: true,
    showFooter: true,
    multiSelect: false,
    selectedItems: [],
    totalServerItems:'totalServerItems',
    pagingOptions: $scope.pagingOptions,
    filterOptions: $scope.filterOptions,
    enableColumnResize: true,
    columnDefs: [
    { field: 'id', displayName: 'Code', cellClass: 'text-center', enableCellEdit:true, width: 120},
    { field: 'name', displayName: 'Nama', enableCellEdit:true, width: 300},
    { field: 'province', displayName: 'Provinsi', width: 195},
    { field: 'city', displayName: 'Kabupaten/Kota', width: 190},
    { field: 'districts', displayName: 'Kecamatan', width: 185},
    { field: 'village', displayName: 'Kelurahan', width: 180},
    { field: 'market', displayName: 'Market', width: 130},
    { field: 'mobile', displayName: 'Telephone', width: 130, cellClass: 'text-right'},
    {displayName: 'Detail', cellClass: 'text-center',
    cellTemplate: 	'<div class="ngCellText" ng-class="col.colIndex()"><a class="btn btn-xs btn-default" ng-href="" ng-click="detail(row.entity.id)" data-toggle="tooltip" title="edit" data-original-title="Edit Client"><i class="fa fa-pencil"></i></a></div>'}

    ]
  };

}])

.controller('CustomerCreateCtrl', ['$window', '$scope', '$location', 'Restangular', 'security', 'i18nNotifications', '$modal', 'APP_CONFIG', '$upload', '$timeout', '$dialogs', function ($window, $scope, $location, Restangular, security, i18nNotifications, $modal, APP_CONFIG, $upload, $timeout, $dialogs) {
  //debugger;
  var original = {};
  original.is_active = true;

  $scope.customer = Restangular.copy(original);

  Restangular.all("regions/province").getList().then(function(regions) {
    $scope.provinces = regions.records;
  });

  Restangular.all("markets").getList({where:"is_active = 1"}).then(function(markets) {
    $scope.markets = markets.records;
  });

  $scope.changeprov = function() {
    Restangular.all("regions/city").getList({where:"param_region.name = '"+$scope.customer.province+"'"}).then(function(regions) {
      $scope.citys = regions.records;
    });
    $scope.customer.city = undefined;
    $scope.customer.districts = undefined;
    $scope.customer.village = undefined;
    $scope.customer.postal = undefined;
  }

  $scope.changecity = function() {
    Restangular.all("regions/districts").getList({where:"param_region.name = '"+$scope.customer.province+"' and param_region.city = '"+$scope.customer.city+"'"}).then(function(regions) {
      $scope.districts = regions.records;
    });
    $scope.customer.districts = undefined;
    $scope.customer.village = undefined;
    $scope.customer.postal = undefined;
  }

  $scope.changedist = function() {
    Restangular.all("regions/village").getList({where:"param_region.name = '"+$scope.customer.province+"' and param_region.city = '"+$scope.customer.city+"' and param_region.districts = '"+$scope.customer.districts+"'"}).then(function(regions) {
      $scope.villages = regions.records;
    });
    $scope.customer.village = undefined;
    $scope.customer.postal = undefined;
  }

  $scope.changepos = function() {
    Restangular.all("regions/postal").getList({where:"param_region.name = '"+$scope.customer.province+"' and param_region.city = '"+$scope.customer.city+"' and param_region.districts = '"+$scope.customer.districts+"' and param_region.village = '"+$scope.customer.village+"'"}).then(function(regions) {
      $scope.postals = regions.records;
      $scope.customer.postal = $scope.postals[0].postal;
    });
    Restangular.all("regions/code").getList({where:"param_region.name = '"+$scope.customer.province+"' and param_region.city = '"+$scope.customer.city+"' and param_region.districts = '"+$scope.customer.districts+"' and param_region.village = '"+$scope.customer.village+"'"}).then(function(regions) {
      $scope.codes = regions.records;
      $scope.customer.code = $scope.codes[0].code;
    });
  }
  
  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.canRemove = function() {
    return false;
  }
  $scope.canSave = function() {
  	return $scope.CustomerForm.$valid && 
    !angular.equals($scope.customer, original);
  };

  $scope.back = function() {
    $window.history.back();
  }

  $scope.save = function() {
    Restangular.all('customers').post($scope.customer).then(function(customer) {
      i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : customer.id});
      $location.path('/home/customer');
    });
  }

  $scope.canRevert = function() {
    return $scope.CustomerForm.$valid && 
    !angular.equals($scope.customer, original);
  }
  $scope.revert = function() {
    $scope.customer =  {};
    $scope.CustomerForm.$setPristine();
  }

}])

.controller('CustomerEditCtrl', ['localizedMessages', '$window', '$scope', '$location', 'Restangular', 'item', 'i18nNotifications', '$dialogs', 'security', 'simpleDeleteConfirm', '$modal', 'APP_CONFIG', '$upload', '$timeout', function (localizedMessages, $window, $scope, $location, Restangular, item, i18nNotifications, $dialogs, security, simpleDeleteConfirm, $modal, APP_CONFIG, $upload, $timeout) {

  //debugger;
  var original = item;
  $scope.customer = Restangular.copy(original);

  Restangular.all("regions/province").getList().then(function(regions) {
    $scope.provinces = regions.records;
  });

  Restangular.all("regions/city").getList({where:"param_region.name = '"+$scope.customer.province+"'"}).then(function(regions) {
    $scope.citys = regions.records;
  });

  Restangular.all("regions/districts").getList({where:"param_region.city = '"+$scope.customer.city+"'"}).then(function(regions) {
    $scope.districts = regions.records;
  });

  Restangular.all("regions/village").getList({where:"param_region.districts = '"+$scope.customer.districts+"'"}).then(function(regions) {
    $scope.villages = regions.records;
  });

  Restangular.all("markets").getList({where:"is_active = 1"}).then(function(markets) {
    $scope.markets = markets.records;
  });

  $scope.changeprov = function() {
    Restangular.all("regions/city").getList({where:"param_region.name = '"+$scope.customer.province+"'"}).then(function(regions) {
      $scope.citys = regions.records;
    });
    $scope.customer.city = undefined;
    $scope.customer.districts = undefined;
    $scope.customer.village = undefined;
    $scope.customer.postal = undefined;
  }

  $scope.changecity = function() {
    Restangular.all("regions/districts").getList({where:"param_region.name = '"+$scope.customer.province+"' and param_region.city = '"+$scope.customer.city+"'"}).then(function(regions) {
      $scope.districts = regions.records;
    });
    $scope.customer.districts = undefined;
    $scope.customer.village = undefined;
    $scope.customer.postal = undefined;
  }

  $scope.changedist = function() {
    Restangular.all("regions/village").getList({where:"param_region.name = '"+$scope.customer.province+"' and param_region.city = '"+$scope.customer.city+"' and param_region.districts = '"+$scope.customer.districts+"'"}).then(function(regions) {
      $scope.villages = regions.records;
    });
    $scope.customer.village = undefined;
    $scope.customer.postal = undefined;
  }

  $scope.changepos = function() {
    Restangular.all("regions/postal").getList({where:"param_region.name = '"+$scope.customer.province+"' and param_region.city = '"+$scope.customer.city+"' and param_region.districts = '"+$scope.customer.districts+"' and param_region.village = '"+$scope.customer.village+"'"}).then(function(regions) {
      $scope.postals = regions.records;
      $scope.customer.postal = $scope.postals[0].postal;
    });
  }

  $scope.item = function() {
    $scope.customer.total = parseInt($scope.customer.price) * parseInt($scope.customer.amount);
  }

  // user permissions
  $scope.canCreate    = security.canCreate;
  $scope.canUpdate    = security.canUpdate;
  $scope.canRead      = security.canRead;
  $scope.canDelete    = security.canDelete;

  $scope.back = function() {
    $window.history.back();
  }

  $scope.canSave = function() {
    return !angular.equals($scope.customer, original);
  }
  
  $scope.save = function() {
    $scope.customer.put().then(function() {
      i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : $scope.customer.id});
      $location.path('/home/customer');
    });
  };

  $scope.canRevert = function() {
    return !angular.equals($scope.customer, original);
  }
  $scope.revert = function() {
    $scope.customer =  Restangular.copy(original);
  }
  
  $scope.canRemove = function() {
    return true;
  }

  $scope.remove = function() {
    simpleDeleteConfirm.openDialog(original, '/home/buy');
  };

}]);

