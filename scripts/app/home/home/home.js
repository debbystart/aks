angular.module('home', [])

.config(['$routeProvider', function ($routeProvider) {
	$routeProvider
	.when('/home', {
		templateUrl: 'scripts/app/home/home/home.tpl.html', 
		controller: 'HomeCtrl'
	});
	
	
}])

.controller('HomeCtrl', ['$scope', '$window', '$location', 'security', 'APP_CONFIG', 'Restangular', 'GeoUTM', function ($scope, $window, $location, security, APP_CONFIG, Restangular, GeoUTM) {

	$scope.custom_get = {};
	$scope.isAuthenticated = security.isAuthenticated;
  	$scope.isAuthorizedRoute = security.isAuthorizedRoute;
	$scope.isAuthorizedRoles  = security.isAuthorizedRoles;
	$scope.canRead = security.canRead;
	$scope.login = security.showLogin;
		
}]);