angular.module('purchaseorder', [
    'shared.dialog',
    'ui.bootstrap.datepicker',
    'ui.bootstrap.typeahead',
    'ui.bootstrap.modal',
    'restangular', 
    'services.i18nNotifications'
    ])

.config(['$routeProvider', 'RestangularProvider', function ($routeProvider, RestangularProvider) {

    $routeProvider
    .when('/home/purchaseorder', {
        controller: 'PurchaseorderListCtrl',
        templateUrl: 'scripts/app/home/data_purchaseorder/list.tpl.html'
    })

    .when('/home/purchaseorder/edit/:id', {
        controller: 'PurchaseorderEditCtrl',
        templateUrl: 'scripts/app/home/data_purchaseorder/detail.tpl.html',
        resolve: {
            item: function(Restangular, $route){
                return Restangular.one('purchaseorders', $route.current.params.id).get({expands: 'purchaseorders_detail'});
            }
        }
    })

    .when('/home/purchaseorder/create', {
        controller: 'PurchaseorderCreateCtrl',
        templateUrl: 'scripts/app/home/data_purchaseorder/create.tpl.html'
    })
    
    .otherwise({redirectTo:'/home'});

    RestangularProvider.setRestangularFields({
        id: 'id'
    });

    RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
        if (operation === 'put') {
            elem._id = undefined;
            return elem;
        }
        return elem;
    });
}])

.controller('PurchaseorderListCtrl', ['$window', '$scope', '$route', 'APP_CONFIG', '$location', 'Restangular', 'security', 'i18nNotifications', function ($window, $scope, $route, APP_CONFIG, $location, Restangular, security, i18nNotifications) {

    // user permissions
    $scope.canCreate    = security.canCreate;
    $scope.canUpdate    = security.canUpdate;
    $scope.canRead      = security.canRead;
    $scope.canDelete    = security.canDelete;

    $scope.create = function() {
        $location.path('home/purchaseorder/create');
    }

    $scope.canSelect = function() {
        return ($scope.gridOptions.selectedItems.length===0?false:true);
    }

    $scope.select = function () {
        $modalInstance.close($scope.gridOptions.selectedItems[0]);
    };

    $scope.back = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.detail = function(row){
        $location.path('home/purchaseorder/edit/'+row);
    };

    $scope.print = function (row) {
        var param = $window.encodeURIComponent('id') + '=' + $window.encodeURIComponent(row.entity.id);
        windowObjectReference = $window.open(APP_CONFIG.appBaseUrl + '/api/purchaseorders/print/po?'+ param, '_blank');
        windowObjectReference.focus();
    };

    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [20, 50, 100],
        pageSize: 50,
        currentPage: 1
    };  
    $scope.setPagingData = function(data, page, pageSize){  
        var pagedData = data.records;
        $scope.myData = pagedData;
        $scope.totalServerItems = data.total;
        if (!$scope.$$phase) {
          $scope.$apply();
      }
        //
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        setTimeout(function () {
          var custom_get = {};

          if($route.current.params)
            custom_get = $route.current.params;

        custom_get.offset = (page - 1) * pageSize; 
        custom_get.limit = pageSize;

        if (searchText='$scope.filterOptions.filterText') {

            custom_get.filter = $scope.filterOptions.filterText;
            custom_get.filter_fields = 'id';

            Restangular.all("purchaseorders").getList(custom_get).then(function(purchaseorders) {
              $scope.setPagingData(purchaseorders,page,pageSize);
          });
        } 
        else {
            Restangular.all("purchaseorders").getList(custom_get).then(function(purchaseorders) {
              $scope.setPagingData(purchaseorders,page,pageSize);
          });
        }
    }, 100);
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
          $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
      }
  }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
          $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
      }
  }, true);

    $scope.gridOptions = {
        data: 'myData',
        enablePaging: true,
        showFooter: true,
        multiSelect: false,
        selectedItems: [],
        totalServerItems:'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        enableColumnResize: true,
        columnDefs: [
        { field: 'is_save', displayName: 'Status', width: 170,
        cellTemplate:   '<div class="ngCellText" style="text-align:center" ng-class="{\'blue\' : row.getProperty(\'is_save\') == 0, \'orange\' : row.getProperty(\'is_save\')==1, \'green\' : row.getProperty(\'is_save\')==2}">'+
        '<span ng-cell-text ng-show="row.getProperty(col.field)==0">Saved</span>'+
        '<span ng-cell-text ng-show="row.getProperty(col.field)==1">Posted</span>'+
        '<span ng-cell-text ng-show="row.getProperty(col.field)==2">Receive</span></div>'},
        { field: 'id', displayName: 'Code', width: 150, cellClass:'text-center', enableCellEdit: true },
        { field: 'supplier', displayName: 'Supplier', width: 450, enableCellEdit: true },
        { field: 'shipping', displayName: 'Shipping', width: 150, enableCellEdit: true },
        { field: 'to_name', displayName: 'Kepada', width: 200, enableCellEdit: true },
        { field: 'total', displayName: 'Total', width: 150, enableCellEdit: true, cellFilter: 'number', cellClass: 'text-right'},
        { field: 'ongkir', displayName: 'Ongkos Kirim', width: 150, enableCellEdit: true, cellFilter: 'number', cellClass: 'text-right'},
        {displayName: 'Action', cellClass:'text-center',
        cellTemplate:   '<div class="ngCellText" ng-class="col.colIndex()"><a class="btn btn-xs btn-default" ng-href="" ng-click="detail(row.entity.id)" data-toggle="tooltip" title="edit" data-original-title="Edit Client"><i class="fa fa-pencil"></i></a> | <a class="btn btn-xs btn-default" ng-href="" ng-click="print(row)" data-toggle="tooltip" title="Export To Pdf" data-original-title="Export To Pdf File"><i class="fa fa-file-pdf-o"></i></a></div>'}
        ]
    };

}])

.controller('PurchaseorderCreateCtrl', ['$timeout', '$rootScope', '$http','$window', '$route', '$scope', '$location', 'Restangular', 'security', 'i18nNotifications', '$modal', '$dialogs', function ($timeout, $rootScope, $http ,$window, $route, $scope, $location, Restangular, security, i18nNotifications, $modal, $dialogs) {

    var original = {};

    original.purchaseorders_detail = [];
    original.districts = '';
    original.village = '';
    original.ongkir = 0;
    original.is_save = 0;
    original.qty = 0;
    original.bruto = 0;
    original.total = 0;
    original.date = new Date();

    $scope.purchaseorder = Restangular.copy(original);

    Restangular.all("markets").getList({where:"is_active = 1"}).then(function(markets) {
        $scope.markets = markets.records;
    });

    Restangular.all("shippings").getList({where:"is_active = 1"}).then(function(shippings) {
        $scope.shippings = shippings.records;
    });

    $scope.$watch(function() {
        return security.currentUser;
    }, function(currentUser) {
        $scope.currentUser = currentUser;
        Restangular.all("companys").getList({where:"is_active = 1 and id = "+$scope.currentUser.company_id}).then(function(companys) {
            $scope.companys = companys.records;
            $scope.purchaseorder.to_name = $scope.companys[0].name;
            $scope.purchaseorder.to_ship = $scope.companys[0].address;
        });
    });

    $scope.format = 'dd-MMM-yyyy';

    $scope.purchaseorder.purchaseorders_detail = original.purchaseorders_detail;

    $scope.detailCellTpl = '<div class="ngCellText" ng-class="col.colIndex()"><a ng-href="" ng-click="removeQuotationItem(row.rowIndex)"><i class="icon-trash"></i></a></div>';

    $scope.gridOptions = {
        data: 'purchaseorder.purchaseorders_detail',
        enablePaging: true,
        showFooter: true,
        multiSelect: false,
        selectedItems: [],
        totalServerItems:'totalServerItems',
        enableColumnResize: true,
        enableCellSelection: true,
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        columnDefs: [
        { displayName: 'Delete', width: 80, cellClass: "text-center", cellTemplate: $scope.detailCellTpl},
        { field: 'item_id', displayName: 'Code', width: 100, cellClass: "text-center"},
        { field: 'item_name', displayName: 'Item'},
        { field: 'item_unit', displayName: 'Unit', width:60, cellClass: "text-center"},
        { field: 'link', displayName: 'Link', width:60, enableCellEdit: true},
        { field: 'item_weight', displayName: 'Berat (gram)', width:100, cellClass: "text-right", cellFilter: 'number'},
        { field: 'total_item_weight', displayName: 'Toral Berat (gram)', width:100, cellClass: "text-right", cellFilter: 'number', visible: false},
        { field: 'qty', displayName: 'Qty*', width: 80, cellClass: "text-right" , enableCellEdit: true, cellFilter: 'number', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-change="calculate()" format="number"/>'},
        { field: 'price', displayName: 'Price*', width: 120, cellClass: "text-right", enableCellEdit: true, cellFilter: 'number', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-change="calculate()" format="number"/>'},
        { field: 'ongkir', displayName: 'Ongkos Kirim' , width: 120, cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'bruto', displayName: 'Bruto' , width: 120, cellClass: "text-right", cellFilter: 'number'},
        { field: 'total_price_item', displayName: 'Total /item' , width: 120, cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'total_price', displayName: 'Total' , width: 120, cellClass: "text-right", cellFilter: 'number:2'}
        ]
    }

    $scope.removeQuotationItem = function(index) {
        $scope.purchaseorder.purchaseorders_detail.splice(index, 1);
        $scope.calculate();
    };

    $scope.change_ongkir = function() {
        for(var i=0; i<$scope.purchaseorder.purchaseorders_detail.length; i++){
            $scope.purchaseorder.purchaseorders_detail[i].qty = 0;
        }
    };

    $scope.calculate = function() {
        $scope.purchaseorder.bruto = 0;
        $scope.purchaseorder.total = 0;
        $scope.purchaseorder.qty = 0;
        $scope.purchaseorder.total_item_weight = 0;
        var x = 0;
        for(var i=0; i<$scope.purchaseorder.purchaseorders_detail.length; i++){
            $scope.purchaseorder.purchaseorders_detail[i].total_item_weight = parseInt($scope.purchaseorder.purchaseorders_detail[i].qty) * parseInt($scope.purchaseorder.purchaseorders_detail[i].item_weight);
            $scope.purchaseorder.purchaseorders_detail[i].bruto = parseInt($scope.purchaseorder.purchaseorders_detail[i].qty) * parseInt($scope.purchaseorder.purchaseorders_detail[i].price);
            $scope.purchaseorder.qty = parseInt($scope.purchaseorder.qty) + parseInt($scope.purchaseorder.purchaseorders_detail[i].qty);
            $scope.purchaseorder.total_item_weight = parseInt($scope.purchaseorder.total_item_weight) + parseInt($scope.purchaseorder.purchaseorders_detail[i].total_item_weight);
        }

        for(var i=0; i<$scope.purchaseorder.purchaseorders_detail.length; i++){
            x = parseFloat($scope.purchaseorder.purchaseorders_detail[i].total_item_weight / $scope.purchaseorder.total_item_weight) * 100;
            $scope.purchaseorder.purchaseorders_detail[i].ongkir = parseFloat($scope.purchaseorder.ongkir) * parseFloat(x / 100);
            $scope.purchaseorder.purchaseorders_detail[i].total_price = parseInt($scope.purchaseorder.purchaseorders_detail[i].bruto) + parseFloat($scope.purchaseorder.purchaseorders_detail[i].ongkir);
            $scope.purchaseorder.purchaseorders_detail[i].total_price_item = parseFloat($scope.purchaseorder.purchaseorders_detail[i].total_price) / parseFloat($scope.purchaseorder.purchaseorders_detail[i].qty);
            $scope.purchaseorder.bruto = parseInt($scope.purchaseorder.bruto) + parseInt($scope.purchaseorder.purchaseorders_detail[i].bruto);
            $scope.purchaseorder.total = parseFloat($scope.purchaseorder.total) + parseFloat($scope.purchaseorder.purchaseorders_detail[i].total_price);
        }
    };

    $scope.supplier = function() {

        var modalInstance = $modal.open({
            templateUrl: 'scripts/app/home/data_purchaseorder/supplier.list.modal.tpl.html',
            controller: ModalInstanceBrowseProjectCtrl,
            resolve: {
                supplier_id: function () {
                    return $scope.purchaseorder;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {

            var customer = Restangular.copy(selectedItem);
            var purchaseorder = Restangular.copy($scope.purchaseorder);

            Restangular.all("regions/province").getList().then(function(regions) {
                $scope.provinces = regions.records;
            });

            Restangular.all("regions/city").getList({where:"param_region.name = '"+customer.province+"'"}).then(function(regions) {
                $scope.citys = regions.records;
            });

            Restangular.all("regions/districts").getList({where:"param_region.city = '"+customer.city+"'"}).then(function(regions) {
                $scope.districts = regions.records;
            });

            Restangular.all("regions/village").getList({where:"param_region.districts = '"+customer.districts+"'"}).then(function(regions) {
                $scope.villages = regions.records;
            });

            purchaseorder.supplier_id = customer.id;
            purchaseorder.supplier_name = customer.name;
            purchaseorder.market_id = customer.market_id;
            purchaseorder.province = customer.province;
            purchaseorder.city = customer.city;
            purchaseorder.districts = customer.districts;
            purchaseorder.village = customer.village;
            purchaseorder.from_ship = customer.address;
            purchaseorder.postal = customer.postal;

            $scope.purchaseorder = purchaseorder;

        }, function () {

        });

    };

    var ModalInstanceBrowseProjectCtrl = function ($scope, $dialog, $modalInstance, supplier_id, Restangular, security, i18nNotifications, $route) {

        $scope.item = supplier_id;

        Restangular.all("markets").getList({where:"is_active = 1"}).then(function(markets) {
            $scope.markets = markets.records;
        });

        // user permissions
        $scope.canCreate    = security.canCreate;
        $scope.canUpdate    = security.canUpdate;
        $scope.canRead      = security.canRead;
        $scope.canDelete    = security.canDelete;
        $scope.isAuthorizedRoute = security.isAuthorizedRoute;

        $scope.change_filter = function(){
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }

        // ng-Grid
        $scope.filterOptions = {
            filterText: "",
            useExternalFilter: true
        };
        $scope.totalServerItems = 0;
        $scope.pagingOptions = {
            pageSizes: [20, 50, 100],
            pageSize: 50,
            currentPage: 1
        };
        $scope.setPagingData = function(data, page, pageSize){
            var pagedData = data.records;
            $scope.myData = pagedData;
            $scope.totalServerItems = data.total;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        };
        $scope.getPagedDataAsync = function (pageSize, page, searchText) {
            setTimeout(function () {
                var custom_get = {};

                if($route.current.params)
                    custom_get = $route.current.params;

                custom_get.offset = (page - 1) * pageSize;
                custom_get.limit = pageSize;

                var where_id = "is_active = 1";

                if($scope.filterOptions.filterText != null){
                    where_id = where_id+ " and (param_supplier.id like '%"+$scope.filterOptions.filterText+"%' or param_supplier.name like '%"+$scope.filterOptions.filterText+"%')";
                }

                if($scope.filterOptions.market_id != undefined){
                    where_id = where_id+ " and param_supplier.market_id = "+$scope.filterOptions.market_id;
                }

                custom_get.where = where_id;

                Restangular.all("suppliers").getList(custom_get).then(function(suppliers) {
                    $scope.setPagingData(suppliers,page,pageSize);
                });

                custom_get.where = undefined;

            }, 100);
        };

        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

        $scope.$watch('pagingOptions', function (newVal, oldVal) {
            if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
            }
        }, true);

        $scope.gridOptions = {
            data: 'myData',
            enablePaging: true,
            showFooter: true,
            multiSelect: false,
            selectedItems: [],
            totalServerItems:'totalServerItems',
            pagingOptions: $scope.pagingOptions,
            filterOptions: $scope.filterOptions,
            columnDefs: [
            { field: 'id', displayName: 'Code', cellClass: 'text-center', enableCellEdit:true, width: 120},
            { field: 'name', displayName: 'Nama', enableCellEdit:true},
            { field: 'province', displayName: 'Provinsi', width: 195},
            { field: 'city', displayName: 'Kabupaten/Kota', width: 190},
            { field: 'districts', displayName: 'Kecamatan', width: 185},
            { field: 'village', displayName: 'Kelurahan', width: 180},
            { field: 'market', displayName: 'Market', width: 130},
            { field: 'mobile', displayName: 'Telephone', width: 130, cellClass: 'text-right'},
            ]
        };

        $scope.canSelect = function() {
            return ($scope.gridOptions.selectedItems.length===0?false:true);
        }

        $scope.select = function () {
            $modalInstance.close($scope.gridOptions.selectedItems[0]);
        };

        $scope.back = function () {
            $modalInstance.dismiss('cancel');
        };

    };
    
    $scope.addItem = function(row) {
        var modalInstance = $modal.open({
            templateUrl: 'scripts/app/home/data_purchaseorder/item.list.modal.tpl.html',
            controller: ModalInstanceAddQuotationItemCreateCtrl,
            resolve: {
                purchaseorder_detail: function(Restangular, $route){
                    return Restangular.copy($scope.purchaseorder);
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {

            $scope.purchaseorder.item_amount = $scope.purchaseorder.item_amount + selectedItem.length;

            for (var i = 0; i < selectedItem.length; i++){
            // reload table
            var purchaseorder_detail            = {};
            purchaseorder_detail.pivot          = {};
            purchaseorder_detail.item_id        = selectedItem[i].id;
            purchaseorder_detail.item_name      = selectedItem[i].name;
            purchaseorder_detail.item_unit      = selectedItem[i].unit;
            purchaseorder_detail.item_weight    = selectedItem[i].weight;
            purchaseorder_detail.qty            = 0;
            purchaseorder_detail.price          = 0;
            purchaseorder_detail.ongkir         = 0;
            purchaseorder_detail.bruto          = 0;
            purchaseorder_detail.total_price    = 0;
            purchaseorder_detail.total_price_item = 0;

            $scope.purchaseorder.purchaseorders_detail.splice($scope.purchaseorder.purchaseorders_detail.length+1,0,purchaseorder_detail);
        }

    }, function () {
    });
    };
    
    var ModalInstanceAddQuotationItemCreateCtrl = function ($scope, $dialog, $modalInstance, purchaseorder_detail, simpleDeleteConfirm, Restangular, security, i18nNotifications, $upload, APP_CONFIG) {

        var original = angular.copy(purchaseorder_detail);
        
        $scope.purchaseorder_detail = original;

        $scope.$watch(function() {
            return security.currentUser;
        }, function(currentUser) {
            $scope.currentUser = currentUser;
        });

        $scope.filterOptions = {
            filterText: "",
            useExternalFilter: true
        };
        $scope.totalServerItems = 0;
        $scope.pagingOptions = {
            pageSizes: [100, 150, 200],
            pageSize: 100,
            currentPage: 1
        };
        $scope.setPagingData = function(data, page, pageSize){
            var pagedData = data.records;
            $scope.myData = pagedData;
            $scope.totalServerItems = data.total;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        };

        $scope.getPagedDataAsync = function (pageSize, page, searchText) {
            $dialogs.wait(msgs[0],0);
            fakeProgress();
            setTimeout(function () {
              var custom_get_items = new Array();

              if($route.current.params){
                custom_get_items = $route.current.params;
            }

            custom_get_items.offset = (page - 1) * pageSize;
            custom_get_items.limit = pageSize;

            if (searchText='$scope.filterOptions.filterText') {
                custom_get_items.filter = $scope.filterOptions.filterText;
                custom_get_items.filter_fields = 'param_item.id, param_item.name';
                Restangular.all("items/po").getList(custom_get_items).then(function(items) {
                  $scope.setPagingData(items,page,pageSize);
                  $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
                  $rootScope.$broadcast('dialogs.wait.complete');
              });
            }
            else {
                Restangular.all("items/po").getList(custom_get_items).then(function(items) {
                  $scope.setPagingData(items,page,pageSize);
                  $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
                  $rootScope.$broadcast('dialogs.wait.complete');
              });
            }
        }, 100);
        };

        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

        $scope.$watch('pagingOptions', function (newVal, oldVal) {
            if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
            }
        }, true);
        var timeoutPromise;
        $scope.$watch('filterOptions', function (newVal, oldVal) {
            $timeout.cancel(timeoutPromise);
            timeoutPromise = $timeout(function(){
              if (newVal !== oldVal) {
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
            }
        },1000);
        }, true);

        $scope.gridOptions = {
            data: 'myData',
            enablePaging: true,
            showFooter: true,
            multiSelect: true,
            selectedItems: [],
            enableColumnResize: true,
            totalServerItems:'totalServerItems',
            pagingOptions: $scope.pagingOptions,
            filterOptions: $scope.filterOptions,
            showSelectionCheckbox: true,
            checkboxHeaderTemplate: '<input class="ngSelectionHeader" type="checkbox" ng-model="allSelected" ng-change="toggleSelectAll(allSelected, true)"/>',
            columnDefs: [
            { field: 'id', displayName: 'Code', width: 130, cellClass:'text-center', enableCellEdit: true },
            { field: 'name', displayName: 'Name', enableCellEdit: true },
            { field: 'unit', displayName: 'Unit', width: 100, cellClass:'text-center' },
            { field: 'qty', displayName: 'Stok', width: 100, cellClass:'text-right' },
            { field: 'brand', displayName: 'Merek', width: 150, cellClass:'text-center' },
            { field: 'group_name', displayName: 'Item Group', width: 240, enableCellEdit: true },
            { field: 'category_name', displayName: 'Item Category', width: 200, enableCellEdit: true },
            { field: 'subcategory_name', displayName: 'Item Subcategory', width: 190, enableCellEdit: true }
            ]
        };

        $scope.canSelect = function() {
            return ($scope.gridOptions.selectedItems.length===0?false:true);
        }

        $scope.select = function () {
            var selectedItem = $scope.gridOptions.selectedItems;
            var isExist = false;
            for(var j = 0; j <$scope.purchaseorder_detail.purchaseorders_detail.length; j++){
                for(var i = 0; i <  selectedItem.length; i++){
                    var as = selectedItem[i];
                    if(as.id==$scope.purchaseorder_detail.purchaseorders_detail[j].item_id){
                        selectedItem.splice(i, 1);
                    }
                }
            }

            for(var i = 0; i <  selectedItem.length; i++){
                var as = selectedItem[i];
                for(var j=0; j <  selectedItem.length; j++){
                    var sa = selectedItem[j];
                    if(i != j){
                        if(as.id==sa.id){
                            selectedItem.splice(j, 1);
                        }
                    }
                }
            }
            $modalInstance.close(selectedItem);
        };

        $scope.back = function () {
            $modalInstance.dismiss('cancel');
        };
    };

    $scope.canCreate    = security.canCreate;
    $scope.canUpdate    = security.canUpdate;
    $scope.canRead      = security.canRead;
    $scope.canDelete    = security.canDelete;

    var progress = 10;
    var msgs = [
    'Processing...',
    'Done!'
    ];
    var i = 0;    
    var fakeProgress = function(){
        $timeout(function(){
            if(progress < 95){
                progress += 5;
                $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': progress});
                fakeProgress();
            }else{
            }
        },1000);
    };

    $scope.canRemove = function() {
        return false;
    }

    $scope.revertprtype = function() {
        $scope.myData = original.purchaseorders_detail= [];
    }

    $scope.back = function() {
        $window.history.back();
    }
    $scope.canSave = function() {
        return $scope.PurchaseorderForm1.$valid && $scope.PurchaseorderForm2.$valid &&
        !angular.equals($scope.purchaseorder, original);
    };

    $scope.canRevert = function() {
        return !angular.equals($scope.purchaseorder, original);
    }
    $scope.revert = function() {
        $scope.purchaseorder =  Restangular.copy(original);
        $scope.purchaseorderForm.$setPristine();
    }

    $scope.save = function() { 
        for(var i=0; i<$scope.purchaseorder.purchaseorders_detail.length; i++){
            if($scope.purchaseorder.purchaseorders_detail[i].qty == undefined || $scope.purchaseorder.purchaseorders_detail[i].qty == '' || $scope.purchaseorder.purchaseorders_detail[i].qty == 0){
                var x = 1;
            }else if($scope.purchaseorder.purchaseorders_detail[i].price == undefined || $scope.purchaseorder.purchaseorders_detail[i].price == '' || $scope.purchaseorder.purchaseorders_detail[i].price == 0){
                var x = 2;
            }
        }

        if(x==1){
            $dialogs.error('Warning', 'Nilai Qty tidak boleh kosong !!!')
        }else if(x==2){
            $dialogs.error('Warning', 'Nilai Price tidak boleh kosong !!!')
        }else{
            progress = 10;
            i = 0;
            $dialogs.wait(msgs[0],0);
            fakeProgress();

            $scope.purchaseorder.date = moment($scope.purchaseorder.date).format('YYYY-MM-DD');

            Restangular.all('purchaseorders').post($scope.purchaseorder).then(function(purchaseorder) {
              i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : purchaseorder.id});
              $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
              $rootScope.$broadcast('dialogs.wait.complete');
              $location.path('/home/purchaseorder');
          });
        }
    };

}])

.controller('PurchaseorderEditCtrl', ['APP_CONFIG','$timeout', '$rootScope', '$http', '$window', '$scope', '$route', '$location', 'Restangular', 'item', 'i18nNotifications', '$dialog', 'security', 'simpleDeleteConfirm', '$modal', '$dialogs', function (APP_CONFIG,$timeout, $rootScope, $http, $window, $scope, $route, $location, Restangular, item, i18nNotifications, $dialog, security, simpleDeleteConfirm, $modal, $dialogs) {

    //debugger;
    var original = item;

    $scope.purchaseorder = Restangular.copy(original);

    Restangular.all("markets").getList({where:"is_active = 1"}).then(function(markets) {
        $scope.markets = markets.records;
    });

    Restangular.all("shippings").getList({where:"is_active = 1"}).then(function(shippings) {
        $scope.shippings = shippings.records;
    });

    Restangular.all("regions/province").getList().then(function(regions) {
        $scope.provinces = regions.records;
    });

    Restangular.all("regions/city").getList({where:"param_region.name = '"+$scope.purchaseorder.province+"'"}).then(function(regions) {
        $scope.citys = regions.records;
    });

    Restangular.all("regions/districts").getList({where:"param_region.city = '"+$scope.purchaseorder.city+"'"}).then(function(regions) {
        $scope.districts = regions.records;
    });

    Restangular.all("regions/village").getList({where:"param_region.districts = '"+$scope.purchaseorder.districts+"'"}).then(function(regions) {
        $scope.villages = regions.records;
    });

    $scope.$watch(function() {
        return security.currentUser;
    }, function(currentUser) {
        $scope.currentUser = currentUser;
    });

    $scope.format = 'dd-MMM-yyyy';

    $scope.purchaseorder.purchaseorders_detail = original.purchaseorders_detail;

    $scope.detailCellTpl = '<div class="ngCellText" ng-class="col.colIndex()"><a ng-href="" ng-click="removeQuotationItem(row.rowIndex, row)"><i class="icon-trash"></i></a></div>';

    $scope.gridOptions = {
        data: 'purchaseorder.purchaseorders_detail',
        enablePaging: true,
        showFooter: true,
        multiSelect: false,
        selectedItems: [],
        totalServerItems:'totalServerItems',
        enableColumnResize: true,
        enableCellSelection: true,
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        columnDefs: [
        { displayName: 'Delete', width: 80, cellClass: "text-center", cellTemplate: $scope.detailCellTpl},
        { field: 'item_id', displayName: 'Code', width: 100, cellClass: "text-center"},
        { field: 'item_name', displayName: 'Item'},
        { field: 'item_unit', displayName: 'Unit', width:60, cellClass: "text-center"},
        { field: 'link', displayName: 'Link', width:60, enableCellEdit: true},
        { field: 'item_weight', displayName: 'Berat (gram)', width:100, cellClass: "text-right", cellFilter: 'number'},
        { field: 'total_item_weight', displayName: 'Toral Berat (gram)', width:100, cellClass: "text-right", cellFilter: 'number', visible: false},
        { field: 'qty', displayName: 'Qty*', width: 80, cellClass: "text-right" , enableCellEdit: true, cellFilter: 'number', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-change="calculate()" format="number"/>'},
        { field: 'price', displayName: 'Price*', width: 120, cellClass: "text-right", enableCellEdit: true, cellFilter: 'number', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-change="calculate()" format="number"/>'},
        { field: 'ongkir', displayName: 'Ongkos Kirim' , width: 120, cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'bruto', displayName: 'Bruto' , width: 120, cellClass: "text-right", cellFilter: 'number'},
        { field: 'total_price_item', displayName: 'Total /item' , width: 120, cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'total_price', displayName: 'Total' , width: 120, cellClass: "text-right", cellFilter: 'number:2'}
        ]
    }

    $scope.purchaseorder.detail_delete = new Array();
    $scope.removeQuotationItem = function(index, row) {
        $scope.purchaseorder.purchaseorders_detail.splice(index, 1);
        $scope.purchaseorder.detail_delete.push(row.entity.id);
        $scope.calculate();
    };

    $scope.change_ongkir = function() {
        for(var i=0; i<$scope.purchaseorder.purchaseorders_detail.length; i++){
            $scope.purchaseorder.purchaseorders_detail[i].qty = 0;
        }
    };

    $scope.calculate = function() {
        $scope.purchaseorder.bruto = 0;
        $scope.purchaseorder.total = 0;
        $scope.purchaseorder.qty = 0;
        $scope.purchaseorder.total_item_weight = 0;
        var x = 0;
        for(var i=0; i<$scope.purchaseorder.purchaseorders_detail.length; i++){
            $scope.purchaseorder.purchaseorders_detail[i].total_item_weight = parseInt($scope.purchaseorder.purchaseorders_detail[i].qty) * parseInt($scope.purchaseorder.purchaseorders_detail[i].item_weight);
            $scope.purchaseorder.purchaseorders_detail[i].bruto = parseInt($scope.purchaseorder.purchaseorders_detail[i].qty) * parseInt($scope.purchaseorder.purchaseorders_detail[i].price);
            $scope.purchaseorder.qty = parseInt($scope.purchaseorder.qty) + parseInt($scope.purchaseorder.purchaseorders_detail[i].qty);
            $scope.purchaseorder.total_item_weight = parseInt($scope.purchaseorder.total_item_weight) + parseInt($scope.purchaseorder.purchaseorders_detail[i].total_item_weight);
        }

        for(var i=0; i<$scope.purchaseorder.purchaseorders_detail.length; i++){
            x = parseFloat($scope.purchaseorder.purchaseorders_detail[i].total_item_weight / $scope.purchaseorder.total_item_weight) * 100;
            $scope.purchaseorder.purchaseorders_detail[i].ongkir = parseFloat($scope.purchaseorder.ongkir) * parseFloat(x / 100);
            $scope.purchaseorder.purchaseorders_detail[i].total_price = parseInt($scope.purchaseorder.purchaseorders_detail[i].bruto) + parseFloat($scope.purchaseorder.purchaseorders_detail[i].ongkir);
            $scope.purchaseorder.purchaseorders_detail[i].total_price_item = parseFloat($scope.purchaseorder.purchaseorders_detail[i].total_price) / parseFloat($scope.purchaseorder.purchaseorders_detail[i].qty);
            $scope.purchaseorder.bruto = parseInt($scope.purchaseorder.bruto) + parseInt($scope.purchaseorder.purchaseorders_detail[i].bruto);
            $scope.purchaseorder.total = parseFloat($scope.purchaseorder.total) + parseFloat($scope.purchaseorder.purchaseorders_detail[i].total_price);
        }
    };

    $scope.supplier = function() {

        var modalInstance = $modal.open({
            templateUrl: 'scripts/app/home/data_purchaseorder/supplier.list.modal.tpl.html',
            controller: ModalInstanceBrowseProjectCtrl,
            resolve: {
                supplier_id: function () {
                    return $scope.purchaseorder;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {

            var customer = Restangular.copy(selectedItem);
            var purchaseorder = Restangular.copy($scope.purchaseorder);

            Restangular.all("regions/province").getList().then(function(regions) {
                $scope.provinces = regions.records;
            });

            Restangular.all("regions/city").getList({where:"param_region.name = '"+customer.province+"'"}).then(function(regions) {
                $scope.citys = regions.records;
            });

            Restangular.all("regions/districts").getList({where:"param_region.city = '"+customer.city+"'"}).then(function(regions) {
                $scope.districts = regions.records;
            });

            Restangular.all("regions/village").getList({where:"param_region.districts = '"+customer.districts+"'"}).then(function(regions) {
                $scope.villages = regions.records;
            });

            purchaseorder.supplier_id = customer.id;
            purchaseorder.supplier = customer.name;
            purchaseorder.market_id = customer.market_id;
            purchaseorder.province = customer.province;
            purchaseorder.city = customer.city;
            purchaseorder.districts = customer.districts;
            purchaseorder.village = customer.village;
            purchaseorder.from_ship = customer.address;
            purchaseorder.postal = customer.postal;

            $scope.purchaseorder = purchaseorder;

        }, function () {

        });

    };

    var ModalInstanceBrowseProjectCtrl = function ($scope, $dialog, $modalInstance, supplier_id, Restangular, security, i18nNotifications, $route) {

        $scope.item = supplier_id;


        // user permissions
        $scope.canCreate    = security.canCreate;
        $scope.canUpdate    = security.canUpdate;
        $scope.canRead      = security.canRead;
        $scope.canDelete    = security.canDelete;
        $scope.isAuthorizedRoute = security.isAuthorizedRoute;

        $scope.change_filter = function(){
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }

        // ng-Grid
        $scope.filterOptions = {
            filterText: "",
            useExternalFilter: true
        };
        $scope.totalServerItems = 0;
        $scope.pagingOptions = {
            pageSizes: [20, 50, 100],
            pageSize: 50,
            currentPage: 1
        };
        $scope.setPagingData = function(data, page, pageSize){
            var pagedData = data.records;
            $scope.myData = pagedData;
            $scope.totalServerItems = data.total;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        };
        $scope.getPagedDataAsync = function (pageSize, page, searchText) {
            setTimeout(function () {
                var custom_get = {};

                if($route.current.params)
                    custom_get = $route.current.params;

                custom_get.offset = (page - 1) * pageSize;
                custom_get.limit = pageSize;

                if (searchText='$scope.filterOptions.filterText') {

                    custom_get.filter = $scope.filterOptions.filterText;
                    custom_get.filter_fields = 'id, name';

                    Restangular.all("suppliers").getList(custom_get).then(function(suppliers) {
                        $scope.setPagingData(suppliers,page,pageSize);
                    });
                }
                else {
                    Restangular.all("suppliers").getList(custom_get).then(function(suppliers) {
                        $scope.setPagingData(suppliers,page,pageSize);
                    });
                }
            }, 100);
        };

        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

        $scope.$watch('pagingOptions', function (newVal, oldVal) {
            if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
            }
        }, true);

        $scope.gridOptions = {
            data: 'myData',
            enablePaging: true,
            showFooter: true,
            multiSelect: false,
            selectedItems: [],
            totalServerItems:'totalServerItems',
            pagingOptions: $scope.pagingOptions,
            filterOptions: $scope.filterOptions,
            columnDefs: [
            { field: 'id', displayName: 'Code', cellClass: 'text-center', enableCellEdit:true, width: 120},
            { field: 'name', displayName: 'Nama', enableCellEdit:true},
            { field: 'province', displayName: 'Provinsi', width: 195},
            { field: 'city', displayName: 'Kabupaten/Kota', width: 190},
            { field: 'districts', displayName: 'Kecamatan', width: 185},
            { field: 'village', displayName: 'Kelurahan', width: 180},
            { field: 'market', displayName: 'Market', width: 130},
            { field: 'mobile', displayName: 'Telephone', width: 130, cellClass: 'text-right'},
            ]
        };

        $scope.canSelect = function() {
            return ($scope.gridOptions.selectedItems.length===0?false:true);
        }

        $scope.select = function () {
            $modalInstance.close($scope.gridOptions.selectedItems[0]);
        };

        $scope.back = function () {
            $modalInstance.dismiss('cancel');
        };

    };
    
    $scope.addItem = function(row) {
        var modalInstance = $modal.open({
            templateUrl: 'scripts/app/home/data_purchaseorder/item.list.modal.tpl.html',
            controller: ModalInstanceAddQuotationItemCreateCtrl,
            resolve: {
                purchaseorder_detail: function(Restangular, $route){
                    return Restangular.copy($scope.purchaseorder);
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {

            $scope.purchaseorder.item_amount = $scope.purchaseorder.item_amount + selectedItem.length;

            for (var i = 0; i < selectedItem.length; i++){
            // reload table
            var purchaseorder_detail            = {};
            purchaseorder_detail.pivot          = {};
            purchaseorder_detail.item_id        = selectedItem[i].id;
            purchaseorder_detail.item_name      = selectedItem[i].name;
            purchaseorder_detail.item_unit      = selectedItem[i].unit;
            purchaseorder_detail.item_weight    = selectedItem[i].weight;
            purchaseorder_detail.qty            = 0;
            purchaseorder_detail.price          = 0;
            purchaseorder_detail.ongkir         = 0;
            purchaseorder_detail.bruto          = 0;
            purchaseorder_detail.total_price    = 0;
            purchaseorder_detail.total_price_item = 0;

            $scope.purchaseorder.purchaseorders_detail.splice($scope.purchaseorder.purchaseorders_detail.length+1,0,purchaseorder_detail);
        }

    }, function () {
    });
    };
    
    var ModalInstanceAddQuotationItemCreateCtrl = function ($scope, $dialog, $modalInstance, purchaseorder_detail, simpleDeleteConfirm, Restangular, security, i18nNotifications, $upload, APP_CONFIG) {

        var original = angular.copy(purchaseorder_detail);
        
        $scope.purchaseorder_detail = original;

        $scope.$watch(function() {
            return security.currentUser;
        }, function(currentUser) {
            $scope.currentUser = currentUser;
        });

        $scope.filterOptions = {
            filterText: "",
            useExternalFilter: true
        };
        $scope.totalServerItems = 0;
        $scope.pagingOptions = {
            pageSizes: [100, 150, 200],
            pageSize: 100,
            currentPage: 1
        };
        $scope.setPagingData = function(data, page, pageSize){
            var pagedData = data.records;
            $scope.myData = pagedData;
            $scope.totalServerItems = data.total;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        };
        $scope.getPagedDataAsync = function (pageSize, page, searchText) {
            $dialogs.wait(msgs[0],0);
            fakeProgress();
            setTimeout(function () {
              var custom_get_items = new Array();

              if($route.current.params){
                custom_get_items = $route.current.params;
            }

            custom_get_items.offset = (page - 1) * pageSize;
            custom_get_items.limit = pageSize;
            
            if (searchText='$scope.filterOptions.filterText') {
                custom_get_items.filter = $scope.filterOptions.filterText;
                custom_get_items.filter_fields = 'param_item.id, param_item.name';
                Restangular.all("items/po").getList(custom_get_items).then(function(items) {
                  $scope.setPagingData(items,page,pageSize);
                  $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
                  $rootScope.$broadcast('dialogs.wait.complete');
              });
            }
            else {
                Restangular.all("items/po").getList(custom_get_items).then(function(items) {
                  $scope.setPagingData(items,page,pageSize);
                  $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
                  $rootScope.$broadcast('dialogs.wait.complete');
              });
            }
        }, 100);
        };

        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

        $scope.$watch('pagingOptions', function (newVal, oldVal) {
            if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
            }
        }, true);
        var timeoutPromise;
        $scope.$watch('filterOptions', function (newVal, oldVal) {
            $timeout.cancel(timeoutPromise);
            timeoutPromise = $timeout(function(){
              if (newVal !== oldVal) {
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
            }
        },1000);
        }, true);


        $scope.gridOptions = {
            data: 'myData',
            enablePaging: true,
            showFooter: true,
            multiSelect: true,
            selectedItems: [],
            enableColumnResize: true,
            totalServerItems:'totalServerItems',
            pagingOptions: $scope.pagingOptions,
            filterOptions: $scope.filterOptions,
            showSelectionCheckbox: true,
            checkboxHeaderTemplate: '<input class="ngSelectionHeader" type="checkbox" ng-model="allSelected" ng-change="toggleSelectAll(allSelected, true)"/>',
            columnDefs: [
            { field: 'id', displayName: 'Code', width: 130, cellClass:'text-center', enableCellEdit: true },
            { field: 'name', displayName: 'Name', enableCellEdit: true },
            { field: 'unit', displayName: 'Unit', width: 100, cellClass:'text-center' },
            { field: 'qty', displayName: 'Stok', width: 100, cellClass:'text-right' },
            { field: 'brand', displayName: 'Merek', width: 150, cellClass:'text-center' },
            { field: 'group_name', displayName: 'Item Group', width: 240, enableCellEdit: true },
            { field: 'category_name', displayName: 'Item Category', width: 200, enableCellEdit: true },
            { field: 'subcategory_name', displayName: 'Item Subcategory', width: 190, enableCellEdit: true }
            ]
        };

        $scope.canSelect = function() {
            return ($scope.gridOptions.selectedItems.length===0?false:true);
        }

        $scope.select = function () {
            var selectedItem = $scope.gridOptions.selectedItems;
            var isExist = false;
            for(var j = 0; j <$scope.purchaseorder_detail.purchaseorders_detail.length; j++){
                for(var i = 0; i <  selectedItem.length; i++){
                    var as = selectedItem[i];
                    if(as.id==$scope.purchaseorder_detail.purchaseorders_detail[j].item_id){
                        selectedItem.splice(i, 1);
                    }
                }
            }

            for(var i = 0; i <  selectedItem.length; i++){
                var as = selectedItem[i];
                for(var j=0; j <  selectedItem.length; j++){
                    var sa = selectedItem[j];
                    if(i != j){
                        if(as.id==sa.id){
                            selectedItem.splice(j, 1);
                        }
                    }
                }
            }
            $modalInstance.close(selectedItem);
        };

        $scope.back = function () {
            $modalInstance.dismiss('cancel');
        };
    };

    $scope.canCreate    = security.canCreate;
    $scope.canUpdate    = security.canUpdate;
    $scope.canRead      = security.canRead;
    $scope.canDelete    = security.canDelete;

    var progress = 10;
    var msgs = [
    'Processing...',
    'Done!'
    ];
    var i = 0;    
    var fakeProgress = function(){
        $timeout(function(){
            if(progress < 95){
                progress += 5;
                $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': progress});
                fakeProgress();
            }else{
            }
        },1000);
    };

    $scope.canRemove = function() {
        return false;
    }

    $scope.back = function() {
        $window.history.back();
    }
    $scope.canSave = function() {
        return $scope.PurchaseorderForm1.$valid && $scope.PurchaseorderForm2.$valid &&
        !angular.equals($scope.purchaseorder, original);
    };

    $scope.canRevert = function() {
        return !angular.equals($scope.purchaseorder, original);
    }
    $scope.revert = function() {
        $scope.purchaseorder =  Restangular.copy(original);
        $scope.purchaseorderForm.$setPristine();
    }

    $scope.save = function(){
        $scope.purchaseorder.is_save = 0;
        $scope.saved();
    };

    $scope.post = function(){
        var dlg = $dialogs.confirm('Konfirmasi','Apakah Anda ingin yakin memposting data ini?');
        dlg.result.then(function(btn){
            $scope.purchaseorder.is_save = 1;
            $scope.saved();
        },function(btn){
        });
    };

    $scope.saved = function() { 
        for(var i=0; i<$scope.purchaseorder.purchaseorders_detail.length; i++){
            if($scope.purchaseorder.purchaseorders_detail[i].qty == undefined || $scope.purchaseorder.purchaseorders_detail[i].qty == '' || $scope.purchaseorder.purchaseorders_detail[i].qty == 0){
                var x = 1;
            }else if($scope.purchaseorder.purchaseorders_detail[i].price == undefined || $scope.purchaseorder.purchaseorders_detail[i].price == '' || $scope.purchaseorder.purchaseorders_detail[i].price == 0){
                var x = 2;
            }
        }

        if(x==1){
            $dialogs.error('Warning', 'Nilai Qty tidak boleh kosong !!!')
        }else if(x==2){
            $dialogs.error('Warning', 'Nilai Price tidak boleh kosong !!!')
        }else{
            progress = 10;
            i = 0;
            $dialogs.wait(msgs[0],0);
            fakeProgress();

            $scope.purchaseorder.date = moment($scope.purchaseorder.date).format('YYYY-MM-DD');

            $scope.purchaseorder.put().then(function() {
              i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : $scope.purchaseorder.id});
              $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
              $rootScope.$broadcast('dialogs.wait.complete');
              $location.path('/home/purchaseorder');
          });
        }
    };

}]);

