angular.module('salesorder', [
    'shared.dialog',
    'ui.bootstrap.datepicker',
    'ui.bootstrap.typeahead',
    'ui.bootstrap.modal',
    'restangular',
    'services.i18nNotifications'
    ])

.config(['$routeProvider', 'RestangularProvider', function ($routeProvider, RestangularProvider) {

    $routeProvider
    .when('/home/salesorder', {
        controller: 'SalesordertListCtrl',
        templateUrl: 'scripts/app/home/data_salesorder/list.tpl.html'
    })

    .when('/home/salesorder/edit/:id', {
        controller: 'SalesordertEditCtrl',
        templateUrl: 'scripts/app/home/data_salesorder/detail.tpl.html',
        resolve: {
            item: function(Restangular, $route){
                return Restangular.one('salesorders', $route.current.params.id).get({expands: 'salesorders_detail'});
            }
        }
    })

    .when('/home/salesorder/create', {
        controller: 'SalesordertCreateCtrl',
        templateUrl: 'scripts/app/home/data_salesorder/create.tpl.html'
    })

    .otherwise({redirectTo:'/home'});

    RestangularProvider.setRestangularFields({
        id: 'id'
    });

    RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
        if (operation === 'put') {
            elem._id = undefined;
            return elem;
        }
        return elem;
    });
}])

.controller('SalesordertListCtrl', ['$window','$filter','APP_CONFIG','$modal','$scope', '$route', '$location', 'Restangular', 'security', 'i18nNotifications', function ($window,$filter,APP_CONFIG,$modal,$scope, $route, $location, Restangular, security, i18nNotifications) {

    $scope.format = 'dd/MM/yyyy';

    // user permissions
    $scope.canCreate    = security.canCreate;
    $scope.canUpdate    = security.canUpdate;
    $scope.canRead      = security.canRead;
    $scope.canDelete    = security.canDelete;
    $scope.isAuthorizedRoute = security.isAuthorizedRoute;

    $scope.$watch(function() {
        return security.currentUser;
    }, function(currentUser) {
        $scope.currentUser = currentUser;
    });

    $scope.create = function() {
        $location.path('/home/salesorder/create');
    }

    $scope.canSelect = function() {
        return ($scope.gridOptions.selectedItems.length===0?false:true);
    }

    $scope.select = function () {
        $modalInstance.close($scope.gridOptions.selectedItems[0]);
    };

    $scope.back = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.detail = function(row){
        $location.path('/home/salesorder/edit/'+row);
    }

    $scope.change_filter = function(){
        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    }

    $scope.print = function (row) {
        var param = $window.encodeURIComponent('id') + '=' + $window.encodeURIComponent(row.entity.id);
        windowObjectReference = $window.open(APP_CONFIG.appBaseUrl + '/api/salesorders/print/so?'+ param, '_blank');
        windowObjectReference.focus();
    };

    $scope.filterOptions = {
        filterText: null,
        filterTextFrom: null,
        filterTextTo: null,
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [20, 25, 50],
        pageSize: 20,
        currentPage: 1
    };
    $scope.setPagingData = function(data, page, pageSize){
        var pagedData = data.records;

        var oneDay = 24*60*60*1000;

        for(var i=0; i<pagedData.length; i++){
            var firstDate = new Date();
            var secondDate = new Date(pagedData[i].expired_selling_date);
            if(pagedData[i].is_save != 2){
                pagedData[i].range_date = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)))+" hari";
            }else{
                pagedData[i].range_date = "Delivery";
            }
        }

        $scope.myData = pagedData;
        $scope.totalServerItems = data.total;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText, fromText, toText) {
        setTimeout(function () {
            var custom_get = {};

            if($route.current.params)
                custom_get = $route.current.params;
            custom_get.offset = (page - 1) * pageSize;
            custom_get.limit = pageSize;

            if (searchText='$scope.filterOptions.filterText') {

                custom_get.filter = $scope.filterOptions.filterText;
                custom_get.filter_fields = 'id, customer_id, selling';

                Restangular.all("salesorders").getList(custom_get).then(function(salesorders) {
                  $scope.setPagingData(salesorders,page,pageSize);
              });
            }
            else {
                Restangular.all("salesorders").getList(custom_get).then(function(salesorders) {
                  $scope.setPagingData(salesorders,page,pageSize);
              });
            }

        }, 100);
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.gridOptions = {
        data: 'myData',
        enablePaging: true,
        showFooter: true,
        multiSelect: false,
        selectedItems: [],
        totalServerItems:'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        enableColumnResize: true,
        enableCellSelection: true,
        enableCellEdit:true,
        columnDefs:
        [
        { field: 'is_save', displayName: 'Status', width: 170,
        cellTemplate:   '<div class="ngCellText" style="text-align:center" ng-class="{\'blue\' : row.getProperty(\'is_save\') == 0, \'green\' : row.getProperty(\'is_save\') == 2, \'orange\' : row.getProperty(\'is_save\')==1, \'yellow\' : row.getProperty(\'is_save\') == 3, \'red\' : row.getProperty(\'is_save\') == 4}">'+
        '<span ng-cell-text ng-show="row.getProperty(col.field)==0">Saved</span>'+
        '<span ng-cell-text ng-show="row.getProperty(col.field)==1">Posted</span>'+
        '<span ng-cell-text ng-show="row.getProperty(col.field)==2">Send</span>'+
        '<span ng-cell-text ng-show="row.getProperty(col.field)==3">In Progress</span>'+
        '<span ng-cell-text ng-show="row.getProperty(col.field)==4">Close</span></div>'},
        { field: 'id', displayName: 'Nomor SO', width: 120, cellClass:"text-center"},
        { field: 'selling', displayName: 'Selling', width: 150},
        { field: 'customer_id', displayName: 'Cust Code', width: 100, cellClass:"text-center"},
        { field: 'customer', displayName: 'Customer', width: 230},
        { field: 'booking', displayName: 'Kode Booking', width: 180},
        { field: 'selling_date',  cellFilter: 'date:\'dd-MMM-yyyy\'', displayName: 'Tanggal Penjualan', cellClass:"text-center", width: 140},
        { field: 'expired_selling_date',  cellFilter: 'date:\'dd-MMM-yyyy\'', displayName: 'Akhir Penjualan', cellClass:"text-center", width: 140},
        { field: 'range_date', displayName: 'Sisa Waktu', width: 100, cellClass: "text-center"},
        { field: 'subtotal', displayName: 'Total Price', width: 120, cellClass: "text-right", cellFilter: 'number'},
        { displayName: 'Action', cellClass: 'text-center',
        cellTemplate:   '<div class="ngCellText" ng-class="col.colIndex()"><a class="btn btn-xs btn-default" ng-href="" ng-click="detail(row.entity.id)" data-toggle="tooltip" title="edit" data-original-title="Edit Client"><i class="fa fa-pencil"></i></a> | <a class="btn btn-xs btn-default" ng-href="" ng-click="print(row)" data-toggle="tooltip" title="Export To PDF" data-original-title="Export To Pdf File"><i class="fa fa-file-pdf-o"></i></a></div>'}
        ]
    }

}])

.controller('SalesordertCreateCtrl', ['$timeout', '$rootScope', '$http','$window', '$route', '$scope', '$location', 'Restangular', 'security', 'i18nNotifications', '$modal', '$dialogs', function ($timeout, $rootScope, $http ,$window, $route, $scope, $location, Restangular, security, i18nNotifications, $modal, $dialogs) {

    var original = {};

    original.salesorders_detail = [];
    original.date = new Date();
    original.ongkir = 0;
    original.bruto = 0;
    original.total = 0;
    original.totalhpp = 0;
    original.totaladminrp = 0;
    original.totalmarginrp = 0;
    original.subtotal = 0;
    original.qty = 0;
    original.disc_rupiah = 0;
    original.total_item_weight = 0;

    $scope.salesorder = Restangular.copy(original);

    $scope.$watch(function() {
        return security.currentUser;
    }, function(currentUser) {
        $scope.currentUser = currentUser;
    });

    Restangular.all("markets").getList({where:"is_active = 1"}).then(function(markets) {
        $scope.markets = markets.records;
    });

    Restangular.all("shippings").getList({where:"is_active = 1"}).then(function(shippings) {
        $scope.shippings = shippings.records;
    });

    $scope.changeprov = function() {
        Restangular.all("regions/city").getList({where:"param_region.name = '"+$scope.salesorder.province+"'"}).then(function(regions) {
          $scope.citys = regions.records;
      });
        $scope.salesorder.city = undefined;
        $scope.salesorder.districts = undefined;
        $scope.salesorder.village = undefined;
        $scope.salesorder.postal = undefined;
    }

    $scope.changecity = function() {
        Restangular.all("regions/districts").getList({where:"param_region.name = '"+$scope.salesorder.province+"' and param_region.city = '"+$scope.salesorder.city+"'"}).then(function(regions) {
          $scope.districts = regions.records;
      });
        $scope.salesorder.districts = undefined;
        $scope.salesorder.village = undefined;
        $scope.salesorder.postal = undefined;
    }

    $scope.changedist = function() {
        Restangular.all("regions/village").getList({where:"param_region.name = '"+$scope.salesorder.province+"' and param_region.city = '"+$scope.salesorder.city+"' and param_region.districts = '"+$scope.salesorder.districts+"'"}).then(function(regions) {
          $scope.villages = regions.records;
      });
        $scope.salesorder.village = undefined;
        $scope.salesorder.postal = undefined;
    }

    $scope.changepos = function() {
        Restangular.all("regions/postal").getList({where:"param_region.name = '"+$scope.salesorder.province+"' and param_region.city = '"+$scope.salesorder.city+"' and param_region.districts = '"+$scope.salesorder.districts+"' and param_region.village = '"+$scope.salesorder.village+"'"}).then(function(regions) {
          $scope.postals = regions.records;
          $scope.salesorder.postal = $scope.postals[0].postal;
      });
        Restangular.all("regions/code").getList({where:"param_region.name = '"+$scope.salesorder.province+"' and param_region.city = '"+$scope.salesorder.city+"' and param_region.districts = '"+$scope.salesorder.districts+"' and param_region.village = '"+$scope.salesorder.village+"'"}).then(function(regions) {
          $scope.codes = regions.records;
          $scope.salesorder.code = $scope.codes[0].code;
      });
    }

    $scope.format = 'dd-MMM-yyyy';
    $scope.minDate = new Date();
    $scope.maxDate = new Date().setDate(new Date().getDate() + 60);

    $scope.changedate = function(index) {
        if(index == 1){
            $scope.salesorder.delivery_date = undefined;
            $scope.salesorder.expired_selling_date = undefined;
        }else if(index == 2){
            if(moment($scope.salesorder.expired_selling_date).format('YYYY-MM-DD') < moment($scope.salesorder.selling_date).format('YYYY-MM-DD')){
                $dialogs.error('Warning', 'Tanggal akhir penjualan tidak boleh lebih kecil dari tanggal penjualan !!!');
                $scope.salesorder.expired_selling_date = undefined;
            }
        }else{
            if(moment($scope.salesorder.delivery_date).format('YYYY-MM-DD') < moment($scope.salesorder.selling_date).format('YYYY-MM-DD')){
                $dialogs.error('Warning', 'Tanggal kirim tidak boleh lebih kecil dari tanggal penjualan !!!');
                $scope.salesorder.delivery_date = undefined;
            }
        }
    };

    $scope.salesorder.salesorder_details = original.salesorder_details;

    $scope.detailCellTpl = '<div class="ngCellText" ng-class="col.colIndex()"><a ng-href="" ng-click="removeQuotationItem(row.rowIndex, row)"><i class="icon-trash"></i></a></div>';

    $scope.gridOptions = {
        data: 'salesorder.salesorders_detail',
        enablePaging: true,
        showFooter: true,
        multiSelect: false,
        selectedItems: [],
        totalServerItems:'totalServerItems',
        enableColumnResize: true,
        enableCellSelection: true,
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        columnDefs: [
        { displayName: 'Del', width: 50, cellClass: "text-center", cellTemplate: $scope.detailCellTpl},
        { field: 'item_id', displayName: 'Code', width: 100, cellClass: "text-center"},
        { field: 'item_name', displayName: 'Item'},
        { field: 'item_unit', displayName: 'Unit', width:80, cellClass: "text-center"},
        { field: 'item_weight', displayName: 'Berat(g)', width:80, cellClass: "text-right", cellFilter: 'number'},
        { field: 'stock', displayName: 'Stok', width:80, cellClass: "text-right", cellFilter: 'number'},
        { field: 'qty', displayName: 'Qty*', width: 90, cellClass: "text-right" , enableCellEdit: true, cellFilter: 'number', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-change="calculate(row)" format="number"/>'},
        { field: 'hpp', displayName: 'HPP(Rp)', width:100, cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'marginrp', displayName: 'Margin(Rp)', width:100, cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'price', displayName: 'Price(Rp)', width: 100, cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'total_item_weight', displayName: 'Total Berat(g)', width:100, cellClass: "text-right", cellFilter: 'number'},
        { field: 'bruto', displayName: 'Bruto' , width: 100, cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'disc_percent', displayName: 'Disc(%)*', width: 80, cellClass: "text-right" , enableCellEdit: true, cellFilter: 'number:2', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-change="disc1(row)" format="number"/>'},
        { field: 'disc_rupiah', displayName: 'Disc(Rp)*', width: 100, cellClass: "text-right" , enableCellEdit: true, cellFilter: 'number:2', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-change="disc2(row)" format="number"/>'},
        { field: 'total_price', displayName: 'Total' , width: 100, cellClass: "text-right", cellFilter: 'number:2'}
        ]
    }

    $scope.removeQuotationItem = function(index, row) {
        $scope.salesorder.salesorders_detail.splice(index, 1);
        $scope.calculate(row)
    };

    $scope.disc1 = function(row) {
        if(row.entity.disc_percent < 100){
            row.entity.disc_rupiah = row.entity.bruto * parseFloat(row.entity.disc_percent / Number(100));
        }else{
            row.entity.disc_rupiah = 0;
            row.entity.disc_percent = 0;
        }
        $scope.calculate(row);
    };

    $scope.disc2 = function(row) {
        if(row.entity.disc_rupiah < row.entity.bruto){
            row.entity.disc_percent = parseFloat(row.entity.disc_rupiah / row.entity.bruto) * Number(100);
        }else{
            row.entity.disc_rupiah = 0;
            row.entity.disc_percent = 0;
        }
        $scope.calculate(row);
    };

    $scope.calculate = function(row) {
        row.entity.total_item_weight = row.entity.qty * row.entity.item_weight;
        row.entity.bruto = row.entity.qty * row.entity.price;
        row.entity.total_price = parseFloat(row.entity.bruto) - parseFloat(row.entity.disc_rupiah);
        row.entity.hpp = row.entity.qty * row.entity.hpp_save;
        row.entity.marginrp = parseFloat(row.entity.total_price) - parseFloat(row.entity.hpp);
        row.entity.adminrp = row.entity.qty * row.entity.adminrp_save;
        $scope.count();
    };

    $scope.count = function() {
        $scope.salesorder.bruto = 0;
        $scope.salesorder.total = 0;
        $scope.salesorder.qty = 0;
        $scope.salesorder.disc_rupiah = 0;
        $scope.salesorder.total_item_weight = 0;
        $scope.salesorder.totalhpp = 0;
        $scope.salesorder.totalmarginrp = 0;
        $scope.salesorder.totaladminrp = 0;
        var x = 0;
        for(var i=0; i<$scope.salesorder.salesorders_detail.length; i++){
            $scope.salesorder.qty = parseInt($scope.salesorder.qty) + parseInt($scope.salesorder.salesorders_detail[i].qty);
            $scope.salesorder.disc_rupiah = parseInt($scope.salesorder.disc_rupiah) + parseInt($scope.salesorder.salesorders_detail[i].disc_rupiah);
            $scope.salesorder.total_item_weight = parseInt($scope.salesorder.total_item_weight) + parseInt($scope.salesorder.salesorders_detail[i].total_item_weight);
            $scope.salesorder.bruto = parseInt($scope.salesorder.bruto) + parseInt($scope.salesorder.salesorders_detail[i].bruto);
            $scope.salesorder.total = parseFloat($scope.salesorder.total) + parseFloat($scope.salesorder.salesorders_detail[i].total_price);
            $scope.salesorder.totalhpp = parseFloat($scope.salesorder.totalhpp) + parseFloat($scope.salesorder.salesorders_detail[i].hpp);
            $scope.salesorder.totalmarginrp = parseFloat($scope.salesorder.totalmarginrp) + parseFloat($scope.salesorder.salesorders_detail[i].marginrp);
            $scope.salesorder.totaladminrp = parseFloat($scope.salesorder.totaladminrp) + parseFloat($scope.salesorder.salesorders_detail[i].adminrp);
        }
        $scope.ongkir();
    };

    $scope.ongkir = function() {
        $scope.salesorder.subtotal = parseFloat($scope.salesorder.ongkir) + parseFloat($scope.salesorder.total);
    };

    $scope.customer = function() {
        $scope.salesorder.salesman_id = null;
        $scope.salesorder.salesman_name = null;

        var modalInstance = $modal.open({
            templateUrl: 'scripts/app/home/data_salesorder/customer.list.modal.tpl.html',
            controller: ModalInstanceBrowseProjectCtrl,
            resolve: {
                customer_id: function () {
                    return $scope.salesorder;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {

            var customer = Restangular.copy(selectedItem);
            var salesorder = Restangular.copy($scope.salesorder);

            salesorder.company_id = customer.company_id;
            salesorder.customer_id = customer.id;
            salesorder.customer_name = customer.name;
            salesorder.address = customer.address;
            salesorder.customer_address = customer.address;
            salesorder.market_id = customer.market_id;
            salesorder.mobile = customer.mobile;
            salesorder.province = customer.province;
            salesorder.city = customer.city;
            salesorder.districts = customer.districts;
            salesorder.village = customer.village;
            salesorder.postal = customer.postal;
            salesorder.email = customer.email;
            salesorder.salesman_id = '001';
            salesorder.salesman_name = 'DEBBY KURNIA DWIYANTO';

            Restangular.all("regions/province").getList().then(function(regions) {
                $scope.provinces = regions.records;
            });

            Restangular.all("regions/city").getList({where:"param_region.name = '"+salesorder.province+"'"}).then(function(regions) {
                $scope.citys = regions.records;
            });

            Restangular.all("regions/districts").getList({where:"param_region.city = '"+salesorder.city+"'"}).then(function(regions) {
                $scope.districts = regions.records;
            });

            Restangular.all("regions/village").getList({where:"param_region.districts = '"+salesorder.districts+"'"}).then(function(regions) {
                $scope.villages = regions.records;
            });

            $scope.salesorder = salesorder;

        }, function () {

        });

    };

    var ModalInstanceBrowseProjectCtrl = function ($scope, $dialog, $modalInstance, customer_id, Restangular, security, i18nNotifications, $route) {

        $scope.item = customer_id;

        Restangular.all("markets").getList({where:'is_active=1'}).then(function(markets) {
            $scope.markets = markets.records;
        });

        // user permissions
        $scope.canCreate    = security.canCreate;
        $scope.canUpdate    = security.canUpdate;
        $scope.canRead      = security.canRead;
        $scope.canDelete    = security.canDelete;
        $scope.isAuthorizedRoute = security.isAuthorizedRoute;

        $scope.change_filter = function(){
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }

        // ng-Grid
        $scope.filterOptions = {
            filterText: "",
            useExternalFilter: true
        };
        $scope.totalServerItems = 0;
        $scope.pagingOptions = {
            pageSizes: [20, 50, 100],
            pageSize: 50,
            currentPage: 1
        };
        $scope.setPagingData = function(data, page, pageSize){
            var pagedData = data.records;
            $scope.myData = pagedData;
            $scope.totalServerItems = data.total;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        };
        $scope.getPagedDataAsync = function (pageSize, page, searchText) {
            setTimeout(function () {
                var custom_get = {};

                if($route.current.params)
                    custom_get = $route.current.params;

                custom_get.offset = (page - 1) * pageSize;
                custom_get.limit = pageSize;

                var where_id = "is_active = 1";

                if($scope.filterOptions.filterText != null){
                    where_id = where_id+ " and (param_customer.id like '%"+$scope.filterOptions.filterText+"%' or param_customer.name like '%"+$scope.filterOptions.filterText+"%')";
                }

                if($scope.filterOptions.market_id != undefined){
                    where_id = where_id+ " and param_customer.market_id = "+$scope.filterOptions.market_id;
                }

                custom_get.where = where_id;

                Restangular.all("customers").getList(custom_get).then(function(customers) {
                    $scope.setPagingData(customers,page,pageSize);
                });
            }, 100);
        };

        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

        $scope.$watch('pagingOptions', function (newVal, oldVal) {
            if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
            }
        }, true);

        $scope.gridOptions = {
            data: 'myData',
            enablePaging: true,
            showFooter: true,
            multiSelect: false,
            selectedItems: [],
            totalServerItems:'totalServerItems',
            pagingOptions: $scope.pagingOptions,
            filterOptions: $scope.filterOptions,
            columnDefs: [
            { field: 'id', displayName: 'Code', cellClass: 'text-center', width: 150},
            { field: 'name', displayName: 'Customer', width: 350},
            { field: 'market', displayName: 'Market', width: 120},
            { field: 'address', displayName: 'Address'}
            ]
        };

        $scope.canSelect = function() {
            return ($scope.gridOptions.selectedItems.length===0?false:true);
        }

        $scope.select = function () {
            $modalInstance.close($scope.gridOptions.selectedItems[0]);
        };

        $scope.back = function () {
            $modalInstance.dismiss('cancel');
        };

    };


    $scope.salesman = function() {
        $scope.salesorder.salesman_id = null;
        $scope.salesorder.salesman_name = null;

        var modalInstance = $modal.open({
            templateUrl: 'scripts/app/home/data_salesorder/salesman.list.modal.tpl.html',
            controller: ModalInstanceBrowseSalesmanCtrl,
            resolve: {
                salesman_id: function () {
                    return $scope.salesorder;
                },
                market_id: function (){
                    return $scope.salesorder.market_id;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {

            var item = Restangular.copy(selectedItem);
            var salesorder = Restangular.copy($scope.salesorder);

            salesorder.salesman_id =item.id;
            salesorder.salesman_name =item.name;

            $scope.salesorder = salesorder;

        }, function () {

        });
    };

    var ModalInstanceBrowseSalesmanCtrl = function (market_id, $scope, $dialog, $modalInstance, salesman_id, Restangular, security, i18nNotifications, $route) {

        $scope.item = salesman_id;

        // user permissions
        $scope.canCreate    = security.canCreate;
        $scope.canUpdate    = security.canUpdate;
        $scope.canRead      = security.canRead;
        $scope.canDelete    = security.canDelete;
        $scope.isAuthorizedRoute = security.isAuthorizedRoute;

        // ng-Grid
        $scope.filterOptions = {
            filterText: "",
            useExternalFilter: true
        };
        $scope.totalServerItems = 0;
        $scope.pagingOptions = {
            pageSizes: [20, 50, 100],
            pageSize: 50,
            currentPage: 1
        };
        $scope.setPagingData = function(data, page, pageSize){
            var pagedData = data.records;
            $scope.myData = pagedData;
            $scope.totalServerItems = data.total;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        };
        $scope.getPagedDataAsync = function (pageSize, page, searchText) {
            setTimeout(function () {
                var custom_get = {};

                if($route.current.params)
                    custom_get = $route.current.params;

                custom_get.offset = (page - 1) * pageSize;
                custom_get.limit = pageSize;

                custom_get.where = undefined;

                if (searchText) {
                    var ft = searchText.toLowerCase();

                    custom_get.filter = ft;
                    custom_get.filter_fields = 'id, name';

                    Restangular.all("salesmans/"+market_id).getList(custom_get).then(function(salesmans) {
                        $scope.setPagingData(salesmans,page,pageSize);
                    });
                }
                else {
                    Restangular.all("salesmans/"+market_id).getList(custom_get).then(function(salesmans) {
                        $scope.setPagingData(salesmans,page,pageSize);
                    });
                }
            }, 100);
        };

        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

        $scope.$watch('pagingOptions', function (newVal, oldVal) {
            if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
            }
        }, true);
        $scope.$watch('filterOptions', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
            }
        }, true);

        $scope.gridOptions = {
            data: 'myData',
            enablePaging: true,
            showFooter: true,
            multiSelect: false,
            selectedItems: [],
            totalServerItems:'totalServerItems',
            pagingOptions: $scope.pagingOptions,
            filterOptions: $scope.filterOptions,
            columnDefs: [
            { field: 'id', displayName: 'Code', cellClass: 'text-center', width: 150},
            { field: 'name', displayName: 'Salesman', width: 350},
            { field: 'address', displayName: 'Address'}
            ]
        };

        $scope.canSelect = function() {
            return ($scope.gridOptions.selectedItems.length===0?false:true);
        }

        $scope.select = function () {
            $modalInstance.close($scope.gridOptions.selectedItems[0]);
        };

        $scope.back = function () {
            $modalInstance.dismiss('cancel');
        };

    };

    $scope.addItem = function(row) {
        var modalInstance = $modal.open({
            templateUrl: 'scripts/app/home/data_salesorder/item.list.modal.tpl.html',
            controller: ModalInstanceAddQuotationItemCreateCtrl,
            resolve: {
                salesorder_detail: function(Restangular, $route){
                    return Restangular.copy($scope.salesorder);
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {

            $scope.salesorder.item_amount = $scope.salesorder.item_amount + selectedItem.length;

            for (var i = 0; i < selectedItem.length; i++){
            // reload table
            var salesorder_detail            = {};
            salesorder_detail.pivot          = {};
            salesorder_detail.item_id        = selectedItem[i].item_id;
            salesorder_detail.item_name      = selectedItem[i].item_name;
            salesorder_detail.item_unit      = selectedItem[i].item_unit;
            salesorder_detail.item_weight    = selectedItem[i].item_weight;
            salesorder_detail.stock          = selectedItem[i].qty;
            salesorder_detail.qty            = 0;
            salesorder_detail.price          = selectedItem[i].item_price;
            salesorder_detail.disc_percent   = 0;
            salesorder_detail.disc_rupiah    = 0;
            salesorder_detail.total_item_weight = 0;
            salesorder_detail.bruto          = 0;
            salesorder_detail.total_price    = 0;
            salesorder_detail.hpp            = selectedItem[i].amounthpp;
            salesorder_detail.marginrp       = selectedItem[i].marginrp;
            salesorder_detail.adminrp        = selectedItem[i].adminrp;
            salesorder_detail.hpp_save       = selectedItem[i].amounthpp;
            salesorder_detail.marginrp_save  = selectedItem[i].marginrp;
            salesorder_detail.adminrp_save   = selectedItem[i].adminrp;

            $scope.salesorder.salesorders_detail.splice($scope.salesorder.salesorders_detail.length+1,0,salesorder_detail);
        }

    }, function () {
    });
    };

    var ModalInstanceAddQuotationItemCreateCtrl = function ($scope, $dialog, $modalInstance, salesorder_detail, simpleDeleteConfirm, Restangular, security, i18nNotifications, $upload, APP_CONFIG) {

        var original = angular.copy(salesorder_detail);

        $scope.salesorder_detail = original;

        $scope.$watch(function() {
            return security.currentUser;
        }, function(currentUser) {
            $scope.currentUser = currentUser;
        });

        $scope.filterOptions = {
            filterText: "",
            useExternalFilter: true
        };
        $scope.totalServerItems = 0;
        $scope.pagingOptions = {
            pageSizes: [20, 50, 100],
            pageSize: 20,
            currentPage: 1
        };
        $scope.setPagingData = function(data, page, pageSize){
            var pagedData = data.records;
            $scope.myData = pagedData;
            $scope.totalServerItems = data.total;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        };
        $scope.getPagedDataAsync = function (pageSize, page, searchText) {
            setTimeout(function () {
                var custom_get = {};

                if($route.current.params)
                    custom_get = $route.current.params;

                custom_get.offset = (page - 1) * pageSize;
                custom_get.limit = pageSize;

                custom_get.where = undefined;

                if (searchText='$scope.filterOptions.filterText') {

                    custom_get.filter = $scope.filterOptions.filterText;
                    custom_get.filter_fields = 'param_pricelist_detail.item_id, param_pricelist_detail.item_name';

                    Restangular.all("pricelists/so/"+$scope.salesorder_detail.market_id).getList(custom_get).then(function(pricelists) {
                        $scope.setPagingData(pricelists,page,pageSize);
                    });
                }
                else {
                    Restangular.all("pricelists/so/"+$scope.salesorder_detail.market_id).getList(custom_get).then(function(pricelists) {
                        $scope.setPagingData(pricelists,page,pageSize);
                    });
                }
            }, 100);
        };

        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

        $scope.$watch('pagingOptions', function (newVal, oldVal) {
            if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
            }
        }, true);
        $scope.$watch('filterOptions', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
            }
        }, true);


        $scope.gridOptions = {
            data: 'myData',
            enablePaging: true,
            showFooter: true,
            multiSelect: true,
            selectedItems: [],
            enableColumnResize: true,
            totalServerItems:'totalServerItems',
            pagingOptions: $scope.pagingOptions,
            filterOptions: $scope.filterOptions,
            showSelectionCheckbox: true,
            checkboxHeaderTemplate: '<input class="ngSelectionHeader" type="checkbox" ng-model="allSelected" ng-change="toggleSelectAll(allSelected, true)"/>',
            columnDefs: [
            { field: 'item_id', displayName: 'Code', width: 150, cellClass:'text-center' },
            { field: 'item_name', displayName: 'Name'},
            { field: 'item_unit', displayName: 'Unit', width: 110, cellClass:'text-center' },
            { field: 'qty', displayName: 'Stock', width: 110, cellClass:'text-right', cellFilter: 'number', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number"/>'},
            { field: 'hpp', displayName: 'HPP', width: 180, cellClass:'text-right', cellFilter: 'number:2', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number"/>'},
            { field: 'operational', displayName: 'Operational', width: 180, cellClass:'text-right', cellFilter: 'number:2', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number"/>'},
            { field: 'amounthpp', displayName: 'Total HPP', width: 180, cellClass:'text-right', cellFilter: 'number:2', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number"/>'},
            { field: 'created_at', displayName: 'Date', width: 200, cellClass:'text-center' }
            ]
        };

        $scope.canSelect = function() {
            return ($scope.gridOptions.selectedItems.length===0?false:true);
        }

        $scope.select = function () {
            var selectedItem = $scope.gridOptions.selectedItems;
            var isExist = false;
            for(var j = 0; j <$scope.salesorder_detail.salesorders_detail.length; j++){
                for(var i = 0; i <  selectedItem.length; i++){
                    var as = selectedItem[i];
                    if(as.id==$scope.salesorder_detail.salesorders_detail[j].item_id){
                        selectedItem.splice(i, 1);
                    }
                }
            }

            for(var i = 0; i <  selectedItem.length; i++){
                var as = selectedItem[i];
                for(var j=0; j <  selectedItem.length; j++){
                    var sa = selectedItem[j];
                    if(i != j){
                        if(as.id==sa.id){
                            selectedItem.splice(j, 1);
                        }
                    }
                }
            }
            $modalInstance.close(selectedItem);
        };

        $scope.back = function () {
            $modalInstance.dismiss('cancel');
        };

    };

    $scope.canCreate    = security.canCreate;
    $scope.canUpdate    = security.canUpdate;
    $scope.canRead      = security.canRead;
    $scope.canDelete    = security.canDelete;

    var progress = 10;
    var msgs = [
    'Processing...',
    'Done!'
    ];
    var i = 0;
    var fakeProgress = function(){
        $timeout(function(){
            if(progress < 95){
                progress += 5;
                $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': progress});
                fakeProgress();
            }else{
            }
        },1000);
    };

    $scope.canRemove = function() {
        return false;
    }

    $scope.back = function() {
        $window.history.back();
    }
    $scope.canSave = function() {
        return $scope.SalesorderForm1.$valid && $scope.SalesorderForm2.$valid &&
        !angular.equals($scope.salesorder, original);
    };

    $scope.canRevert = function() {
        return !angular.equals($scope.salesorder, original);
    }
    $scope.revert = function() {
        $scope.salesorder =  Restangular.copy(original);
        $scope.salesorderForm.$setPristine();
    }

    $scope.save = function() {
        for(var i=0; i<$scope.salesorder.salesorders_detail.length; i++){
            if($scope.salesorder.salesorders_detail[i].qty == undefined || $scope.salesorder.salesorders_detail[i].qty == '' || $scope.salesorder.salesorders_detail[i].qty == 0){
                var x = 1;
            }
        }

        if(x==1){
            $dialogs.error('Warning', 'Nilai Qty tidak boleh kosong !!!')
        }else{
            progress = 10;
            i = 0;
            $dialogs.wait(msgs[0],0);
            fakeProgress();

            $scope.salesorder.date = moment($scope.salesorder.date).format('YYYY-MM-DD');
            $scope.salesorder.selling_date = moment($scope.salesorder.selling_date).format('YYYY-MM-DD');
            $scope.salesorder.expired_selling_date = moment($scope.salesorder.expired_selling_date).format('YYYY-MM-DD');
            $scope.salesorder.delivery_date = moment($scope.salesorder.delivery_date).format('YYYY-MM-DD');

            Restangular.all('salesorders').post($scope.salesorder).then(function(salesorder) {
              i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : salesorder.id});
              $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
              $rootScope.$broadcast('dialogs.wait.complete');
              $location.path('/home/salesorder');
          });
        }
    };

}])

.controller('SalesordertEditCtrl', ['APP_CONFIG','$timeout', '$rootScope', '$http', '$window', '$scope', '$route', '$location', 'Restangular', 'item', 'i18nNotifications', '$dialog', 'security', 'simpleDeleteConfirm', '$modal', '$dialogs', function (APP_CONFIG,$timeout, $rootScope, $http, $window, $scope, $route, $location, Restangular, item, i18nNotifications, $dialog, security, simpleDeleteConfirm, $modal, $dialogs) {

    //debugger;
    var original = item;

    $scope.salesorder = Restangular.copy(original);

    $scope.$watch(function() {
        return security.currentUser;
    }, function(currentUser) {
        $scope.currentUser = currentUser;
    });

    Restangular.all("markets").getList({where:"is_active = 1"}).then(function(markets) {
        $scope.markets = markets.records;
    });

    Restangular.all("shippings").getList({where:"is_active = 1"}).then(function(shippings) {
        $scope.shippings = shippings.records;
    });

    Restangular.all("regions/province").getList().then(function(regions) {
        $scope.provinces = regions.records;
    });

    Restangular.all("regions/city").getList({where:"param_region.name = '"+$scope.salesorder.province+"'"}).then(function(regions) {
        $scope.citys = regions.records;
    });

    Restangular.all("regions/districts").getList({where:"param_region.city = '"+$scope.salesorder.city+"'"}).then(function(regions) {
        $scope.districts = regions.records;
    });

    Restangular.all("regions/village").getList({where:"param_region.districts = '"+$scope.salesorder.districts+"'"}).then(function(regions) {
        $scope.villages = regions.records;
    });

    $scope.changeprov = function() {
        Restangular.all("regions/city").getList({where:"param_region.name = '"+$scope.salesorder.province+"'"}).then(function(regions) {
          $scope.citys = regions.records;
      });
        $scope.salesorder.city = undefined;
        $scope.salesorder.districts = undefined;
        $scope.salesorder.village = undefined;
        $scope.salesorder.postal = undefined;
    }

    $scope.changecity = function() {
        Restangular.all("regions/districts").getList({where:"param_region.name = '"+$scope.salesorder.province+"' and param_region.city = '"+$scope.salesorder.city+"'"}).then(function(regions) {
          $scope.districts = regions.records;
      });
        $scope.salesorder.districts = undefined;
        $scope.salesorder.village = undefined;
        $scope.salesorder.postal = undefined;
    }

    $scope.changedist = function() {
        Restangular.all("regions/village").getList({where:"param_region.name = '"+$scope.salesorder.province+"' and param_region.city = '"+$scope.salesorder.city+"' and param_region.districts = '"+$scope.salesorder.districts+"'"}).then(function(regions) {
          $scope.villages = regions.records;
      });
        $scope.salesorder.village = undefined;
        $scope.salesorder.postal = undefined;
    }

    $scope.changepos = function() {
        Restangular.all("regions/postal").getList({where:"param_region.name = '"+$scope.salesorder.province+"' and param_region.city = '"+$scope.salesorder.city+"' and param_region.districts = '"+$scope.salesorder.districts+"' and param_region.village = '"+$scope.salesorder.village+"'"}).then(function(regions) {
          $scope.postals = regions.records;
          $scope.salesorder.postal = $scope.postals[0].postal;
      });
        Restangular.all("regions/code").getList({where:"param_region.name = '"+$scope.salesorder.province+"' and param_region.city = '"+$scope.salesorder.city+"' and param_region.districts = '"+$scope.salesorder.districts+"' and param_region.village = '"+$scope.salesorder.village+"'"}).then(function(regions) {
          $scope.codes = regions.records;
          $scope.salesorder.code = $scope.codes[0].code;
      });
    }

    $scope.format = 'dd-MMM-yyyy';
    $scope.minDate = new Date();
    $scope.maxDate = new Date().setDate(new Date().getDate() + 60);

    $scope.changedate = function(index) {
        if(index == 1){
            $scope.salesorder.delivery_date = undefined;
            $scope.salesorder.expired_selling_date = undefined;
        }else if(index == 2){
            if(moment($scope.salesorder.expired_selling_date).format('YYYY-MM-DD') < moment($scope.salesorder.selling_date).format('YYYY-MM-DD')){
                $dialogs.error('Warning', 'Tanggal akhir penjualan tidak boleh lebih kecil dari tanggal penjualan !!!');
                $scope.salesorder.expired_selling_date = undefined;
            }
        }else{
            if(moment($scope.salesorder.delivery_date).format('YYYY-MM-DD') < moment($scope.salesorder.selling_date).format('YYYY-MM-DD')){
                $dialogs.error('Warning', 'Tanggal kirim tidak boleh lebih kecil dari tanggal penjualan !!!');
                $scope.salesorder.delivery_date = undefined;
            }
        }
    };

    $scope.salesorder.salesorder_details = original.salesorder_details;

    $scope.detailCellTpl = '<div class="ngCellText" ng-class="col.colIndex()"><a ng-href="" ng-click="removeQuotationItem(row.rowIndex, row)"><i class="icon-trash"></i></a></div>';

    $scope.gridOptions = {
        data: 'salesorder.salesorders_detail',
        enablePaging: true,
        showFooter: true,
        multiSelect: false,
        selectedItems: [],
        totalServerItems:'totalServerItems',
        enableColumnResize: true,
        enableCellSelection: true,
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        columnDefs: [
        { displayName: 'Del', width: 50, cellClass: "text-center", cellTemplate: $scope.detailCellTpl},
        { field: 'item_id', displayName: 'Code', width: 100, cellClass: "text-center"},
        { field: 'item_name', displayName: 'Item'},
        { field: 'item_unit', displayName: 'Unit', width:80, cellClass: "text-center"},
        { field: 'item_weight', displayName: 'Berat(g)', width:80, cellClass: "text-right", cellFilter: 'number'},
        { field: 'qty', displayName: 'Qty*', width: 90, cellClass: "text-right" , enableCellEdit: true, cellFilter: 'number', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-change="calculate(row)" format="number"/>'},
        { field: 'hpp', displayName: 'HPP(Rp)', width:100, cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'marginrp', displayName: 'Margin(Rp)', width:100, cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'price', displayName: 'Price(Rp)', width: 100, cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'total_item_weight', displayName: 'Total Berat(g)', width:100, cellClass: "text-right", cellFilter: 'number'},
        { field: 'bruto', displayName: 'Bruto' , width: 100, cellClass: "text-right", cellFilter: 'number:2'},
        { field: 'disc_percent', displayName: 'Disc(%)*', width: 80, cellClass: "text-right" , enableCellEdit: true, cellFilter: 'number:2', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-change="disc1(row)" format="number"/>'},
        { field: 'disc_rupiah', displayName: 'Disc(Rp)*', width: 100, cellClass: "text-right" , enableCellEdit: true, cellFilter: 'number:2', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-change="disc2(row)" format="number"/>'},
        { field: 'total_price', displayName: 'Total' , width: 100, cellClass: "text-right", cellFilter: 'number:2'}
        ]
    }

    $scope.salesorder.detail_delete = new Array();
    $scope.removeQuotationItem = function(index, row) {
        $scope.salesorder.salesorders_detail.splice(index, 1);
        $scope.salesorder.detail_delete.push(row.entity.id);
        $scope.calculate(row);
    };

    $scope.disc1 = function(row) {
        if(row.entity.disc_percent < 100){
            row.entity.disc_rupiah = row.entity.bruto * parseFloat(row.entity.disc_percent / Number(100));
        }else{
            row.entity.disc_rupiah = 0;
            row.entity.disc_percent = 0;
        }
        $scope.calculate(row);
    };

    $scope.disc2 = function(row) {
        if(row.entity.disc_rupiah < row.entity.bruto){
            row.entity.disc_percent = parseFloat(row.entity.disc_rupiah / row.entity.bruto) * Number(100);
        }else{
            row.entity.disc_rupiah = 0;
            row.entity.disc_percent = 0;
        }
        $scope.calculate(row);
    };

    $scope.calculate = function(row) {
        row.entity.total_item_weight = row.entity.qty * row.entity.item_weight;
        row.entity.bruto = row.entity.qty * row.entity.price;
        row.entity.total_price = parseFloat(row.entity.bruto) - parseFloat(row.entity.disc_rupiah);
        row.entity.hpp = row.entity.qty * row.entity.hpp_save;
        row.entity.marginrp = parseFloat(row.entity.total_price) - parseFloat(row.entity.hpp);
        row.entity.adminrp = row.entity.qty * row.entity.adminrp_save;
        $scope.count();
    };

    $scope.count = function() {
        $scope.salesorder.bruto = 0;
        $scope.salesorder.total = 0;
        $scope.salesorder.qty = 0;
        $scope.salesorder.disc_rupiah = 0;
        $scope.salesorder.total_item_weight = 0;
        $scope.salesorder.totalhpp = 0;
        $scope.salesorder.totalmarginrp = 0;
        $scope.salesorder.totaladminrp = 0;
        var x = 0;
        for(var i=0; i<$scope.salesorder.salesorders_detail.length; i++){
            $scope.salesorder.qty = parseInt($scope.salesorder.qty) + parseInt($scope.salesorder.salesorders_detail[i].qty);
            $scope.salesorder.disc_rupiah = parseInt($scope.salesorder.disc_rupiah) + parseInt($scope.salesorder.salesorders_detail[i].disc_rupiah);
            $scope.salesorder.total_item_weight = parseInt($scope.salesorder.total_item_weight) + parseInt($scope.salesorder.salesorders_detail[i].total_item_weight);
            $scope.salesorder.bruto = parseInt($scope.salesorder.bruto) + parseInt($scope.salesorder.salesorders_detail[i].bruto);
            $scope.salesorder.total = parseFloat($scope.salesorder.total) + parseFloat($scope.salesorder.salesorders_detail[i].total_price);
            $scope.salesorder.totalhpp = parseFloat($scope.salesorder.totalhpp) + parseFloat($scope.salesorder.salesorders_detail[i].hpp);
            $scope.salesorder.totalmarginrp = parseFloat($scope.salesorder.totalmarginrp) + parseFloat($scope.salesorder.salesorders_detail[i].marginrp);
            $scope.salesorder.totaladminrp = parseFloat($scope.salesorder.totaladminrp) + parseFloat($scope.salesorder.salesorders_detail[i].adminrp);
        }
        $scope.ongkir();
    };

    $scope.ongkir = function() {
        $scope.salesorder.subtotal = parseFloat($scope.salesorder.ongkir) + parseFloat($scope.salesorder.total);
    };

    $scope.salesman = function() {
        $scope.salesorder.salesman_id = null;
        $scope.salesorder.salesman_name = null;

        var modalInstance = $modal.open({
            templateUrl: 'scripts/app/home/data_salesorder/salesman.list.modal.tpl.html',
            controller: ModalInstanceBrowseSalesmanCtrl,
            resolve: {
                salesman_id: function () {
                    return $scope.salesorder;
                },
                market_id: function (){
                    return $scope.salesorder.market_id;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {

            var item = Restangular.copy(selectedItem);
            var salesorder = Restangular.copy($scope.salesorder);

            salesorder.salesman_id =item.id;
            salesorder.salesman_name =item.name;

            $scope.salesorder = salesorder;

        }, function () {

        });
    };

    var ModalInstanceBrowseSalesmanCtrl = function (market_id, $scope, $dialog, $modalInstance, salesman_id, Restangular, security, i18nNotifications, $route) {

        $scope.item = salesman_id;

        // user permissions
        $scope.canCreate    = security.canCreate;
        $scope.canUpdate    = security.canUpdate;
        $scope.canRead      = security.canRead;
        $scope.canDelete    = security.canDelete;
        $scope.isAuthorizedRoute = security.isAuthorizedRoute;

        // ng-Grid
        $scope.filterOptions = {
            filterText: "",
            useExternalFilter: true
        };
        $scope.totalServerItems = 0;
        $scope.pagingOptions = {
            pageSizes: [20, 50, 100],
            pageSize: 50,
            currentPage: 1
        };
        $scope.setPagingData = function(data, page, pageSize){
            var pagedData = data.records;
            $scope.myData = pagedData;
            $scope.totalServerItems = data.total;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        };
        $scope.getPagedDataAsync = function (pageSize, page, searchText) {
            setTimeout(function () {
                var custom_get = {};

                if($route.current.params)
                    custom_get = $route.current.params;

                custom_get.offset = (page - 1) * pageSize;
                custom_get.limit = pageSize;

                custom_get.where = undefined;

                if (searchText) {
                    var ft = searchText.toLowerCase();

                    custom_get.filter = ft;
                    custom_get.filter_fields = 'id, name';

                    Restangular.all("salesmans/"+market_id).getList(custom_get).then(function(salesmans) {
                        $scope.setPagingData(salesmans,page,pageSize);
                    });
                }
                else {
                    Restangular.all("salesmans/"+market_id).getList(custom_get).then(function(salesmans) {
                        $scope.setPagingData(salesmans,page,pageSize);
                    });
                }
            }, 100);
        };

        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

        $scope.$watch('pagingOptions', function (newVal, oldVal) {
            if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
            }
        }, true);
        $scope.$watch('filterOptions', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
            }
        }, true);

        $scope.gridOptions = {
            data: 'myData',
            enablePaging: true,
            showFooter: true,
            multiSelect: false,
            selectedItems: [],
            totalServerItems:'totalServerItems',
            pagingOptions: $scope.pagingOptions,
            filterOptions: $scope.filterOptions,
            columnDefs: [
            { field: 'id', displayName: 'Code', cellClass: 'text-center', width: 150},
            { field: 'name', displayName: 'Salesman', width: 350},
            { field: 'address', displayName: 'Address'}
            ]
        };

        $scope.canSelect = function() {
            return ($scope.gridOptions.selectedItems.length===0?false:true);
        }

        $scope.select = function () {
            $modalInstance.close($scope.gridOptions.selectedItems[0]);
        };

        $scope.back = function () {
            $modalInstance.dismiss('cancel');
        };

    };

    $scope.addItem = function(row) {
        var modalInstance = $modal.open({
            templateUrl: 'scripts/app/home/data_salesorder/item.list.modal.tpl.html',
            controller: ModalInstanceAddQuotationItemCreateCtrl,
            resolve: {
                salesorder_detail: function(Restangular, $route){
                    return Restangular.copy($scope.salesorder);
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {

            $scope.salesorder.item_amount = $scope.salesorder.item_amount + selectedItem.length;

            for (var i = 0; i < selectedItem.length; i++){
            // reload table
            var salesorder_detail            = {};
            salesorder_detail.pivot          = {};
            salesorder_detail.item_id        = selectedItem[i].item_id;
            salesorder_detail.item_name      = selectedItem[i].item_name;
            salesorder_detail.item_unit      = selectedItem[i].item_unit;
            salesorder_detail.item_weight    = selectedItem[i].item_weight;
            salesorder_detail.stock          = selectedItem[i].qty;
            salesorder_detail.qty            = 0;
            salesorder_detail.price          = selectedItem[i].item_price;
            salesorder_detail.disc_percent   = 0;
            salesorder_detail.disc_rupiah    = 0;
            salesorder_detail.total_item_weight = 0;
            salesorder_detail.bruto          = 0;
            salesorder_detail.total_price    = 0;
            salesorder_detail.hpp            = selectedItem[i].amounthpp;
            salesorder_detail.marginrp       = selectedItem[i].marginrp;
            salesorder_detail.adminrp        = selectedItem[i].adminrp;
            salesorder_detail.hpp_save       = selectedItem[i].amounthpp;
            salesorder_detail.marginrp_save  = selectedItem[i].marginrp;
            salesorder_detail.adminrp_save   = selectedItem[i].adminrp;

            $scope.salesorder.salesorders_detail.splice($scope.salesorder.salesorders_detail.length+1,0,salesorder_detail);
        }

    }, function () {
    });
    };

    var ModalInstanceAddQuotationItemCreateCtrl = function ($scope, $dialog, $modalInstance, salesorder_detail, simpleDeleteConfirm, Restangular, security, i18nNotifications, $upload, APP_CONFIG) {

        var original = angular.copy(salesorder_detail);

        $scope.salesorder_detail = original;

        $scope.$watch(function() {
            return security.currentUser;
        }, function(currentUser) {
            $scope.currentUser = currentUser;
        });

        $scope.filterOptions = {
            filterText: "",
            useExternalFilter: true
        };
        $scope.totalServerItems = 0;
        $scope.pagingOptions = {
            pageSizes: [20, 50, 100],
            pageSize: 20,
            currentPage: 1
        };
        $scope.setPagingData = function(data, page, pageSize){
            var pagedData = data.records;
            $scope.myData = pagedData;
            $scope.totalServerItems = data.total;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        };
        $scope.getPagedDataAsync = function (pageSize, page, searchText) {
            setTimeout(function () {
                var custom_get = {};

                if($route.current.params)
                    custom_get = $route.current.params;

                custom_get.offset = (page - 1) * pageSize;
                custom_get.limit = pageSize;

                custom_get.where = undefined;

                if (searchText='$scope.filterOptions.filterText') {

                    custom_get.filter = $scope.filterOptions.filterText;
                    custom_get.filter_fields = 'param_pricelist_detail.item_id, param_pricelist_detail.item_name';

                    Restangular.all("pricelists/so/"+$scope.salesorder_detail.market_id).getList(custom_get).then(function(pricelists) {
                        $scope.setPagingData(pricelists,page,pageSize);
                    });
                }
                else {
                    Restangular.all("pricelists/so/"+$scope.salesorder_detail.market_id).getList(custom_get).then(function(pricelists) {
                        $scope.setPagingData(pricelists,page,pageSize);
                    });
                }
            }, 100);
        };

        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

        $scope.$watch('pagingOptions', function (newVal, oldVal) {
            if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
            }
        }, true);
        $scope.$watch('filterOptions', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
            }
        }, true);


        $scope.gridOptions = {
            data: 'myData',
            enablePaging: true,
            showFooter: true,
            multiSelect: true,
            selectedItems: [],
            enableColumnResize: true,
            totalServerItems:'totalServerItems',
            pagingOptions: $scope.pagingOptions,
            filterOptions: $scope.filterOptions,
            showSelectionCheckbox: true,
            checkboxHeaderTemplate: '<input class="ngSelectionHeader" type="checkbox" ng-model="allSelected" ng-change="toggleSelectAll(allSelected, true)"/>',
            columnDefs: [
            { field: 'item_id', displayName: 'Code', width: 150, cellClass:'text-center' },
            { field: 'item_name', displayName: 'Name'},
            { field: 'item_unit', displayName: 'Unit', width: 110, cellClass:'text-center' },
            { field: 'qty', displayName: 'Stock', width: 110, cellClass:'text-right', cellFilter: 'number', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number"/>'},
            { field: 'hpp', displayName: 'HPP', width: 180, cellClass:'text-right', cellFilter: 'number:2', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number"/>'},
            { field: 'operational', displayName: 'Operational', width: 180, cellClass:'text-right', cellFilter: 'number:2', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number"/>'},
            { field: 'amounthpp', displayName: 'Total HPP', width: 180, cellClass:'text-right', cellFilter: 'number:2', editableCellTemplate: '<input ng-class="col.colIndex()" ng-input="COL_FIELD" ng-model="COL_FIELD" format="number"/>'},
            { field: 'created_at', displayName: 'Date', width: 200, cellClass:'text-center' }
            ]
        };

        $scope.canSelect = function() {
            return ($scope.gridOptions.selectedItems.length===0?false:true);
        }

        $scope.select = function () {
            var selectedItem = $scope.gridOptions.selectedItems;
            var isExist = false;
            for(var j = 0; j <$scope.salesorder_detail.salesorders_detail.length; j++){
                for(var i = 0; i <  selectedItem.length; i++){
                    var as = selectedItem[i];
                    if(as.id==$scope.salesorder_detail.salesorders_detail[j].item_id){
                        selectedItem.splice(i, 1);
                    }
                }
            }

            for(var i = 0; i <  selectedItem.length; i++){
                var as = selectedItem[i];
                for(var j=0; j <  selectedItem.length; j++){
                    var sa = selectedItem[j];
                    if(i != j){
                        if(as.id==sa.id){
                            selectedItem.splice(j, 1);
                        }
                    }
                }
            }
            $modalInstance.close(selectedItem);
        };

        $scope.back = function () {
            $modalInstance.dismiss('cancel');
        };

    };

    $scope.canCreate    = security.canCreate;
    $scope.canUpdate    = security.canUpdate;
    $scope.canRead      = security.canRead;
    $scope.canDelete    = security.canDelete;

    var progress = 10;
    var msgs = [
    'Processing...',
    'Done!'
    ];
    var i = 0;
    var fakeProgress = function(){
        $timeout(function(){
            if(progress < 95){
                progress += 5;
                $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': progress});
                fakeProgress();
            }else{
            }
        },1000);
    };

    $scope.canRemove = function() {
        return false;
    }

    $scope.back = function() {
        $window.history.back();
    }
    $scope.canSave = function() {
        return $scope.SalesorderForm1.$valid && $scope.SalesorderForm2.$valid &&
        !angular.equals($scope.salesorder, original);
    };

    $scope.canRevert = function() {
        return !angular.equals($scope.salesorder, original);
    }
    $scope.revert = function() {
        $scope.salesorder =  Restangular.copy(original);
        $scope.salesorderForm.$setPristine();
    }

    $scope.save = function(){
        $scope.salesorder.is_save = 0;
        $scope.saved();
    };

    $scope.post = function(){
        var dlg = $dialogs.confirm('Konfirmasi','Apakah Anda ingin yakin memposting data ini?');
        dlg.result.then(function(btn){
            $scope.salesorder.is_save = 1;
            $scope.saved();
        },function(btn){
        });
    };

    $scope.saved = function() {
        for(var i=0; i<$scope.salesorder.salesorders_detail.length; i++){
            if($scope.salesorder.salesorders_detail[i].qty == undefined || $scope.salesorder.salesorders_detail[i].qty == '' || $scope.salesorder.salesorders_detail[i].qty == 0){
                var x = 1;
            }
        }

        if(x==1){
            $dialogs.error('Warning', 'Nilai Qty tidak boleh kosong !!!')
        }else{
            progress = 10;
            i = 0;
            $dialogs.wait(msgs[0],0);
            fakeProgress();
            $scope.salesorder.put().then(function() {
              i18nNotifications.pushForNextRoute('crud.save.success', 'success', {id : $scope.salesorder.id});
              $rootScope.$broadcast('dialogs.wait.progress',{msg: msgs[0],'progress': 100});
              $rootScope.$broadcast('dialogs.wait.complete');
              $location.path('/home/salesorder');
          });
        }
    };

}]);